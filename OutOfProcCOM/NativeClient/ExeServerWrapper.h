
#if 0
#pragma once
#include <atlstr.h>

class ExeServerWrapper : public IDispEventImpl<0, ExeServerWrapper, &DIID_IServerEvents,&LIBID_ROTProviderDotNetDlg, 1, 0>
{
public:
	ExeServerWrapper(void);
	virtual ~ExeServerWrapper(void);

	BEGIN_SINK_MAP(ExeServerWrapper)
        SINK_ENTRY_EX(0, DIID_ICOMROTVictimEvents, 1, OnClose)
		SINK_ENTRY_EX(0, DIID_ICOMROTVictimEvents, 2, OnCSharpCallsMfc)
	END_SINK_MAP()

	// incoming calls from the C# app
	STDMETHOD_(void, OnClose)(BSTR param);
	STDMETHOD_(void, OnCSharpCallsMfc)(BSTR param);

	// outgoing calls to the C# app

	//connect and call start()
	bool Start();
	// an example function call
	bool MfcCallsCSharp(CString s);
	//call stop and disconnect
	bool Stop();

protected:
	IServer* m_pIF;

};

#endif