#if 0
#include "StdAfx.h"
#include "ExeServerWrapper.h"

ExeServerWrapper::ExeServerWrapper(void)
{
	USES_CONVERSION;

	m_pIF = NULL;
}

ExeServerWrapper::~ExeServerWrapper(void)
{
	try
	{
		if (m_pIF)
		{
			m_pIF->MFCStop();
			DispEventUnadvise(m_pIF);
			m_pIF->Release();
			m_pIF = NULL;
		}
	}
	catch (...)
	{
	}
}

//connect and call start()
bool ExeServerWrapper::Start()
{
	try
	{
		USES_CONVERSION;

		if (m_pIF != NULL)
			return false;

		IRunningObjectTable* pRunningObjectTable = NULL;
		IMoniker* pMoniker = NULL;

		HRESULT hr = GetRunningObjectTable(0, &pRunningObjectTable);

		LPOLESTR strClsid;
		StringFromCLSID(CLSID_COMROTVictim, &strClsid);
		CreateItemMoniker(T2W("!"), strClsid, &pMoniker);

		IUnknown* punk = NULL;

		IBindCtx* pctx;
		hr = CreateBindCtx(0, &pctx);
		if (pRunningObjectTable->GetObject(pMoniker, &punk) != S_OK)
		{
			pctx->Release();
			throw 1; //object not running
		}

		hr = punk->QueryInterface(__uuidof(ICOMROTVictim), (((void**)&m_pIF)));

		hr = DispEventAdvise(m_pIF);
		pctx->Release();
		punk->Release();

		m_pIF->MFCStart();
	}
	catch (...)
	{
		return false;
	}
	return true;
}

//How to disconnect
bool ExeServerWrapper::Stop()
{
	try
	{
		if (!m_pIF)
			return false;

		m_pIF->MFCStop();
		DispEventUnadvise(m_pIF);
		m_pIF->Release();
		m_pIF = NULL;
	}
	catch (...)
	{
		return false;
	}
	return true;
}

// an example function-call
bool ExeServerWrapper::MfcCallsCSharp(CString s)
{
	try
	{
		m_pIF->MFCCallsCSharp(s.AllocSysString());
	}
	catch (...)
	{
		return false;
	}
	return true;
}

//called by the C# app
void ExeServerWrapper::OnClose(BSTR)
{
	AfxMessageBox("CSharpApp closed");
	DispEventUnadvise(m_pIF);
	m_pIF->Release();
	m_pIF = NULL;
}

//called by the C# app
void ExeServerWrapper::OnCSharpCallsMfc(BSTR str)
{
	AfxMessageBox("OnCSharpCallsMfc param: " + CString(str));
}
#endif