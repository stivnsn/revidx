#include <atlbase.h>
#include <atlcom.h>
#include <iostream>
#include <iomanip>



#include <Contract_h.h>
#include <Contract_i.c>
//https://docs.microsoft.com/en-us/cpp/atl/implementing-the-event-handling-interface?view=msvc-170
// https://stackoverflow.com/questions/10923717/handle-c-sharp-com-events-in-c

//class ATL_NO_VTABLE CConnect1 :
//	public CComObjectRootEx<CComSingleThreadModel>,
//	public CComCoClass<CConnect1, &CLSID_Server>,
//	public IConnectionPointContainerImpl<CConnect1>,
//	public IConnectionPointImpl<CConnect1, &IID_IPropertyNotifySink>,
//	public IServer
//{
//public:
//	CConnect1()
//	{
//	}
//
//	DECLARE_REGISTRY_RESOURCEID(IDR_Connect1)
//
//
//	BEGIN_COM_MAP(CConnect1)
//		COM_INTERFACE_ENTRY(IServer)
//		COM_INTERFACE_ENTRY(IConnectionPointContainer)
//	END_COM_MAP()
//
//	BEGIN_CONNECTION_POINT_MAP(CConnect1)
//		CONNECTION_POINT_ENTRY(IID_IPropertyNotifySink)
//	END_CONNECTION_POINT_MAP()
//
//
//	DECLARE_PROTECT_FINAL_CONSTRUCT()
//
//	HRESULT FinalConstruct()
//	{
//		return S_OK;
//	}
//
//	void FinalRelease()
//	{
//	}
//
//public:
//
//};
#if 1
//static _ATL_FUNC_INFO cardInserted;  // 'placeholder' object to carry event information (see below)
//static _ATL_FUNC_INFO cardRemoved;  // 'placeholder' object to carry event information (see below)
class EventWrapper : public IDispEventImpl<0, EventWrapper, &__uuidof(IServerEvents), &LIBID_ServerLib, 1 ,0 >
{
public:
	// now you need to declare a sink map - a map of methods handling the events
	BEGIN_SINK_MAP(EventWrapper)
		SINK_ENTRY_EX(0, __uuidof(IServerEvents), 0x1, CEClose)
		SINK_ENTRY_EX(0, __uuidof(IServerEvents), 0x2, CECSharpCallsMfc)
		// event interface id (can be more than 1)---+      |      |                   |
		// must match dispid of your event -----------------+      |                   |
		// method which handles the event  ------------------------+                   |
		// type information for event, see below --------------------------------------+
	END_SINK_MAP()

	// declare the type info object. You will need one for each method with different signature.
		// it will be defined in the .cpp file, as it is a static member


	STDMETHOD_(void, CEClose)()
	{
		// usually it is defined it in the .cpp file
		std::cout << "CeCCloseTriggered: " << std::endl;
		//return 0;
	}

	// method which handles the event
	STDMETHOD_(void, CECSharpCallsMfc)(BSTR type)
	{
		// usually it is defined it in the .cpp file
		std::cout << "CeCCloseTriggered: "<< std::endl;
		//return 0;
	}

};
#endif 


//[event_receiver(com)]
//class CReceiver {
//public:
//	HRESULT MyHandler1(int nValue) {
//		printf_s("MyHandler1 was called with value %d.\n", nValue);
//		return S_OK;
//	}
//
//	HRESULT MyHandler2(int nValue) {
//		printf_s("MyHandler2 was called with value %d.\n", nValue);
//		return S_OK;
//	}
//
//	//void HookEvent(IServer* pSource) {
//	//	__hook(&IServerEvents::MyEvent, pSource, &IServerEvents::MyHandler1);
//	//	__hook(&IServerEvents::MyEvent, pSource, &IServerEvents::MyHandler2);
//	//}
//
//	//void UnhookEvent(IEventSource* pSource) {
//	//	__unhook(&IServerEvents::IServerEvents, pSource, &CReceiver::MyHandler1);
//	//	__unhook(&IServerEvents::IServerEvents, pSource, &CReceiver::MyHandler2);
//	//}
//};

int main()
{
	::SetConsoleOutputCP(CP_UTF8);

	HRESULT hr;
	hr = ::CoInitializeEx(0, COINITBASE_MULTITHREADED);
	if (FAILED(hr))
	{
		std::cout << "CoInitializeEx failure: " << std::hex << std::showbase << hr << std::endl;
		return EXIT_FAILURE;
	}
	CComPtr<IUnknown> iUnknown;
	CComPtr<IServer> server; 
	CLSID       clsid;
	//
	//USES_CONVERSION;
	//IServer* m_pIF = NULL;
	//if (m_pIF != NULL)
	//	return false;

	//IRunningObjectTable* pRunningObjectTable = NULL;
	//IMoniker* pMoniker = NULL;


	//hr = GetRunningObjectTable(0, &pRunningObjectTable);

	//if (FAILED(hr))
	//{
	//	throw 1; //object not running
	//}

	//LPOLESTR strClsid;
	//hr = StringFromCLSID(CLSID_Server, &strClsid);
	//if (FAILED(hr))
	//{
	//	throw 1; //object not running
	//}
	//hr = CreateItemMoniker(L"!", strClsid, &pMoniker);

	//if (FAILED(hr))
	//{
	//	throw 1; //object not running
	//}
	//IUnknown* punk = NULL;
	//IBindCtx* pctx = NULL;
	//hr = CreateBindCtx(0, &pctx);
	//if (FAILED(hr))
	//{
	//	pMoniker->Release();
	//	throw 1; //object not running
	//}
	//hr = pRunningObjectTable->GetObject(pMoniker, &punk);
	//if (FAILED(hr))
	//{
 //		pctx->Release();
	//	throw 1; //object not running
	//}

	//hr = punk->QueryInterface(__uuidof(IServer), (((void**)&m_pIF)));

	////hr = DispEventAdvise(m_pIF);
	//pctx->Release();
	//punk->Release();

	//m_pIF->RunRevidX();

	hr = CLSIDFromProgID(OLESTR("OutOfProcCOM.ExeServer"), &clsid);
		hr = ::GetActiveObject(CLSID_Server, NULL, &iUnknown);
	if (FAILED(hr))
	{
		hr = ::CoCreateInstance(CLSID_Server, nullptr, CLSCTX_LOCAL_SERVER, __uuidof(IServer), (void**)&server);
		if (FAILED(hr))
		{
			std::cout << "CoCreateInstance failure: " << std::hex << std::showbase << hr << std::endl;
			return EXIT_FAILURE;
		}
	}
	else
	{
		hr = iUnknown->QueryInterface(__uuidof(IServer), (void**)&server);
		if (FAILED(hr))
		{
			MessageBox(NULL, L"Failed interfacing to server", L"Failed get active object", 0);
			return -1;
		}
	}
	//CComPtr<IServerEvents> serverEvents;
	//hr = ::CoCreateInstance(IID_IServerEvents, nullptr, CLSCTX_LOCAL_SERVER, __uuidof(IServerEvents), (void**)&serverEvents);
	//if (FAILED(hr))
	//{
	//	std::cout << "CoCreateInstance failure: " << std::hex << std::showbase << hr << std::endl;
	//	return EXIT_FAILURE;
	//}
	EventWrapper ev;
	hr = ev.DispEventAdvise(server/*COM interface*/);
	/*EventWrapper ev;
	ev.DispEventAdvise(server);*/
	// receiving events
	//ev.DispEventUnadvise(server);
	double pi;
	hr = server->ComputePi(&pi);
	if (FAILED(hr))
	{
		std::cout << "Failure: " << std::hex << std::showbase << hr << std::endl;
		return EXIT_FAILURE;
	}
	hr = server->RunRevidX();
	if (FAILED(hr))
	{
		std::cout << "Failure: " << std::hex << std::showbase << hr << std::endl;
		return EXIT_FAILURE;
	}

	// receiving events
	ev.DispEventUnadvise(server/*COM interface*/);
	std::cout << u8"\u03C0 = " << std::setprecision(16) << pi << std::endl;

	::CoUninitialize();

	return EXIT_SUCCESS;
}
