﻿using System;
using System.Runtime.InteropServices;

[ComVisible(true)]
[Guid(Contract.Constants.ServerInterface)]
[InterfaceType(ComInterfaceType.InterfaceIsDual)]
public interface IServer
{
    /// <summary>
    /// Compute the value of the constant Pi.
    /// </summary>
    /// 
    [DispId(1)]
    double ComputePi();
    /// Compute the value of the constant Pi.
    /// </summary>
    /// 
    [DispId(2)]
    double ComputePi2();

    [DispId(3)]
    void RunRevidX();
}
