﻿using System;

namespace Contract
{
    internal sealed class Constants
    {


        public const string ServerEventClass = "C1506593-B6A9-4F01-84A0-EF6DD4B56612";
        public static readonly Guid ServerEventClassGuid = Guid.Parse(ServerEventClass);

        public const string ServerClass = "AF080472-F173-4D9D-8BE7-435776617347";
        public static readonly Guid ServerClassGuid = Guid.Parse(ServerClass);
        public const string RotServerClass = "991A4208-EC47-4907-8F57-5D83E7773000";

        public const string ServerInterface = "F586D6F4-AF37-441E-80A6-3D33D977882D";

        public const string TypeLibraryName = "RevidXComServer.tlb";
    }
}
