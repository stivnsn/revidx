﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using RevidX.Desktop;
using System.Linq;
using COMRegistration;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Forms;

namespace OutOfProcCOM
{


    [Guid(Contract.Constants.ServerEventClass)]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    [ComVisible(true)]
    public interface IServerEvents
    {
        [DispId(1)]
        void CEClose();
        [DispId(2)]
        void CECSharpCallsMfc(string s);
    }

    [ComVisible(true)]
    [GuidAttribute(Contract.Constants.ServerClass)]
    [ComSourceInterfaces(typeof(IServerEvents))]
    [ClassInterface(ClassInterfaceType.None)]
    public sealed class ExeServer : IServer, IDisposable
    {

        public delegate void ComEvent(string p);
        public delegate void ComEventNoParam();
        public event ComEvent CECSharpCallsMfc;
        public event ComEventNoParam CEClose;

        private DesktopClientMain main;
        //private string _dllPath;
        Thread currentThread = null;
        int m_dwRegister;
        bool m_bMFCConnected;
        public ExeServer()
        {
            Debug.WriteLine("RevidXComServer constructor");


            Trace.WriteLine($"Trying to register");
            Guid guid = Marshal.GenerateGuidForType(typeof(ExeServer)); //typeof(COMROTVictim)
            Ole32.RegisterActiveObject(this, ref guid, 0, out m_dwRegister);
            Trace.WriteLine($"Registered");

            MessageBox.Show("Registering: " + guid.ToString());
            // Define the AssemblyResolve event in the constructor of the COM Object.
            //AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            //// Get the path where COM Object's dll is
            //string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            //UriBuilder uri = new UriBuilder(codeBase);
            //string path = Uri.UnescapeDataString(uri.Path);
            //_dllPath = System.IO.Path.GetDirectoryName(path);

            //Debug.WriteLine("Dll path: " + _dllPath);

        }

        ~ExeServer()
        {
            Dispose();
        }

        //Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        //{
        //    //https://stackoverflow.com/questions/66276355/could-not-load-file-or-assembly-using-com-interop-dll-hell

        //    Debug.WriteLine("Event run...");
        //    // Get the name of the assembly that failed loading
        //    var name = new AssemblyName(args.Name);

        //    Debug.WriteLine($"Assembly name: {name}, {name.FullName}");
        //    // Manage the assembly that I know will cause problems
        //    string[] bindingAssembly = new string[] { "Microsoft.Extensions.Logging.Abstractions", "System.Text.Json"
        //        , "System.Runtime.CompilerServices.Unsafe"
        //        , "System.Text.Encodings.Web"
        //        , "Microsoft.Bcl.AsyncInterfaces" };

        //    if (bindingAssembly.Contains(name.Name))
        //    {

        //        Debug.WriteLine($"It is abstraction!");

        //        // Load the assembly from the COM Object's folder, and use whatever version there is
        //        return Assembly.LoadFrom(System.IO.Path.Combine(_dllPath, name.Name + ".dll"));
        //    }


        //    // I should not get here. If I do I need to add another manual assembly load for the dll that failed loading
        //    return null;
        //}


        public void Dispose()
        {
            if (m_dwRegister != 0)
            {
                if (m_bMFCConnected)
                    CSharpClose();

                IBindCtx bc;
                Ole32.CreateBindCtx(0, out bc);

                IRunningObjectTable rot;
                bc.GetRunningObjectTable(out rot);
                rot.Revoke(m_dwRegister);
                m_dwRegister = 0;
            }

        }

        void IServer.RunRevidX()
        {
            Trace.WriteLine($"Checking if Revidx instance exists??");
            if (main != null)
            {

                Trace.WriteLine($"Revidx instance exists!!");
                return;
            }
            Trace.WriteLine($"Main is null!!");
            m_bMFCConnected = true;
            currentThread = new Thread(new ThreadStart(this.RunThread));
            currentThread.SetApartmentState(ApartmentState.STA);
            currentThread.Start();
        }

        double IServer.ComputePi()
        {
            CSharpCallsMfc("Heyyyoo");
            m_bMFCConnected = true;
            Trace.WriteLine($"Running {nameof(ExeServer)}.{nameof(IServer.ComputePi)}");
            return 0;
        }
        double IServer.ComputePi2()
        {
            CSharpCallsMfc("Heyyyoo");
            m_bMFCConnected = true;
            Trace.WriteLine($"Running {nameof(ExeServer)}.{nameof(IServer.ComputePi2)}");
            return 0;
        }
        private void RunThread()

        {
            Trace.WriteLine($"Creating new instance!!");
            RevidX.Desktop.Program.Start(out main);
        }


        public void StopRevidX()
        {
            RevidX.Desktop.Program.Stop();
            m_bMFCConnected = false;

        }

        public void CSharpClose()
        {
            if (CEClose != null)
            {
                Trace.WriteLine("CEClose is not Null");
                CEClose();
            }
            else
            {
                Trace.WriteLine("CEClose is Null");

            }

            m_bMFCConnected = false;
        }

        public void CSharpCallsMfc(string s)
        {
            Trace.WriteLine(s);
            try
            {
                if (CSharpClose != null)
                {
                    CSharpClose();
                }
                else
                {

                    Trace.WriteLine("CEClose is Null");
                }
                if (CECSharpCallsMfc != null)
                {

                    Trace.WriteLine("CECSHarpcallsMFC not Null, watch for break...");
                    CECSharpCallsMfc(s);
                }
                else
                    Trace.WriteLine("CECSHarpcallsMFC is Null");
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw ex;
            }
        }

    }
}
