

//Now we are ready to use our COM component. Here is sample code:

// ComClient.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <system_error>

int _tmain(int argc, _TCHAR* argv[])
{
	CoInitialize(0);

	//
	// 
	CComPtr<IRevidXServer> iReadXmlPtr;
	//HRESULT hr = iReadXmlPtr.CoCreateInstance(CLSID_RevidXServer);
	//
	CComPtr<IUnknown> iUnknownPtr;
	//IDispatchPtr pApp;
	HRESULT hr = ::GetActiveObject(CLSID_RevidXServer, NULL, &iUnknownPtr);
	if (SUCCEEDED(hr))
	{
		hr = iUnknownPtr->QueryInterface(__uuidof(IRevidXServer), (void**)&iReadXmlPtr);
		if (FAILED(hr))
		{
			MessageBox(NULL, L"Failed interfacing to server", L"Failed get active object", 0);
			return -1;
		}
	}
	else
	{
		HRESULT hr = iReadXmlPtr.CoCreateInstance(CLSID_RevidXServer);
		if (FAILED(hr))
		{
			MessageBox(NULL, L"Failed creating instance", L"Failed creating instance", 0);
			return -1;
		}
	}
	_bstr_t filePath(L"C:\\temp\\simpleXml.xml");
	_bstr_t xml = iReadXmlPtr->ComputePi();
	iReadXmlPtr->RunRevidX();
	MessageBox(NULL, xml, L"Running. Leave this on", 0);
	iReadXmlPtr->StopRevidX();
	iReadXmlPtr.Release();
	//else
	//{
	//_com_error err(hr);
	//std::string message = std::system_category().message(hr);
	//LPCTSTR errMsg = err.ErrorMessage();
	//MessageBox(NULL, errMsg, L"Failed", 0);
	//}

	CoUninitialize();
	return 0;
}

////PS D:\workspace\revidx\RevidXServer> sxstrace Parse -logfile:SxSTrace.etl -outfile:SxSTrace.txt
//Parsing log file SxSTrace.etl...
//Parsing finished! Output saved to file SxSTrace.txt.
//PS D:\workspace\revidx\RevidXServer> start notepad++ .\SxSTrace.txt
//PS D:\workspace\revidx\RevidXServer> SxsTrace Trace -logfile:SxsTrace.etl
//Tracing started. Trace will be saved to file SxsTrace.etl.
//Press Enter to stop tracing...
//
//PS D:\workspace\revidx\RevidXServer> sxstrace Parse -logfile:SxSTrace.etl -outfile:SxSTrace.txt
//Parsing log file SxSTrace.etl...
//Parsing finished! Output saved to file SxSTrace.txt.
//PS D:\workspace\revidx\RevidXServer> start notepad++ .\SxSTrace.txt