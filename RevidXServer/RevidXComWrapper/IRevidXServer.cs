﻿using RevidX.Desktop;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

namespace RevidXComWrapper
{

    // resources

    // blog https://www.bizicbojan.com/post/2014/02/27/Registration-Free-COM-with-NET-Example-and-pitfalls.aspx

    // msdn https://docs.microsoft.com/en-us/previous-versions/dotnet/articles/ms973915(v=msdn.10)?redirectedfrom=MSDN

    [ComVisible(true)]
    [Guid(ContractGuids.ServerInterface)]
    public interface IRevidXServer
    {
        /// <summary>
        /// Compute the value of the constant Pi.
        /// </summary>
        double ComputePi();
        void RunRevidX();
        void StopRevidX();
    }
    [ComVisible(true)]
    [Guid(ContractGuids.ServerClass)]
    [ClassInterface(ClassInterfaceType.None)]
    public class RevidXServer : IRevidXServer
    {
        private DesktopClientMain main;
        private string _dllPath;
        Thread currentThread = null;
        public RevidXServer()
        {
            Debug.WriteLine("RevidXComServer constructor");
            // Define the AssemblyResolve event in the constructor of the COM Object.
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            // Get the path where COM Object's dll is
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            _dllPath = System.IO.Path.GetDirectoryName(path);

            Debug.WriteLine("Dll path: " + _dllPath);

        }

        Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            //https://stackoverflow.com/questions/66276355/could-not-load-file-or-assembly-using-com-interop-dll-hell

            Debug.WriteLine("Event run...");
            // Get the name of the assembly that failed loading
            var name = new AssemblyName(args.Name);

            Debug.WriteLine($"Assembly name: {name}, {name.FullName}");
            // Manage the assembly that I know will cause problems
            string[] bindingAssembly = new string[] { "Microsoft.Extensions.Logging.Abstractions", "System.Text.Json"
                , "System.Runtime.CompilerServices.Unsafe"
                , "System.Text.Encodings.Web"
                , "Microsoft.Bcl.AsyncInterfaces" };

            if (bindingAssembly.Contains(name.Name))
            {

                Debug.WriteLine($"It is abstraction!");

                // Load the assembly from the COM Object's folder, and use whatever version there is
                return Assembly.LoadFrom(System.IO.Path.Combine(_dllPath, name.Name + ".dll"));
            }


            // I should not get here. If I do I need to add another manual assembly load for the dll that failed loading
            return null;
        }

        public void RunRevidX()
        {
            currentThread = new Thread(new ThreadStart(this.RunThread));

            currentThread.SetApartmentState(ApartmentState.STA);

            currentThread.Start();
        }

        public double ComputePi()
        {

            return 0;
        }
        private void RunThread()

        {
            main = Program.Start();
        }


        public void StopRevidX()
        {
            Program.Stop();

        }
    }
}