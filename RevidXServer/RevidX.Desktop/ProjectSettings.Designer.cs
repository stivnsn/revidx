﻿namespace RevidX.Desktop
{
    partial class ProjectSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openMonitoredPath = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.monitoredPathTxt = new System.Windows.Forms.TextBox();
            this.selectPathBtn = new System.Windows.Forms.Button();
            this.selectFolderDlg = new System.Windows.Forms.FolderBrowserDialog();
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openMonitoredPath
            // 
            this.openMonitoredPath.Location = new System.Drawing.Point(372, 34);
            this.openMonitoredPath.Name = "openMonitoredPath";
            this.openMonitoredPath.Size = new System.Drawing.Size(102, 22);
            this.openMonitoredPath.TabIndex = 25;
            this.openMonitoredPath.Text = "Open";
            this.openMonitoredPath.UseVisualStyleBackColor = true;
            this.openMonitoredPath.Click += new System.EventHandler(this.openMonitoredPath_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 15);
            this.label1.TabIndex = 24;
            this.label1.Text = "Monitored path";
            // 
            // monitoredPathTxt
            // 
            this.monitoredPathTxt.Location = new System.Drawing.Point(11, 34);
            this.monitoredPathTxt.Margin = new System.Windows.Forms.Padding(2);
            this.monitoredPathTxt.Name = "monitoredPathTxt";
            this.monitoredPathTxt.ReadOnly = true;
            this.monitoredPathTxt.Size = new System.Drawing.Size(263, 23);
            this.monitoredPathTxt.TabIndex = 22;
            // 
            // selectPathBtn
            // 
            this.selectPathBtn.Location = new System.Drawing.Point(277, 34);
            this.selectPathBtn.Margin = new System.Windows.Forms.Padding(2);
            this.selectPathBtn.Name = "selectPathBtn";
            this.selectPathBtn.Size = new System.Drawing.Size(90, 23);
            this.selectPathBtn.TabIndex = 23;
            this.selectPathBtn.Text = "Select...";
            this.selectPathBtn.UseVisualStyleBackColor = true;
            this.selectPathBtn.Click += new System.EventHandler(this.selectPathBtn_Click);
            // 
            // okBtn
            // 
            this.okBtn.Location = new System.Drawing.Point(308, 108);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 26;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(399, 108);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 27;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // ProjectSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 143);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.openMonitoredPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.monitoredPathTxt);
            this.Controls.Add(this.selectPathBtn);
            this.Name = "ProjectSettings";
            this.Text = "ProjectSettings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openMonitoredPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox monitoredPathTxt;
        private System.Windows.Forms.Button selectPathBtn;
        private System.Windows.Forms.FolderBrowserDialog selectFolderDlg;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelBtn;
    }
}