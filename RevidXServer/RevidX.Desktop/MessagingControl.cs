﻿using Microsoft.AspNetCore.SignalR.Client;
using RevidX.ServiceClient.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RevidX.Desktop
{
    public partial class MessagingControl : UserControl
    {
        public MessagingControl()
        {
            InitializeComponent();
        }

        private DesktopClientMain parentForm;

        public List<UserProfile> UserProfiles { get; set; }


        delegate void AddMessagesCallback(ServiceClient.Models.Message message);
        private void AddMessage(ServiceClient.Models.Message message)
        {
            if (mainStatusTxt.InvokeRequired)
            {
                AddMessagesCallback d = new AddMessagesCallback(AddMessage);
                this.Invoke(d, message);
            }
            else
            {
                if (UserProfiles == null || UserProfiles.Count == 0)
                {
                    throw new Exception("User count for this project is 0.");
                }
                var user = "Unknown";
                if (message.Sender != null)
                {
                    user = message.Sender.Name;
                }
                else
                {
                    var name = UserProfiles.Where(up => up.Id == message.SenderId).Select(up => up.Name).FirstOrDefault();
                    user = name == null ? user : name;
                }
                var encodedMsg = $"{user}: {message.Text}";
                mainStatusTxt.AppendText(encodedMsg + "\r\n");
                mainStatusTxt.SelectionStart = mainStatusTxt.Text.Length;
                // scroll it automatically
                mainStatusTxt.ScrollToCaret();
            }
        }

        public void AddMessageAsync(ServiceClient.Models.Message message)
        {
            AddMessage(message);
        }

        public bool InitializeMessages(List<UserProfile> userProfiles, List<ServiceClient.Models.Message> messages, DesktopClientMain desktopClientMain)
        {
            UserProfiles = userProfiles;
            mainStatusTxt.Clear();
            this.parentForm = desktopClientMain;
            foreach (var message in messages)
            {
                AddMessage(message);
            }
            return true;
        }

        private async Task SendMessage()
        {
            await parentForm.UpdateHubConnection();
            if (parentForm.hubConnection != null)
            {
                ServiceClient.Models.Message message = new ServiceClient.Models.Message();
                message.SenderId = parentForm.UserProfile.Id;
                message.Sender = parentForm.UserProfile;
                message.ProjectId = parentForm.selectedUserProject.ProjectId;
                message.SendTimeUtc = DateTime.UtcNow;
                message.Text = messageTxt.Text;
                await parentForm.hubConnection.SendAsync("SendMessage", message);
                AddMessageAsync(message);
                messageTxt.Text = string.Empty;
            }
            else
            {
                Log.Error("Could not send message. Logged out.");
                MessageBox.Show("Could not send message. Logged out.");
            }
        }

        private async void SendMessageBackBtn_Click(object sender, EventArgs e)
        {
            try
            {
                await SendMessage();
            }
            catch (Exception ex)
            {
                Log.Error("Sending message failed. " + ex.Message);
                MessageBox.Show("Sending message failed.");
            }
        }

        private async void messageTxt_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    await SendMessage();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Sending message failed. " + ex.Message);
                MessageBox.Show("Sending message failed.");
            }
        }
    }
}
