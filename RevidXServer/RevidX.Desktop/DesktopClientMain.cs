﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.AspNetCore.SignalR.Client;
using IdentityModel.OidcClient;
using IdentityModel.OidcClient.Browser;
using Serilog;
using RevidX.ServiceClient;
using RevidX.ServiceClient.Models;
using RevidX.Desktop;
using System.IO;
using Microsoft.Win32;
using System.Diagnostics;
using EDMDProcessor;
using System.Security.Claims;
using Message = RevidX.ServiceClient.Models.Message;
using IdentityModel;
using System.Runtime.InteropServices;
using WinFormsWebView2Ext;

namespace RevidX.Desktop
{

    public partial class DesktopClientMain : Form
    {
        //https://github.com/IdentityModel/IdentityModel.OidcClient.Samples/blob/main/NetCoreConsoleClient/src/NetCoreConsoleClient/SystemBrowser.cs
        public HubConnection hubConnection;
        private List<UserProject> UserProjects;
        public UserProject selectedUserProject;
        public Dictionary<string, (FSMonitor, UserProject)> fsMonitorRing { get; set; } = new Dictionary<string, (FSMonitor, UserProject)>();

        public UserProfile UserProfile { get; set; }
        public CustomTokenProvider CustomTokenProvider { get; set; }

        private bool IsLoggedIn { get; set; } = false;

        public string SERVER_URI = "https://localhost:5001";
        private const string LOGINTXT = "Login";
        private const string LOGOUTTXT = "Logout";


        public async Task UpdateHubConnection()
        {
            if (hubConnection.State != HubConnectionState.Connected)
            {
                Log.Warning("Hub connection is closed. Trying to reconnect.");
                await InitializeHubConnection();
            }
            else
            {
                Log.Information(String.Format("Hub connection state is {0}", hubConnection.State.ToString()));
            }
        }
        public async Task<RevidXApiCaller> GetRevidXApiCallerAsync()
        {
            if (hubConnection.State != HubConnectionState.Connected)
            {
                Log.Warning("Hub connection is closed. Trying to reconnect.");
                await InitializeHubConnection();
            }
            else
            {
                Log.Information(String.Format("Hub connection state is {0}", hubConnection.State.ToString()));
            }
            return new RevidXApiCaller(await CustomTokenProvider.GetAccessToken(), new Uri(SERVER_URI));
        }

        public static string LocalAppFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "RevidX.Desktop");
        public static string logFileLocation = Path.Combine(LocalAppFolder, "RevidXClient.log");
        public DesktopClientMain()
        {
            try
            {
                if (Directory.Exists(Path.GetDirectoryName(logFileLocation)))
                {
                    var dirinfo = Directory.CreateDirectory(Path.GetDirectoryName(logFileLocation));
                    if (dirinfo == null || !dirinfo.Exists)
                    {

                        MessageBox.Show("App data location could not be initialized. App data directory should have read and write permissions");
                        return;
                    }
                }
                if (string.IsNullOrEmpty(Desktop.Properties.Settings.Default.ServerUri))
                {
                    Desktop.Properties.Settings.Default.ServerUri = SERVER_URI;
                    Desktop.Properties.Settings.Default.Save();
                }
                else
                {
                    SERVER_URI = Desktop.Properties.Settings.Default.ServerUri;
                }
                Log.Logger = new LoggerConfiguration()
                    .WriteTo.File(logFileLocation, rollingInterval: RollingInterval.Hour)
                    .CreateLogger();
                Log.Information("Log initialized!");
                InitializeComponent();
                var appName = Process.GetCurrentProcess().ProcessName + ".exe";
                _ = OpenLoginWindow();
                DisableAllControls();
                notifyIcon.ContextMenuStrip = new ContextMenuStrip();
                notifyIcon.ContextMenuStrip.LayoutStyle = ToolStripLayoutStyle.HorizontalStackWithOverflow;
                ToolStripMenuItem quitItem = new ToolStripMenuItem();
                quitItem.Text = "Quit";
                quitItem.Click += QuitNotifyItem_Click;
                notifyIcon.ContextMenuStrip.Items.Add(quitItem);
                monitoredPathTxt.Enabled = false;
                selectPathBtn.Enabled = false;
                userProjectWebBtn.Enabled = false;
                removePathBtn.Enabled = false;
                openMonitoredPathBtn.Enabled = false;
            }
            catch (Exception ex)
            {
                Log.Error("Could not initialize form correctly." + ex.Message);
                MessageBox.Show("Could not initialize form correctly. Server might be unavailable. " +
                    "Ensure server availability then try Login again.");
            }
        }

        ~DesktopClientMain()
        {
            notifyIcon.Dispose();
            Log.CloseAndFlush();
        }

        private OidcClientOptions GetOidcClientOptions(string serverUri)
        {
            return new OidcClientOptions
            {
                Authority = serverUri,//"https://demo.identityserver.io",// "https://localhost:44342/Identity/Account/Login",
                ClientId = "RevidXServer.Desktop", //"interactive.public",
                RedirectUri = serverUri + "/authentication/login-callback", // redirectUri, //
                PostLogoutRedirectUri = serverUri + "/authentication/logout-callback",
                Scope = "RevidXServer.ServerAPI openid profile", //"openid profile api",
                                                                 //FilterClaims = false,
                                                                 //ClientSecret = "secret".ToSha256(),
                Browser = new WinFormsWebView(), //browser,
                                                 //Flow = OidcClientOptions.AuthenticationFlow.AuthorizationCode,
                                                 //ResponseMode = OidcClientOptions.AuthorizeResponseMode.Redirect
            };
        }

        private async Task<bool> OpenLoginWindow()
        {
            try
            {
                LoginBtn.Text = LOGINTXT;
                //string redirectUri = string.Format($"http://127.0.0.1:{browser.Port}");

                profileLink.Visible = false;
                var oidcClient = new OidcClient(GetOidcClientOptions(SERVER_URI));
                var result = await oidcClient.LoginAsync(new LoginRequest());
                if (result.IsError)
                {
                    IsLoggedIn = false;
                    DisableAllControls();

                    profileLink.Visible = true;
                    profileLink.Text = $"Not logged in.";
                    profileLink.Enabled = false;
                    return false;
                }
                else
                {
                    IsLoggedIn = true;
                    LoginBtn.Text = LOGOUTTXT;
                    LoginBtn.Enabled = false;
                    var userInfo = await oidcClient.GetUserInfoAsync(result.AccessToken);
                    IncreaseProgress(50);
                    string UserId = userInfo.Claims.Where(c => c.Type == (ClaimTypes.Name)).FirstOrDefault()?.Value;
                    if (!string.IsNullOrEmpty(UserId))
                    {
                        profileLink.Click += (e, o) =>
                        {
                            ProcessStartInfo sInfo = new ProcessStartInfo(SERVER_URI + "/app");
                            sInfo.UseShellExecute = true;
                            System.Diagnostics.Process.Start(sInfo);
                        };
                        profileLink.Text = $"{UserId}";
                        profileLink.Enabled = true;
                    }
                    else
                    {
                        profileLink.Text = $"Not logged in.";
                        profileLink.Enabled = false;
                    }

                    profileLink.Visible = true;
                    CustomTokenProvider = new CustomTokenProvider(oidcClient, result);
                    await InitializeHubConnection();
                    RevidXApiCaller revidXApiCaller = await GetRevidXApiCallerAsync();
                    UserProfile = await revidXApiCaller.GetUserProfileAsync();
                    if (UserProfile == null)
                    {
                        Log.Error("Could not get user profile.");
                        throw new Exception("Could not get user profile.");
                    }
                    IncreaseProgress(90);
                    UpdateUserProjectList();

                    EnableAllControls();

                    LoginBtn.Enabled = true;
                    IncreaseProgress(0);
                }
            }
            catch (Exception ex)
            {
                IncreaseProgress(0);
                LoginBtn.Enabled = true;
                Log.Error($"Error in initializing connection: {ex.Message}. Check log for more info.");
                MessageBox.Show($"Error in initializing connection: {ex.Message}. Check log for more info.");
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error("Inner Exception: " + ex.Message);
                }
                return false;
            }
            return true;
        }

        private async Task InitializeHubConnection()
        {
            hubConnection = new HubConnectionBuilder()
                   .WithUrl(SERVER_URI + "/exchangenotify", options =>
                   {
                       options.AccessTokenProvider = async () =>
                       {
                           //var accessTokenResult = await AccessTokenProvider.RequestAccessToken();
                           //accessTokenResult.TryGetToken(out var accessToken);

                           return await CustomTokenProvider.GetAccessToken();
                       };
                   }).WithAutomaticReconnect()
                   .Build();
            hubConnection.On<Message>("ReceiveMessage", (message) =>
            {
                try
                {
                    var userProject = UserProjects.Where(up => up.ProjectId == message.ProjectId).FirstOrDefault();
                    if (userProject != null)
                    {
                        var user = "User";
                        if (message.Sender != null)
                        {
                            if (message.SenderId == UserProfile.Id)
                            {
                                user = "You";
                            }
                            else
                            {
                                user = message.Sender.Name;
                            }
                        }
                        var encodedMsg = $"{user}: {message.Text}";
                        //userProject.NewMessages.Add(message);
                        Log.Information("Message from {user} was received.", user);
                        if (message.SenderId != UserProfile.Id)
                        {
                            string messageStr = message.Text;
                            if (message.Sender != null)
                            {
                                messageStr = $"{message.Sender.Name}: {messageStr}";
                            }
                            notifyIcon.ShowBalloonTip(500, "New message", $"{messageStr}", ToolTipIcon.None);
                            notifyIcon.Visible = true;
                            notifyIcon.Tag = userProject;
                            notifyIcon.Click += NotifyIcon_Click;
                            notifyIcon.BalloonTipClicked += new EventHandler((sender, e) => SelectProjectInProjectLV(userProject.Id));
                        }
                        if (selectedUserProject != null && selectedUserProject.Id == userProject.Id)
                        {
                            messagingControl.AddMessageAsync(message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Receiving message failed: " + ex.Message);
                }
            });
            hubConnection.On<Message>("FileUploadedToProject", (message) =>
            {
                try
                {
                    var userProject = UserProjects.Where(up => up.ProjectId == message.ProjectId).FirstOrDefault();
                    if (userProject != null)
                    {
                        userProject.NewMessages.Add(message);
                        var user = "User";
                        if (message.Sender != null)
                        {
                            if (message.SenderId == UserProfile.Id)
                            {
                                user = "You";
                            }
                            else
                            {
                                user = message.Sender.Name;
                            }
                        }
                        var encodedMsg = $"{user}: {message.Text}";
                        Log.Information($"File pull from user was received. {encodedMsg}");
                        if (message.SenderId != UserProfile.Id)
                        {
                            notifyIcon.ShowBalloonTip(0, "Click to pull", encodedMsg, ToolTipIcon.None);
                            notifyIcon.Visible = true;
                            notifyIcon.Tag = userProject;
                            notifyIcon.Click += NotifyIcon_Click;
                            notifyIcon.BalloonTipClicked += new EventHandler((sender, e) => SelectProjectInProjectLV(userProject.Id));
                        }
                        if (selectedUserProject != null && selectedUserProject.Id == userProject.Id)
                        {
                            messagingControl.AddMessageAsync(message);
                            DialogResult dlgRes = DialogResult.Yes;
                            if (ApplicationIsActivated())
                            {
                                dlgRes = MessageBox.Show("Current project updated. Update view?", "Project updated", MessageBoxButtons.YesNo);
                            }
                            if (dlgRes == DialogResult.Yes)
                            {
                                messagingControl.AddMessageAsync(message);
                                SelectProjectInProjectLV(selectedUserProject.Id);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Receiving file upload message failed: " + ex.Message);
                }
            });
            hubConnection.On<Message>("FilePulled", (message) =>
            {
                try
                {
                    var userProject = UserProjects.Where(up => up.ProjectId == message.ProjectId).FirstOrDefault();
                    if (userProject != null)
                    {
                        userProject.NewMessages.Add(message);
                        var user = "User";
                        if (message.Sender != null)
                        {
                            if (message.SenderId == UserProfile.Id)
                            {
                                user = "You";
                            }
                            else
                            {
                                user = message.Sender.Name;
                            }
                        }
                        var encodedMsg = $"{user}: {message.Text}";
                        Log.Information($"File pull from user was received. {encodedMsg}");
                        if (message.SenderId != UserProfile.Id)
                        {
                            notifyIcon.ShowBalloonTip(0, "File was pulled", encodedMsg, ToolTipIcon.None);
                            notifyIcon.Visible = true;
                            notifyIcon.Tag = userProject;
                            notifyIcon.Click += NotifyIcon_Click;
                            notifyIcon.BalloonTipClicked += new EventHandler((sender, e) => SelectProjectInProjectLV(userProject.Id));
                        }
                        if (selectedUserProject != null && selectedUserProject.Id == userProject.Id)
                        {
                            messagingControl.AddMessageAsync(message);
                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Receiving file upload message failed: " + ex.Message);
                }
            });
            hubConnection.On<Message>("CancelledAction", (message) =>
            {
                try
                {
                    var userProject = UserProjects.Where(up => up.ProjectId == message.ProjectId).FirstOrDefault();
                    if (userProject != null)
                    {
                        userProject.NewMessages.Add(message);
                        var user = "User";
                        if (message.Sender != null)
                        {
                            if (message.SenderId == UserProfile.Id)
                            {
                                user = "You";
                            }
                            else
                            {
                                user = message.Sender.Name;
                            }
                        }
                        var encodedMsg = $"{user}: {message.Text}";
                        Log.Information($"Revision cancellation from user was received. {encodedMsg}");
                        if (message.SenderId != UserProfile.Id)
                        {
                            notifyIcon.ShowBalloonTip(0, "Transaction cancelled", encodedMsg, ToolTipIcon.None);
                            notifyIcon.Visible = true;
                            notifyIcon.Tag = userProject;
                            notifyIcon.Click += NotifyIcon_Click;
                            notifyIcon.BalloonTipClicked += new EventHandler((sender, e) => SelectProjectInProjectLV(userProject.Id));
                        }
                        if (selectedUserProject != null && selectedUserProject.Id == userProject.Id)
                        {
                            messagingControl.AddMessageAsync(message);
                            DialogResult dlgRes = DialogResult.Yes;
                            if (ApplicationIsActivated())
                            {
                                dlgRes = MessageBox.Show("Current project updated. Update view?", "Project updated", MessageBoxButtons.YesNo);
                            }
                            if (dlgRes == DialogResult.Yes)
                            {
                                messagingControl.AddMessageAsync(message);
                                SelectProjectInProjectLV(selectedUserProject.Id);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Receiving file upload message failed: " + ex.Message);
                }
            });
            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            await hubConnection.StartAsync();
        }

        private void NotifyIcon_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            Log.Error("Not implemented.");
        }

        private bool newLocalFileDetected(string path, UserProject userProject)
        {
            // notify
            var file = new FileInfo(path);
            var projectName = userProject.Project == null ? "unknown" : userProject.Project.Name;
            notifyIcon.ShowBalloonTip(25000, "New file detected", "Ready to push for " + projectName + " project.", ToolTipIcon.Info);
            notifyIcon.Visible = true;
            notifyIcon.BalloonTipClicked += new EventHandler((sender, e) => SelectProjectInProjectLV(userProject.Id));

            if (selectedUserProject != null && selectedUserProject.Id == userProject.Id)
            {
                UpdateCurrentProject(selectedUserProject);
                DialogResult dlgRes = DialogResult.Yes;
                if (ApplicationIsActivated())
                {
                    dlgRes = MessageBox.Show("Current project updated. Update view?", "Project updated", MessageBoxButtons.YesNo);
                }
                if (dlgRes == DialogResult.Yes)
                {
                    SelectProjectInProjectLV(selectedUserProject.Id);
                }
            }
            return true;
        }

        public void OnChanged(object source, FileSystemEventArgs e, UserProject userProject)
        {
            try
            {
                var pathDirectory = Path.GetDirectoryName(e.FullPath);
                if (selectedUserProject != null
                    && selectedUserProject.Id == userProject.Id
                    && HelperUtilities.IsProjectPathValid(userProject.MonitoredPath)
                    && userProject.MonitoredPath == pathDirectory)
                {
                    UpdateWithProjectContext(selectedUserProject);
                }
                // Specify what is done when a file is changed, created, or deleted.
                Log.Information("File was changed: ", e.FullPath);
                switch (e.ChangeType)
                {
                    case WatcherChangeTypes.Changed:
                        Log.Information("File modified.");
                        //might want to add this file for review before pushing
                        newLocalFileDetected(e.FullPath, userProject);
                        break;
                    case WatcherChangeTypes.Created:
                        Log.Information("File created.");
                        newLocalFileDetected(e.FullPath, userProject);
                        // file should be added for pushing
                        break;
                    case WatcherChangeTypes.Deleted:
                        Log.Information("File deleted.");
                        SelectProjectInProjectLV(userProject.Id);
                        break;
                    default:
                        Log.Error("Unexpected file change type fired OnChange event.");
                        return;
                }
            }
            catch (Exception ex)
            {
                Log.Error("FS monitor failed on file change. " + ex.Message);
                MessageBox.Show("FS monitor failed on file change. ");
            }

        }


        delegate Task UpdateCurrentProjectCallback(UserProject userProject);
        public async Task UpdateCurrentProject(UserProject userProject)
        {
            if (projectLV.InvokeRequired)
            {
                UpdateCurrentProjectCallback d = new UpdateCurrentProjectCallback(UpdateCurrentProject);
                this.Invoke(d, userProject);
            }
            else
            {
                if (userProject != null)
                {
                    RevidXApiCaller revidXApiCaller = await GetRevidXApiCallerAsync();
                    var updatedProject = await revidXApiCaller.GetUserProject(userProject.Id);
                    var keyExists = projectLV.Items.ContainsKey(userProject.Id);
                    if (updatedProject != null && keyExists)
                    {
                        projectLV.Items[userProject.Id].Tag = updatedProject;
                        selectedUserProject = updatedProject;
                    }
                    else
                    {
                        throw new Exception("Project being updated doesn't exists.");
                    }
                }
                else
                {
                    Log.Warning("Can't update, non project selected");
                }
            }
        }

        delegate void UpdateUserProjectListCallback();
        private async void UpdateUserProjectList()
        {
            if (projectLV.InvokeRequired)
            {
                UpdateUserProjectListCallback d = new UpdateUserProjectListCallback(UpdateUserProjectList);
                this.Invoke(d);
            }
            else
            {
                try
                {
                    projectLV.Items.Clear();
                    notifyIcon.ContextMenuStrip.Items.Clear();
                    ToolStripMenuItem projectsItem = new ToolStripMenuItem();
                    projectsItem.Text = "Projects";
                    projectsItem.Enabled = false;
                    notifyIcon.ContextMenuStrip.Items.Add(projectsItem);
                    if (CustomTokenProvider == null)
                    {
                        throw new Exception("Token provider unavailable.");
                    }
                    RevidXApiCaller revidXApiCaller = await GetRevidXApiCallerAsync();
                    UserProjects = await revidXApiCaller.GetUserProjects();
                    if (UserProjects == null)
                    {
                        Log.Error("Could not get user projects.");
                        throw new Exception("Could not get user projects.");
                    }
                    //set fsmonitor for each project and keep them active
                    foreach (var fsMonitor in fsMonitorRing)
                    {
                        fsMonitor.Value.Item1.Dispose();
                        fsMonitorRing.Remove(fsMonitor.Key);
                    }
                    List<UserProject> inactiveProjects = new List<UserProject>();


                    foreach (var userProject in UserProjects)
                    {
                        ListViewItem lvItem = new ListViewItem();
                        lvItem.Text = userProject.Name;
                        lvItem.Tag = userProject;
                        lvItem.Name = userProject.Id;
                        await hubConnection.SendAsync("JoinGroup", userProject.ProjectId);
                        string warning = string.Empty;
                        if (string.IsNullOrEmpty(userProject.MonitoredPath))
                        {
                            warning = "Path is not specified. Select a valid path.";
                            monitoredPathTxt.Text = $"{userProject.MonitoredPath}";
                            inactiveProjects.Add(userProject);
                            projectLV.Items.Add(lvItem);
                        }
                        else
                        {
                            bool isPathValid = Directory.Exists(userProject.MonitoredPath) && Path.IsPathRooted(userProject.MonitoredPath);
                            if (isPathValid)
                            {
                                var samePathUP = UserProjects.Where(up => HelperUtilities.IsPointingToSameLoc(up.MonitoredPath, userProject.MonitoredPath) && up.Id != userProject.Id);
                                if (samePathUP.Any())
                                {
                                    warning = $"Path is already in use by project {samePathUP.First().MonitoredPath}";
                                    projectLV.Items.Add(lvItem);
                                    inactiveProjects.Add(userProject);
                                    monitoredPathTxt.Text = $"{userProject.MonitoredPath}";
                                    continue;
                                }
                                var validPath = userProject.MonitoredPath;
                                var fsMonitor = new FSMonitor(validPath.ToString());
                                fsMonitor.Run((s, e) => OnChanged(s, e, userProject), null, null);// if error, monitor failed to run. Warn and put to inactive Projects
                                                                                                  // else put into listbox and highlight
                                if (fsMonitor.IsActive)
                                {
                                    projectLV.Items.Insert(0, lvItem);
                                    fsMonitorRing.Add(userProject.ProjectId, (fsMonitor, userProject));
                                }
                                else
                                {
                                    warning = "Filesystem monitor was not able to run. Check directory, permissions, logs or contact support.";
                                    monitoredPathTxt.Text = $"{userProject.MonitoredPath}";
                                    projectLV.Items.Add(lvItem);
                                    inactiveProjects.Add(userProject);
                                }
                            }
                            else
                            {
                                warning = "Invalid path, please select a valid path.";
                                monitoredPathTxt.Text = $"{userProject.MonitoredPath}";
                                projectLV.Items.Add(lvItem);
                                inactiveProjects.Add(userProject);
                                continue;
                            }
                        }
                        //lvItem.SubItems.Add(lvItem.Text);
                        if (!string.IsNullOrEmpty(warning))
                        {
                            lvItem.UseItemStyleForSubItems = false;
                            lvItem.SubItems.Add(warning, Color.IndianRed, lvItem.BackColor, lvItem.Font);
                        }
                        ToolStripMenuItem item = new ToolStripMenuItem();
                        item.Text = userProject.Name;
                        item.Name = userProject.Id;
                        item.Click += ProjectNotifyItem_Click;
                        notifyIcon.ContextMenuStrip.Items.Add(item);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    ToolStripSeparator toolStripSeparator1 = new ToolStripSeparator();
                    notifyIcon.ContextMenuStrip.Items.Add(toolStripSeparator1);
                    ToolStripMenuItem quitItem = new ToolStripMenuItem();
                    quitItem.Text = "Quit";
                    quitItem.Click += QuitNotifyItem_Click;
                    notifyIcon.ContextMenuStrip.Items.Add(quitItem);
                }
            }
        }

        delegate void SelectProjectInProjectLVCallback(string userProjectId);
        private void SelectProjectInProjectLV(string userProjectId)
        {
            if (projectLV.InvokeRequired)
            {
                SelectProjectInProjectLVCallback d = new SelectProjectInProjectLVCallback(SelectProjectInProjectLV);
                this.Invoke(d, userProjectId);
            }
            else
            {
                if (projectLV.Items.ContainsKey(userProjectId))
                {
                    Show();
                    this.Activate();
                    projectLV.Select();
                    foreach (ListViewItem item in projectLV.Items)
                    {
                        item.Selected = false; // should trigger reselection for update purposes
                        if (item.Name == userProjectId)
                        {

                            item.Selected = true;
                            monitoredPathTxt.Text = ((UserProject)item.Tag).MonitoredPath;
                        }
                    }
                }
            }
        }

        private void ProjectNotifyItem_Click(object sender, EventArgs e)
        {
            var item = (ToolStripMenuItem)sender;
            SelectProjectInProjectLV(item.Name);
        }
        private void QuitNotifyItem_Click(object sender, EventArgs e)
        {
            var window = MessageBox.Show(
           "Completely close RevidX Desktop?",
           "Close RevidX Desktop",
           MessageBoxButtons.YesNo);
            if (window == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// General handlers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void RevidXTrayItem_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                notifyIcon.ContextMenuStrip.Show(Cursor.Position, ToolStripDropDownDirection.AboveRight);
            }
        }

        private void EnableAllControls()
        {
            groupBox1.Enabled = true;
            revisionSystemControl.Enabled = true;
            messagingControl.Enabled = true;
        }

        private void DisableAllControls()
        {
            groupBox1.Enabled = false;
            revisionSystemControl.Enabled = false;
            messagingControl.Enabled = false;
        }

        private async void LoginBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsLoggedIn)
                {
                    OidcClient oidcClient = new OidcClient(GetOidcClientOptions(SERVER_URI));
                    var result = await oidcClient.LogoutAsync();
                    await OpenLoginWindow();
                    IsLoggedIn = false;
                    //LoginBtn.Text = LOGINTXT;
                    //DisableAllControls();
                }
                else
                {
                    await OpenLoginWindow();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Server not reachable: " + ex.Message);
                MessageBox.Show("Server not reachable!" + ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error("Inner Exception: " + ex.Message);
                }
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    Hide();
                    notifyIcon.ShowBalloonTip(200, "Minimized", "RevidX Desktop is minimized.", ToolTipIcon.Info);
                    notifyIcon.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Minimizing/restoring window failed. " + ex.Message);
                MessageBox.Show("Minimizing/restoring window failed. ");
            }
        }

        private void RevidXTrayItem_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                Show();
            }
            catch (Exception ex)
            {
                Log.Error("Tray app handling failed. " + ex.Message);
                MessageBox.Show("Tray app handling failed.");
            }
        }

        private async void projectLV_ItemSelectionChanged(object sender, EventArgs e)
        {
            try
            {
                ListView listView = (ListView)sender;
                if (listView.SelectedItems.Count == 1)
                {
                    DisableAllControls();
                    projectLoadingProgressBar.Step = 60;
                    selectedUserProject = (UserProject)listView.SelectedItems[0].Tag;
                    await UpdateCurrentProject(selectedUserProject);
                    projectLoadingProgressBar.PerformStep();
                    await UpdateWithProjectContext(selectedUserProject);
                    projectLoadingProgressBar.PerformStep();
                    projectLoadingProgressBar.Value = 100;
                    projectLoadingProgressBar.Value = 0;
                    EnableAllControls();
                    EnableProjectListControls();

                }
                else
                {
                    DisableProjectListControls();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Project selection failed. " + ex.Message);
                MessageBox.Show("Project selection failed. ");
                projectLoadingProgressBar.Value = 0;
                EnableAllControls();
            }
        }

        private void EnableProjectListControls()
        {
            revisionSystemControl.Enabled = true;
            messagingControl.Enabled = true;
            selectPathBtn.Enabled = true;
            openMonitoredPathBtn.Enabled = true;
            userProjectWebBtn.Enabled = true;
            removePathBtn.Enabled = true;
        }

        private void DisableProjectListControls()
        {
            revisionSystemControl.Enabled = false;
            messagingControl.Enabled = false;
            selectPathBtn.Enabled = false;
            openMonitoredPathBtn.Enabled = false;
            userProjectWebBtn.Enabled = false;
            removePathBtn.Enabled = false;
        }

        public async Task<bool> UpdateWithProjectContext(UserProject userProject)
        {
            RevidXApiCaller revidXApiCaller = await GetRevidXApiCallerAsync();

            var userProfiles = await revidXApiCaller.GetUserProfilesByProjectId(selectedUserProject.ProjectId);
            var messages = await revidXApiCaller.GetProjectMessages(userProject.ProjectId);
            var monitoredPath = selectedUserProject.MonitoredPath;
            if (monitoredPath != null && monitoredPath.Length > 0 && Directory.Exists(monitoredPath)
                        && !UserProjects.Where(up => up.Id != selectedUserProject.Id && HelperUtilities.IsPointingToSameLoc(up.MonitoredPath, monitoredPath)).Any())
            {
                monitoredPathTxt.Text = monitoredPath;
                messagingControl.InitializeMessages(userProfiles, messages, this);
                var result = await revisionSystemControl.InitializeRevisionSystem(selectedUserProject, this);
                revisionSystemControl.Enabled = true;
            }
            else
            {
                messagingControl.InitializeMessages(userProfiles, messages, this);
                revisionSystemControl.Enabled = false;
            }

            return true;
        }
        public void IncreaseProgress(int value = 0, int step = 0)
        {
            projectLoadingProgressBar.Step = step;
            projectLoadingProgressBar.Value = value;
            projectLoadingProgressBar.PerformStep();
        }


        private void refreshUPBtn_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateUserProjectList();
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Could not refresh because not connected. {0}", ex.Message));
            }
        }

        private void settingsBtn_Click(object sender, EventArgs e)
        {
            var settingsBtn = new SettingsDlg();
            settingsBtn.ShowDialog();
            SERVER_URI = Desktop.Properties.Settings.Default.ServerUri;
        }

        private async Task<bool> UpdateProjectDirectory(string monitoredPath)
        {
            try
            {
                selectedUserProject.MonitoredPath = monitoredPath;
                RevidXApiCaller revidXApiCaller = await GetRevidXApiCallerAsync();
                if (!await revidXApiCaller.UpdateUserProject(selectedUserProject))
                {
                    throw new Exception("Project could not be updated remotely with selected path: " + selectedUserProject.MonitoredPath);
                }
                var fSMonitor = new FSMonitor(selectedUserProject.MonitoredPath);
                fSMonitor.Run((s, e) => OnChanged(s, e, selectedUserProject), null, null);
                if (fSMonitor.IsActive)
                {

                    if (fsMonitorRing.ContainsKey(selectedUserProject.ProjectId))
                    {
                        (FSMonitor, UserProject) fsMonitorTuple;
                        if (fsMonitorRing.TryGetValue(selectedUserProject.ProjectId, out fsMonitorTuple))
                        {
                            fsMonitorTuple.Item1.Dispose();
                            fsMonitorRing.Remove(selectedUserProject.ProjectId);
                        }

                    }
                    await UpdateCurrentProject(selectedUserProject);
                    var result = await UpdateWithProjectContext(selectedUserProject);
                    if (!result)
                    {
                        throw new Exception("Collaboration tab could not be updated.");
                    }
                }
                else
                {
                    throw new Exception("FS monitor could not be started on specified folder: " + selectedUserProject.MonitoredPath + ". Check permissions and reselect again");
                }
                fsMonitorRing.Add(selectedUserProject.ProjectId, (fSMonitor, selectedUserProject));
                UpdateUserProjectList();
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return false;
            }
        }


        private void viewProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void openMonitoredPath_Click(object sender, EventArgs e)
        {
            if (monitoredPathTxt != null && Directory.Exists(monitoredPathTxt.Text))
            {
                Process.Start("explorer.exe", monitoredPathTxt.Text);
            }
            else
            {

                Log.Error("Path does not exist or not initialized: " + monitoredPathTxt.Text);
                MessageBox.Show("Path does not exist. ");
            }
        }

        private async void selectPathBtn_Click(object sender, EventArgs e)
        {
            try
            {
                selectFolderDlg.ShowDialog();
                var selectedPath = selectFolderDlg.SelectedPath;
                if (selectedPath == null || selectedPath.Length == 0)
                {
                    return;
                }
                if (!HelperUtilities.IsProjectPathValid(selectedPath))
                {
                    throw new Exception("Selected path is not valid: " + selectedPath);
                }
                var result = await UpdateProjectDirectory(selectedPath);
                if (!result)
                {
                    MessageBox.Show("Could not update project. Check log!");
                    return;
                }
                monitoredPathTxt.Text = selectedPath;

            }
            catch (Exception ex)
            {
                Log.Error("Selecting path failed! " + ex.Message);
                MessageBox.Show("Selecting path failed! " + ex.Message);
            }
        }

        private async void removePathBtn_Click(object sender, EventArgs e)
        {
            try
            {
                selectedUserProject.MonitoredPath = "";
                RevidXApiCaller revidXApiCaller = await GetRevidXApiCallerAsync();
                if (!await revidXApiCaller.UpdateUserProject(selectedUserProject))
                {
                    throw new Exception("Project could not be updated remotely with selected path: " + selectedUserProject.MonitoredPath);
                }
                SelectProjectInProjectLV(selectedUserProject.Id);
                UpdateUserProjectList();
            }
            catch (Exception ex)
            {
                Log.Error("Selecting path failed! " + ex.Message);
                MessageBox.Show("Selecting path failed! " + ex.Message);
            }
        }

        private void settingsBtn_Click_1(object sender, EventArgs e)
        {
            var settingsBtn = new SettingsDlg();
            settingsBtn.ShowDialog();
            SERVER_URI = Desktop.Properties.Settings.Default.ServerUri;
        }


        /// <summary>Returns true if the current application has focus, false otherwise</summary>
        public static bool ApplicationIsActivated()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;       // No window is currently activated
            }

            var procId = Process.GetCurrentProcess().Id;
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            return activeProcId == procId;
        }


        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        private void DesktopClientMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.ApplicationExitCall)
            {
                this.WindowState = FormWindowState.Minimized;
                e.Cancel = true;
            }
        }
    }
}
