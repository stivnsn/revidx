﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RevidX.Desktop
{
    public partial class SettingsDlg : Form
    {
        static string StartupRunRegKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
        static string StartupValueName = "RevidX.Desktop";
        string currentAppPath = Process.GetCurrentProcess().MainModule.FileName;

        public SettingsDlg()
        {
            InitializeComponent();
            serverUriTxt.Text = Desktop.Properties.Settings.Default.ServerUri;
            keepIDZChk.Checked = Desktop.Properties.Settings.Default.KeepIDZFilePull;
            try
            {
                string path = string.Empty;
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey(StartupRunRegKey, true))
                {
                    if (key != null)
                    {
                        path = (string)key.GetValue(StartupValueName);
                        if (!string.IsNullOrEmpty(path) && Path.GetFullPath(path) != Path.GetFullPath(currentAppPath))
                        {
                            MessageBox.Show($"Startup path {path} does not equal to current app path {currentAppPath}. Will be updated on OK.");
                        }
                    }
                }
                startupChk.Checked = !string.IsNullOrEmpty(path);
            }
            catch (Exception ex)
            {
                startupChk.Enabled = false;
                MessageBox.Show($"Error reading registry. {ex.Message}");
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void okBtn_Click(object sender, EventArgs e)
        {

            SaveSettings();
        }


        private void openLogFileBtn_Click(object sender, EventArgs e)
        {
            var lastLogFile = new DirectoryInfo(Path.GetDirectoryName(DesktopClientMain.logFileLocation)).GetFiles()
                .Where(f => f.Name.Contains("RevidXClient")).OrderByDescending(f => f.LastWriteTime).FirstOrDefault();
            if (lastLogFile != null)
            {
                Process.Start("notepad.exe", lastLogFile.FullName);
            }
        }

        private void SaveSettings()
        {
            Desktop.Properties.Settings.Default.KeepIDZFilePull = keepIDZChk.Checked;
            if (!Uri.IsWellFormedUriString(serverUriTxt.Text, UriKind.Absolute))
            {
                MessageBox.Show("Uri is not valid.");
                return;
            }
            Desktop.Properties.Settings.Default.ServerUri = serverUriTxt.Text;
            Desktop.Properties.Settings.Default.Save();
            try
            {
                try
                {
                    if (startupChk.Checked)
                    {
                        using (RegistryKey key = Registry.CurrentUser.OpenSubKey(StartupRunRegKey, true))
                        {
                            if (key != null)
                            {
                                key.SetValue(StartupValueName, currentAppPath, RegistryValueKind.String);
                            }
                        }
                    }
                    else
                    {
                        using (RegistryKey key = Registry.CurrentUser.OpenSubKey(StartupRunRegKey, true))
                        {
                            if (key != null)
                            {
                                var path = string.Empty;
                                path = (string)key.GetValue(StartupValueName);
                                if (!string.IsNullOrEmpty(path))
                                {
                                    key.DeleteValue(StartupValueName);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error reading registry. {ex.Message}");
                }

            }
            catch
            {
                startupChk.Enabled = false;
                MessageBox.Show("Error writting to registry.");
                return;
            }
            this.Close();
        }

        private void SettingsDlg_KeyPress(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SettingsDlg_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            { 
                SaveSettings();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
