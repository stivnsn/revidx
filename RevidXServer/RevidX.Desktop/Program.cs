using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RevidX.Desktop
{
    public static class Program
    {

        [STAThread]
        public static void Start(out DesktopClientMain desktopClient)
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            desktopClient = new DesktopClientMain();
            Application.Run(desktopClient);
        }

        [STAThread]
        public static void Stop()
        {

            Application.Exit();
        }
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            var exists = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1;
            var forceInstance = false;
            for (int i = 0; i < args.Length; i++)
            {
                forceInstance = args[i] == "/forceinstance";
                Console.WriteLine("args[{0}] == {1}", i, args[i]);
            }
            if (exists && !forceInstance)
            {
                MessageBox.Show("Another instance is already running");
                return;
            }
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DesktopClientMain());
        }
    }
}
