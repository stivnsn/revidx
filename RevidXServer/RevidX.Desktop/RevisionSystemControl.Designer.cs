﻿
namespace RevidX.Desktop
{
    partial class RevisionSystemControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusShowLbl = new System.Windows.Forms.Label();
            this.pushPullActionBtn = new System.Windows.Forms.Button();
            this.collabStatusLbl = new System.Windows.Forms.Label();
            this.cancelLastActionBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusShowLbl
            // 
            this.statusShowLbl.AutoSize = true;
            this.statusShowLbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.statusShowLbl.Location = new System.Drawing.Point(51, 19);
            this.statusShowLbl.Name = "statusShowLbl";
            this.statusShowLbl.Size = new System.Drawing.Size(14, 17);
            this.statusShowLbl.TabIndex = 22;
            this.statusShowLbl.Text = "-";
            // 
            // pushPullActionBtn
            // 
            this.pushPullActionBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pushPullActionBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pushPullActionBtn.Enabled = false;
            this.pushPullActionBtn.Location = new System.Drawing.Point(6, 38);
            this.pushPullActionBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pushPullActionBtn.Name = "pushPullActionBtn";
            this.pushPullActionBtn.Size = new System.Drawing.Size(289, 71);
            this.pushPullActionBtn.TabIndex = 1;
            this.pushPullActionBtn.Text = "No action available";
            this.pushPullActionBtn.UseVisualStyleBackColor = true;
            this.pushPullActionBtn.Click += new System.EventHandler(this.pushPullActionBtn_Click);
            // 
            // collabStatusLbl
            // 
            this.collabStatusLbl.AutoSize = true;
            this.collabStatusLbl.Location = new System.Drawing.Point(6, 19);
            this.collabStatusLbl.Name = "collabStatusLbl";
            this.collabStatusLbl.Size = new System.Drawing.Size(39, 15);
            this.collabStatusLbl.TabIndex = 18;
            this.collabStatusLbl.Text = "Status";
            // 
            // cancelLastActionBtn
            // 
            this.cancelLastActionBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelLastActionBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cancelLastActionBtn.Enabled = false;
            this.cancelLastActionBtn.Location = new System.Drawing.Point(301, 38);
            this.cancelLastActionBtn.Name = "cancelLastActionBtn";
            this.cancelLastActionBtn.Size = new System.Drawing.Size(156, 71);
            this.cancelLastActionBtn.TabIndex = 2;
            this.cancelLastActionBtn.Text = "Cancel";
            this.cancelLastActionBtn.UseVisualStyleBackColor = true;
            this.cancelLastActionBtn.Click += new System.EventHandler(this.cancelLastActionBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.statusShowLbl);
            this.groupBox1.Controls.Add(this.collabStatusLbl);
            this.groupBox1.Controls.Add(this.cancelLastActionBtn);
            this.groupBox1.Controls.Add(this.pushPullActionBtn);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(463, 114);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Collaboration";
            // 
            // RevisionSystemControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "RevisionSystemControl";
            this.Size = new System.Drawing.Size(469, 120);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label statusShowLbl;
        public System.Windows.Forms.Button pushPullActionBtn;
        private System.Windows.Forms.Label collabStatusLbl;
        private System.Windows.Forms.Button cancelLastActionBtn;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
