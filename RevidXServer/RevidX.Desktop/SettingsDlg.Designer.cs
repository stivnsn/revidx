﻿namespace RevidX.Desktop
{
    partial class SettingsDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.serverUriTxt = new System.Windows.Forms.TextBox();
            this.serverUriLbl = new System.Windows.Forms.Label();
            this.keepIDZChk = new System.Windows.Forms.CheckBox();
            this.openLogFileBtn = new System.Windows.Forms.Button();
            this.startupChk = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // okBtn
            // 
            this.okBtn.Location = new System.Drawing.Point(207, 107);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(137, 22);
            this.okBtn.TabIndex = 0;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(349, 107);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(137, 22);
            this.cancelBtn.TabIndex = 1;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // serverUriTxt
            // 
            this.serverUriTxt.Location = new System.Drawing.Point(66, 10);
            this.serverUriTxt.Name = "serverUriTxt";
            this.serverUriTxt.Size = new System.Drawing.Size(331, 20);
            this.serverUriTxt.TabIndex = 2;
            // 
            // serverUriLbl
            // 
            this.serverUriLbl.AutoSize = true;
            this.serverUriLbl.Location = new System.Drawing.Point(9, 13);
            this.serverUriLbl.Name = "serverUriLbl";
            this.serverUriLbl.Size = new System.Drawing.Size(60, 13);
            this.serverUriLbl.TabIndex = 3;
            this.serverUriLbl.Text = "Server URI";
            // 
            // keepIDZChk
            // 
            this.keepIDZChk.AutoSize = true;
            this.keepIDZChk.Location = new System.Drawing.Point(9, 57);
            this.keepIDZChk.Name = "keepIDZChk";
            this.keepIDZChk.Size = new System.Drawing.Size(182, 17);
            this.keepIDZChk.TabIndex = 4;
            this.keepIDZChk.Text = "Keep IDZ file format when pulling";
            this.keepIDZChk.UseVisualStyleBackColor = true;
            this.keepIDZChk.Visible = false;
            // 
            // openLogFileBtn
            // 
            this.openLogFileBtn.Location = new System.Drawing.Point(8, 109);
            this.openLogFileBtn.Name = "openLogFileBtn";
            this.openLogFileBtn.Size = new System.Drawing.Size(64, 20);
            this.openLogFileBtn.TabIndex = 5;
            this.openLogFileBtn.Text = "Open Log";
            this.openLogFileBtn.UseVisualStyleBackColor = true;
            this.openLogFileBtn.Click += new System.EventHandler(this.openLogFileBtn_Click);
            // 
            // startupChk
            // 
            this.startupChk.AutoSize = true;
            this.startupChk.Location = new System.Drawing.Point(10, 36);
            this.startupChk.Name = "startupChk";
            this.startupChk.Size = new System.Drawing.Size(145, 17);
            this.startupChk.TabIndex = 6;
            this.startupChk.Text = "Start on Windows startup";
            this.startupChk.UseVisualStyleBackColor = true;
            // 
            // SettingsDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(485, 139);
            this.Controls.Add(this.startupChk);
            this.Controls.Add(this.openLogFileBtn);
            this.Controls.Add(this.keepIDZChk);
            this.Controls.Add(this.serverUriLbl);
            this.Controls.Add(this.serverUriTxt);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.okBtn);
            this.Name = "SettingsDlg";
            this.Text = "Settings";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SettingsDlg_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.TextBox serverUriTxt;
        private System.Windows.Forms.Label serverUriLbl;
        private System.Windows.Forms.CheckBox keepIDZChk;
        private System.Windows.Forms.Button openLogFileBtn;
        private System.Windows.Forms.CheckBox startupChk;
    }
}