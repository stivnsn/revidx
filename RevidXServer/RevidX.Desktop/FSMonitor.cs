﻿using Serilog;
using System;
using System.IO;

namespace RevidX.Desktop
{
    public class FSMonitor
    {
        string DirectoryPath;
        FileSystemWatcher watcher = new FileSystemWatcher();
        public FSMonitor(string directoryPath)
        {
            DirectoryPath = directoryPath;
            //Log.Logger = new LoggerConfiguration().WriteTo.Console()
            //    .WriteTo.File(Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) + "\\FSMonitor.log", rollingInterval: RollingInterval.Day)
            //    .CreateLogger();
            Log.Information("Log initialized for {nameof}", nameof(FSMonitor));
        }

        bool errorsFound = false;

        public bool IsActive
        {
            get { return !errorsFound; }
        }

        public void Pause()
        {
            watcher.EnableRaisingEvents = false;
        }
        public void Continue()
        {
            watcher.EnableRaisingEvents = true;
        }

        public void Run(FileSystemEventHandler onChanged, EventHandler onRenamed, EventHandler onError)
        {
            
            watcher.Path = DirectoryPath;
            watcher.NotifyFilter = NotifyFilters.LastAccess
                                | NotifyFilters.LastWrite
                                | NotifyFilters.FileName
                                | NotifyFilters.DirectoryName;
            watcher.Filter = "*.idx";

            // Add event handlers.
            watcher.Changed += onChanged;
            watcher.Created += onChanged;
            watcher.Deleted += onChanged;
            watcher.Renamed += OnChanged;
            watcher.Error += OnError;

            // Begin watching.
            watcher.EnableRaisingEvents = true;
            Log.Information("Started FSMonitor watcher.");
        }

        public void Dispose()
        {
            watcher.Dispose();
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            // Specify what is done when a file is changed, created, or deleted.
            Log.Information("File was changed: ", e.FullPath);
            switch(e.ChangeType)
            {
                case WatcherChangeTypes.Changed:
                    Log.Information("File modified.");
                    //might want to add this file for review before pushing
                    break;
                case WatcherChangeTypes.Created:
                    Log.Information("File created.");
                    //file should be added for pushing
                    break;
                case WatcherChangeTypes.Deleted:
                    Log.Information("File deleted.");
                    //nothing should happen
                    break;
                default:
                    Log.Error("Unexpected file change type fired OnChange event.");
                    return;
            }
        }

        private void OnRenamed(object source, RenamedEventArgs e)
        {
            // Specify what is done when a file is renamed.
            Log.Information("File was renamed:");
            Log.Information(e.OldFullPath, "=>", e.FullPath);


        }

        private void OnError(object source, ErrorEventArgs e) 
        {
            errorsFound = true;
            Log.Error("Error happened");
            Log.Error("Probably quitting");
            Log.Error("Error: "+ e.GetException().Message + DateTime.Now.ToString());
        }
    }
}
