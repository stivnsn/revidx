﻿using EDMDProcessor;
using RevidX.ServiceClient.Models;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using RevidX.ServiceClient;
using System.Collections.Generic;
using System.Text.Json;
using PCBXRevision;
using EDMDSerialization;
using static RevidX.ServiceClient.Models.BinaryContent;

namespace RevidX.Desktop
{
    public class RevisionManager
    {
        RevidXApiCaller RevidXApiCaller { get; set; }

        public RevisionManager(RevidXApiCaller revidXApiCaller)
        {
            RevidXApiCaller = revidXApiCaller;
        }

        public async Task<TransactionFile> GetTransactionFileToPush(UserProject userProject, List<TransactionFile.TransactionFileType> fileTypesToPush)
        {
            TransactionFile transactionFile = null;
            FileInfo fileToPush = null;
            var localFilesList = new DirectoryInfo(userProject.MonitoredPath).GetFiles()
                                            .OrderByDescending(f => f.LastWriteTime);
            bool foundFileToPush = false;
            foreach (var file in localFilesList)
            {
                if (Path.GetExtension(file.Name) == ".idx")
                {
                    var isLocalFileNewerThanRevision = userProject.CurrentRevision == null ||
                        file.LastWriteTimeUtc > userProject.CurrentRevision.ModificationTime;

                    if (isLocalFileNewerThanRevision)
                    {
                        if (foundFileToPush)
                        {
                            //multiple upload files warning, taking latest
                            break;
                        }
                        fileToPush = file;
                        foundFileToPush = true;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            if (fileToPush != null)
            {
                try
                {
                    byte[] streamContent = null;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (FileStream stream = File.Open(fileToPush.FullName, FileMode.Open, FileAccess.Read))
                        {
                            if (stream == null)
                            {
                                throw new Exception("Reading stream failed of selected file failed: " + fileToPush.FullName);
                            }
                            stream.CopyTo(ms);
                            stream.Close();
                            streamContent = ms.ToArray();
                        }
                    }
                    EDMDReader reader = new EDMDReader(fileToPush.FullName);
                    var dataset = reader.DeserializeEDMDFile();
                    var fileType = EDMDReader.GetFileType(dataset);
                    if (fileToPush != null)
                    {
                        if (fileTypesToPush != null)
                        {
                            if(!fileTypesToPush.Contains(TransactionFile.GetTransactionFileTypeEnum(fileType.ToString())))
                            {
                                throw new Exception($"{fileType} is not expected for current collaboration state. Check if wrong file was exported, or contact support.");
                            }
                            transactionFile = new TransactionFile();
                            var pcbContainer = EDMDReader.ConstructPCBFromFile(dataset);
                            if (pcbContainer == null)
                            {
                                throw new Exception("PCB could not be constructed. Not allowed currently.");
                            }
                            switch(fileType)
                            {
                                case SendType.BASELINE:
                                    transactionFile.Description = string.Format("{0}.", pcbContainer.QuickPCBStats.GetNrOfObjectTypesString());
                                    break;
                                case SendType.PROPOSAL:
                                    transactionFile.Description = string.Format("{0}. {1}.", pcbContainer.QuickPCBStats.GetNrOfChangesString(), pcbContainer.QuickPCBStats.GetNrOfObjectTypesString());
                                    break;
                                case SendType.RESPONSE:
                                    transactionFile.Description = string.Format("{0}. {1}. {2}", pcbContainer.QuickPCBStats.GetNrOfChangesAcceptedString(), pcbContainer.QuickPCBStats.GetNrOfChangesString(), pcbContainer.QuickPCBStats.GetNrOfObjectTypesString());
                                    break;
                                default:
                                    Log.Error(string.Format("Send type not supported: {0}", fileType.ToString()));
                                    break;
                            }
                            var zipBinary = await FileUtils.AddToArchiveAsync(streamContent, fileToPush.Name);
                            transactionFile.BinaryContent = new BinaryContent(zipBinary, FILE_TYPE.IDZ.ToString());
                            transactionFile.FullPath = fileToPush.Name;
                            transactionFile.LastWriteTime = fileToPush.LastWriteTimeUtc;
                            transactionFile.LastAccessTime = fileToPush.LastAccessTimeUtc;
                            transactionFile.UserProfileId = userProject.UserProfileId;
                            transactionFile.TransactionFileTypeStr = fileType.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Fatal("Fatal error in updating local files! " + ex.Message);
                }

            }
            string currentBSFile = string.Empty;
            string currentRespFile = string.Empty;
            string currentPropFile = string.Empty;
            if (userProject.CurrentRevision != null)
            {
                var currentRevision = userProject.CurrentRevision;
                if (currentRevision.BaselineFile != null)
                {
                    currentBSFile = currentRevision.BaselineFile.FileName;
                }
                if (currentRevision.ResponseFile != null)
                {
                    currentRespFile = currentRevision.ResponseFile.FileName;
                }
                if (currentRevision.ProposalFile != null)
                {
                    currentPropFile = currentRevision.ProposalFile.FileName;
                }
            }
            if (transactionFile == null 
                || (transactionFile.FullPath == currentBSFile)
                || (transactionFile.FullPath == currentRespFile)
                || (transactionFile.FullPath == currentPropFile))
            {
                Log.Error("File seems to be named the same as previous. Not allowing.");
                return null;
                //transactionFile = null;
            }
            return transactionFile;
        }

        public async Task<bool> PushFileToTransaction(string filepath, UserProject userProject)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            if (fileInfo == null)
            {
                throw new Exception("Uploading selected file failed: " + filepath);
            }
            byte[] binContent = null;
            using (MemoryStream ms = new MemoryStream())
            using (Stream stream = File.OpenRead(filepath))
            {
                if (stream == null)
                {
                    throw new Exception("Reading stream failed of selected file failed: " + filepath);
                }
                stream.CopyTo(ms);
                stream.Close();
                binContent = ms.ToArray();
            }
            if(binContent == null || binContent.Length == 0)
            {
                throw new Exception("Binary content is null");
            }
            TransactionFile transactionFile = new TransactionFile();
            transactionFile.FullPath = fileInfo.Name;
            transactionFile.LastAccessTime = fileInfo.LastAccessTimeUtc;
            transactionFile.LastWriteTime = fileInfo.LastWriteTimeUtc;
            transactionFile.UserProfileId = userProject.UserProfileId;
            EDMDReader reader = new EDMDReader(fileInfo.FullName);
            var dataset = reader.DeserializeEDMDFile();
            if (dataset == null)
            {
                throw new Exception("Dataset could not be retrieved from selected file: " + filepath);
            }
            var fileType = EDMDReader.GetFileType(dataset);
            if (fileType == PCBXRevision.SendType.UNKNOWN)
            {
                throw new Exception("Type of file could not be retrieved from selected file: " + filepath);
            }
            var zipBinContent = await FileUtils.AddToArchiveAsync(binContent, fileInfo.Name);
            transactionFile.BinaryContent = new BinaryContent(zipBinContent, FILE_TYPE.IDZ.ToString());
            transactionFile.TransactionFileTypeStr = fileType.ToString();
            var transactionFiles = new List<TransactionFile>();
            transactionFiles.Add(transactionFile);
            Transaction transaction = new Transaction();
            transaction.TransactionFiles = transactionFiles;
            transaction.ProjectId = userProject.ProjectId;
            transaction.CreationTime = DateTime.UtcNow;
            transaction = await RevidXApiCaller.CreateTransaction(transaction);
            if (transaction == null)
            {
                throw new Exception("Creating transaction for selected file failed: " + filepath);
            }
            return true;
        }
    }
}
