﻿
namespace RevidX.Desktop
{
    partial class DesktopClientMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesktopClientMain));
            this.LoginBtn = new System.Windows.Forms.Button();
            this.profileLink = new System.Windows.Forms.LinkLabel();
            this.projectLV = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.selectPathDlg = new System.Windows.Forms.OpenFileDialog();
            this.selectFolderDlg = new System.Windows.Forms.FolderBrowserDialog();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.projectLoadingProgressBar = new System.Windows.Forms.ProgressBar();
            this.refreshUPBtn = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.removePathBtn = new System.Windows.Forms.Button();
            this.openMonitoredPathBtn = new System.Windows.Forms.Button();
            this.monitoredPathTxt = new System.Windows.Forms.TextBox();
            this.selectPathBtn = new System.Windows.Forms.Button();
            this.userProjectWebBtn = new System.Windows.Forms.Button();
            this.settingsBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.messagingControl = new RevidX.Desktop.MessagingControl();
            this.revisionSystemControl = new RevidX.Desktop.RevisionSystemControl();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LoginBtn
            // 
            this.LoginBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LoginBtn.Location = new System.Drawing.Point(462, 21);
            this.LoginBtn.Margin = new System.Windows.Forms.Padding(2);
            this.LoginBtn.Name = "LoginBtn";
            this.LoginBtn.Size = new System.Drawing.Size(53, 24);
            this.LoginBtn.TabIndex = 8;
            this.LoginBtn.Text = "Login";
            this.LoginBtn.UseVisualStyleBackColor = true;
            this.LoginBtn.Click += new System.EventHandler(this.LoginBtn_Click);
            // 
            // profileLink
            // 
            this.profileLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.profileLink.Location = new System.Drawing.Point(236, 5);
            this.profileLink.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.profileLink.Name = "profileLink";
            this.profileLink.Size = new System.Drawing.Size(279, 15);
            this.profileLink.TabIndex = 9;
            this.profileLink.TabStop = true;
            this.profileLink.Text = "Profile";
            this.profileLink.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // projectLV
            // 
            this.projectLV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projectLV.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.projectLV.FullRowSelect = true;
            this.projectLV.GridLines = true;
            this.projectLV.Location = new System.Drawing.Point(5, 21);
            this.projectLV.Margin = new System.Windows.Forms.Padding(2);
            this.projectLV.Name = "projectLV";
            this.projectLV.Size = new System.Drawing.Size(364, 379);
            this.projectLV.TabIndex = 1;
            this.projectLV.UseCompatibleStateImageBehavior = false;
            this.projectLV.View = System.Windows.Forms.View.Details;
            this.projectLV.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.projectLV_ItemSelectionChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Tag = "Project";
            this.columnHeader1.Text = "Project";
            this.columnHeader1.Width = 200;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Details";
            this.columnHeader2.Width = 200;
            // 
            // selectPathDlg
            // 
            this.selectPathDlg.FileName = "openFileDialog1";
            // 
            // selectFolderDlg
            // 
            this.selectFolderDlg.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "RevidX Desktop";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RevidXTrayItem_MouseClick);
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.RevidXTrayItem_MouseDoubleClick);
            // 
            // projectLoadingProgressBar
            // 
            this.projectLoadingProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projectLoadingProgressBar.Location = new System.Drawing.Point(12, 476);
            this.projectLoadingProgressBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.projectLoadingProgressBar.Name = "projectLoadingProgressBar";
            this.projectLoadingProgressBar.Size = new System.Drawing.Size(948, 10);
            this.projectLoadingProgressBar.TabIndex = 25;
            // 
            // refreshUPBtn
            // 
            this.refreshUPBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.refreshUPBtn.BackgroundImage = global::RevidX.Desktop.Properties.Resources.refresh;
            this.refreshUPBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.refreshUPBtn.Location = new System.Drawing.Point(339, 405);
            this.refreshUPBtn.Name = "refreshUPBtn";
            this.refreshUPBtn.Size = new System.Drawing.Size(30, 30);
            this.refreshUPBtn.TabIndex = 7;
            this.refreshUPBtn.UseVisualStyleBackColor = true;
            this.refreshUPBtn.Click += new System.EventHandler(this.refreshUPBtn_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.settingsBtn);
            this.splitContainer1.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer1.Panel2.Controls.Add(this.profileLink);
            this.splitContainer1.Panel2.Controls.Add(this.messagingControl);
            this.splitContainer1.Panel2.Controls.Add(this.LoginBtn);
            this.splitContainer1.Panel2.Controls.Add(this.revisionSystemControl);
            this.splitContainer1.Size = new System.Drawing.Size(948, 454);
            this.splitContainer1.SplitterDistance = 379;
            this.splitContainer1.TabIndex = 29;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.removePathBtn);
            this.groupBox1.Controls.Add(this.openMonitoredPathBtn);
            this.groupBox1.Controls.Add(this.monitoredPathTxt);
            this.groupBox1.Controls.Add(this.selectPathBtn);
            this.groupBox1.Controls.Add(this.projectLV);
            this.groupBox1.Controls.Add(this.userProjectWebBtn);
            this.groupBox1.Controls.Add(this.refreshUPBtn);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(374, 441);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Projects";
            // 
            // removePathBtn
            // 
            this.removePathBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.removePathBtn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.removePathBtn.Location = new System.Drawing.Point(236, 408);
            this.removePathBtn.Name = "removePathBtn";
            this.removePathBtn.Size = new System.Drawing.Size(22, 23);
            this.removePathBtn.TabIndex = 4;
            this.removePathBtn.Text = "X";
            this.removePathBtn.UseVisualStyleBackColor = true;
            this.removePathBtn.Click += new System.EventHandler(this.removePathBtn_Click);
            // 
            // openMonitoredPathBtn
            // 
            this.openMonitoredPathBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.openMonitoredPathBtn.BackgroundImage = global::RevidX.Desktop.Properties.Resources.computer_folder_open;
            this.openMonitoredPathBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.openMonitoredPathBtn.Location = new System.Drawing.Point(267, 405);
            this.openMonitoredPathBtn.Name = "openMonitoredPathBtn";
            this.openMonitoredPathBtn.Size = new System.Drawing.Size(30, 30);
            this.openMonitoredPathBtn.TabIndex = 5;
            this.openMonitoredPathBtn.UseVisualStyleBackColor = true;
            this.openMonitoredPathBtn.Click += new System.EventHandler(this.openMonitoredPath_Click);
            // 
            // monitoredPathTxt
            // 
            this.monitoredPathTxt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.monitoredPathTxt.Location = new System.Drawing.Point(5, 409);
            this.monitoredPathTxt.Margin = new System.Windows.Forms.Padding(2);
            this.monitoredPathTxt.Name = "monitoredPathTxt";
            this.monitoredPathTxt.ReadOnly = true;
            this.monitoredPathTxt.Size = new System.Drawing.Size(192, 23);
            this.monitoredPathTxt.TabIndex = 2;
            // 
            // selectPathBtn
            // 
            this.selectPathBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.selectPathBtn.Location = new System.Drawing.Point(201, 408);
            this.selectPathBtn.Margin = new System.Windows.Forms.Padding(2);
            this.selectPathBtn.Name = "selectPathBtn";
            this.selectPathBtn.Size = new System.Drawing.Size(30, 23);
            this.selectPathBtn.TabIndex = 3;
            this.selectPathBtn.Text = "...";
            this.selectPathBtn.UseVisualStyleBackColor = true;
            this.selectPathBtn.Click += new System.EventHandler(this.selectPathBtn_Click);
            // 
            // userProjectWebBtn
            // 
            this.userProjectWebBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.userProjectWebBtn.BackgroundImage = global::RevidX.Desktop.Properties.Resources.world_wide_web1;
            this.userProjectWebBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userProjectWebBtn.Location = new System.Drawing.Point(303, 405);
            this.userProjectWebBtn.Name = "userProjectWebBtn";
            this.userProjectWebBtn.Size = new System.Drawing.Size(30, 30);
            this.userProjectWebBtn.TabIndex = 6;
            this.userProjectWebBtn.UseVisualStyleBackColor = true;
            // 
            // settingsBtn
            // 
            this.settingsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.settingsBtn.BackgroundImage = global::RevidX.Desktop.Properties.Resources.settings_gear;
            this.settingsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.settingsBtn.Location = new System.Drawing.Point(520, 5);
            this.settingsBtn.Name = "settingsBtn";
            this.settingsBtn.Size = new System.Drawing.Size(40, 40);
            this.settingsBtn.TabIndex = 10;
            this.settingsBtn.UseVisualStyleBackColor = true;
            this.settingsBtn.Click += new System.EventHandler(this.settingsBtn_Click_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::RevidX.Desktop.Properties.Resources.logo2;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(154, 59);
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // messagingControl
            // 
            this.messagingControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.messagingControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.messagingControl.Location = new System.Drawing.Point(3, 229);
            this.messagingControl.Name = "messagingControl";
            this.messagingControl.Size = new System.Drawing.Size(557, 220);
            this.messagingControl.TabIndex = 12;
            this.messagingControl.UserProfiles = null;
            // 
            // revisionSystemControl
            // 
            this.revisionSystemControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.revisionSystemControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.revisionSystemControl.Location = new System.Drawing.Point(3, 70);
            this.revisionSystemControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.revisionSystemControl.Name = "revisionSystemControl";
            this.revisionSystemControl.Size = new System.Drawing.Size(557, 154);
            this.revisionSystemControl.TabIndex = 11;
            // 
            // DesktopClientMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 497);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.projectLoadingProgressBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(504, 447);
            this.Name = "DesktopClientMain";
            this.Text = "RevidX Desktop";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DesktopClientMain_FormClosing);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button LoginBtn;
        private System.Windows.Forms.LinkLabel profileLink;
        
        private System.Windows.Forms.ListView projectLV;
        private System.Windows.Forms.OpenFileDialog selectPathDlg;

        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ProgressBar projectLoadingProgressBar;
        private System.Windows.Forms.Button refreshUPBtn;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button userProjectWebBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FolderBrowserDialog selectFolderDlg;
        private MessagingControl messagingControl;
        private RevisionSystemControl revisionSystemControl;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button openMonitoredPathBtn;
        private System.Windows.Forms.TextBox monitoredPathTxt;
        private System.Windows.Forms.Button selectPathBtn;
        private System.Windows.Forms.Button removePathBtn;
        private System.Windows.Forms.Button settingsBtn;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
    }
}

