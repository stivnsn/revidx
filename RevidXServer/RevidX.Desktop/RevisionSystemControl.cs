﻿using RevidX.ServiceClient.Models;
using RevidX.Desktop;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.AspNetCore.SignalR.Client;
using EDMDProcessor;
using System.Collections.Generic;
using Message = RevidX.ServiceClient.Models.Message;
using RevidX.ServiceClient;
using System.Text.Json;
using System.IO.Compression;
using EDMDSerialization;
using PCBXRevision;
using static RevidX.ServiceClient.Models.BinaryContent;

namespace RevidX.Desktop
{
    public partial class RevisionSystemControl : UserControl
    {
        DesktopClientMain parentForm { get; set; }
        UserProject selectedUserProject { get; set; }

        public RevisionSystemControl()
        {
            InitializeComponent();
        }

        TransactionFile FileToPull { get; set; }
        TransactionFile FileToPush { get; set; }

        Revision revisionToUpdateTo { get; set; }

        private async void pushPullActionBtn_Click(object sender, EventArgs e)
        {
            try
            {

                if (FileToPull != null)
                {
                    parentForm.fsMonitorRing[selectedUserProject.ProjectId].Item1.Pause();
                    await PullSelectedFile(FileToPull);
                    parentForm.fsMonitorRing[selectedUserProject.ProjectId].Item1.Continue();
                }
                else if (FileToPush != null)
                {
                    await PushFile(FileToPush);

                }
                else if(revisionToUpdateTo != null)
                {
                    //int i = 0;
                    //GenerateDiffOrBaselineFile();
                }
                await parentForm.UpdateCurrentProject(parentForm.selectedUserProject);
                await CheckForNextAction(parentForm.selectedUserProject);

            }
            catch (Exception ex)
            {
                Log.Error("Error caught on push/pull. Message: " + ex.Message);
                // parentForm.fsMonitorRing[selectedUserProject.ProjectId].Item1.Continue();
            }
        }


        public async Task<bool> InitializeRevisionSystem(UserProject userProject, DesktopClientMain parent)
        {
            parentForm = parent;
            selectedUserProject = userProject;
            await CheckForNextAction(selectedUserProject);
            return true;
        }

        delegate void UpdateCollaborationTabCallback(string message);

        private void UpdateCollaborationTab(string message)
        {
            if (statusShowLbl.InvokeRequired || pushPullActionBtn.InvokeRequired)
            {
                UpdateCollaborationTabCallback d = new UpdateCollaborationTabCallback(UpdateCollaborationTab);
                this.Invoke(d, message);
            }
            else
            {
                string statusTxt = string.Empty;
                string pushBtnTxt = string.Empty;
                bool enabled = false;
                if (FileToPull != null)
                {
                    message = "Ready to pull file " + FileToPull.FullPath + " from RevidX server.";
                    pushBtnTxt = "Pull";
                    enabled = true;
                }
                else if (FileToPush != null)
                {
                    message = "Ready to push file " + FileToPush.FullPath + " to RevidX server.";
                    pushBtnTxt = "Push";
                    enabled = true;
                }
                else if (revisionToUpdateTo != null)
                {
                    message = "Ready to update to latest revision";
                    pushBtnTxt = "Update";
                    enabled = true;
                }    
                else
                {
                    pushBtnTxt = "Action unavailable.";
                    enabled = false;
                }
                statusShowLbl.Text = message;
                pushPullActionBtn.Enabled = enabled;
                pushPullActionBtn.Text = pushBtnTxt;
            }
        }

        private async Task<bool> CheckForNextAction(UserProject userProject)
        {
            FileToPush = null;
            FileToPull = null;
            string message = string.Empty;
            bool retval = false;
            try
            {
                RevidXApiCaller revidXApiCaller = await parentForm.GetRevidXApiCallerAsync();
                var revisionMessageTuple = await revidXApiCaller.GetAvailableRevision(userProject.Id);
                bool checkForPull = false;
                bool checkForPush = false;
                bool responseCancellable = false;
                bool proposalCancellable = false;
                var fileTypesToPush = new List<TransactionFile.TransactionFileType>();
                switch (revisionMessageTuple.Item1)
                {
                    case ServiceClient.RevisionState.NoRevisions:
                        Log.Information("Initial state. Can upload baseline only.");
                        message = "Initial state. Can upload baseline only.";
                        checkForPush = true;
                        fileTypesToPush.Add(TransactionFile.TransactionFileType.BASELINE);
                        break;
                    case ServiceClient.RevisionState.AtLatestState:
                        Log.Information("At latest state. Can upload baseline or proposal.");
                        message = "At latest state. Can upload baseline or proposal.";
                        fileTypesToPush.Add(TransactionFile.TransactionFileType.BASELINE);
                        fileTypesToPush.Add(TransactionFile.TransactionFileType.PROPOSAL);
                        checkForPush = true;
                        break;
                    case ServiceClient.RevisionState.ExpectingResponsePush:
                        Log.Information("At previous state. You are expected to send response.");
                        message = "At previous state. You are expected to send response.";
                        fileTypesToPush.Add(TransactionFile.TransactionFileType.RESPONSE);
                        checkForPush = true;
                        responseCancellable = true;
                        break;
                    case ServiceClient.RevisionState.ProposalAvailable:
                        Log.Information("At previous state. Proposal is available to take.");
                        message = "At previous state. Proposal is available to take.";
                        checkForPull = true;
                        break;
                    case ServiceClient.RevisionState.ResponseAvailable:
                        Log.Information("At previous state. Response is available to take.");
                        message = "At previous state. Response is available to take.";
                        checkForPull = true;
                        break;
                    case ServiceClient.RevisionState.UpdateToLatest:
                        Log.Information("At previous state. Can update to latest state.");
                        message = "At previous state. Can update to latest state.";
                        checkForPull = true;
                        break;
                    case ServiceClient.RevisionState.UpdateToPrevious:
                        Log.Information("At earlier state. Can update to previous state.");
                        message = "At earlier state. Can update to previous state.";
                        checkForPull = true;
                        break;
                    case ServiceClient.RevisionState.AwaitingResponsePush:
                        Log.Information("At previous state. You are awaiting for response push.");
                        message = "At previous state. You are awaiting for response push.";
                        checkForPull = true;
                        break;
                    case ServiceClient.RevisionState.AwaitingProposalPull:
                        Log.Information("At previous state. You are awaiting for proposal pull.");
                        message = "At previous state. You are awaiting for proposal pull.";
                        proposalCancellable = true;
                        break;
                    case ServiceClient.RevisionState.AtPreviousState:
                        Log.Information("At previous state. Can not update to latest state.");
                        message = "At previous state. Can not update to latest state.";
                        break;
                    case ServiceClient.RevisionState.RevisionError:
                        Log.Error("Server returned revision error.");
                        message = "Server returned revision error.";
                        throw new Exception("Server returned revision error.");
                    default:
                        Log.Error("State not supported: " + revisionMessageTuple.Item1);
                        message = "State not supported: " + revisionMessageTuple.Item1;
                        throw new Exception("Server returned revision error.");
                }
                if (revisionMessageTuple.Item2 == null)
                {
                    if (checkForPull || !checkForPush)
                    {
                        Log.Information("Server doesn't accept push, yet nothing to pull.");
                        //message = "Fatal error. Server doesn't accept push, yet nothing to pull.";
                        //throw new Exception("Fatal error. Server doesn't accept push, yet nothing to pull.");
                    }
                    else
                    {
                        Log.Information("No available revisions.");
                        RevisionManager revisionManager = new RevisionManager(revidXApiCaller);
                        FileToPush = await revisionManager.GetTransactionFileToPush(userProject, fileTypesToPush);
                    }
                }
                else
                {
                    if (checkForPush || !checkForPull)
                    {
                        Log.Error("Server awaits push, yet pull available.");
                        message = "Fatal error. Server awaits push, yet pull available.";
                        throw new Exception("Fatal error. Server awaits push, yet pull available.");
                    }
                    else
                    {
                        var revision = revisionMessageTuple.Item2;
                        if (revision.ResponseFile != null && revisionMessageTuple.Item1 == ServiceClient.RevisionState.ResponseAvailable)
                        {
                            FileToPull = revision.ResponseFile;
                        }
                        else if (revision.BaselineFile != null)
                        {
                            FileToPull = revision.BaselineFile;
                        }
                        else if (revision.ProposalFile != null)
                        {
                            FileToPull = revision.ProposalFile;
                        }
                        else
                        {
                            message = "Fatal error. No file in revision. " + revisionMessageTuple;
                            throw new Exception("Revision available but no file");
                        }
                    }
                }
                cancelLastActionBtn.Enabled = false;
                if (responseCancellable)
                {
                    cancelLastActionBtn.Enabled = true;
                }
                else if (proposalCancellable)
                {
                    cancelLastActionBtn.Enabled = true;
                }
                retval = true;
            }
            catch (Exception ex)
            {
                Log.Error("Error caught on checking state. Message: " + ex.Message);
            }
            UpdateCollaborationTab(message);
            return retval;
        }

        private async Task<bool> PushFile(TransactionFile transactionFile)
        {
            parentForm.IncreaseProgress(0, 20);
            if (parentForm.selectedUserProject == null)
            {
                Log.Fatal("Selected user project is null.");
                throw new Exception("Selected user project is null.");
            }
            Revision revision = null;
            RevidXApiCaller revidXApiCaller = await parentForm.GetRevidXApiCallerAsync();
            switch (transactionFile.TransactionFileTypeStr)
            {
                case "RESPONSE":
                    revision = selectedUserProject.Project.LatestRevision;
                    revision.ResponseFile = transactionFile;
                    revision.ModificationTime = DateTime.UtcNow;
                    revision.PreviousRevision = null;
                    revision.Project = null;
                    revision.AwaitedByUser = null;
                    revision.Description = transactionFile.Description;
                    //  actually will save response without baseline file - baseline for user will be generated on demand
                    ///generate baseline file beforehand 
                    var baselineFile = await revidXApiCaller.GetTransactionFile(selectedUserProject.CurrentRevision.BaselineFileId);
                    if (baselineFile == null)
                    {
                        Log.Fatal("Latest baseline file could not be retrieved");
                        throw new Exception("Latest baseline file could not be retrieved");
                    }
                    using (MemoryStream responseStream = new MemoryStream(await FileUtils.GetIDXFromContentAsync(transactionFile.BinaryContent)))
                    {
                        EDMDDataSet responseDataSet = null;
                        EDMDReader responseReader = new EDMDReader(responseStream);
                        responseDataSet = responseReader.DeserializeEDMDFile();
                        if (responseDataSet == null)
                        {
                            Log.Fatal("Response not valid for constructing.");
                            throw new Exception("Response not valid for constructing.");
                        }

                        using (MemoryStream stream = new MemoryStream(await FileUtils.GetIDXFromContentAsync(baselineFile.BinaryContent)))
                        {
                            EDMDReader edmdReader = new EDMDReader(stream);
                            var retDataset = edmdReader.ApplyResponse(responseDataSet);
                            if (retDataset == null)
                            {
                                Log.Fatal("Response could not be applied to given baseline.");
                                throw new Exception("Response could not be applied to given baseline.");
                            }
                            var genBaselineBinary = EDMDReader.OutputToStream(retDataset).ToArray();
                            if (genBaselineBinary == null)
                            {
                                Log.Fatal("Could not create stream from generated baseline.");
                                throw new Exception("Could not create stream from generated baseline.");
                            }
                            var genBaselinePCB = EDMDReader.ConstructPCBFromFile(retDataset);
                            if (genBaselinePCB == null)
                            {
                                Log.Fatal("Could not create pcb from generated baseline.");
                                throw new Exception("Could not create pcb from generated baseline.");
                            }
                            revision.BaselineFileId = null;
                            revision.BaselineFile = new TransactionFile();
                            revision.BaselineFile.LastAccessTime = DateTime.UtcNow;
                            revision.BaselineFile.LastWriteTime = DateTime.UtcNow;
                            revision.BaselineFile.FileName = revision.BaselineFile.FullPath = EdmdRevisionSystem.GenerateFileName(TransactionFile.TransactionFileType.BASELINE.ToString());
                            var zipStream = await FileUtils.AddToArchiveAsync(genBaselineBinary,
                                revision.BaselineFile.FullPath);
                            revision.BaselineFile.BinaryContent = new BinaryContent(zipStream.ToArray(), FILE_TYPE.IDZ.ToString());
                            revision.BaselineFile.TransactionFileTypeStr = TransactionFile.TransactionFileType.BASELINE.ToString();
                        }
                    }
                    break;
                case "PROPOSAL":
                    revision = new Revision();
                    revision.ProposalFile = transactionFile;
                    revision.CreationTime = DateTime.UtcNow;
                    revision.ModificationTime = DateTime.UtcNow;
                    revision.PreviousRevisionId = selectedUserProject.Project.LatestRevisionId;
                    revision.Description = transactionFile.Description;
                    break;
                case "BASELINE":
                    revision = new Revision();
                    revision.BaselineFile = transactionFile;
                    revision.CreationTime = DateTime.UtcNow;
                    revision.ModificationTime = DateTime.UtcNow;
                    revision.Description = transactionFile.Description;
                    if (selectedUserProject.CurrentRevision != null)
                    {
                        revision.PreviousRevisionId = selectedUserProject.Project.LatestRevisionId;
                    }
                    break;
                default:
                    Log.Error("File type to upload is UNKNOWN.");
                    return false;
            }
            revision.ProjectId = selectedUserProject.ProjectId;
            revision.ModificationTime = DateTime.UtcNow;
            parentForm.IncreaseProgress(40);
            revision = await revidXApiCaller.PushRevision(revision, selectedUserProject.Id);
            if (revision == null)
            {
                throw new Exception("Creating transaction for selected file failed: " + transactionFile.FullPath);
            }
            Message message = new Message();
            message.SenderId = parentForm.UserProfile.Id;
            message.Sender = parentForm.UserProfile;
            message.ProjectId = selectedUserProject.ProjectId;
            message.SendTimeUtc = DateTime.UtcNow;
            message.Text = "Pushed new file to RevidX Server."; 
            await parentForm.UpdateHubConnection();
            await parentForm.hubConnection.SendAsync("FileUploadedToProject", message, false);
            parentForm.IncreaseProgress(70);
            await parentForm.UpdateCurrentProject(parentForm.selectedUserProject);
            parentForm.IncreaseProgress(100);
            parentForm.IncreaseProgress(0);
            return true;
        }



        private async Task<bool> PullSelectedFile(TransactionFile transactionFile)
        {
            if (transactionFile == null)
            {
                throw new Exception("Data of selected item is invalid.");
            }
            if (transactionFile.TransactionFileTypeStr == TransactionFile.TransactionFileType.UNKNOWN.ToString())
            {
                throw new Exception("File type is unknown. Not allowed for use in collaboration");
            }

            parentForm.IncreaseProgress(10, 20);
            RevidXApiCaller revidXApiCaller = await parentForm.GetRevidXApiCallerAsync();
            TransactionFile transactionFileWithContent = await revidXApiCaller.PullTransactionFile(transactionFile.Id, parentForm.selectedUserProject.Id);

            if (transactionFileWithContent == null)
            {
                return false;
            }
            parentForm.IncreaseProgress(0);
            parentForm.fsMonitorRing[parentForm.selectedUserProject.ProjectId].Item1.Pause();
            var stream = new MemoryStream(await FileUtils.GetIDXFromContentAsync(transactionFileWithContent.BinaryContent));
            var path = Path.GetFileName(transactionFileWithContent.FileName);
            var fullPath = Path.Combine(parentForm.selectedUserProject.MonitoredPath, path);
            //if (!Desktop.Properties.Settings.Default.KeepIDZFilePull && transactionFileWithContent.BinaryContent.FileType == BinaryContent.FILE_TYPE.IDZ.ToString())
            //{
            //    stream = FileUtils.UnpackArchive(stream);
            //    Path.ChangeExtension(fullPath, "idx");
            //}
            if (File.Exists(fullPath))
            {
                var result = MessageBox.Show("Warning", String.Format("Overwrite local file with the same name {0}?", Path.GetFileName(fullPath)), MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    return false;
                }
                File.Delete(fullPath);
            }
            FileStream fileStream = new FileStream(fullPath, FileMode.Create, System.IO.FileAccess.Write);
            stream.CopyTo(fileStream);
            fileStream.Flush();
            fileStream.Close();
            parentForm.fsMonitorRing[parentForm.selectedUserProject.ProjectId].Item1.Continue();
            parentForm.IncreaseProgress(95);
            await parentForm.UpdateCurrentProject(parentForm.selectedUserProject);

            Message message = new Message();
            message.SenderId = parentForm.UserProfile.Id;
            message.Sender = parentForm.UserProfile;
            message.ProjectId = selectedUserProject.ProjectId;
            message.SendTimeUtc = DateTime.UtcNow;
            message.Text = "Pulled file from server."; 
            await parentForm.UpdateHubConnection();
                await parentForm.hubConnection.SendAsync("FilePulled", message);

            parentForm.IncreaseProgress(0);
            return true;
        }

        internal void CheckForRevisionActions(string filename)
        {
            //detected filename push trigger checks

        }

        private async void cancelLastActionBtn_Click(object sender, EventArgs e)
        {
            try
            {
                parentForm.IncreaseProgress(0);
                RevidXApiCaller revidXApiCaller = await parentForm.GetRevidXApiCallerAsync();
                var result = await revidXApiCaller.CancelRevision(parentForm.selectedUserProject.Id);
                if (!result)
                {
                    throw new Exception("Cancelling revision call did not succeed.");
                }
                parentForm.IncreaseProgress(40);
                Message message = new Message();
                message.SenderId = parentForm.UserProfile.Id;
                message.ProjectId = selectedUserProject.ProjectId;
                message.SendTimeUtc = DateTime.UtcNow;
                message.Text = "Cancelled latest action."; 
                await parentForm.UpdateHubConnection();
                await parentForm.hubConnection.SendAsync("CancelledAction", message, false);
                selectedUserProject = await revidXApiCaller.GetUserProject(selectedUserProject.Id);
                parentForm.IncreaseProgress(90);
                await CheckForNextAction(parentForm.selectedUserProject);
                parentForm.IncreaseProgress(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

    }
}
