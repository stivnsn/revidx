﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevidX.Desktop
{
    public class HelperUtilities
    {


        public static bool IsProjectPathValid(string path)
        {
            return !string.IsNullOrEmpty(path)
                && Directory.Exists(path);
        }

        public static bool IsPointingToSameLoc(string path1, string path2)
        {
            // outputs true
            if(string.IsNullOrWhiteSpace(path1) || string.IsNullOrWhiteSpace(path2))
            {
                return false;
            }
            return string.Equals(Path.GetFullPath(path1), Path.GetFullPath(path2), StringComparison.OrdinalIgnoreCase);

        }
    }
}
