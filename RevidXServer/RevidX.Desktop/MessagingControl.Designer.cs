﻿namespace RevidX.Desktop
{
    partial class MessagingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.messageTxt = new System.Windows.Forms.TextBox();
            this.SendMessageBackBtn = new System.Windows.Forms.Button();
            this.mainStatusTxt = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.messageTxt);
            this.groupBox1.Controls.Add(this.SendMessageBackBtn);
            this.groupBox1.Controls.Add(this.mainStatusTxt);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(358, 458);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Messaging";
            // 
            // messageTxt
            // 
            this.messageTxt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.messageTxt.Location = new System.Drawing.Point(5, 430);
            this.messageTxt.Margin = new System.Windows.Forms.Padding(2);
            this.messageTxt.Name = "messageTxt";
            this.messageTxt.Size = new System.Drawing.Size(238, 23);
            this.messageTxt.TabIndex = 2;
            this.messageTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.messageTxt_KeyDown);
            // 
            // SendMessageBackBtn
            // 
            this.SendMessageBackBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SendMessageBackBtn.Location = new System.Drawing.Point(247, 430);
            this.SendMessageBackBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SendMessageBackBtn.Name = "SendMessageBackBtn";
            this.SendMessageBackBtn.Size = new System.Drawing.Size(106, 23);
            this.SendMessageBackBtn.TabIndex = 3;
            this.SendMessageBackBtn.Text = "Send Message Back";
            this.SendMessageBackBtn.UseVisualStyleBackColor = true;
            this.SendMessageBackBtn.Click += new System.EventHandler(this.SendMessageBackBtn_Click);
            // 
            // mainStatusTxt
            // 
            this.mainStatusTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainStatusTxt.Location = new System.Drawing.Point(5, 21);
            this.mainStatusTxt.Margin = new System.Windows.Forms.Padding(2);
            this.mainStatusTxt.Name = "mainStatusTxt";
            this.mainStatusTxt.Size = new System.Drawing.Size(348, 405);
            this.mainStatusTxt.TabIndex = 1;
            this.mainStatusTxt.Text = "";
            // 
            // MessagingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "MessagingControl";
            this.Size = new System.Drawing.Size(364, 464);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox messageTxt;
        private System.Windows.Forms.Button SendMessageBackBtn;
        private System.Windows.Forms.RichTextBox mainStatusTxt;
    }
}
