﻿using RevidX.ServiceClient.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RevidX.Desktop
{
    public partial class ProjectSettings : Form
    {
        private UserProject selectedUserProject;

        public string tempMonitoredPath { get; set; }
        public ProjectSettings()
        {
            InitializeComponent();
        }

        public ProjectSettings(UserProject selectedUserProject)
        {
            InitializeComponent();
            this.selectedUserProject = selectedUserProject;
            if(!string.IsNullOrWhiteSpace(selectedUserProject.MonitoredPath))
            {
                tempMonitoredPath = monitoredPathTxt.Text = selectedUserProject.MonitoredPath;
            }
        }

        private void openMonitoredPath_Click(object sender, EventArgs e)
        {
            if (tempMonitoredPath != null && Directory.Exists(tempMonitoredPath))
            {
                Process.Start("explorer.exe", tempMonitoredPath);
            }
            else
            {

                Log.Error("Path does not exist or not initialized: " + tempMonitoredPath);
                MessageBox.Show("Path does not exist. ");
            }
        }

        private void selectPathBtn_Click(object sender, EventArgs e)
        {
            try
            {
                selectFolderDlg.ShowDialog();
                var selectedPath = selectFolderDlg.SelectedPath;
                if (selectedPath == null || selectedPath.Length == 0)
                {
                    return;
                }
                if (!HelperUtilities.IsProjectPathValid(selectedPath))
                {
                    throw new Exception("Selected path is not valid: " + selectedPath);
                }
                tempMonitoredPath = selectedPath;
                monitoredPathTxt.Text = selectedPath;

            }
            catch (Exception ex)
            {
                Log.Error("Selecting path failed! " + ex.Message);
                MessageBox.Show("Selecting path failed! " + ex.Message);
            }
        }


        private void okBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
