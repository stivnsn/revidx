﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCBXRevision
{

    public enum BSplineCurveForm
    {

        /// <remarks/>
        PolylineForm,

        /// <remarks/>
        CircularArc,

        /// <remarks/>
        EllipticArc,

        /// <remarks/>
        ParabolicArc,

        /// <remarks/>
        HyperbolicArc,

        /// <remarks/>
        Unspecified,
    }

    public class Curve
    {
        public double Thickness { get; set; }
    }
    public class CompositeCurve : Curve
    {
        public List<Curve> Curves { get; set; }
    }
    public class Line : Curve
    {
        public Point3 Point { get; set; }
        public Point3 Vector { get; set; }

    }
    public class Polyline : Curve
    {
        public List<Point3> Points;
    }
    public class Arc : Curve
    {
        public double Angle { get; set; }
        public Point3 StartPoint { get; set; }
        public Point3 EndPoint { get; set; }
    }
    public class OffsetCurve : Curve
    {
        public Curve BasisCurve { get; set; }
        public double distance { get; set; }
        public IEnumerable<Curve> Curves { get; set; }
        public bool? SelfIntersect { get; set; }
    }
    public class TrimmedCurve : Curve
    {
        public List<Curve> Curves { get; set; }
        public Point3 StartPoint { get; set; }
        public Point3 EndPoint { get; set; }
    }
    public class Hyperbola : Curve
    {
        public Point3 Center { get; set; }
        public double SemiMajor { get; set; }
        public double SemiMinor { get; set; }
    }

    public class Parabola : Curve
    {
        public List<Point3> Focus { get; set; }
        public List<Point3> Apex { get; set; }
    }
    public class Circle3Point : Curve
    {
        public Point3 Point1 { get; set; }
        public Point3 Point2 { get; set; }
        public Point3 Point3 { get; set; }
    }
    public class
        CircleCenter : Curve
    {
        public Point3 Center { get; set; }
        public double Diameter { get; set; }
    }
    public class Ellipse : Curve
    {
        public Point3 Center { get; set; }
        public double SemiMajor { get; set; }
        public double SemiMinor { get; set; }
    }
    public class BSplineCurve : Curve
    {
        public BSplineCurveForm? BSplineCurveForm { get; set; }
        public bool? Closed { get; set; }
        public bool? Intersect { get; set; }
        public List<Point3> Points { get; set; }
        public double Degree { get; set; }
    }

    public class CurveSet
    {
        public List<Curve> Curves { get; set; } = new
            List<Curve>();
        public ShapeDescriptionType ShapeDescriptionType { get; set; }
        public ShapeElementType? ShapeElementType { get; set; }
        public double LowerBound { get; set; }
        public double UpperBound { get; set; }
        public bool IsInverted { get; set; }
    }
}
