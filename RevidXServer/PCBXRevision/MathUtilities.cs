﻿using PCBXRevision;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace PCBXRevision
{
    public class MathUtilities
    {

        public const double DISTANCE_THRESHOLD = 1e-5;

        public static (double, double) lineMiddlePoint (double x1, double y1, double x3, double y3) => ((x1 + x3) / 2, (y1 + y3) / 2);
        public static double lineLength(double x1, double y1, double x2, double y2) => Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        public static double DegToRad(double degAngle) => degAngle * Math.PI / 180;
        public static double RadToDeg(double radAngle) => radAngle * 180 / Math.PI;
        public static bool IsZero(double value) => Math.Abs(value) < DISTANCE_THRESHOLD;

        public static (double, double) GetMidPointFromTwoPointAngleArc(Segment segment)
        {
            double x2 = 0, y2 = 0;
            double x1 = segment.StartPoint.X;
            double y1 = segment.StartPoint.Y;
            double x3 = segment.EndPoint.X;
            double y3 = segment.EndPoint.Y;
            var angleRad = DegToRad(segment.AngleDeg);
            (double, double) halfwayPoint = lineMiddlePoint(x1, y1, x3, y3);
            double halfLength = lineLength(x1, y1, halfwayPoint.Item1, halfwayPoint.Item2);
            double sinAngle = Math.Sin(angleRad / 2);
            if (IsZero(sinAngle))
            {
                throw new DivideByZeroException();
            }
            double radius = Math.Abs(halfLength / sinAngle);
            double halfwayPointRadiusLength = Math.Sqrt(Math.Pow(radius, 2) - Math.Pow(halfLength, 2));
            double middleX = halfwayPoint.Item1 - x1;
            double middleY = halfwayPoint.Item2 - y1;
            double midAlpha = Math.Atan2(middleY, middleX);
            double midBeta = midAlpha + (angleRad < 0 ? Math.PI / 2 * (-1) : Math.PI / 2);
            double centerX = halfwayPointRadiusLength * Math.Cos(midBeta);
            double centerY = halfwayPointRadiusLength * Math.Sin(midBeta);
            centerX += middleX;
            centerY += middleY;
            centerX += x1;
            centerY += y1;

            double middleAngle = Math.Atan2(halfwayPoint.Item2 - centerY, halfwayPoint.Item1 - centerX);
            x2 = centerX + radius * Math.Cos(middleAngle);
            y2 = centerY + radius * Math.Sin(middleAngle);
            return (x2, y2);
        }
    }
}
