﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace PCBXRevision
{
    public class RxRevBase
    { }
    public class RxRevDiff
    {
        public PCBContainer PCBContainer;
    }


    public class PCBContainerUtils
    {

        public static PCBContainer CompileDiffsToSingle(List<PCBContainer> deserializedContainers)
        {
            var singleDiff = new PCBContainer();
            for (int i = deserializedContainers.Count - 1; i < 0; --i)
            {
                var deserializedContainer = deserializedContainers[i];
                if (deserializedContainer.Board != null && singleDiff.Board == null)
                {
                    if (singleDiff.Board.ObjectData.IsAccepted)
                    {
                        singleDiff.Board = deserializedContainer.Board;
                    }
                }
                foreach (var component in deserializedContainer.Components)
                {
                    if (!singleDiff.Components.ContainsKey(component.Key))
                    {
                        if (singleDiff.Components[component.Key].ObjectData.IsAccepted)
                        {
                            singleDiff.Components[component.Key] = component.Value;
                        }
                    }
                }
                foreach (var area in deserializedContainer.ConstraintAreas)
                {
                    if (!singleDiff.ConstraintAreas.ContainsKey(area.Key))
                    {
                        if (singleDiff.ConstraintAreas[area.Key].ObjectData.IsAccepted)
                        {
                            singleDiff.ConstraintAreas[area.Key] = area.Value;
                        }
                    }
                }
                foreach (var hole in deserializedContainer.Holes)
                {
                    if (!singleDiff.Holes.ContainsKey(hole.Key))
                    {
                        if (singleDiff.ConstraintAreas[hole.Key].ObjectData.IsAccepted)
                        {
                            singleDiff.Holes[hole.Key] = hole.Value;
                        }
                    }
                }
                foreach (var cutout in deserializedContainer.Cutouts)
                {
                    if (!singleDiff.Cutouts.ContainsKey(cutout.Key))
                    {
                        if (singleDiff.ConstraintAreas[cutout.Key].ObjectData.IsAccepted)
                        {
                            singleDiff.Cutouts[cutout.Key] = cutout.Value;
                        }
                    }
                }
                foreach (var layer in deserializedContainer.Layers)
                {
                    if (!singleDiff.Layers.ContainsKey(layer.Key))
                    {
                        singleDiff.Layers[layer.Key] = layer.Value;
                    }
                }
                foreach (var layer in deserializedContainer.LayerStackups)
                {
                    if (!singleDiff.LayerStackups.ContainsKey(layer.Key))
                    {
                        singleDiff.LayerStackups[layer.Key] = layer.Value;
                    }
                }
            }
            return singleDiff;
        }
    }

    [Guid("21451781-DAE7-4DEF-8C61-70A3E44E3AF0")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    public class PCBContainer
    {

        public const double DISTANCE_THRESHOLD = 0.00000001;
        public PCBContainer()
        {
            Header = new Header();
            Components = new Dictionary<Identifier, Component>();
            ConstraintAreas = new Dictionary<Identifier, ConstraintArea>();
            Holes = new Dictionary<Identifier, Hole>();
            Cutouts = new Dictionary<Identifier, Cutout>();
            LayerStackups = new Dictionary<string, LayerStackup>();
            Layers = new Dictionary<string, Layer>();
            ProcessInstructions = new List<ProcessInstruction>();
        }
        public QuickPCBStats QuickPCBStats { get; set; } = new QuickPCBStats();

        public SendType SendType { get; set; }

        public Header Header { get; set; }
        public List<ProcessInstruction> ProcessInstructions { get; set; }

        public Board Board { get; set; }

        public Dictionary<Identifier, Component> Components { get; set; }

        public Dictionary<Identifier, ConstraintArea> ConstraintAreas { get; set; }

        public Dictionary<Identifier, Hole> Holes { get; set; }

        public Dictionary<Identifier, Cutout> Cutouts { get; set; }


        public Dictionary<string, LayerStackup> LayerStackups { get; set; }
        public Dictionary<string, Layer> Layers { get; set; }
    }

    [Guid("C22C4C41-AEEF-4384-84D9-93A78360CCE7")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    public class QuickPCBStats
    {
        public string GetNrOfChangesString()
        {
            var tempString = string.Empty;
            if (ObjectsAddedCount > 0)
            {
                tempString += string.Format("{0} addition", ObjectsAddedCount);
            }
            if (ObjectsDeletedCount > 0)
            {
                tempString += string.Format("{0} deletion", ObjectsDeletedCount);
            }
            if (ObjectsModifiedCount > 0)
            {
                tempString += string.Format("{0} modification", ObjectsModifiedCount);
            }
            return tempString;
        }

        public string GetNrOfChangesAcceptedString()
        {
            return string.Format("{0} of {1} changes accepted", ProposedObjectsAccepted, ProposedObjectsTotal);
        }

        public string GetNrOfObjectTypesString()
        {
            var tempString = string.Empty;
            if (ComponentsCount > 0)
            {
                tempString += string.Format(" {0} components", ComponentsCount);
            }
            if (AreasCount > 0)
            {
                tempString += string.Format(" {0} areas", AreasCount);
            }
            if (HolesCount > 0)
            {
                tempString += string.Format(" {0} holes", HolesCount);
            }
            if (LayersCount > 0)
            {
                tempString += string.Format(" {0} layers", LayersCount);
            }
            if (BoardsCount > 0)
            {
                tempString += string.Format(" {0} board", BoardsCount);
            }
            if (!string.IsNullOrEmpty(tempString))
            {
                tempString = string.Format("Contains{0}.", tempString);
            }
            return tempString;
        }

        public int ProposedObjectsTotal { get; set; } = 0;
        public int ProposedObjectsAccepted { get; set; } = 0;
        public int ObjectsAddedCount { get; set; } = 0;
        public int ObjectsDeletedCount { get; set; } = 0;
        public int ObjectsModifiedCount { get; set; } = 0;
        public int AreasCount { get; set; } = 0;
        public int ComponentsCount { get; set; } = 0;
        public int HolesCount { get; set; } = 0;
        public int LayersCount { get; set; } = 0;
        public int BoardsCount { get; set; } = 0;
        public int CutoutCount { get; set; } = 0;
    }

    public enum SendType
    {
        BASELINE,
        PROPOSAL,
        RESPONSE,
        DELTA,
        UNKNOWN
    }

    public enum ChangeType
    {
        ADDITION,
        DELETION,
        MODIFICATION
    }

    public enum ItemSide
    {
        TOP,
        BOTTOM,
        ALL,
        BOTH,
        NONE
    }

    public enum HoleType
    {
        Via,
        MountingHole,
        Pin,
        Unknwon
    }


    public enum InterStratumType
    {
        Cutout,
        CutoutInterStratumTypeCutout,
        CutoutEdgeSegment,
        MilledCutout,
        PlatedCutout,
        PartiallyPlatedCutout,
        PlatedCutoutEdgeSegment,
        PhysicalConnectivityInterruptingCutout,
        Unknown,
        CustomElectroMechanicalComponent,
        MechanicalComponent,
        StratumFeatureTemplateComponent,
        UnsupportedPassage,
        PlatedInterStratumFeature,
        ComponentTerminationPassage,
        FilledVia,
        Via
    }

    public enum ConstraintAreaType
    {
        Undefined,
        PlaceKeepout,
        RouteKeepout,
        PlaneKeepout,
        ViaKeepout,
        PlaceOutline,
        RouteOutline,
        OtherOutline,
        PlacementRegion,
        Stiffener,
        FlexibleArea
    }

    public enum ComponentType
    {
        Electrical,
        Mechanical
    }


    public enum StratumType
    {
        DESIGN,
        DOCUMENT
    }
    public enum SurfaceDesignationType
    {
        PRIMARY,
        SECONDARY,
        AVERAGE
    }

    public enum AssemblyComponentType
    {
        UNKNOWN,
        ASSEMBLYGROUP,
        LAMINATE,
        THERMAL,
        PHYSICAL,
        PADSTACK
    }

    public enum ShapeDescriptionType
    {
        External,
        Regular
    }

    public enum ShapeElementType
    {
        NonFeatureShapeElement,
        FeatureShapeElement,
        PartFeature,
        PartMountingFeature,
        ComponentTerminationPassageTemplateTerminal,
        ComponentTerminationPassageTemplateInterfaceTerminal,
        ComponentTerminationPassageTemplateJoinTerminal,
        PrintedPartCrossSectionTemplateTerminal,
        PrintedPartTemplateInterfaceTerminal,
        PrintedPartTemplateJoinTerminal,
        StructuredPrintedPartTemplateTerminal,
        PrintedPartTemplateTerminal,
        PartToolingFeature,
        FiducialPartFeature,
        ThermalFeature,
        TestPointPartFeature,
        ConnectionZone,
        ComponentTerminal,
        BareDieComponentTerminal,
        Land,
        Package,
        PackageTerminal,
        ViaTerminal,
    }

    public class GeometricalShape : ObjectData
    {
        public GeometricalShape(ObjectData objectData)
        {
            ObjectData = ObjectData;
            InnerOutlines = new List<Outline>();
        }

        public ObjectData ObjectData { get; set; }

        public List<CurveSet> CurveSets { get; set; }
        public Outline Outline { get; set; }
        public List<Outline> InnerOutlines { get; set; }
    }

    public class Identifier : IEquatable<Identifier>
    {
        public Identifier(string systemScope, string objectName)
        {
            SystemScope = systemScope;
            ObjectName = objectName;
        }
        public string SystemScope { get; set; }
        public string ObjectName { get; set; }

        public string GetNameCombined() => String.Format("{0}|{1}", SystemScope, ObjectName);
        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Identifier))
            {
                return false;
            }
            return Equals(obj as Identifier);
        }
        public bool Equals(Identifier identifier)
        {
            return this == identifier;
        }
        public override int GetHashCode()
        {
            //unchecked // Overflow is fine, just wrap
            //{
            //    int hash = (int)2166136261;
            //    // Suitable nullity checks etc, of course :)
            //    hash = (hash * 16777619) ^ ObjectName.GetHashCode();
            //    hash = (hash * 16777619) ^ SystemScope.GetHashCode();
            //    return hash;
            //}
            return Tuple.Create(SystemScope, ObjectName).GetHashCode();
        }
        public static bool operator ==(Identifier x, Identifier y)
        {
            if (object.ReferenceEquals(null, x))
                return object.ReferenceEquals(null, y);
            return x.SystemScope == y.SystemScope && x.ObjectName == y.ObjectName;
        }

        public static bool operator !=(Identifier x, Identifier y)
        {
            return !(x == y);
        }
    }
    public class VersionNumber
    {
        public VersionNumber(Identifier identifier, int version, int revision, int sequence)
        {
            Version = version;
            Revision = revision;
            Sequence = sequence;
            Identifier = identifier;
        }
        /// <summary>
        /// Identifier that appears in revision number
        /// </summary>
        public Identifier Identifier { get; set; }

        public int Version { get; }
        public int Revision { get; }
        public int Sequence { get; }
    }

    public class ObjectData
    {
        public ObjectData()
        {
        }

        public VersionNumber VersionNumber { get; set; }

        public Identifier Identifier { get; set; }




        public bool? Accepted { get; set; }
        public bool IsAccepted
        {
            get
            {
                return Accepted != null && Accepted == true;
            }
            set
            {
                Accepted = value;
            }
        }


        public bool IsAcceptedOrNull
        {
            get
            {
                return Accepted == null || Accepted == true;
            }
        }


        public Dictionary<string, string> UserProperties { get; set; } = new Dictionary<string, string>();

        public ChangeType? ChangeType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string[] DisplayDescription { get; set; }
        public string[] DisplayName { get; set; }
        public string AssembleToName { get; set; }
    }

    public class Header
    {
        public string id;

        public string Creator { get; set; }
        public string CreatorName { get; set; }
        public string Description { get; set; }
        public string[] DisplayDescription { get; set; }
        public string[] DisplayName { get; set; }
        public string GlobalUnitAmperage { get; set; }
        public string GlobalUnitAngle { get; set; }
        public string GlobalUnitCapacitance { get; set; }
        public string GlobalUnitLength { get; set; }
        public string GlobalUnitPower { get; set; }
        public string GlobalUnitResistance { get; set; }
        public string GlobalUnitVoltage { get; set; }
        public string GlobalUnitWeight { get; set; }
        public string GlobalUnitWork { get; set; }
        public bool IsAttributeChanged { get; set; }
        public bool IsAttributeChangedSpecified { get; set; }
        public bool? IsSelfContained { get; set; }
        public bool IsSelfContainedSpecified { get; set; }
        public string Name { get; set; }
        public string PostProcessor { get; set; }
        public string PostProcessorVersion { get; set; }
        public string System { get; set; }
        public string[] Text { get; set; }
        public Dictionary<string, string> UserProperty { get; set; }
        public string CreatorSystem { get; set; }
        public string CreatorCompany { get; set; }
    }

    public class ProcessInstruction
    {
        public string id;

        public string Description { get; set; }
        public string[] DisplayDescription { get; set; }
        public string[] DisplayName { get; set; }
        public bool IsAttributeChanged { get; set; }
        public string Name { get; set; }
    }

    public class LayerStackup
    {
        public LayerStackup()
        {
            Layers = new List<string>();
        }
        public string Name { get; set; }

        public List<string> Layers { get; set; }
    }

    public class Layer
    {
        public Layer(ObjectData objectData)
        {
            ObjectData = objectData;
        }
        public ObjectData ObjectData { get; set; }
    }

    public class Board
    {
        public StratumType StratumType { get; set; }
        public SurfaceDesignationType? SurfaceDesignationType { get; set; }
        public Board(GeometricalShape geometricalShape, ObjectData objectData)
        {
            GeometricalShape = geometricalShape;
            ObjectData = objectData;
        }
        public ObjectData ObjectData { get; set; }
        public GeometricalShape GeometricalShape { get; set; }
        public double Thickness { get; set; }
    }

    public class Shape
    {
        public Shape()
        {
            Outline = new Outline();
            InnerOutlines = new List<Outline>();
        }
        public Outline Outline { get; set; }
        public List<Outline> InnerOutlines { get; set; }
    }

    public class ConstraintArea
    {
        public ConstraintArea(GeometricalShape geometricalShape, ObjectData objectData)
        {
            GeometricalShape = geometricalShape;
            ObjectData = objectData;
        }
        public ObjectData ObjectData { get; set; }
        public GeometricalShape GeometricalShape { get; set; }
        public ItemSide Side { get; set; }
        public ConstraintAreaType Purpose { get; set; }
    }

    public class Hole
    {
        public Hole(GeometricalShape geometricalShape, ObjectData objectData)
        {
            GeometricalShape = geometricalShape;
            ObjectData = objectData;
        }

        public InterStratumType HoleInterStratumType { get; set; }

        public ObjectData ObjectData { get; set; }
        public GeometricalShape GeometricalShape { get; set; }
        public string MHName { get; set; }
        public string Padstack { get; set; }
        public string InstanceName { get; set; }
        public HoleType HoleType { get; set; }
    }

    public class Component
    {
        public Component(GeometricalShape geometricalShape, ObjectData objectData)
        {
            GeometricalShape = geometricalShape;
            ObjectData = objectData;
        }
        public ObjectData ObjectData { get; set; }
        public GeometricalShape GeometricalShape { get; set; }
        public ComponentType Type { get; set; }
        public ItemSide Side { get; set; }
        public string RefDes { get; set; }
        public string PartNumber { get; set; }
        public string PackageName { get; set; }
        public string ComponentName
        {
            get { return RefDes + "|" + PartNumber + "|" + PackageName; }
        }

        public AssemblyComponentType? AssemblyComponentType { get; set; }
    }

    public class Cutout
    {
        public Cutout(GeometricalShape geometricalShape, ObjectData objectData)
        {
            GeometricalShape = geometricalShape;
            ObjectData = objectData;
        }
        public ObjectData ObjectData { get; set; }
        public GeometricalShape GeometricalShape { get; set; }
        public InterStratumType CutoutInterStratumType { get; set; }
    }

    public class Point3
    {
        public Point3()
        {
            X = Y = Z = 0;

        }

        public Point3(Point3 point)
        {
            X = point.X;
            Y = point.Y;
            Z = point.Z;
        }

        public Point3(double x, double y, double z = 0)
        {
            X = x;
            Y = y;
            Z = z;
        }
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }

    public class Segment
    {
        public Segment()
        {
            StartPoint = new Point3();
            EndPoint = new Point3();
        }

        public Segment(Point3 startPoint, Point3 endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
        }

        public Segment(Point3 startPoint, Point3 endPoint, double angleDeg)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
            AngleDeg = angleDeg;
            var result = MathUtilities.GetMidPointFromTwoPointAngleArc(this);
            MidPoint = new Point3(result.Item1, result.Item2);
        }

        public Segment(Point3 startPoint, Point3 midPoint, Point3 endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
            MidPoint = midPoint;
        }

        public Point3 StartPoint { get; set; }
        public Point3 MidPoint { get; set; }
        public Point3 EndPoint { get; set; }
        public double AngleDeg { get; set; }
    }

    public class Outline
    {
        public Outline()
        {
            Transformation = new Transformation();
            Segments = new List<Segment>();
            UpperBound = LowerBound = 0;
        }


        private Transformation transformation;
        public Transformation Transformation
        {
            get
            {
                return transformation;
            }
            set
            {
                transformation = value;
            }
        }
        public List<Segment> Segments { get; set; }
        public double UpperBound { get; set; }
        public double LowerBound { get; set; }

        public double ThicknessCalculated { get { return Math.Abs(UpperBound - LowerBound); } }
    }

    public class Transformation
    {

        public TransformationMatrix TransformationMatrix { get; set; }

        public Transformation()
        { }

        public bool HasTransformation()
        {
            return Math.Abs(X) > double.Epsilon
                || Math.Abs(Y) > double.Epsilon
                || Math.Abs(Z) > double.Epsilon
                || Math.Abs(XRotRad) > double.Epsilon
                || Math.Abs(YRotRad) > double.Epsilon
                || Math.Abs(ZRotRad) > double.Epsilon;
        }

        public Transformation(Transformation transformation)
        {
            X = transformation.X;
            Y = transformation.Y;
            Z = transformation.Z;
            XRotRad = transformation.XRotRad;
            YRotRad = transformation.YRotRad;
            ZRotRad = transformation.ZRotRad;
        }

        public Transformation(TransformationMatrix transformationMatrix)
        {
            if (transformationMatrix == null)
            {
                return;
            }
            TransformationMatrix = transformationMatrix;
            if (Math.Abs(Math.Abs(transformationMatrix.zx) - 1) > PCBContainer.DISTANCE_THRESHOLD)
            {
                YRotRad = Math.Asin(transformationMatrix.zx) * -1;
                XRotRad = Math.Atan2(transformationMatrix.zy / Math.Cos(YRotRad),
                    transformationMatrix.zz / Math.Cos(YRotRad));
                ZRotRad = Math.Atan2(transformationMatrix.yx / Math.Cos(YRotRad),
                    transformationMatrix.xx / Math.Cos(YRotRad));
            }
            else
            {
                ZRotRad = 0;
                if (Math.Abs(transformationMatrix.zx - 1) < PCBContainer.DISTANCE_THRESHOLD)
                {
                    YRotRad = Math.PI / 2;
                    XRotRad = ZRotRad + Math.Atan2(transformationMatrix.xy, transformationMatrix.xz);
                }
                else if (Math.Abs(transformationMatrix.zx + 1) < PCBContainer.DISTANCE_THRESHOLD)
                {
                    YRotRad = (Math.PI / 2) * -1;
                    XRotRad = ZRotRad * -1
                        + Math.Atan2((transformationMatrix.xy * -1), (transformationMatrix.xz * -1));
                }
                else
                {
                    throw new DivideByZeroException("Transformation conversion error.");
                }
            }
            X = transformationMatrix.tx;
            Y = transformationMatrix.ty;
            Z = transformationMatrix.tz;
        }

        public string TransformationString
        {
            get
            {
                return "X:" + X + "|Y:" + Y + "|Z:" + Z + "|α:" + ZRotRad + "|β:" + YRotRad + "|γ:" + XRotRad;
            }
        }

        public double X { get; set; } = 0;
        public double Y { get; set; } = 0;
        public double Z { get; set; } = 0;
        public double XRotRad { get; set; } = 0;
        public double YRotRad { get; set; } = 0;
        public double ZRotRad { get; set; } = 0;

        public double XRotDeg { get { return MathUtilities.RadToDeg(XRotRad); } set { XRotRad = MathUtilities.DegToRad(value); } }
        public double YRotDeg { get { return MathUtilities.RadToDeg(YRotRad); } set { YRotRad = MathUtilities.DegToRad(value); } }
        public double ZRotDeg { get { return MathUtilities.RadToDeg(ZRotRad); } set { ZRotRad = MathUtilities.DegToRad(value); } }
    }

    public class TransformationMatrix
    {
        public double xx { get; set; }
        public double xy { get; set; }
        public double xz { get; set; }
        public double yx { get; set; }
        public double yy { get; set; }
        public double yz { get; set; }
        public double zx { get; set; }
        public double zy { get; set; }
        public double zz { get; set; }
        public double tx { get; set; }
        public double ty { get; set; }
        public double tz { get; set; }
    }



}
