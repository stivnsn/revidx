﻿using EDMDProcessor;
using System;
using System.IO;
using System.Linq;

namespace EDMDTester
{
    class Program
    {
        static readonly string edmdDemoLocation = Path.Combine("D:", "edmd_demo");
        static string testFilesLocation = Path.Combine("D:", "workspace", "revidx", "RevidXServer", "EDMDTester", "test_data");
        static string pixRacerScenarioLocation = Path.Combine(testFilesLocation, "pixracer_proposal_scenario");

        static readonly string boardOnlyBaseline = Path.Combine(testFilesLocation, "baseline_board_only.idx");
        static readonly string indFull = Path.Combine(testFilesLocation, "baseline_ind_full.idx");
        static readonly string baselineMcdFewItems = Path.Combine(testFilesLocation, "baseline_ms_starter_01.idx");
        static readonly string msStarter = Path.Combine(testFilesLocation, "baseline_ms_starter_01.idx");
        static readonly string cadenceCMBaseline = Path.Combine(testFilesLocation, "cadence_baseline_arduino.idx");
        static readonly string mgArduinoBaseline = Path.Combine(testFilesLocation, "baseline_mgc_arduino_custom.idx");
        static readonly string mgArduinoProposal = Path.Combine(testFilesLocation, "proposal_mgc_arduino_custom.idx");
        static readonly string mgArduinoResponse = Path.Combine(testFilesLocation, "response_mgc_arduino_custom.idx");
        static readonly string mgPixFullBaseline = Path.Combine(testFilesLocation, "pixracer_xped_full.idx");

        static readonly string pixBaseline = Path.Combine(pixRacerScenarioLocation, "baseline_06.idx");
        static readonly string pixProposal1 = Path.Combine(pixRacerScenarioLocation, "proposal_01.idx");
        static readonly string pixResponse1 = Path.Combine(pixRacerScenarioLocation, "response_00.idx");
        static readonly string pixResponse2 = Path.Combine(pixRacerScenarioLocation, "response_01.idx");
        static readonly string pixBaselineGen = Path.Combine(pixRacerScenarioLocation, "baseline_gen_06.idx");
        static readonly string pixBaselineGen2 = Path.Combine(pixRacerScenarioLocation, "baseline_gen_02.idx");
        static readonly string aliceDir = Path.Combine(edmdDemoLocation, "aliceLocal");
        static readonly string bobDir = Path.Combine(edmdDemoLocation, "bobLocal");
        static readonly string charlieDir = Path.Combine(edmdDemoLocation, "charlieLocal");

        static readonly string scenarioFiles = Path.Combine(edmdDemoLocation, "pixScenarioFiles");

        static readonly (string, string)[] pixRacerDemoActions = new (string,string)[] {(aliceDir, "baseline_06.idx"),
                                            (bobDir, "proposal_01.idx"),
                                            (aliceDir, "response_00.idx"),
                                            (charlieDir, "proposal_02.idx"),
                                            (bobDir, "response_01.idx"),
                                            (aliceDir, "proposal_03.idx"),
                                            //(bobDir, "response_03.idx"),
                                            (aliceDir, "proposal_04.idx"),
                                            (bobDir, "response_04.idx") };


        static void Main(string[] args)
        {

            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("EDMD_DEMO_PURPOSE")))
            {
                playConsoleDemo();
            }
            else
            {
                EDMDReader processor = new EDMDReader(pixBaseline);
                //var pcbContainer = EDMDReader.ConstructPCBFromFile(processor.DeserializeEDMDFile());
                EDMDReader responseProcessor = new EDMDReader(pixResponse1);
                var appliedDataset = processor.ApplyResponse(responseProcessor.DeserializeEDMDFile());
                EDMDReader.OutputToFile(pixBaselineGen, appliedDataset);
                EDMDReader resultProcessor = new EDMDReader(pixBaselineGen);
                var deserializedGen = resultProcessor.DeserializeEDMDFile();
                var resultDataSet = EDMDReader.ConstructPCBFromFile(deserializedGen);

                EDMDReader processor2 = new EDMDReader(pixBaseline);
                EDMDReader responseProcessor2 = new EDMDReader(pixResponse2);
                var appliedDataset2 = processor2.ApplyResponse(responseProcessor2.DeserializeEDMDFile());
                EDMDReader.OutputToFile(pixBaselineGen2, appliedDataset2);
                EDMDReader resultProcessor2 = new EDMDReader(pixBaselineGen);
            }

        }

        static private void ClearAndPrepareDemoEnvironment()
        {
            if (!Directory.Exists(edmdDemoLocation))
            {
                Directory.CreateDirectory(edmdDemoLocation);
            }
            if (!Directory.Exists(aliceDir))
            {
                Directory.CreateDirectory(aliceDir);
            }
            else
            {
                foreach (var file in Directory.GetFiles(aliceDir))
                {
                    File.Delete(file);
                }
            }
            if (!Directory.Exists(scenarioFiles))
            {
                Directory.CreateDirectory(scenarioFiles);
            }
            else
            {
                foreach (var file in Directory.GetFiles(scenarioFiles))
                {
                    File.Delete(file);
                }
            }
            if (!Directory.Exists(bobDir))
            {
                Directory.CreateDirectory(bobDir);
            }
            else
            {
                foreach (var file in Directory.GetFiles(bobDir))
                {
                    File.Delete(file);
                }
            }
            if (!Directory.Exists(charlieDir))
            {
                Directory.CreateDirectory(charlieDir);
            }
            else
            {
                foreach (var file in Directory.GetFiles(charlieDir))
                {
                    File.Delete(file);
                }
            }
            if (!Directory.Exists(pixRacerScenarioLocation))
            {
                throw new Exception(string.Format("Could not find scenario files on {0}", pixRacerScenarioLocation));
            }
        }

        private static  void playConsoleDemo()
        {
            try
            {
                Console.WriteLine("RevidX Demonstration - automated scenario");
                Console.WriteLine("");
                Console.WriteLine("Press any key to prepare environment...");
                Console.ReadKey();
                Console.WriteLine("");
                Console.WriteLine("Clearing and creating dirs...");
                Console.WriteLine("");
                ClearAndPrepareDemoEnvironment();
                Console.WriteLine(string.Format("Copying scenario files from repo: {0}", scenarioFiles));
                Console.WriteLine("");
                foreach (var file in Directory.GetFiles(pixRacerScenarioLocation))
                {
                    File.Copy(file, Path.Combine(scenarioFiles, Path.GetFileName(file)));
                }
                Console.WriteLine("Finished preparing. Scenario can start.");
                Console.WriteLine("Press any key to start...");
                Console.ReadKey();
                Console.WriteLine("");
                foreach (var destFileAction in pixRacerDemoActions)
                {
                    var destName = Path.Combine(edmdDemoLocation, destFileAction.Item1);
                    var fileName = Path.Combine(scenarioFiles, destFileAction.Item2);
                    if (!File.Exists(fileName))
                    {
                        throw new Exception(string.Format("File does not exist: {0}", fileName));
                    }
                    Console.WriteLine(string.Format("Exporting {0} to {1}. Press for next step...", Path.GetFileName(fileName), Path.GetFileName(destName)));
                    Console.WriteLine("");
                    var destFilename = Path.Combine(destName, Path.GetFileName(fileName));
                    File.Copy(fileName, destFilename);
                    File.SetLastWriteTimeUtc(destFilename, DateTime.UtcNow);
                    Console.WriteLine("");
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }
            Console.WriteLine("Exiting. Press Enter to clear environment and exit, or any key to exit...");
            if(Console.ReadKey().Key == ConsoleKey.Enter)
            {

                ClearAndPrepareDemoEnvironment();
            }
        }

    }
}
