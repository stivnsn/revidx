﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Linq;
using EDMDSerialization.EDMDObjects;
using PCBXRevision;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using Serilog;
using EDMDSerialization;

namespace EDMDProcessor
{
    public class EDMDReader
    {
        private static CultureInfo _culture = new CultureInfo("en");
        private MemoryStream _fileMStream { get; set; }
        private string _filename { get; set; }

        public static XmlSerializerNamespaces CreateSerializerNamespaces()
        {
            XmlSerializerNamespaces xmlNamespaces = new XmlSerializerNamespaces();

            xmlNamespaces.Add("administration", "http://www.prostep.org/ecad-mcad/edmd/3.0/administration");
            xmlNamespaces.Add("annotation", "http://www.prostep.org/ecad-mcad/edmd/3.0/annotation");
            xmlNamespaces.Add("computational", "http://www.prostep.org/ecad-mcad/edmd/3.0/computational");
            xmlNamespaces.Add("d2", "http://www.prostep.org/ecad-mcad/edmd/3.0/geometry.d2");
            xmlNamespaces.Add("external", "http://www.prostep.org/ecad-mcad/edmd/3.0/external");
            xmlNamespaces.Add("foundation", "http://www.prostep.org/ecad-mcad/edmd/3.0/foundation");
            xmlNamespaces.Add("grouping", "http://www.prostep.org/ecad-mcad/edmd/3.0/grouping");
            //xmlNamespaces.Add("idx", "http://www.ptc.com/edmd/1.0/idx");
            xmlNamespaces.Add("material", "http://www.prostep.org/ecad-mcad/edmd/3.0/material");
            xmlNamespaces.Add("pdm", "http://www.prostep.org/ecad-mcad/edmd/3.0/pdm");
            xmlNamespaces.Add("property", "http://www.prostep.org/ecad-mcad/edmd/3.0/property");
            xmlNamespaces.Add("text", "http://www.prostep.org/ecad-mcad/edmd/3.0/text");
            xmlNamespaces.Add("ws", "http://www.prostep.org/ecad-mcad/edmd/3.0/ws");
            xmlNamespaces.Add("xsd", "http://www.w3.org/2001/XMLSchema");
            xmlNamespaces.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");

            return xmlNamespaces;
        }//CreateSerializerNamespaces()

        public EDMDReader(string filename)
        {
            _fileMStream = new MemoryStream();
            using (var fs = new FileStream(filename, FileMode.Open))
            {
                fs.CopyTo(_fileMStream);
            }
            _fileMStream.Seek(0, SeekOrigin.Begin);
            //_fileMStream = new FileStream(filename, FileMode.Open);
        }

        public EDMDReader(MemoryStream ms)
        {
            ms.Seek(0, SeekOrigin.Begin);
            _fileMStream = ms;
        }

        public static byte[] OutputToStream(EDMDDataSet dataSet)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(EDMDDataSet));
                StreamWriter writer = new StreamWriter(ms);
                XmlSerializerNamespaces xmlNamespaces = CreateSerializerNamespaces();
                xmlSerializer.Serialize(writer, dataSet, xmlNamespaces);
                writer.Close();
                return ms.ToArray();
            }
        }

        public static EDMDDataSet OutputToFile(string filename, EDMDDataSet dataSet)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(EDMDDataSet));
            TextWriter writer = new StreamWriter(filename);
            XmlSerializerNamespaces xmlNamespaces = CreateSerializerNamespaces();
            xmlSerializer.Serialize(writer, dataSet, xmlNamespaces);
            writer.Close();
            return dataSet;
        }

        public EDMDDataSet DeserializeEDMDFile()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(EDMDDataSet));
            return (EDMDDataSet)xmlSerializer.Deserialize(_fileMStream);
        }


        public static SendType GetFileType(EDMDDataSet dataSet)
        {
            foreach (var processInstruction in dataSet.ProcessInstruction)
            {
                switch (processInstruction)
                {
                    case EDMDProcessInstructionSendInformation sendInformation:
                        return SendType.BASELINE;
                    case EDMDProcessInstructionSendChanges sendChanges:
                        break;
                    default:
                        return SendType.UNKNOWN;

                }
            }
            foreach (var obj in dataSet.Body.Items.Reverse())
            {
                switch (obj)
                {
                    case EDMDItem item:
                        if (item.ItemType == EDMDItemType.assembly && (item.ItemInstance == null || item.ItemInstance.Length != 1))
                        {
                            // layers, continue;
                            continue;
                        }
                        else if (item.ItemType == EDMDItemType.assembly)
                        {
                            if (item.Accept != null && item.Accept.Length == 1)
                            {
                                return SendType.RESPONSE;
                            }
                            else
                            {
                                return SendType.PROPOSAL;
                            }
                        }
                        break;
                    default:
                        continue;
                }
            }
            return SendType.UNKNOWN;
        }

        private static string CreateAndReturnLatest(ref Dictionary<string, int> idWordMaxNumMap, string ident, bool createNew = false)
        {
            var idWordNum = Regex.Match(ident, @"([a-zA-Z]+)(\d+)");
            int idNum = 0;
            string idWord = null;
            if (idWordNum.Groups.Count == 3)
            {
                idWord = idWordNum.Groups[1].Value;
                if (int.TryParse(idWordNum.Groups[2].Value, out idNum))
                {
                    int latestNum = 0;
                    if (idWordMaxNumMap.TryGetValue(idWord, out latestNum))
                    {
                        if (latestNum < idNum)
                        {
                            idWordMaxNumMap[idWord] = idNum;
                        }
                        else if (createNew)
                        {
                            idNum = latestNum + 1;
                            idWordMaxNumMap[idWord] = idNum;
                        }
                        else
                        {
                            return ident;
                        }
                    }
                    else
                    {
                        idWordMaxNumMap.Add(idWord, idNum);
                    }
                    return string.Concat(idWord, idNum);
                }
            }
            return ident;
        }

        static string RemapIdentificators(ref Dictionary<string, int> idWordMaxNumMap, ref Dictionary<string, string> identRemapDictionary, string id)
        {
            string newId = id;
            if (identRemapDictionary.ContainsKey(id))
            {
                newId = identRemapDictionary[id];
            }
            else
            {
                newId = CreateAndReturnLatest(ref idWordMaxNumMap, id, true);
                identRemapDictionary.Add(id, newId);
            }
            return newId;
        }

        // if idWordMaxNumMap provided it will apply new ids to given dataset

        public static SortedEdmdItems GetSortedEdmdObjects(EDMDDataSet dataSet, Dictionary<string, int> externalIdWordMaxNumMap = null)
        {
            SortedEdmdItems sortedEdmdItems = null;
            try
            {
                //Log.Information("File: " + _filename);
                Log.Information("CreatorName: " + dataSet.Header.CreatorName);
                Log.Information("Creator sys: " + dataSet.Header.CreatorSystem);
                Log.Information("initialized sorted edmd items");
                //dataSet.ProcessInstruction == EDMDProcessInstructionSendInformation;
                sortedEdmdItems = new SortedEdmdItems();
                sortedEdmdItems.IsBaseline = true;
                if (dataSet.ProcessInstruction == null)
                {
                    Log.Error("EDMD process instruction is null.");
                    throw new Exception("EDMD process instruction is null.");
                }
                Log.Information("ProcessInstruction size: " + dataSet.ProcessInstruction.Length);
                foreach (var processInstruction in dataSet.ProcessInstruction)
                {
                    switch (processInstruction)
                    {
                        case EDMDProcessInstructionSendInformation sendInformation:
                            sortedEdmdItems.IsBaseline = true;
                            break;
                        case EDMDProcessInstructionSendChanges sendChanges:
                            sortedEdmdItems.IsBaseline = false;
                            foreach (var change in sendChanges.Changes)
                            {
                                if (change.GetType() != typeof(EDMDTransaction))
                                {
                                    throw new Exception("Unsupported change type" + change.GetType().ToString());
                                }
                                EDMDTransaction transaction = (EDMDTransaction)change;
                                if (transaction.Change.Length != 1)
                                {
                                    throw new Exception("Unsupported number of changes inside transaction " + transaction.Change.Length);
                                }
                                var actualChange = transaction.Change[0];
                                bool newItemIsNullOrEmptyNumber = transaction.Change[0].NewItem == null
                                    || string.IsNullOrEmpty(transaction.Change[0].NewItem.Number);
                                bool predItemIsNullOrEmptyNumber = transaction.Change[0].PredecessorItem == null
                                    || string.IsNullOrEmpty(transaction.Change[0].PredecessorItem.Number);
                                bool isDeletedItemInstance = transaction.Change[0].DeletedInstanceName != null;
                                if (newItemIsNullOrEmptyNumber && predItemIsNullOrEmptyNumber)
                                {
                                    throw new Exception("Unsupported process instruction type" + processInstruction.GetType().ToString());
                                }
                                else if (isDeletedItemInstance || (!predItemIsNullOrEmptyNumber && newItemIsNullOrEmptyNumber))
                                {
                                    string predItemNumber = actualChange.PredecessorItem.Number;
                                    sortedEdmdItems.ItemsToDelete.Add(new Identifier(actualChange.PredecessorItem.SystemScope, predItemNumber));
                                    sortedEdmdItems.Changes.Add((predItemNumber, null), actualChange);
                                }
                                else if (!newItemIsNullOrEmptyNumber && !predItemIsNullOrEmptyNumber) // mod
                                {
                                    string newItemNumber = actualChange.NewItem.Number;
                                    string predItemNumber = actualChange.PredecessorItem.Number;
                                    sortedEdmdItems.ItemsToDelete.Add(new Identifier(actualChange.PredecessorItem.SystemScope, predItemNumber));
                                    sortedEdmdItems.ItemsToAdd.Add(new Identifier(actualChange.NewItem.SystemScope, newItemNumber));
                                    sortedEdmdItems.Changes.Add((predItemNumber, newItemNumber), actualChange);
                                }
                                else if (!newItemIsNullOrEmptyNumber) // add
                                {
                                    string newItemNumber = actualChange.NewItem.Number;
                                    sortedEdmdItems.ItemsToAdd.Add(new Identifier(actualChange.NewItem.SystemScope, newItemNumber));
                                    sortedEdmdItems.Changes.Add((null, newItemNumber), actualChange);
                                }
                                else
                                {
                                    throw new Exception(string.Format("Change item case not supported! Review!"));
                                }
                                //string itemNumber = !string.IsNullOrEmpty(newItemNumber) ? newItemNumber : predItemNumber;
                                //sortedEdmdItems.Changes.Add(itemNumber, actualChange);
                            }
                            break;
                        default:
                            throw new Exception("Unsupported process instruction type" + processInstruction.GetType().ToString());

                    }
                }
                Log.Information("Body.Items size: " + dataSet.Body.Items.Length);
                bool remapIdentificators = externalIdWordMaxNumMap != null;
                Dictionary<string, string> identRemapDictionary = new Dictionary<string, string>();
                foreach (var obj in dataSet.Body.Items)
                {
                    List<string> referencingIds = new List<string>();
                    string systemScope = null;
                    string objectName = null;
                    if (remapIdentificators)
                    {
                        obj.id = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, obj.id);
                    }
                    else
                    {
                        CreateAndReturnLatest(ref sortedEdmdItems.IdWordMaxNumMap, obj.id);
                    }
                    switch (obj)
                    {
                        case EDMDItemShape itemShape:
                            if (itemShape.ShapeElement != null)
                            {
                                foreach (var shapeElement in itemShape.ShapeElement)
                                {
                                    var localShapeElement = shapeElement;
                                    if (remapIdentificators)
                                    {
                                        localShapeElement = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, localShapeElement);
                                    }
                                    referencingIds.Add(localShapeElement);
                                }
                                itemShape.ShapeElement = referencingIds.ToArray();
                            }
                            switch (itemShape)
                            {
                                case EDMDKeepIn keepIn:
                                    sortedEdmdItems.KeepIns.Add(keepIn.id, keepIn);
                                    break;
                                case EDMDKeepOut keepOut:
                                    sortedEdmdItems.KeepOuts.Add(keepOut.id, keepOut);
                                    break;
                                case EDMDFunctionalItemShape functionalItemShape:
                                    sortedEdmdItems.FunctionalItemShapes.Add(functionalItemShape.id, functionalItemShape);
                                    break;
                                case EDMDNonFeatureItemShape nonFeatureItemShape:
                                    sortedEdmdItems.NonFeatureItemShapes.Add(nonFeatureItemShape.id, nonFeatureItemShape);
                                    break;
                                case EDMDInterStratumFeature interStratumFeature:
                                    sortedEdmdItems.InterStratumFeatures.Add(interStratumFeature.id, interStratumFeature);
                                    break;
                                case EDMDAssemblyComponent assemblyComponent:
                                    sortedEdmdItems.AssemblyComponents.Add(assemblyComponent.id, assemblyComponent);
                                    break;
                                case EDMDStratum stratum:
                                    if (stratum.StratumType == EDMDStratumType.DesignLayerStratum)
                                    {
                                        sortedEdmdItems.DesignLayerStratums.Add(stratum.id, stratum);
                                    }
                                    sortedEdmdItems.Stratums.Add(stratum.id, stratum);
                                    break;
                                default:

#if DEBUG
                                    throw new Exception("Unsupported object type. Should be reviewed: " + obj.GetType());
#else
                                Log.Error("Unsupported object type. Should be reviewed: " + obj.GetType());
                                continue;
#endif
                            }
                            // should sort stratums keepouts keepings interstratum-holes nonfeature-other assembly-comp
                            sortedEdmdItems.ItemShapes.Add(itemShape.id, itemShape);
                            break;
                        case EDMDStratumTechnology stratumTechnology:
                            sortedEdmdItems.StratumTechnologies.Add(stratumTechnology.id, stratumTechnology);
                            break;
                        case EDMDSystem systems:
                            sortedEdmdItems.Systems.Add(systems.id, systems);
                            break;
                        case EDMDExternalSource externalSource:
                            sortedEdmdItems.ExternalSources.Add(externalSource.id, externalSource);
                            break;
                        case EDMDPerson person:
                            sortedEdmdItems.Persons.Add(person.id, person);
                            break;
                        case EDMDUnit unit:
                            sortedEdmdItems.Units.Add(unit.id, unit);
                            break;
                        case EDMDCompositeCurve compositeCurve:
                            if (compositeCurve.Curve != null)
                            {
                                foreach (var curve in compositeCurve.Curve)
                                {
                                    var localCurve = curve;
                                    if (remapIdentificators)
                                    {
                                        localCurve = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, localCurve);
                                    }
                                    referencingIds.Add(localCurve);
                                }
                                compositeCurve.Curve = referencingIds.ToArray();
                            }
                            sortedEdmdItems.CompositeCurves.Add(compositeCurve.id, compositeCurve);
                            break;
                        case EDMDCartesianPoint point:
                            sortedEdmdItems.Points.Add(point.id, point);
                            break;
                        case EDMDCurveSet2d curveSet:
                            if (curveSet.DetailedGeometricModelElement != null)
                            {
                                foreach (var modelElement in curveSet.DetailedGeometricModelElement)
                                {
                                    var localModelElement = modelElement;
                                    if (remapIdentificators)
                                    {
                                        localModelElement = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, localModelElement);
                                    }
                                    referencingIds.Add(localModelElement);
                                }
                                curveSet.DetailedGeometricModelElement = referencingIds.ToArray();
                            }
                            sortedEdmdItems.CurveSets.Add(curveSet.id, curveSet);
                            break;
                        case EDMDCurve curve:
                            switch (curve)
                            {
                                case EDMDLine line:
                                    if (remapIdentificators)
                                    {
                                        line.Point = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, line.Point);
                                        line.Vector = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, line.Vector);
                                    }
                                    referencingIds.Add(line.Point);
                                    referencingIds.Add(line.Vector);
                                    break;
                                case EDMDPolyLine poly:
                                    foreach (var point in poly.Point)
                                    {
                                        var localPoint = point;
                                        if (remapIdentificators)
                                        {
                                            localPoint = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, localPoint);
                                        }
                                        referencingIds.Add(localPoint);
                                    }
                                    poly.Point = referencingIds.ToArray();
                                    break;
                                case EDMDArc arc:
                                    if (remapIdentificators)
                                    {
                                        arc.StartPoint = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, arc.StartPoint);
                                        arc.EndPoint = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, arc.EndPoint);
                                    }
                                    referencingIds.Add(arc.StartPoint);
                                    referencingIds.Add(arc.EndPoint);
                                    break;
                                case EDMDCircle3Point circle3Point:
                                    if (remapIdentificators)
                                    {
                                        circle3Point.P1 = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, circle3Point.P1);
                                        circle3Point.P2 = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, circle3Point.P2);
                                        circle3Point.P3 = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, circle3Point.P3);
                                    }
                                    referencingIds.Add(circle3Point.P1);
                                    referencingIds.Add(circle3Point.P2);
                                    referencingIds.Add(circle3Point.P3);
                                    break;
                                case EDMDCircleCenter circleCenter:
                                    if (remapIdentificators)
                                    {
                                        circleCenter.Center = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, circleCenter.Center);
                                    }
                                    referencingIds.Add(circleCenter.Center);
                                    break;
                                default:
                                    break;
                            }
                            sortedEdmdItems.Curves.Add(curve.id, curve);
                            break;
                        case EDMDShapeElement shapeElement:
                            if (remapIdentificators)
                            {
                                shapeElement.DefiningShape = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, shapeElement.DefiningShape);
                            }
                            referencingIds.Add(shapeElement.DefiningShape);
                            sortedEdmdItems.ShapeElements.Add(shapeElement.id, shapeElement);
                            break;
                        case EDMDItem item:
                            if (!string.IsNullOrEmpty(item.Shape))
                            {
                                if (remapIdentificators)
                                {
                                    item.Shape = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, item.Shape);
                                }
                                referencingIds.Add(item.Shape);
                            }
                            if (item.ItemType == EDMDItemType.assembly && item.ItemInstance == null)
                            {
                                sortedEdmdItems.LayerItems.Add(item.id, item);
                                continue;
                            }
                            else if (item.ItemType == EDMDItemType.assembly)
                            {
                                sortedEdmdItems.AssemblyItems.Add(item.id, item);
                            }
                            else
                            {
                                sortedEdmdItems.SingleItems.Add(item.id, item);
                            }
                            if (item.ItemInstance != null)
                            {
                                foreach (var instance in item.ItemInstance)
                                {
                                    if (remapIdentificators)
                                    {
                                        instance.Item = RemapIdentificators(ref externalIdWordMaxNumMap, ref identRemapDictionary, instance.Item);
                                    }
                                    referencingIds.Add(instance.Item);
                                    EDMDItemInstance temp;
                                    if (sortedEdmdItems.ItemInstances.TryGetValue((item.id, instance.Item), out temp))
                                    {
                                        throw new Exception("This item instance pair key should not already exist: " + item.id + " " + instance.Item);
                                    }
                                    if (instance.InstanceName != null && instance.InstanceName.Length > 0)
                                    {
                                        foreach (var instanceName in instance.InstanceName)
                                        {
                                            if (!string.IsNullOrEmpty(instanceName.SystemScope) && !string.IsNullOrEmpty(instanceName.ObjectName))
                                            {
                                                var identifier = new Identifier(instanceName.SystemScope, instanceName.ObjectName);
                                                if (!sortedEdmdItems.ItemsByObjectName.ContainsKey(identifier))
                                                {
                                                    sortedEdmdItems.ItemsByObjectName.Add(identifier, item);
                                                }
                                                else if (!sortedEdmdItems.IsBaseline)
                                                {
                                                    var existingItem = sortedEdmdItems.ItemsByObjectName[identifier];
                                                    sortedEdmdItems.AssemblyItems.Remove(existingItem.id);
                                                    sortedEdmdItems.ItemsByObjectName[identifier] = item;
                                                    Log.Information(string.Format("Systemscope and object name already exists for some reason! {0}. Persumed is a changed item with new value after"
                                                        , instanceName.ObjectName));
                                                    //throw new Exception("Systemscope and object name already exists for some reason!");
                                                }
                                                else
                                                {
                                                    Log.Error(string.Format("Systemscope and object name already exists for some reason! {0}", instanceName.ObjectName));
                                                    //throw new Exception("Systemscope and object name already exists for some reason!");
                                                }
                                            }
                                            else if (item.ItemType != EDMDItemType.single)
                                            {
                                                Log.Warning(string.Format("Object name and systemscope should be available: {0}", instance.id));
                                                throw new Exception("Object name and systemscope should be available");
                                            }
                                            else
                                            {

                                                Log.Warning(string.Format("Object name and systemscope should be available: {0}, but might be pin", instance.id));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.Error("Item instance is empty: " + instance.id);
                                    }
                                    sortedEdmdItems.ItemInstances.Add((item.id, instance.Item), instance);
                                    if (instance.UserProperty != null)
                                    {
                                        foreach (EDMDUserSimpleProperty userSimpleProperty in instance.UserProperty)
                                        {
                                            string tempVal;
                                            if (sortedEdmdItems.UserProperties.TryGetValue((instance.Item, userSimpleProperty.Key.First().ObjectName), out tempVal))
                                            {
                                                if (tempVal != userSimpleProperty.Value)
                                                {
                                                    throw new Exception(" Multiple userprop values for the same item instance");
                                                }
                                            }
                                            else
                                            {
                                                sortedEdmdItems.UserProperties.Add((instance.Item, userSimpleProperty.Key.First().ObjectName), userSimpleProperty.Value);
                                            }
                                        }
                                    }
                                    // key is "holding item id and instance name tuple"
                                    //var result = sortedEdmdItems.ItemInstances.Where(ii => ii.Key.Item1 == "ITEM5").FirstOrDefault();
                                }
                                sortedEdmdItems.Items.Add(item.id, item);
                            }
                            break;
                        case EDMDExternalGeometricModel geometricModel:
                            sortedEdmdItems.ExternalGeometricModels.Add(geometricModel.id, geometricModel);
                            break;
                        default:
#if DEBUG
                            throw new Exception("Unsupported object type. Should be reviewed: " + obj.GetType());
#else
                        Log.Error("Unsupported object type. Should be reviewed: " + obj.GetType());
                        continue;
#endif

                    }
                    sortedEdmdItems.AllObjectsById.Add(obj.id, new ItemHolder(obj, referencingIds, systemScope, objectName));
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if (ex.InnerException != null)
                {
                    Log.Error(ex.InnerException.Message);
                }
            }
            return sortedEdmdItems;
        }


        public static PCBContainer ConstructPCBFromFile(EDMDDataSet dataSet)
        {
            PCBContainer pcbContainer = null;
            var isSuccess = false;
            try
            {
                SortedEdmdItems sortedEdmdItems = GetSortedEdmdObjects(dataSet);
                pcbContainer = new PCBContainer();
                var components = pcbContainer.Components;
                var constraintAreas = pcbContainer.ConstraintAreas;
                var holes = pcbContainer.Holes;
                var layerStackups = pcbContainer.LayerStackups;
                var layers = pcbContainer.Layers;
                var cutouts = pcbContainer.Cutouts;
                pcbContainer.Header = IdxPcbConverter.CreatePcbHeader(dataSet.Header);
                foreach (var processInstruction in dataSet.ProcessInstruction)
                {
                    pcbContainer.ProcessInstructions.Add(IdxPcbConverter.CreatePcbProcessInstruction(processInstruction));
                }
                pcbContainer.SendType = sortedEdmdItems.IsBaseline ? SendType.BASELINE : SendType.PROPOSAL;
                Log.Information("Processing assembly items. Count: " + sortedEdmdItems.AssemblyItems.Count);
                foreach (var assemblyItemPair in sortedEdmdItems.AssemblyItems)
                {
                    if (assemblyItemPair.Value == null)
                    {
                        throw new Exception(String.Format("Assembly item key is null. Item " + assemblyItemPair.Key));
                    }
                    var assemblyItem = assemblyItemPair.Value;
                    Log.Information("Processing new assembly item: " + assemblyItem.id);
                    ObjectData objectData = new ObjectData();
                    objectData = IdxPcbConverter.ConvertAssemblyItemData(assemblyItem);
                    if (assemblyItem.Accept != null && assemblyItem.Accept.Length == 1)
                    {
                        if (sortedEdmdItems.IsBaseline)
                        {
                            Log.Error("Baseline cannot have accept attributes: " + assemblyItem.id);
                            throw new Exception(String.Format("Baseline cannot have accept attributes: {0}", assemblyItem.id));
                        }
                        pcbContainer.SendType = SendType.RESPONSE;
                        objectData.Accepted = assemblyItem.Accept[0].Value;
                        ++pcbContainer.QuickPCBStats.ProposedObjectsTotal;
                        if (objectData.Accepted == true)
                        {
                            ++pcbContainer.QuickPCBStats.ProposedObjectsAccepted;
                        }
                    }
                    else if (pcbContainer.SendType == SendType.RESPONSE)
                    {
                        throw new Exception(String.Format("Send type is response, but there is accept property missing: {0}", assemblyItem.id));
                    }
                    var multipleItemInstances = assemblyItem.ItemInstance.Length > 1;
                    if (multipleItemInstances)
                    {
                        Log.Warning("Multiple instances, watch for layers");
                    }
                    foreach (var itemInstance in assemblyItem.ItemInstance)
                    {
                        Log.Information("Processing new item instance: " + itemInstance.Item);
                        objectData.UserProperties = IdxPcbConverter.CreatePcbUserProperties(itemInstance.UserProperty);
                        if (itemInstance.InstanceName == null || itemInstance.InstanceName.Length == 0)
                        {
                            throw new Exception(String.Format("No instances in item: {0}", assemblyItem.id));
                        }
                        foreach (var instanceName in itemInstance.InstanceName)
                        {
                            if (string.IsNullOrEmpty(instanceName.SystemScope) || string.IsNullOrEmpty(instanceName.ObjectName))
                            {
                                throw new Exception(String.Format("Object name and systemscope should be available: {0}", assemblyItem.id));
                            }
                            objectData.Identifier = new Identifier(instanceName.SystemScope, instanceName.ObjectName);
                            if (sortedEdmdItems.Changes.Count > 0 && sortedEdmdItems.Changes != null)
                            {
                                var changeItem = sortedEdmdItems.Changes.Where(c => c.Key.Item1 == instanceName.ObjectName || c.Key.Item2 == instanceName.ObjectName).FirstOrDefault();
                                if (changeItem.Key.Item1 == null && changeItem.Key.Item2 == null)
                                {
                                    Log.Error(string.Format("Change item could not be found for: {0}.", instanceName.ObjectName));
                                    //throw new Exception(string.Format("Change item could not be found for: {0}.", instanceName.ObjectName));
                                }
                                else if (changeItem.Key.Item1 == null)
                                {
                                    objectData.ChangeType = ChangeType.ADDITION;
                                    ++pcbContainer.QuickPCBStats.ObjectsAddedCount;
                                }
                                else if (changeItem.Key.Item2 == null)
                                {
                                    objectData.ChangeType = ChangeType.DELETION;
                                    ++pcbContainer.QuickPCBStats.ObjectsDeletedCount;
                                }
                                else
                                {
                                    objectData.ChangeType = ChangeType.MODIFICATION;
                                    ++pcbContainer.QuickPCBStats.ObjectsModifiedCount;
                                }
                            }
                        }
                        if (!sortedEdmdItems.SingleItems.ContainsKey(itemInstance.Item))
                        {
                            if (!sortedEdmdItems.LayerItems.ContainsKey(itemInstance.Item))
                            {
                                throw new Exception(String.Format("Item instance Item {0} doesn't appear in single or layer items", itemInstance.Item));
                            }
                            var layerItem = sortedEdmdItems.LayerItems[itemInstance.Item];
                            var layerObjectData = IdxPcbConverter.CreatePcbSingleItemObjectData(layerItem);
                            var layer = new Layer(layerObjectData);
                            layers[itemInstance.Item] = layer;
                            if (layerStackups.ContainsKey(assemblyItem.Name))
                            {
                                var layerStackup = layerStackups[assemblyItem.Name];
                                layerStackup.Layers.Add(itemInstance.Item);
                                layerStackups[assemblyItem.Name] = layerStackup;
                            }
                            else
                            {
                                var layerStackup = new LayerStackup();
                                layerStackup.Name = assemblyItem.Name;
                                layerStackup.Layers.Add(itemInstance.Item);
                                layerStackups.Add(assemblyItem.Name, layerStackup);
                            }
                            ++pcbContainer.QuickPCBStats.LayersCount;
                            continue;
                        }
                        EDMDItem singleItem = sortedEdmdItems.SingleItems[itemInstance.Item];
                        ObjectData singleItemObjectData = IdxPcbConverter.CreatePcbSingleItemObjectData(singleItem);
                        if (singleItem.Shape == null)
                        {
                            throw new Exception(string.Format("Single item shape is null {0}", singleItem.id));
                        }
                        EDMDItemShape itemShape;
                        if (sortedEdmdItems.ItemShapes == null || !sortedEdmdItems.ItemShapes.TryGetValue(singleItem.Shape, out itemShape))
                        {
                            throw new Exception(String.Format("Could not find itemShape {0}", singleItem.Shape));
                        }
                        if (itemShape.ShapeElement == null || itemShape.ShapeElement.Length == 0)
                        {
                            throw new Exception("Shape element does not exist");
                        }
                        Log.Information("Shape Element found for: " + singleItem.Shape);
                        GeometricalShape geometricalShape = new GeometricalShape(singleItemObjectData);
                        List<EDMDShapeElement> shapeElements = new List<EDMDShapeElement>();
                        geometricalShape.CurveSets = new List<CurveSet>();
                        foreach (var shapeElementName in itemShape.ShapeElement)
                        {
                            EDMDShapeElement shapeElement = null;
                            if (!sortedEdmdItems.ShapeElements.TryGetValue(shapeElementName, out shapeElement))
                            {
                                throw new Exception(string.Format("Shape element known but not found: {0}"));
                            }
                            var curves = new List<Curve>();
                            if (!sortedEdmdItems.CurveSets.ContainsKey(shapeElement.DefiningShape))
                            {
                                Log.Warning(String.Format("Contains external geometric model. Might be deletion. Skip for now. ", shapeElement.DefiningShape));
                            }
                            else
                            {
                                var curveSet = sortedEdmdItems.CurveSets[shapeElement.DefiningShape];
                                foreach (string curveId in curveSet.DetailedGeometricModelElement)
                                {
                                    EDMDCurve curve = null;
                                    if (sortedEdmdItems.Curves.ContainsKey(curveId))
                                    {
                                        curve = sortedEdmdItems.Curves[curveId];
                                    }
                                    else
                                    {
                                        curve = sortedEdmdItems.CompositeCurves[curveId];
                                    }

                                    curves.Add(CreateCurve(curve, sortedEdmdItems));
                                }
                                geometricalShape.CurveSets.Add(new CurveSet() { 
                                    Curves = curves, IsInverted = shapeElement.InvertedSpecified && shapeElement.Inverted == true
                                , LowerBound = curveSet.LowerBound.Value, UpperBound = curveSet.UpperBound.Value, 
                                    ShapeElementType = IdxPcbConverter.ConvertShapeElementType(shapeElement.ShapeElementType)}); ;
                                Outline outline;
                                if (!ConvertShapeElement(out outline, shapeElement, sortedEdmdItems))
                                {
                                    if (objectData.ChangeType == ChangeType.DELETION)
                                    {
                                        Log.Warning("No outline, but it's a deletion.");
                                    }
                                    else
                                    {
                                        Log.Error("Failed reading shapeElement for: " + shapeElement.id);
                                        continue;
                                    }
                                }
                                if (shapeElement.InvertedSpecified && shapeElement.Inverted == true)
                                {
                                    geometricalShape.InnerOutlines.Add(outline);
                                }
                                else
                                {
                                    if (geometricalShape.Outline != null)
                                    {
                                        throw new Exception(String.Format("Multiple outer outlines defined for shape {0}", singleItem.Shape));
                                    }
                                    geometricalShape.Outline = outline;
                                }
                                if (itemInstance.Transformation != null && !geometricalShape.Outline.Transformation.HasTransformation())
                                {
                                    geometricalShape.Outline.Transformation = IdxPcbConverter.GetTransformation(itemInstance.Transformation);
                                }
                                shapeElements.Add(shapeElement);
                            }
                        }
                        Log.Information("Item shape exists for " + itemShape.id);
                        switch (itemShape)
                        {
                            case EDMDStratum stratum:
                                EDMDStratumTechnology stratumTechnology = null;
                                if (stratum.StratumTechnology != null && sortedEdmdItems.StratumTechnologies.TryGetValue(stratum.StratumTechnology, out stratumTechnology))
                                {
                                    //design + other signal tracks
                                    //design + Lands only pads
                                    //design + powerorground plane
                                    //var layer = new Layer(geometricalShape, objectData);
                                    //layers.Add(layer);
                                    //Log.Information("Stratum technology found: " + stratum.StratumTechnology);
                                    //++pcbContainer.QuickPCBStats.LayersCount;
                                }
                                else
                                {
                                    pcbContainer.Board = IdxPcbConverter.CreatePcbBoard(stratum, itemInstance, geometricalShape, objectData);
                                    ++pcbContainer.QuickPCBStats.BoardsCount;
                                }
                                break;
                            case EDMDAssemblyComponent assemblyComponent:
                                Component component = IdxPcbConverter.CreatePcbComponent(assemblyComponent, geometricalShape, objectData);
                                components.Add(objectData.Identifier, component);
                                ++pcbContainer.QuickPCBStats.ComponentsCount;
                                break;
                            case EDMDInterStratumFeature interStratumFeature:
                                foreach (var shapeElement in shapeElements)
                                {
                                    Log.Warning("Might be more shape elements but could not process them! ");
                                    switch (shapeElement.ShapeElementType.Value)
                                    {

                                        case EDMDShapeElementType.ViaTerminal:
                                            Hole hole = IdxPcbConverter.CreatePcbViaHole(interStratumFeature, geometricalShape, objectData);
                                            holes.Add(objectData.Identifier, hole);
                                            ++pcbContainer.QuickPCBStats.HolesCount;
                                            //via
                                            break;
                                        case EDMDShapeElementType.PartMountingFeature:
                                            Hole mtgHole = IdxPcbConverter.CreatePcbMountingHole(interStratumFeature, geometricalShape, objectData);
                                            holes.Add(objectData.Identifier, mtgHole);
                                            ++pcbContainer.QuickPCBStats.HolesCount;
                                            //part mounting
                                            break;
                                        case EDMDShapeElementType.ComponentTerminal:
                                            Hole pinHole = IdxPcbConverter.CreatePcbPinHole(interStratumFeature, geometricalShape, objectData);
                                            holes.Add(objectData.Identifier, pinHole);
                                            ++pcbContainer.QuickPCBStats.HolesCount;
                                            break;

                                        case EDMDShapeElementType.FeatureShapeElement:
                                        default:
                                            Cutout cutout = IdxPcbConverter.CreatePcbCutout(interStratumFeature, geometricalShape, objectData);
                                            cutouts.Add(objectData.Identifier, cutout);
                                            ++pcbContainer.QuickPCBStats.CutoutCount;
                                            break;
                                    }
                                }
                                break;
                            case EDMDKeepIn keepIn:
                                ConstraintArea keepInZone = IdxPcbConverter.CreatePcbKeepInArea(keepIn, geometricalShape, objectData);
                                constraintAreas.Add(objectData.Identifier, keepInZone);
                                ++pcbContainer.QuickPCBStats.AreasCount;
                                break;
                            case EDMDKeepOut keepOut:
                                ConstraintArea keepOutZone = IdxPcbConverter.CreatePcbKeepOutArea(keepOut, geometricalShape, objectData);
                                constraintAreas.Add(objectData.Identifier, keepOutZone);
                                ++pcbContainer.QuickPCBStats.AreasCount;
                                // keepouts
                                break;
                            case EDMDNonFeatureItemShape nonFeatureItemShape:
                                ConstraintArea otherOutlineZone = IdxPcbConverter.CreatePcbNonFeatureArea(nonFeatureItemShape, geometricalShape, objectData);
                                constraintAreas.Add(objectData.Identifier, otherOutlineZone);
                                ++pcbContainer.QuickPCBStats.AreasCount;
                                // other outlines
                                break;
                            case EDMDFunctionalItemShape functionalItemShape:
                                switch (functionalItemShape.FunctionalItemShapeType)
                                {
                                    case EDMDFunctionalItemShapeType.MechanicalItem:
                                        Component mechanicalComponent = IdxPcbConverter.CreatePcbMechanicalComponent(functionalItemShape, geometricalShape, objectData);
                                        components.Add(objectData.Identifier, mechanicalComponent);
                                        ++pcbContainer.QuickPCBStats.ComponentsCount;
                                        break;
                                    case EDMDFunctionalItemShapeType.UserArea:
                                    case EDMDFunctionalItemShapeType.PlacementGroupArea:
                                    case EDMDFunctionalItemShapeType.Stiffener:
                                    case EDMDFunctionalItemShapeType.FlexibleArea:
                                    default:
                                        ConstraintArea constraintArea = IdxPcbConverter.CreatePcbFunctionalItemArea(functionalItemShape, geometricalShape, objectData);
                                        constraintAreas.Add(objectData.Identifier, constraintArea);
                                        ++pcbContainer.QuickPCBStats.AreasCount;
                                        break;
                                }
                                break;
                            default:
                                throw new Exception("Unknown itemshape type :" + itemShape.Name + " " + itemShape.GetType().ToString());
                        }

                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if (ex.InnerException != null)
                {
                    Log.Error(ex.InnerException.Message);
                }
            }
            if (isSuccess)
            {
                Log.Information("Processing finished.");
            }
            else
            {
                Log.Error("Processing finished unsuccessfully.");
                return null;

            }
            return pcbContainer;
        }

        /// <summary>
        /// Should read and sort proposal files, set accept flaged based on pcb construct and modify to response file
        /// </summary>
        /// <param name="proposalDataSet"></param>
        /// <returns></returns>
        public static EDMDDataSet ApplyAndConvertToResponse(EDMDDataSet proposalDataSet, PCBContainer pcbContainer)
        {

            EDMDDataSet retDataSet = null;
            try
            {
                //Log.Information("File: " + _filename);
                Log.Information("CreatorName: " + proposalDataSet.Header.CreatorName);
                Log.Information("Creator sys: " + proposalDataSet.Header.CreatorSystem);
                Log.Information("initialized sorted edmd items");
                var sortedEdmdObjects = GetSortedEdmdObjects(proposalDataSet);
                List<string> itemIdsToRemove = new List<string>();
                foreach (var assemblyItemPair in sortedEdmdObjects.ItemsByObjectName)
                {
                    var identifier = assemblyItemPair.Key;
                    if (pcbContainer.Board != null && pcbContainer.Board.ObjectData.Identifier == identifier)
                    {

                        var logProp = new EDMDLogicProperty();
                        logProp.Value = pcbContainer.Board.ObjectData.IsAccepted;
                        assemblyItemPair.Value.Accept = new EDMDLogicProperty[1];
                        assemblyItemPair.Value.Accept[0] = logProp;
                        continue;
                    }
                    else
                    {
                        if (pcbContainer.Components.ContainsKey(identifier))
                        {
                            var component = pcbContainer.Components[identifier];
                            var logProp = new EDMDLogicProperty();
                            logProp.Value = component.ObjectData.IsAccepted;
                            assemblyItemPair.Value.Accept = new EDMDLogicProperty[1];
                            assemblyItemPair.Value.Accept[0] = logProp;
                            continue;
                        }
                        if (pcbContainer.Cutouts.ContainsKey(identifier))
                        {
                            var cutout = pcbContainer.Cutouts[identifier];
                            var logProp = new EDMDLogicProperty();
                            logProp.Value = cutout.ObjectData.IsAccepted;
                            assemblyItemPair.Value.Accept = new EDMDLogicProperty[1];
                            assemblyItemPair.Value.Accept[0] = logProp;
                            continue;
                        }
                        if (pcbContainer.Holes.ContainsKey(identifier))
                        {
                            var hole = pcbContainer.Holes[identifier];
                            var logProp = new EDMDLogicProperty();
                            logProp.Value = hole.ObjectData.IsAccepted;
                            assemblyItemPair.Value.Accept = new EDMDLogicProperty[1];
                            assemblyItemPair.Value.Accept[0] = logProp;
                            continue;
                        }
                        if (pcbContainer.ConstraintAreas.ContainsKey(identifier))
                        {
                            var area = pcbContainer.ConstraintAreas[identifier];
                            var logProp = new EDMDLogicProperty();
                            logProp.Value = area.ObjectData.IsAccepted;
                            assemblyItemPair.Value.Accept = new EDMDLogicProperty[1];
                            assemblyItemPair.Value.Accept[0] = logProp;
                            continue;
                        }
                        Log.Warning(string.Format("Object from proposal was not found: {0}", identifier.GetNameCombined()));
                        //throw new Exception(string.Format("Object from proposal was not found: {0}", objectName));
                    }
                }
                retDataSet = proposalDataSet;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if (ex.InnerException != null)
                {
                    Log.Error(ex.InnerException.Message);
                }
            }
            return retDataSet;
        }


        /// <summary>
        /// Should find response object in baseline, for mod replace assembly item, update single item, but replace all referenced items 
        /// </summary>
        /// <param name="responseDataSet"></param>
        /// <returns></returns>
        public EDMDDataSet ApplyResponse(EDMDDataSet responseDataSet)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(EDMDDataSet));
            var dataSet = (EDMDDataSet)xmlSerializer.Deserialize(_fileMStream);
            SortedEdmdItems sortedEdmdItems = GetSortedEdmdObjects(dataSet);
            var sortedResponse = GetSortedEdmdObjects(responseDataSet, sortedEdmdItems.IdWordMaxNumMap);
            EDMDDataSet retDataSet = null;
            try
            {
                Log.Information("File: " + _filename);
                Log.Information("CreatorName: " + dataSet.Header.CreatorName);
                Log.Information("Creator sys: " + dataSet.Header.CreatorSystem);
                Log.Information("initialized sorted edmd items");
                var sortedEdmdObjects = GetSortedEdmdObjects(dataSet);
                if (!sortedEdmdObjects.IsBaseline)
                {
                    throw new Exception("Cannot apply response to a non baseline dataset");
                }
                List<string> itemIdsToRemove = new List<string>();
                foreach (var assemblyItemToDelete in sortedResponse.ItemsToDelete)
                {
                    var assemblyItemResponses = sortedResponse.ItemsByObjectName[assemblyItemToDelete];
                    if (assemblyItemResponses.Accept == null || assemblyItemResponses.Accept.Length != 1)
                    {
                        throw new Exception("Accept value is undefined in response: " + assemblyItemToDelete);
                    }
                    if (!assemblyItemResponses.Accept[0].Value)
                    {
                        continue;
                    }
                    var itemToDelete = sortedEdmdItems.ItemsByObjectName[assemblyItemToDelete];
                    var mainAssemblyItem = sortedEdmdItems.AllObjectsById[itemToDelete.id];
                    var refItems = mainAssemblyItem.ItemReferences;
                    itemIdsToRemove.Add(itemToDelete.id);
                    AddReferencedItemIdRec(ref itemIdsToRemove, ref sortedEdmdItems, refItems);
                }
                dataSet.Body.Items = dataSet.Body.Items.Where(i => !itemIdsToRemove.Contains(i.id)).ToArray();
                List<EDMDObject> objectsToAdd = new List<EDMDObject>();
                foreach (var assemblyItemToAdd in sortedResponse.ItemsToAdd)
                {
                    var assemblyItemResponse = sortedResponse.ItemsByObjectName[assemblyItemToAdd];
                    if (assemblyItemResponse.Accept == null || assemblyItemResponse.Accept.Length != 1)
                    {
                        throw new Exception("Accept value is undefined in response: " + assemblyItemToAdd);
                    }
                    if (!assemblyItemResponse.Accept[0].Value)
                    {
                        continue;
                    }
                    assemblyItemResponse.Accept = null;
                    var responseItem = sortedResponse.AllObjectsById[assemblyItemResponse.id];

                    objectsToAdd.Add(responseItem.Item);
                    var refItems = responseItem.ItemReferences;
                    AddReferencedItemsRec(ref objectsToAdd, ref sortedResponse, refItems);
                }
                dataSet.Body.Items = dataSet.Body.Items.Concat(objectsToAdd).ToArray();
                retDataSet = dataSet;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if (ex.InnerException != null)
                {
                    Log.Error(ex.InnerException.Message);
                }
            }
            return retDataSet;
        }

        public bool ValidateProposal(EDMDDataSet proposalDataset, out string errors)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(EDMDDataSet));
            var dataSet = (EDMDDataSet)xmlSerializer.Deserialize(_fileMStream);
            SortedEdmdItems sortedEdmdItems = GetSortedEdmdObjects(dataSet);
            var sortedResponse = GetSortedEdmdObjects(proposalDataset, sortedEdmdItems.IdWordMaxNumMap);
            bool hadErrors = false;
            errors = String.Empty;
            try
            {
                Log.Information("File: " + _filename);
                Log.Information("CreatorName: " + dataSet.Header.CreatorName);
                Log.Information("Creator sys: " + dataSet.Header.CreatorSystem);
                Log.Information("initialized sorted edmd items");
                var sortedEdmdObjects = GetSortedEdmdObjects(dataSet);
                if (!sortedEdmdObjects.IsBaseline)
                {
                    throw new Exception("Cannot apply response to a non baseline dataset");
                }
                List<string> itemIdsToRemove = new List<string>();
                foreach (var assemblyItemToDelete in sortedResponse.ItemsToDelete)
                {
                    if(!sortedEdmdItems.ItemsByObjectName.ContainsKey(assemblyItemToDelete))
                    {
                        errors = $"{errors}{assemblyItemToDelete.ObjectName} does not exist in baseline, yet exists in modification/deletion. ";
                        hadErrors = true;
                    }
                    else
                    {
                        var itemToDelete = sortedEdmdItems.ItemsByObjectName[assemblyItemToDelete];
                        itemIdsToRemove.Add(itemToDelete.id);
                    }
                }
                List<EDMDObject> objectsToAdd = new List<EDMDObject>();
                foreach (var assemblyItemToAdd in sortedResponse.ItemsToAdd)
                {
                    if (sortedEdmdItems.ItemsByObjectName.ContainsKey(assemblyItemToAdd) && !sortedResponse.ItemsToDelete.Contains(assemblyItemToAdd))
                    {

                        errors = $"{errors}{assemblyItemToAdd.ObjectName} being added already exists in baseline. ";
                        hadErrors = true;
                    }
                    else if (!sortedEdmdItems.ItemsByObjectName.ContainsKey(assemblyItemToAdd) && sortedResponse.ItemsToDelete.Contains(assemblyItemToAdd))
                    {

                        errors = $"{errors}{assemblyItemToAdd.ObjectName} being modified doesn't exist in baseline. Related to error in modification/deletion. ";
                        hadErrors = true;
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if (ex.InnerException != null)
                {
                    Log.Error(ex.InnerException.Message);
                }
            }
            return !hadErrors;
        }

        private static void AddReferencedItemsRec(ref List<EDMDObject> objectsToAdd, ref SortedEdmdItems sortedResponse, List<string> refItems)
        {
            if (refItems != null)
            {
                foreach (var refItem in refItems)
                {
                    ItemHolder subItem = null;
                    if (!sortedResponse.AllObjectsById.TryGetValue(refItem, out subItem)
                        || subItem == null)
                    {
                        throw new Exception("Item referenced does not exist in object items." + refItem);
                    }
                    AddReferencedItemsRec(ref objectsToAdd, ref sortedResponse, subItem.ItemReferences);
                    //subItem.Item.id = CreateAndReturnLatest(ref sortedEdmdItems.IdWordMaxNumMap, subItem.Item.id);
                    objectsToAdd.Add(subItem.Item);
                }
            }
        }

        private static void AddReferencedItemIdRec(ref List<string> objectsToAdd, ref SortedEdmdItems sortedResponse, List<string> refItems)
        {
            if (refItems != null)
            {
                foreach (var refItem in refItems)
                {
                    ItemHolder subItem = null;
                    if (!sortedResponse.AllObjectsById.TryGetValue(refItem, out subItem)
                        || subItem == null)
                    {
                        throw new Exception("Item referenced does not exist in object items." + refItem);
                    }
                    AddReferencedItemIdRec(ref objectsToAdd, ref sortedResponse, subItem.ItemReferences);
                    //subItem.Item.id = CreateAndReturnLatest(ref sortedEdmdItems.IdWordMaxNumMap, subItem.Item.id);
                    if (!(subItem.Item is EDMDItem) || ((EDMDItem)subItem.Item).ItemType == EDMDItemType.assembly)
                    {
                        objectsToAdd.Add(refItem);
                    }
                }
            }
        }

        private static List<(string, string)> ReadUserProperty(EDMDItemInstance itemInstance)
        {
            List<(string, string)> userProps = new List<(string, string)>();
            foreach (EDMDUserSimpleProperty userProp in itemInstance.UserProperty)
            {
                if (userProp.Key.Length != 1)
                {
#if DEBUG
                    throw new Exception("user simple property has more keys than expected");
#else
                    Log.Error("user simple property has more keys than expected. Review this baseline.");
#endif
                }
                userProps.Add((userProp.Key.First().ObjectName, userProp.Value));
            }
            return userProps;
        }

        static Point3 CreatePoint(EDMDCartesianPoint eDMDCartesianPoint)
        {
            Point3 point = new Point3();
            point.X = eDMDCartesianPoint.X.Value;
            point.Y = eDMDCartesianPoint.Y.Value;
            if (eDMDCartesianPoint.Z != null)
            {
                point.Z = eDMDCartesianPoint.Z.Value;
            }
            return point;
        }

        private static Curve CreateCurve(EDMDCurve curve, SortedEdmdItems sortedEdmdItems)
        {
            Curve pcbCurve = null;
            switch (curve)
            {
                case EDMDCompositeCurve compositeCurve:
                    var eDMDCompositeCurve =
                    new CompositeCurve()
                    {
                        Curves = compositeCurve.Curve.Select(c => CreateCurve(sortedEdmdItems.Curves[c], sortedEdmdItems)).ToList()
                    };
                    if (compositeCurve.Thickness != null)
                    {
                        eDMDCompositeCurve.Thickness = compositeCurve.Thickness.Value;
                    }
                    pcbCurve = eDMDCompositeCurve;
                    break;
                case EDMDLine line:
                    pcbCurve = new Line()
                    {
                        Point = CreatePoint(sortedEdmdItems.Points[line.Point]),
                        Vector = CreatePoint(sortedEdmdItems.Points[line.Vector])
                    };
                    if (line.Thickness != null)
                    {
                        pcbCurve.Thickness = line.Thickness.Value;
                    }
                    break;
                case EDMDPolyLine polyline:
                    var poly = new Polyline()
                    {
                        Points = polyline.Point.Select(p => CreatePoint(sortedEdmdItems.Points[p])).ToList(),
                    };
                    if (polyline.Thickness != null)
                    {
                        poly.Thickness = polyline.Thickness.Value;
                    }
                    pcbCurve = poly;
                    break;
                case EDMDArc arc:
                    pcbCurve = new Arc()
                    {
                        StartPoint = CreatePoint(sortedEdmdItems.Points[arc.StartPoint]),
                        EndPoint = CreatePoint(sortedEdmdItems.Points[arc.EndPoint]),
                        Angle = arc.IncludeAngle.Value
                    };
                    if (arc.Thickness != null)
                    {
                        pcbCurve.Thickness = arc.Thickness.Value;
                    }
                    break;
                case EDMDCircle3Point circle3Point:
                    pcbCurve = new Circle3Point()
                    {
                        Point1 = CreatePoint(sortedEdmdItems.Points[circle3Point.P1]),
                        Point2 = CreatePoint(sortedEdmdItems.Points[circle3Point.P2]),
                        Point3 = CreatePoint(sortedEdmdItems.Points[circle3Point.P3])
                    };
                    if (circle3Point.Thickness != null)
                    {
                        pcbCurve.Thickness = circle3Point.Thickness.Value;
                    }
                    break;
                case EDMDCircleCenter circleCenter:
                    pcbCurve = new CircleCenter()
                    {
                        Center = CreatePoint(sortedEdmdItems.Points[circleCenter.Center]),
                        Diameter = circleCenter.Diameter.Value
                    };
                    if (circleCenter.Thickness != null)
                    {
                        pcbCurve.Thickness = circleCenter.Thickness.Value;
                    }
                    break;
                case EDMDEllipse ellipse:
                    pcbCurve = new Ellipse()
                    {
                        Center = CreatePoint(sortedEdmdItems.Points[ellipse.Center]),
                        SemiMajor = ellipse.SemiMajor.Value,
                        SemiMinor = ellipse.SemiMinor.Value
                    };
                    if (ellipse.Thickness != null)
                    {
                        pcbCurve.Thickness = ellipse.Thickness.Value;
                    }
                    break;
                case EDMDParabola parabola:
                    pcbCurve = new Parabola()
                    {
                        Focus = parabola.Focus.Select(f => CreatePoint(sortedEdmdItems.Points[f])).ToList(),
                        Apex = parabola.Apex.Select(f => CreatePoint(sortedEdmdItems.Points[f])).ToList()
                    };
                    if (parabola.Thickness != null)
                    {
                        pcbCurve.Thickness = parabola.Thickness.Value;
                    }
                    break;
                case EDMDHyperbola hyperbola:
                    pcbCurve = new Hyperbola()
                    {
                        Center = CreatePoint(sortedEdmdItems.Points[hyperbola.Center]),
                        SemiMajor = hyperbola.SemiMajor.Value,
                        SemiMinor = hyperbola.SemiMinor.Value
                    };
                    if (hyperbola.Thickness != null)
                    {
                        pcbCurve.Thickness = hyperbola.Thickness.Value;
                    }
                    break;
                case EDMDBSplineCurve bSplineCurve:
                    pcbCurve = new BSplineCurve()
                    {
                        Closed = bSplineCurve.ClosedCurve,
                        BSplineCurveForm = IdxPcbConverter.ConvertBSplineCurveForm(bSplineCurve.CurveForm),
                        Intersect = bSplineCurve.SelfIntersect,
                        Points = bSplineCurve.ControlPointsList.Select(p => CreatePoint(sortedEdmdItems.Points[p])).ToList()
                    };
                    if (bSplineCurve.Thickness != null)
                    {
                        pcbCurve.Thickness = bSplineCurve.Thickness.Value;
                    }
                    break;
                case EDMDTrimmedCurve trimmedCurve:
                    pcbCurve = new TrimmedCurve()
                    {
                        Curves = trimmedCurve.Curve.Select(c => CreateCurve(sortedEdmdItems.Curves[c], sortedEdmdItems)).ToList(),
                        StartPoint = CreatePoint(sortedEdmdItems.Points[trimmedCurve.Start]),
                        EndPoint = CreatePoint(sortedEdmdItems.Points[trimmedCurve.End]),
                    };
                    if (trimmedCurve.Thickness != null)
                    {
                        pcbCurve.Thickness = trimmedCurve.Thickness.Value;
                    }
                    break;
                case EDMDOffsetCurve offsetCurve:
                    pcbCurve = new OffsetCurve()
                    {
                        BasisCurve = CreateCurve(sortedEdmdItems.Curves[offsetCurve.BasisCurve], sortedEdmdItems),
                        SelfIntersect = offsetCurve.SelfIntersect,
                    };
                    if (offsetCurve.Thickness != null)
                    {
                        pcbCurve.Thickness = offsetCurve.Thickness.Value;
                    }
                    break;
                default:
                    throw new Exception(String.Format("Curve format not known {0}", curve.GetType()));
            }
            return pcbCurve;
        }

        public static bool ConvertShapeElement(out Outline outline, EDMDShapeElement shapeElement, SortedEdmdItems sortedEdmdItems)
        {
            if (!sortedEdmdItems.CurveSets.ContainsKey(shapeElement.DefiningShape))
            {
                outline = null;
                Log.Error("Contains external geometric model. Skip for now. " + shapeElement.DefiningShape);
                return false;
            }
            var curveSet = sortedEdmdItems.CurveSets[shapeElement.DefiningShape];
            outline = new Outline();
            if (shapeElement.ShapeDescriptionTransformation != null)
            {
                outline.Transformation = IdxPcbConverter.GetTransformation(shapeElement.ShapeDescriptionTransformation);
            }

            outline.UpperBound = curveSet.UpperBound.Value;
            outline.LowerBound = curveSet.LowerBound.Value;
            if (curveSet.DetailedGeometricModelElement.Length > 0)
            {
                foreach (var curveId in curveSet.DetailedGeometricModelElement)
                {
                    if (sortedEdmdItems.Curves.ContainsKey(curveId))
                    {
                        var curve = sortedEdmdItems.Curves[curveId];
                        if (!ConvertCurveToSegment(ref outline, curve, sortedEdmdItems))
                        {
                            Log.Error("Unable to convert curve to segment for " + shapeElement.DefiningShape);
                            return false;
                        }
                    }
                    else if (sortedEdmdItems.CompositeCurves.ContainsKey(curveId))
                    {
                        var compositeCurve = sortedEdmdItems.CompositeCurves[curveId];
                        outline = ConvertCompositeCurveToSegmentList(compositeCurve, sortedEdmdItems);
                        if (outline.Segments == null || outline.Segments.Count < 1)
                        {
                            Log.Error("Converted zero curvers for " + shapeElement.DefiningShape);
                            return false;
                        }

                    }
                    else
                    {
                        throw new Exception("DetailedGeometricModelElement should be contained inside composite curve or curves set: " + curveId);
                    }
                }
            }
            return true;
        }

        public static bool ConvertCurveToSegment(ref Outline outline, EDMDCurve curve, SortedEdmdItems sortedEdmdItems)
        {
            var segments = outline.Segments;
            switch (curve)
            {
                case EDMDLine line:
                    var startPoint = sortedEdmdItems.Points[line.Point];
                    var vector = sortedEdmdItems.Points[line.Vector];
                    segments.Add(new Segment(
                        new Point3(startPoint.X.Value, startPoint.Y.Value),
                        new Point3(vector.X.Value, vector.Y.Value)));
                    break;
                case EDMDPolyLine poly:
                    Point3 lastPoint = null;
                    foreach (var pointStr in poly.Point)
                    {
                        var point = sortedEdmdItems.Points[pointStr];
                        if (lastPoint == null)
                        {
                            lastPoint = new Point3(point.X.Value, point.Y.Value);
                            continue;
                        }
                        var currentPoint = new Point3(point.X.Value, point.Y.Value);
                        segments.Add(new Segment(
                            lastPoint,
                            currentPoint
                            ));
                        lastPoint = currentPoint;
                    }
                    break;
                case EDMDArc arc:
                    bool isPositiveAngle = arc.IncludeAngle.Value > 0;
                    var startPointArc = true ? sortedEdmdItems.Points[arc.StartPoint] : sortedEdmdItems.Points[arc.EndPoint];
                    var endPointArc = true ? sortedEdmdItems.Points[arc.EndPoint] : sortedEdmdItems.Points[arc.StartPoint];
                    var normalizedAngle = arc.IncludeAngle.Value % 360;
                    double angleDeg = isPositiveAngle ? normalizedAngle : normalizedAngle;
                    segments.Add(new Segment(
                        new Point3(startPointArc.X.Value, startPointArc.Y.Value),
                        new Point3(endPointArc.X.Value, endPointArc.Y.Value), angleDeg));
                    break;
                case EDMDCircle3Point circle3Point:
                    var firstPoint = sortedEdmdItems.Points[circle3Point.P1];
                    var secondPoint = sortedEdmdItems.Points[circle3Point.P2];
                    var thirdPoint = sortedEdmdItems.Points[circle3Point.P3];
                    segments.Add(new Segment(
                        new Point3(firstPoint.X.Value, firstPoint.Y.Value),
                        new Point3(secondPoint.X.Value, secondPoint.Y.Value),
                        new Point3(thirdPoint.X.Value, thirdPoint.Y.Value)));
                    break;
                    throw new NotImplementedException("Curve type circle3point not supported");
                case EDMDCircleCenter circleCenter:
                    var centerPoint = sortedEdmdItems.Points[circleCenter.Center];
                    var radius = circleCenter.Diameter.Value / 2;
                    segments.Add(new Segment(
                        new Point3(centerPoint.X.Value + radius, centerPoint.Y.Value),
                        new Point3(centerPoint.X.Value, centerPoint.Y.Value + radius),
                        new Point3(centerPoint.X.Value - radius, centerPoint.Y.Value)));
                    segments.Add(new Segment(
                        new Point3(centerPoint.X.Value - radius, centerPoint.Y.Value),
                        new Point3(centerPoint.X.Value, centerPoint.Y.Value - radius),
                        new Point3(centerPoint.X.Value + radius, centerPoint.Y.Value)));

                    break;
                default:
                    throw new NotImplementedException("Unknown curve type.");
            }

            Log.Information(segments.Last().StartPoint.X + " " + segments.Last().StartPoint.Y + " - " + segments.Last().EndPoint.X + " " + segments.Last().EndPoint.Y + " " + segments.Last().AngleDeg);
            return true;
        }

        public static Outline ConvertCompositeCurveToSegmentList(EDMDCompositeCurve compositeCurve, SortedEdmdItems sortedEdmdItems)
        {
            var outline = new Outline();
            Log.Information("Converting curve to segment list. Count: " + compositeCurve.Curve.Length);
            foreach (string curveId in compositeCurve.Curve)
            {
                var item = sortedEdmdItems.Curves[curveId];
                if (!ConvertCurveToSegment(ref outline, item, sortedEdmdItems))
                {
                    Log.Error("Converting curve list failed. Quitting the rest");
                }
            }
            Log.Information("Number of segments converted from curve: " + outline.Segments.Count);
            return outline;
        }



    }
}
