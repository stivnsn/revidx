﻿using PCBXRevision;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace EDMDSerialization
{
    public class IdxPcbConverter
    {
        private const string THICHNESS = "THICKNESS";

        private static CultureInfo _culture = new CultureInfo("en");



        public static BSplineCurveForm? ConvertBSplineCurveForm(EDMDBSplineCurveForm? bSplineCurveForm)
        {
            switch (bSplineCurveForm)
            {
                case EDMDBSplineCurveForm.PolylineForm:
                    return BSplineCurveForm.PolylineForm;
                case EDMDBSplineCurveForm.CircularArc:
                    return BSplineCurveForm.CircularArc;
                case EDMDBSplineCurveForm.EllipticArc:
                    return BSplineCurveForm.EllipticArc;
                case EDMDBSplineCurveForm.HyperbolicArc:
                    return BSplineCurveForm.HyperbolicArc;
                case EDMDBSplineCurveForm.Unspecified:
                    return BSplineCurveForm.Unspecified;
                default:
                    throw new Exception(String.Format("Unknown bSplineCurve form {0}", bSplineCurveForm.ToString()));
            }
        }

        public static EDMDBSplineCurveForm? ConvertBSplineCurveForm(BSplineCurveForm? bSplineCurveForm)
        {
            switch (bSplineCurveForm)
            {
                case BSplineCurveForm.PolylineForm:
                    return EDMDBSplineCurveForm.PolylineForm;
                case BSplineCurveForm.CircularArc:
                    return EDMDBSplineCurveForm.CircularArc;
                case BSplineCurveForm.EllipticArc:
                    return EDMDBSplineCurveForm.EllipticArc;
                case BSplineCurveForm.HyperbolicArc:
                    return EDMDBSplineCurveForm.HyperbolicArc;
                case BSplineCurveForm.Unspecified:
                    return EDMDBSplineCurveForm.Unspecified;
                default:
                    throw new Exception(String.Format("Unknown bSplineCurve form {0}", bSplineCurveForm.ToString()));
            }
        }





        public static ItemSide ConvertSideToPCBSide(string side)
        {
            switch (side)
            {
                case "ALL":
                    return ItemSide.ALL;
                case "TOP":
                    return ItemSide.TOP;
                case "BOTTOM":
                    return ItemSide.BOTTOM;
                case "BOTH":
                    return ItemSide.BOTH;
                default:
                    return ItemSide.NONE;
            }
        }
        public static string ConvertSideToPCBSide(ItemSide side)
        {
            switch (side)
            {
                case ItemSide.ALL:
                    return "ALL";
                case ItemSide.TOP:
                    return "TOP";
                case ItemSide.BOTTOM:
                    return "BOTTOM";
                case ItemSide.BOTH:
                    return "BOTH";
                default:
                    return "";
            }
        }
        public static ConstraintAreaType ConvertEDMDFunctionalPurposeToPCB(EDMDFunctionalItemShapeType type)
        {
            switch (type)
            {
                case EDMDFunctionalItemShapeType.UserArea:
                case EDMDFunctionalItemShapeType.PlacementGroupArea:
                    return ConstraintAreaType.PlacementRegion;
                case EDMDFunctionalItemShapeType.FlexibleArea:
                    return ConstraintAreaType.FlexibleArea;
                case EDMDFunctionalItemShapeType.Stiffener:
                    return ConstraintAreaType.Stiffener;
                default:
                    throw new Exception(String.Format("Unkown functional type {0}", type.ToString()));
            }
        }
        public static ConstraintAreaType ConvertEDMDKeepInPurposeToPCB(EDMDKeepConstraintPurpose purpose)
        {
            switch (purpose)
            {
                case EDMDKeepConstraintPurpose.Route:
                case EDMDKeepConstraintPurpose.Via:
                    return ConstraintAreaType.RouteOutline;
                case EDMDKeepConstraintPurpose.ComponentPlacement:
                    return ConstraintAreaType.PlaceOutline;
                case EDMDKeepConstraintPurpose.Other:
                    return ConstraintAreaType.OtherOutline;
                default:
                    throw new Exception(String.Format("Unkown purpose type {0}", purpose.ToString()));
            }
        }
        public static ConstraintAreaType ConvertEDMDKeepOutPurposeToPCB(EDMDKeepConstraintPurpose purpose)
        {
            switch (purpose)
            {
                case EDMDKeepConstraintPurpose.ComponentPlacement:
                    return ConstraintAreaType.PlaceKeepout;
                case EDMDKeepConstraintPurpose.Route:
                    return ConstraintAreaType.RouteKeepout;
                case EDMDKeepConstraintPurpose.Plane:
                    return ConstraintAreaType.PlaneKeepout;
                case EDMDKeepConstraintPurpose.Via:
                    return ConstraintAreaType.ViaKeepout;
            }
            throw new Exception(String.Format("Unkown purpose type {0}", purpose.ToString()));
        }



        public static EDMDShapeElementType? ConvertShapeElementType(ShapeElementType? shapeElementType)
        {
            switch (shapeElementType)
            {
                case ShapeElementType.BareDieComponentTerminal:
                    return EDMDShapeElementType.BareDieComponentTerminal;
                case ShapeElementType.ComponentTerminal:
                    return EDMDShapeElementType.ComponentTerminal;
                case ShapeElementType.ComponentTerminationPassageTemplateInterfaceTerminal:
                    return EDMDShapeElementType.ComponentTerminationPassageTemplateInterfaceTerminal;
                case ShapeElementType.ComponentTerminationPassageTemplateJoinTerminal:
                    return EDMDShapeElementType.ComponentTerminationPassageTemplateJoinTerminal;
                case ShapeElementType.ComponentTerminationPassageTemplateTerminal:
                    return EDMDShapeElementType.ComponentTerminationPassageTemplateJoinTerminal;
                case ShapeElementType.ConnectionZone:
                    return EDMDShapeElementType.ConnectionZone;
                case ShapeElementType.FeatureShapeElement:
                    return EDMDShapeElementType.FeatureShapeElement;
                case ShapeElementType.FiducialPartFeature:
                    return EDMDShapeElementType.FiducialPartFeature;
                case ShapeElementType.Land:
                    return EDMDShapeElementType.Land;
                case ShapeElementType.NonFeatureShapeElement:
                    return EDMDShapeElementType.NonFeatureShapeElement;
                case ShapeElementType.Package:
                    return EDMDShapeElementType.Package;
                case ShapeElementType.PackageTerminal:
                    return EDMDShapeElementType.PackageTerminal;
                case ShapeElementType.PartFeature:
                    return EDMDShapeElementType.PartFeature;
                case ShapeElementType.PartMountingFeature:
                    return EDMDShapeElementType.PartMountingFeature;
                case ShapeElementType.PartToolingFeature:
                    return EDMDShapeElementType.PartToolingFeature;
                case ShapeElementType.PrintedPartCrossSectionTemplateTerminal:
                    return EDMDShapeElementType.PrintedPartCrossSectionTemplateTerminal;
                case ShapeElementType.PrintedPartTemplateInterfaceTerminal:
                    return EDMDShapeElementType.PrintedPartTemplateInterfaceTerminal;
                case ShapeElementType.PrintedPartTemplateJoinTerminal:
                    return EDMDShapeElementType.PrintedPartTemplateJoinTerminal;
                case ShapeElementType.PrintedPartTemplateTerminal:
                    return EDMDShapeElementType.PrintedPartTemplateTerminal;
                case ShapeElementType.StructuredPrintedPartTemplateTerminal:
                    return EDMDShapeElementType.StructuredPrintedPartTemplateTerminal;
                case ShapeElementType.TestPointPartFeature:
                    return EDMDShapeElementType.TestPointPartFeature;
                case ShapeElementType.ThermalFeature:
                    return EDMDShapeElementType.ThermalFeature;
                case ShapeElementType.ViaTerminal:
                    return EDMDShapeElementType.ViaTerminal;
                case null:
                    return null;
                default:
                    throw new Exception(String.Format("Shape element type {0} not found", shapeElementType.ToString()));
            }
        }

        public static ShapeElementType? ConvertShapeElementType(EDMDShapeElementType? shapeElementType)
        {
            switch (shapeElementType)
            {
                case EDMDShapeElementType.BareDieComponentTerminal:
                    return ShapeElementType.BareDieComponentTerminal;
                case EDMDShapeElementType.ComponentTerminal:
                    return ShapeElementType.ComponentTerminal;
                case EDMDShapeElementType.ComponentTerminationPassageTemplateInterfaceTerminal:
                    return ShapeElementType.ComponentTerminationPassageTemplateInterfaceTerminal;
                case EDMDShapeElementType.ComponentTerminationPassageTemplateJoinTerminal:
                    return ShapeElementType.ComponentTerminationPassageTemplateJoinTerminal;
                case EDMDShapeElementType.ComponentTerminationPassageTemplateTerminal:
                    return ShapeElementType.ComponentTerminationPassageTemplateJoinTerminal;
                case EDMDShapeElementType.ConnectionZone:
                    return ShapeElementType.ConnectionZone;
                case EDMDShapeElementType.FeatureShapeElement:
                    return ShapeElementType.FeatureShapeElement;
                case EDMDShapeElementType.FiducialPartFeature:
                    return ShapeElementType.FiducialPartFeature;
                case EDMDShapeElementType.Land:
                    return ShapeElementType.Land;
                case EDMDShapeElementType.NonFeatureShapeElement:
                    return ShapeElementType.NonFeatureShapeElement;
                case EDMDShapeElementType.Package:
                    return ShapeElementType.Package;
                case EDMDShapeElementType.PackageTerminal:
                    return ShapeElementType.PackageTerminal;
                case EDMDShapeElementType.PartFeature:
                    return ShapeElementType.PartFeature;
                case EDMDShapeElementType.PartMountingFeature:
                    return ShapeElementType.PartMountingFeature;
                case EDMDShapeElementType.PartToolingFeature:
                    return ShapeElementType.PartToolingFeature;
                case EDMDShapeElementType.PrintedPartCrossSectionTemplateTerminal:
                    return ShapeElementType.PrintedPartCrossSectionTemplateTerminal;
                case EDMDShapeElementType.PrintedPartTemplateInterfaceTerminal:
                    return ShapeElementType.PrintedPartTemplateInterfaceTerminal;
                case EDMDShapeElementType.PrintedPartTemplateJoinTerminal:
                    return ShapeElementType.PrintedPartTemplateJoinTerminal;
                case EDMDShapeElementType.PrintedPartTemplateTerminal:
                    return ShapeElementType.PrintedPartTemplateTerminal;
                case EDMDShapeElementType.StructuredPrintedPartTemplateTerminal:
                    return ShapeElementType.StructuredPrintedPartTemplateTerminal;
                case EDMDShapeElementType.TestPointPartFeature:
                    return ShapeElementType.TestPointPartFeature;
                case EDMDShapeElementType.ThermalFeature:
                    return ShapeElementType.ThermalFeature;
                case EDMDShapeElementType.ViaTerminal:
                    return ShapeElementType.ViaTerminal;
                case null:
                    return null;
                default:
                    throw new Exception(String.Format("Shape element type {0} not found", shapeElementType.ToString()));
            }
        }


        public static Dictionary<string, string> CreatePcbUserProperties(EDMDUserProperty[] userProperties)
        {
            if (userProperties == null)
            {
                return null;
            }
            return userProperties.Select(u => (EDMDUserSimpleProperty)u).ToDictionary(s => s.Key.First().ObjectName, s => s.Value);
        }

        public static Header CreatePcbHeader(EDMDHeader edmdHeader)
        {
            var pcbHeader = new Header();
            pcbHeader.Creator = edmdHeader.Creator;
            pcbHeader.CreatorName = edmdHeader.CreatorName;
            pcbHeader.CreatorCompany = edmdHeader.CreatorCompany;
            pcbHeader.Description = edmdHeader.Description;
            pcbHeader.CreatorSystem = edmdHeader.CreatorSystem;
            if (edmdHeader.DisplayDescription != null)
            {
                pcbHeader.DisplayDescription = edmdHeader.DisplayDescription.Select(a => a.ObjectName).ToArray();
            }
            if (edmdHeader.DisplayName != null)
            {
                pcbHeader.DisplayName = edmdHeader.DisplayName.Select(a => a.ObjectName).ToArray();
            }
            pcbHeader.GlobalUnitAmperage = edmdHeader.GlobalUnitAmperage;
            pcbHeader.GlobalUnitAngle = edmdHeader.GlobalUnitAngle;
            pcbHeader.GlobalUnitAmperage = edmdHeader.GlobalUnitAmperage;
            pcbHeader.GlobalUnitCapacitance = edmdHeader.GlobalUnitCapacitance;
            pcbHeader.GlobalUnitLength = edmdHeader.GlobalUnitLength;
            pcbHeader.GlobalUnitPower = edmdHeader.GlobalUnitPower;
            pcbHeader.GlobalUnitResistance = edmdHeader.GlobalUnitResistance;
            pcbHeader.GlobalUnitVoltage = edmdHeader.GlobalUnitVoltage;
            pcbHeader.GlobalUnitWeight = edmdHeader.GlobalUnitWeight;
            pcbHeader.GlobalUnitWork = edmdHeader.GlobalUnitWork;
            pcbHeader.id = edmdHeader.id;
            pcbHeader.IsAttributeChanged = edmdHeader.IsAttributeChanged;
            pcbHeader.IsAttributeChangedSpecified = edmdHeader.IsAttributeChangedSpecified;
            pcbHeader.IsSelfContained = edmdHeader.IsSelfContained;
            pcbHeader.Name = edmdHeader.Name;
            pcbHeader.PostProcessor = edmdHeader.PostProcessor;
            pcbHeader.PostProcessorVersion = edmdHeader.PostProcessorVersion;
            pcbHeader.System = edmdHeader.System;
            pcbHeader.Text = edmdHeader.Text;
            pcbHeader.UserProperty = CreatePcbUserProperties(edmdHeader.UserProperty);
            return pcbHeader;
        }

        public static ProcessInstruction CreatePcbProcessInstruction(EDMDProcessInstruction processInstruction)
        {
            var pcbProcessInstruction = new ProcessInstruction();
            pcbProcessInstruction.Name = processInstruction.Name;
            pcbProcessInstruction.Description = processInstruction.Description;
            if (processInstruction.DisplayDescription != null)
            {
                pcbProcessInstruction.DisplayDescription = processInstruction.DisplayDescription.Select(dd => dd.ObjectName).ToArray();
            }
            if (processInstruction.DisplayName != null)
            {
                pcbProcessInstruction.DisplayName = processInstruction.DisplayName.Select(dd => dd.ObjectName).ToArray();
            }
            pcbProcessInstruction.id = processInstruction.id;
            pcbProcessInstruction.IsAttributeChanged = processInstruction.IsAttributeChanged;
            return pcbProcessInstruction;
        }


        internal static Board CreatePcbBoard(EDMDStratum stratum, EDMDItemInstance itemInstance, GeometricalShape geometricalShape, ObjectData objectData)
        {
            var board = new Board(geometricalShape, objectData);
            board.StratumType = ConvertStratumType(stratum.StratumType);
            board.SurfaceDesignationType = ConvertSurfaceDesignationType(stratum.StratumSurfaceDesignation);
            var userProperties = board.ObjectData.UserProperties = CreatePcbUserProperties(itemInstance.UserProperty);
            if (userProperties.ContainsKey(THICHNESS))
            {
                double thickness;
                double.TryParse(userProperties["THICKNESS"], NumberStyles.Any, _culture, out thickness);
                board.Thickness = thickness;
            }
            if (itemInstance.Transformation != null && !geometricalShape.Outline.Transformation.HasTransformation())
            {
                Log.Information("Board transformation found!");
                board.GeometricalShape.Outline.Transformation = GetTransformation(itemInstance.Transformation);
            }
            else
            {
                Log.Information("Board contains no transformation.");
            }
            return board;
        }

        internal static Component CreatePcbComponent(EDMDAssemblyComponent assemblyComponent, GeometricalShape geometricalShape, ObjectData objectData)
        {
            var component = new Component(geometricalShape, objectData);
            component.AssemblyComponentType = CreatePcbComponentType(assemblyComponent.AssemblyComponentType);
            SetComponentDataFromUserProps(ref component);
            component.Type = ComponentType.Electrical;
            return component;
        }

        private static void SetHoleDataFromUserProps(ref Hole hole)
        {
            if (hole.GeometricalShape.Outline != null && hole.GeometricalShape.Outline.Transformation == null)
            {
                string xLoc = "0";
                hole.ObjectData.UserProperties.TryGetValue("XLOC", out xLoc);
                string yLoc = "0";
                hole.ObjectData.UserProperties.TryGetValue("YLOC", out yLoc);
                string rotation = "0";
                hole.ObjectData.UserProperties.TryGetValue("ROTATION", out rotation);
                hole.GeometricalShape.Outline.Transformation = new Transformation();
                hole.GeometricalShape.Outline.Transformation.X = xLoc == null ? 0 : double.Parse(xLoc, _culture);
                hole.GeometricalShape.Outline.Transformation.Y = yLoc == null ? 0 : double.Parse(yLoc, _culture);
                hole.GeometricalShape.Outline.Transformation.ZRotRad = rotation == null ? 0 : double.Parse(rotation, _culture) * Math.PI / 180;
            }
            string mhName = String.Empty;
            string padstack = String.Empty;
            if (hole.ObjectData.UserProperties != null)
            {
                hole.ObjectData.UserProperties.TryGetValue("MHNAME", out mhName);
                hole.ObjectData.UserProperties.TryGetValue("PADSTACK", out padstack);
            }
            hole.MHName = mhName;
            hole.Padstack = padstack;
        }
        private static void SetComponentDataFromUserProps(ref Component component)
        {
            if (component.GeometricalShape.Outline != null && component.GeometricalShape.Outline.Transformation == null)
            {
                component.GeometricalShape.Outline.Transformation = new Transformation();
                string xLoc = "0";
                component.ObjectData.UserProperties.TryGetValue("XLOC", out xLoc);
                string yLoc = "0";
                component.ObjectData.UserProperties.TryGetValue("YLOC", out yLoc);
                string rotation = "0";
                component.ObjectData.UserProperties.TryGetValue("ROTATION", out rotation);
                component.GeometricalShape.Outline.Transformation.X = xLoc == null ? 0 : double.Parse(xLoc, _culture);
                component.GeometricalShape.Outline.Transformation.Y = yLoc == null ? 0 : double.Parse(yLoc, _culture);
                component.GeometricalShape.Outline.Transformation.ZRotRad = rotation == null ? 0 : double.Parse(rotation, _culture) * Math.PI / 180;
            }
            string pkgName;
            component.ObjectData.UserProperties.TryGetValue("PKGNAME", out pkgName);
            string partNum;
            component.ObjectData.UserProperties.TryGetValue("PARTNUM", out partNum);
            string refDes;
            component.ObjectData.UserProperties.TryGetValue("REFDES", out refDes);
            string side;
            component.ObjectData.UserProperties.TryGetValue("SIDE", out side);
            component.PackageName = pkgName;
            component.PartNumber = partNum;
            component.RefDes = refDes;
            component.Side = ConvertSideToPCBSide(side);
        }

        private static void SetConstraintAreaDataFromUserProps(ref ConstraintArea constraintArea)
        {
            string side;
            constraintArea.ObjectData.UserProperties.TryGetValue("SIDE", out side);
            constraintArea.Side = ConvertSideToPCBSide(side);
        }

        public static Transformation GetTransformation(EDMDTransformation transformation)
        {
            TransformationMatrix TransformationMatrix = new TransformationMatrix();
            TransformationMatrix.xx = transformation.xx;
            TransformationMatrix.xy = transformation.xy;
            TransformationMatrix.xz = transformation.xz.GetValueOrDefault(0);
            TransformationMatrix.yx = transformation.yx;
            TransformationMatrix.yy = transformation.yy;
            TransformationMatrix.yz = transformation.yz.GetValueOrDefault(0);
            TransformationMatrix.zx = transformation.zx.GetValueOrDefault(0);
            TransformationMatrix.zy = transformation.zy.GetValueOrDefault(0);
            TransformationMatrix.zz = transformation.zz.GetValueOrDefault(0);
            TransformationMatrix.tx = transformation.tx.Value;
            TransformationMatrix.ty = transformation.ty.Value;
            TransformationMatrix.tz = transformation.tz == null ? 0 : transformation.tz.Value;
            return new Transformation(TransformationMatrix);
        }

        internal static VersionNumber CreateVersionNumber(EDMDIdentifier[] identifiers)
        {
            if (identifiers.Length > 1)
            {
                throw new ArgumentException("Number of identifiers for item is >0. Unknown use case.");
            }
            else if (identifiers.Length == 0)
            {
                throw new ArgumentException("Number of identifiers for item is < 1. Unknown use case.");
            }
            var identifier = identifiers[0];
            var versionNumber = new VersionNumber(new Identifier(identifier.SystemScope, identifier.Number), int.Parse(identifier.Version), int.Parse(identifier.Revision), int.Parse(identifier.Sequence));
            return versionNumber;
        }
        internal static EDMDIdentifier[] CreateVersionNumber(VersionNumber versionNumber, Identifier identifier)
        {
            EDMDIdentifier edmdIdentifier = new EDMDIdentifier()
            {
                Version = versionNumber.Version.ToString(),
                Revision = versionNumber.Revision.ToString(),
                Sequence = versionNumber.Sequence.ToString(),
                Number = identifier.ObjectName.ToString(),
                SystemScope = identifier.SystemScope.ToString()
            };
            return new EDMDIdentifier[] { edmdIdentifier };

        }

        internal static ObjectData ConvertAssemblyItemData(EDMDItem assemblyItem)
        {
            var objectData = new ObjectData();
            objectData.VersionNumber = CreateVersionNumber(assemblyItem.Identifier);
            objectData.Name = assemblyItem.Name;
            objectData.Description = assemblyItem.Description;
            if (assemblyItem.DisplayDescription != null)
            {
                objectData.DisplayDescription = assemblyItem.DisplayDescription.Select(a => a.ObjectName).ToArray();
            }
            if (assemblyItem.DisplayName != null)
            {
                objectData.DisplayName = assemblyItem.DisplayName.Select(a => a.ObjectName).ToArray();
            }
            objectData.AssembleToName = assemblyItem.AssembleToName;
            return objectData;
        }
        internal static bool ConvertAssemblyItemData(ref EDMDItem assemblyItem, ObjectData objectData)
        {
            assemblyItem.Identifier = new EDMDIdentifier[] { new EDMDIdentifier {
                Version = objectData.VersionNumber.Version.ToString(),
                Revision = objectData.VersionNumber.Revision.ToString(),
                Sequence = objectData.VersionNumber.Sequence.ToString(),
            } };
            assemblyItem.Name = objectData.Name;
            assemblyItem.Description = objectData.Description;
            if (objectData.DisplayDescription != null)
            {
                assemblyItem.DisplayDescription = objectData.DisplayDescription.Select(a => new EDMDName() { ObjectName = a }).ToArray();
            }
            if (objectData.DisplayName != null)
            {
                assemblyItem.DisplayName = objectData.DisplayName.Select(a => new EDMDName() { ObjectName = a } ).ToArray();
            }
            assemblyItem.AssembleToName = objectData.AssembleToName;
            return true;
        }

        internal static ObjectData CreatePcbSingleItemObjectData(EDMDItem singleItem)
        {
            var objectData = new ObjectData();
            if (singleItem.Identifier != null)
            {
                objectData.VersionNumber = CreateVersionNumber(singleItem.Identifier);
            }
            objectData.Name = singleItem.Name;
            objectData.Description = singleItem.Description;
            if (singleItem.DisplayDescription != null)
            {
                objectData.DisplayDescription = singleItem.DisplayDescription.Select(a => a.ObjectName).ToArray();
            }
            if (singleItem.DisplayName != null)
            {
                objectData.DisplayName = singleItem.DisplayName.Select(a => a.ObjectName).ToArray();
            }
            return objectData;
        }

        internal static StratumType ConvertStratumType(EDMDStratumType stratumType)
        {
            switch (stratumType)
            {
                case EDMDStratumType.DocumentationLayerStratum:
                    return StratumType.DOCUMENT;
                case EDMDStratumType.DesignLayerStratum:
                    return StratumType.DESIGN;
                default:
                    throw new Exception(String.Format("Unkown stratum type {0}", stratumType.ToString()));
            }
        }
        internal static EDMDStratumType ConvertStratumType(StratumType stratumType)
        {
            switch (stratumType)
            {
                case StratumType.DOCUMENT:
                    return EDMDStratumType.DocumentationLayerStratum;
                case StratumType.DESIGN:
                    return EDMDStratumType.DesignLayerStratum;
                default:
                    throw new Exception(String.Format("Unkown stratum type {0}", stratumType.ToString()));
            }
        }

        internal static SurfaceDesignationType? ConvertSurfaceDesignationType(EDMDStratumSurfaceDesignation? surfaceDesignation)
        {
            switch (surfaceDesignation)
            {
                case EDMDStratumSurfaceDesignation.PrimarySurface:
                    return SurfaceDesignationType.PRIMARY;
                case EDMDStratumSurfaceDesignation.SecondarySurface:
                    return SurfaceDesignationType.SECONDARY;
                case EDMDStratumSurfaceDesignation.AverageSurface:
                    return SurfaceDesignationType.AVERAGE;
                case null:
                    return null;
                default:
                    throw new Exception(String.Format("Unkown surface designation type {0}", surfaceDesignation.ToString()));
            }
        }

        internal static EDMDStratumSurfaceDesignation? ConvertSurfaceDesignationType(SurfaceDesignationType? surfaceDesignation)
        {
            switch (surfaceDesignation)
            {
                case SurfaceDesignationType.PRIMARY:
                    return EDMDStratumSurfaceDesignation.PrimarySurface;
                case SurfaceDesignationType.SECONDARY:
                    return EDMDStratumSurfaceDesignation.SecondarySurface;
                case SurfaceDesignationType.AVERAGE:
                    return EDMDStratumSurfaceDesignation.AverageSurface;
                case null:
                    return null;
                default:
                    throw new Exception(String.Format("Unkown surface designation type {0}", surfaceDesignation.ToString()));
            }
        }

        internal static AssemblyComponentType? CreatePcbComponentType(EDMDAssemblyComponentType? componentType)
        {
            switch (componentType)
            {
                case EDMDAssemblyComponentType.Physical:
                    return AssemblyComponentType.PHYSICAL;
                case EDMDAssemblyComponentType.AssemblyGroupComponent:
                    return AssemblyComponentType.ASSEMBLYGROUP;
                case EDMDAssemblyComponentType.LaminateComponent:
                    return AssemblyComponentType.LAMINATE;
                case EDMDAssemblyComponentType.Padstack:
                    return AssemblyComponentType.PADSTACK;
                case EDMDAssemblyComponentType.ThermalComponent:
                    return AssemblyComponentType.THERMAL;
                case null:
                    return null;
                default:
                    throw new Exception(String.Format("Unkown component type {0}", componentType.ToString()));
            }
        }

        internal static ConstraintArea CreatePcbKeepInArea(EDMDKeepIn keepIn, GeometricalShape geometricalShape, ObjectData objectData)
        {
            var keepInZone = new ConstraintArea(geometricalShape, objectData);
            if (keepIn.Purpose == null || keepIn.Purpose.Length != 1)
            {
                keepInZone.Purpose = ConstraintAreaType.Undefined;
            }
            else
            {
                keepInZone.Purpose = ConvertEDMDKeepInPurposeToPCB(keepIn.Purpose[0]);
            }
            SetConstraintAreaDataFromUserProps(ref keepInZone);
            return keepInZone;
        }

        internal static ConstraintArea CreatePcbFunctionalItemArea(EDMDFunctionalItemShape functionalItemShape, GeometricalShape geometricalShape, ObjectData objectData)
        {
            var keepInZone = new ConstraintArea(geometricalShape, objectData);
            if (functionalItemShape.FunctionalItemShapeTypeSpecified && functionalItemShape.FunctionalItemShapeType != null)
            {
                keepInZone.Purpose = ConvertEDMDFunctionalPurposeToPCB(functionalItemShape.FunctionalItemShapeType.Value);
            }
            else
            {
                keepInZone.Purpose = ConstraintAreaType.Undefined;
            }
            SetConstraintAreaDataFromUserProps(ref keepInZone);
            return keepInZone;
        }

        internal static ConstraintArea CreatePcbKeepOutArea(EDMDKeepOut keepOut, GeometricalShape geometricalShape, ObjectData objectData)
        {
            ConstraintArea keepOutZone = new ConstraintArea(geometricalShape, objectData);
            if (keepOut.Purpose == null || keepOut.Purpose.Length != 1)
            {
                keepOutZone.Purpose = ConstraintAreaType.Undefined;
            }
            keepOutZone.Purpose = ConvertEDMDKeepOutPurposeToPCB(keepOut.Purpose[0]);
            SetConstraintAreaDataFromUserProps(ref keepOutZone);
            return keepOutZone;
        }

        internal static ConstraintArea CreatePcbNonFeatureArea(EDMDNonFeatureItemShape nonFeatureItemShape, GeometricalShape geometricalShape, ObjectData objectData)
        {
            ConstraintArea otherOutlineArea = new ConstraintArea(geometricalShape, objectData);
            otherOutlineArea.Purpose = ConstraintAreaType.OtherOutline;
            SetConstraintAreaDataFromUserProps(ref otherOutlineArea);
            return otherOutlineArea;
        }

        internal static Cutout CreatePcbCutout(EDMDInterStratumFeature interStratumFeature, GeometricalShape geometricalShape, ObjectData objectData)
        {
            var cutout = new Cutout(geometricalShape, objectData);
            cutout.CutoutInterStratumType = ConvertInterStratumType(interStratumFeature.InterStratumFeatureType);
            return cutout;
        }

        private static InterStratumType ConvertInterStratumType(EDMDInterStratumFeatureType eDMDInterStratumFeatureType)
        {
            switch (eDMDInterStratumFeatureType)
            {
                case EDMDInterStratumFeatureType.Cutout:
                    return InterStratumType.Cutout;
                case EDMDInterStratumFeatureType.CutoutEdgeSegment:
                    return InterStratumType.CutoutEdgeSegment;
                case EDMDInterStratumFeatureType.MilledCutout:
                    return InterStratumType.MilledCutout;
                case EDMDInterStratumFeatureType.PlatedCutout:
                    return InterStratumType.PlatedCutout;
                case EDMDInterStratumFeatureType.PartiallyPlatedCutout:
                    return InterStratumType.PartiallyPlatedCutout;
                case EDMDInterStratumFeatureType.PlatedCutoutEdgeSegment:
                    return InterStratumType.PlatedCutoutEdgeSegment;
                case EDMDInterStratumFeatureType.PhysicalConnectivityInterruptingCutout:
                    return InterStratumType.PhysicalConnectivityInterruptingCutout;
                case EDMDInterStratumFeatureType.CustomElectroMechanicalComponent:
                    return InterStratumType.CustomElectroMechanicalComponent;
                case EDMDInterStratumFeatureType.MechanicalComponent:
                    return InterStratumType.MechanicalComponent;
                case EDMDInterStratumFeatureType.StratumFeatureTemplateComponent:
                    return InterStratumType.StratumFeatureTemplateComponent;
                case EDMDInterStratumFeatureType.UnsupportedPassage:
                    return InterStratumType.UnsupportedPassage;
                case EDMDInterStratumFeatureType.PlatedInterStratumFeature:
                    return InterStratumType.PlatedInterStratumFeature;
                case EDMDInterStratumFeatureType.ComponentTerminationPassage:
                    return InterStratumType.ComponentTerminationPassage;
                case EDMDInterStratumFeatureType.FilledVia:
                    return InterStratumType.FilledVia;
                case EDMDInterStratumFeatureType.Via:
                    return InterStratumType.Via;
                default:
                    return InterStratumType.Unknown;
            }
        }

        internal static Component CreatePcbMechanicalComponent(EDMDFunctionalItemShape functionalItemShape, GeometricalShape geometricalShape, ObjectData objectData)
        {
            var mechanicalComponent = new Component(geometricalShape, objectData);
            mechanicalComponent.Type = ComponentType.Mechanical;
            return mechanicalComponent;
        }

        internal static Hole CreatePcbViaHole(EDMDInterStratumFeature interStratumFeature, GeometricalShape geometricalShape, ObjectData objectData)
        {
            var hole = new Hole(geometricalShape, objectData);
            hole.HoleType = HoleType.Via;
            hole.HoleInterStratumType = ConvertInterStratumType(interStratumFeature.InterStratumFeatureType);
            SetHoleDataFromUserProps(ref hole);
            return hole;
        }


        internal static Hole CreatePcbMountingHole(EDMDInterStratumFeature interStratumFeature, GeometricalShape geometricalShape, ObjectData objectData)
        {
            var hole = new Hole(geometricalShape, objectData);
            hole.HoleType = HoleType.MountingHole;
            hole.HoleInterStratumType = ConvertInterStratumType(interStratumFeature.InterStratumFeatureType);
            SetHoleDataFromUserProps(ref hole);
            return hole;
        }

        internal static Hole CreatePcbPinHole(EDMDInterStratumFeature interStratumFeature, GeometricalShape geometricalShape, ObjectData objectData)
        {
            var hole = new Hole(geometricalShape, objectData);
            hole.HoleType = HoleType.Pin;
            hole.HoleInterStratumType = ConvertInterStratumType(interStratumFeature.InterStratumFeatureType);
            SetHoleDataFromUserProps(ref hole);
            return hole;
        }
    }
}
