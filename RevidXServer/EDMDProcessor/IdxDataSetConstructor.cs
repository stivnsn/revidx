﻿using PCBXRevision;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDMDSerialization
{
    public class EdmdObjectCounter
    {
        public static readonly string ITEM = "ITEM";
        public static readonly string POINT = "PT";
        public static readonly string LINE = "LINE";
        public static readonly string POLYLINE = "POLYLN";
        public static readonly string ARC = "ARC";
        public static readonly string CURVE = "CURVE";
        public static readonly string CIRCLE = "CIRCLE";
        public static readonly string STRATUM = "STRATUM";
        public static readonly string CURVESET = "CURVESET";
        public static readonly string COMPOSITECURVE = "COMPOSITECURVE";
        public static readonly string SHAPE = "SHAPE";
        public static readonly string HOLE = "HOLE";
        public static readonly string AREA = "AREA";
        public static readonly string COMPONENT = "COMP";


        Dictionary<string, int> objectCounter = new Dictionary<string, int>();


        public string GenerateNewId(string item)
        {
            int id = 1;
            if (objectCounter.ContainsKey(item))
            {
                id = ++objectCounter[item];
            }
            else
            {
                objectCounter.Add(item, id);
            }
            return string.Format("{0}{1}", item, id);
        }
    }
    public class IdxDataSetConstructor
    {

        EdmdObjectCounter counter = new EdmdObjectCounter();
        public Dictionary<object, string> AssignedObjects { get; set; } = new Dictionary<object, string>();

        List<EDMDObject> Items { get; set; } = new List<EDMDObject>();

        Dictionary<string, string> LayerItems { get; set; } = new Dictionary<string, string>();
        public EDMDDataSet ReconstructIdxFile(PCBContainer pcbContainer)
        {
            try
            {
                var edmdDataSet = new EDMDDataSet();
                edmdDataSet.Header = CreateEdmdHeader(pcbContainer.Header);
                var processInstructions = new List<EDMDProcessInstruction>();
                foreach (var pi in pcbContainer.ProcessInstructions)
                {
                    processInstructions.Add(CreateEdmdProcessInstruction(pi));
                }
                edmdDataSet.ProcessInstruction = processInstructions.ToArray();
                foreach (var layer in pcbContainer.Layers.Values)
                {
                    var result = CreateEdmdLayer(layer);
                    if (!result)
                    {
                        throw new Exception("Creating Edmd Layer failed");
                    }
                }
                foreach (var layerStackup in pcbContainer.LayerStackups.Values)
                {
                    var result = CreateEdmdLayerStackup(layerStackup, pcbContainer.Layers);
                    if (!result)
                    {
                        throw new Exception("Creating Edmd Layer failed");
                    }
                }
                if (pcbContainer.Board != null)
                {
                    var result = CreateEdmdBoard(pcbContainer.Board);
                    if (!result)
                    {

                        throw new Exception("Creating Edmd Board failed");
                    }
                }
                foreach (var cutout in pcbContainer.Cutouts)
                {
                }
                foreach (var hole in pcbContainer.Holes)
                {
                }
                foreach (var area in pcbContainer.ConstraintAreas)
                {
                }
                foreach (var component in pcbContainer.Components)
                {

                }
                edmdDataSet.Body = new EDMDDataSetBody();
                edmdDataSet.Body.Items = Items.ToArray();
                return edmdDataSet;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            return null;
        }

        public static EDMDHeader CreateEdmdHeader(Header pcbHeader)
        {
            var edmdHeader = new EDMDHeader();
            edmdHeader.Creator = pcbHeader.Creator;
            edmdHeader.CreatorCompany = pcbHeader.CreatorCompany;
            edmdHeader.CreatorName = pcbHeader.CreatorName;
            edmdHeader.Description = pcbHeader.Description;
            edmdHeader.CreatorSystem = pcbHeader.CreatorSystem;
            if (pcbHeader.DisplayDescription != null)
            {
                edmdHeader.DisplayDescription = pcbHeader.DisplayDescription.Select(a => new EDMDName() { ObjectName = a }).ToArray();
            }
            if (pcbHeader.DisplayName != null)
            {
                edmdHeader.DisplayName = pcbHeader.DisplayName.Select(a => new EDMDName() { ObjectName = a }).ToArray();
            }
            edmdHeader.GlobalUnitAmperage = pcbHeader.GlobalUnitAmperage;
            edmdHeader.GlobalUnitAngle = pcbHeader.GlobalUnitAngle;
            edmdHeader.GlobalUnitAmperage = pcbHeader.GlobalUnitAmperage;
            edmdHeader.GlobalUnitCapacitance = pcbHeader.GlobalUnitCapacitance;
            edmdHeader.GlobalUnitLength = pcbHeader.GlobalUnitLength;
            edmdHeader.GlobalUnitPower = pcbHeader.GlobalUnitPower;
            edmdHeader.GlobalUnitResistance = pcbHeader.GlobalUnitResistance;
            edmdHeader.GlobalUnitVoltage = pcbHeader.GlobalUnitVoltage;
            edmdHeader.GlobalUnitWeight = pcbHeader.GlobalUnitWeight;
            edmdHeader.GlobalUnitWork = pcbHeader.GlobalUnitWork;
            edmdHeader.id = pcbHeader.id;
            edmdHeader.IsAttributeChanged = pcbHeader.IsAttributeChanged;
            edmdHeader.IsAttributeChangedSpecified = pcbHeader.IsAttributeChangedSpecified;
            edmdHeader.IsSelfContained = pcbHeader.IsSelfContained;
            edmdHeader.Name = pcbHeader.Name;
            edmdHeader.PostProcessor = pcbHeader.PostProcessor;
            edmdHeader.PostProcessorVersion = pcbHeader.PostProcessorVersion;
            edmdHeader.System = pcbHeader.System;
            edmdHeader.Text = pcbHeader.Text;
            edmdHeader.UserProperty = CreateEdmdUserProperties(pcbHeader.UserProperty);
            return edmdHeader;
        }


        public static EDMDUserProperty[] CreateEdmdUserProperties(Dictionary<string, string> userProperties)
        {
            if (userProperties == null)
            {
                return null;
            }
            return userProperties.Select(s => new EDMDUserSimpleProperty()
            {
                Key = new EDMDName[] {  new EDMDName()
                {
                    ObjectName = s.Key
                } },
                ValidValue = new string[] { s.Value }
            }
            ).ToArray();
        }

        public static EDMDProcessInstruction CreateEdmdProcessInstruction(ProcessInstruction processInstruction)
        {
            var edmdProcessInstruction = new EDMDProcessInstruction();
            edmdProcessInstruction.Name = processInstruction.Name;
            edmdProcessInstruction.Description = processInstruction.Description;
            if (processInstruction.DisplayDescription != null)
            {
                edmdProcessInstruction.DisplayDescription = processInstruction.DisplayDescription.Select(dd => new EDMDName() { ObjectName = dd }).ToArray();
            }
            if (processInstruction.DisplayName != null)
            {
                edmdProcessInstruction.DisplayName = processInstruction.DisplayName.Select(dd => new EDMDName() { ObjectName = dd }).ToArray();
            }
            edmdProcessInstruction.id = processInstruction.id;
            edmdProcessInstruction.IsAttributeChanged = processInstruction.IsAttributeChanged;
            return edmdProcessInstruction;
        }

        private string CreatePoint(Point3 point)
        {
            EDMDCartesianPoint edmdPoint = new EDMDCartesianPoint();
            edmdPoint.id = counter.GenerateNewId(EdmdObjectCounter.POINT);
            edmdPoint.X = new EDMDLengthProperty() { Value = point.X };
            edmdPoint.Y = new EDMDLengthProperty() { Value = point.Y };
            edmdPoint.Z = new EDMDLengthProperty() { Value = point.Z };
            Items.Add(edmdPoint);
            return edmdPoint.id;

        }

        private string CreateEdmdGeometry(CurveSet curveSet)
        {
            EDMDCurveSet2d curveSet2D = new EDMDCurveSet2d();
            curveSet2D.id = counter.GenerateNewId(EdmdObjectCounter.CURVESET);
            curveSet2D.LowerBound = new EDMDLengthProperty() { Value = curveSet.LowerBound };
            curveSet2D.UpperBound = new EDMDLengthProperty() { Value = curveSet.UpperBound };
            curveSet2D.ShapeDescriptionType = curveSet.ShapeDescriptionType == ShapeDescriptionType.External ? EDMDShapeDescriptionType.ExternalGeometricModel : EDMDShapeDescriptionType.GeometricModel;
            curveSet2D.DetailedGeometricModelElement = curveSet.Curves.Select(c => CreateCurve(c)).ToArray();
            Items.Add(curveSet2D);
            return curveSet2D.id;
        }

        private string CreateCurve(Curve curve)
        {
            EDMDCurve edmdCurve = null;
            switch (curve)
            {
                case CompositeCurve compositeCurve:
                    var eDMDCompositeCurve =
                    new EDMDCompositeCurve()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.COMPOSITECURVE),
                        Thickness = new EDMDLengthProperty() { Value = compositeCurve.Thickness }
                        ,
                        Curve = compositeCurve.Curves.Select(c => CreateCurve(c)).ToArray()
                    };
                    edmdCurve = eDMDCompositeCurve;
                    break;
                case Line line:
                    edmdCurve = new EDMDLine()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.LINE),
                        Thickness = new EDMDLengthProperty()
                        { Value = line.Thickness },
                        Point = CreatePoint(line.Point),
                        Vector = CreatePoint(line.Vector)
                    };

                    break;
                case Polyline polyline:
                    var poly = new EDMDPolyLine()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.POLYLINE),
                        Thickness = new EDMDLengthProperty() { Value = polyline.Thickness },
                        Point = polyline.Points.Select(p => CreatePoint(p)).ToArray()
                    };
                    edmdCurve = poly;
                    break;
                case Arc arc:
                    edmdCurve = new EDMDArc()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.ARC),
                        Thickness = new EDMDLengthProperty() { Value = arc.Thickness },
                        StartPoint = CreatePoint(arc.StartPoint),
                        EndPoint = CreatePoint(arc.EndPoint),
                        IncludeAngle = new EDMDAngleProperty() { Value = arc.Angle }

                    };
                    break;
                case Circle3Point circle3Point:
                    edmdCurve = new EDMDCircle3Point()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.CIRCLE),
                        Thickness = new EDMDLengthProperty() { Value = circle3Point.Thickness },
                        P1 = CreatePoint(circle3Point.Point1),
                        P2 = CreatePoint(circle3Point.Point2),
                        P3 = CreatePoint(circle3Point.Point3)
                    };
                    break;
                case CircleCenter circleCenter:

                    edmdCurve = new EDMDCircleCenter()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.CIRCLE),
                        Thickness = new EDMDLengthProperty() { Value = circleCenter.Thickness },
                        Center = CreatePoint(circleCenter.Center),
                        Diameter = new EDMDLengthProperty() { Value = circleCenter.Diameter }
                    };
                    break;
                case Ellipse ellipse:
                    edmdCurve = new EDMDEllipse()
                    {

                        id = counter.GenerateNewId(EdmdObjectCounter.CURVE),
                        Thickness = new EDMDLengthProperty() { Value = ellipse.Thickness },
                        Center = CreatePoint(ellipse.Center),
                        SemiMajor = new EDMDLengthProperty() { Value = ellipse.SemiMajor },
                        SemiMinor = new EDMDLengthProperty() { Value = ellipse.SemiMinor }
                    };
                    break;
                case Parabola parabola:
                    edmdCurve = new EDMDParabola()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.CURVE),
                        Thickness = new EDMDLengthProperty() { Value = parabola.Thickness },
                        Focus = parabola.Focus.Select(f => CreatePoint(f)).ToArray(),
                        Apex = parabola.Apex.Select(f => CreatePoint(f)).ToArray()
                    };
                    break;
                case Hyperbola hyperbola:
                    edmdCurve = new EDMDHyperbola()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.CURVE),
                        Thickness = new EDMDLengthProperty() { Value = hyperbola.Thickness },
                        Center = CreatePoint(hyperbola.Center),
                        SemiMajor = new EDMDLengthProperty() { Value = hyperbola.SemiMajor },
                        SemiMinor = new EDMDLengthProperty() { Value = hyperbola.SemiMinor }
                    };
                    break;
                case BSplineCurve bSplineCurve:
                    edmdCurve = new EDMDBSplineCurve()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.CURVE),
                        Thickness = new EDMDLengthProperty() { Value = bSplineCurve.Thickness },
                        ClosedCurve = bSplineCurve.Closed,
                        CurveForm = IdxPcbConverter.ConvertBSplineCurveForm(bSplineCurve.BSplineCurveForm),
                        SelfIntersect = bSplineCurve.Intersect,
                        ControlPointsList = bSplineCurve.Points.Select(p => CreatePoint(p)).ToArray()
                    };
                    break;
                case TrimmedCurve trimmedCurve:
                    edmdCurve = new EDMDTrimmedCurve()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.CURVE),
                        Curve = trimmedCurve.Curves.Select(c => CreateCurve(c)).ToArray(),
                        Start = CreatePoint(trimmedCurve.StartPoint),
                        End = CreatePoint(trimmedCurve.EndPoint),
                        Thickness = new EDMDLengthProperty() { Value = trimmedCurve.Thickness }
                    };
                    break;
                case OffsetCurve offsetCurve:
                    edmdCurve = new EDMDOffsetCurve()
                    {
                        id = counter.GenerateNewId(EdmdObjectCounter.CURVE),
                        BasisCurve = CreateCurve(offsetCurve.BasisCurve),
                        SelfIntersect = offsetCurve.SelfIntersect,
                        Thickness = new EDMDLengthProperty() { Value = offsetCurve.Thickness }
                    };
                    break;
                default:
                    throw new Exception(String.Format("Curve format not known {0}", curve.GetType()));
            }
            return edmdCurve.id;
        }

        private bool CreateEdmdBoard(Board board)
        {

            var shapeStrings = new List<string>();
            foreach (var curveSet in board.GeometricalShape.CurveSets)
            {
                var edmdShape = CreateEdmdShape(curveSet);
                Items.Add(edmdShape);
                shapeStrings.Add(edmdShape.id);
            }
            EDMDStratum eDMDStratum = new EDMDStratum();
            eDMDStratum.id = counter.GenerateNewId(EdmdObjectCounter.STRATUM);
            eDMDStratum.ShapeElement = shapeStrings.ToArray();
            eDMDStratum.StratumType = IdxPcbConverter.ConvertStratumType(board.StratumType);
            eDMDStratum.StratumSurfaceDesignation = IdxPcbConverter.ConvertSurfaceDesignationType(board.SurfaceDesignationType);
            eDMDStratum.StratumSurfaceDesignationSpecified = eDMDStratum.StratumSurfaceDesignation.HasValue;
            Items.Add(eDMDStratum);
            var edmdItem = CreateEdmdItem(board.ObjectData);
            edmdItem.Shape = eDMDStratum.id;
            Items.Add(edmdItem);
            return true;
        }

        private EDMDItem CreateEdmdItem(ObjectData objectData)
        {
            EDMDItem singleItem = new EDMDItem();
            singleItem.id = counter.GenerateNewId(EdmdObjectCounter.ITEM);
            Items.Add(singleItem);
            var itemInstance = new EDMDItemInstance();
            itemInstance.Item = singleItem.id;
            itemInstance.InstanceName = new EDMDName[]
            {
                new EDMDName()
                {
                    SystemScope = objectData.Identifier.SystemScope,
                    ObjectName = objectData.Identifier.ObjectName
                }
            };
            EDMDItem edmdItem = new EDMDItem();
            edmdItem.Name = objectData.Name;
            IdxPcbConverter.ConvertAssemblyItemData(ref edmdItem, objectData);
            edmdItem.ItemType = EDMDItemType.assembly;
            edmdItem.id = counter.GenerateNewId(EdmdObjectCounter.ITEM);
            edmdItem.ItemInstance = new EDMDItemInstance[] { itemInstance };
            edmdItem.Name = objectData.Name;
            edmdItem.ItemType = EDMDItemType.assembly;
            edmdItem.AssembleToName = objectData.AssembleToName;
            edmdItem.Name = objectData.Name;
            edmdItem.Identifier = IdxPcbConverter.CreateVersionNumber(objectData.VersionNumber, objectData.Identifier);
            return edmdItem;
        }

        private EDMDObject CreateEdmdShape(CurveSet curveSet)
        {
            EDMDShapeElement edmdShape = new EDMDShapeElement();
            edmdShape.id = counter.GenerateNewId(EdmdObjectCounter.SHAPE);
            edmdShape.DefiningShape = CreateEdmdGeometry(curveSet);
            edmdShape.ShapeElementType = IdxPcbConverter.ConvertShapeElementType(curveSet.ShapeElementType);
            return edmdShape;
        }

        private bool CreateEdmdLayer(Layer layer)
        {
            EDMDItem edmdItem = new EDMDItem();
            edmdItem.id = counter.GenerateNewId(EdmdObjectCounter.ITEM);
            edmdItem.Name = layer.ObjectData.Name;
            edmdItem.ItemType = EDMDItemType.assembly;
            LayerItems.Add(layer.ObjectData.Name, edmdItem.id);
            Items.Add(edmdItem);
            return true;
        }
        private bool CreateEdmdLayerStackup(LayerStackup layerStackup, Dictionary<string, Layer> layers)
        {
            EDMDItem edmdItem = new EDMDItem();
            edmdItem.Name = layerStackup.Name;
            edmdItem.ItemType = EDMDItemType.assembly;
            edmdItem.ItemInstance = new EDMDItemInstance[layerStackup.Layers.Count];
            List<EDMDItemInstance> itemInstances = new List<EDMDItemInstance>();
            foreach (var layerName in layerStackup.Layers)
            {
                var layer = layers[layerName];
                //EDMDItem edmdItemLayer = new EDMDItem();
                //edmdItemLayer.id = counter.GenerateNewId(EdmdObjectCounter.ITEM);
                //Items.Add(edmdItemLayer);
                EDMDItemInstance itemInstance = new EDMDItemInstance();
                itemInstance.Item = LayerItems[layer.ObjectData.Name];
                itemInstance.UserProperty = CreateEdmdUserProperties(layer.ObjectData.UserProperties);
                itemInstances.Add(itemInstance);
            }
            edmdItem.ItemInstance = itemInstances.ToArray();
            Items.Add(edmdItem);
            return true;
        }
    }
}
