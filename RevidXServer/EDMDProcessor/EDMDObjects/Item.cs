﻿using PCBXRevision;
using System;
using System.Collections.Generic;
using System.Text;

namespace EDMDSerialization.EDMDObjects
{

    public class SystemScopeObjectNameIdentificator
    {
        public SystemScopeObjectNameIdentificator(string systemScope, string objectName)
        {
            SystemScope = systemScope;
            ObjectName = objectName;
        }


        public string SystemScope { get; set; }
        public string ObjectName { get; set; }
    }

    public class ItemHolder
    {
        public ItemHolder(EDMDObject item, List<string> itemReferences = null, string systemScope = null, string objectName = null)
        {
            Item = item;
            SystemScope = systemScope;
            ObjectName = objectName;
            ItemReferences = itemReferences;
        }
        public string SystemScope { get; set; }
        public string ObjectName { get; set; }
        public EDMDObject Item { get; set; }
        public List<string> ItemReferences { get; set; }
    }

    public class SortedEdmdItems
    {

        public bool IsBaseline { get; set; }
        public List<Identifier> ItemsToDelete { get; set; } = new List<Identifier>();
        public List<Identifier> ItemsToAdd { get; set; } = new List<Identifier>();

        public  SortedEdmdItems()
        { }
        public Dictionary<string, int> IdWordMaxNumMap = new Dictionary<string, int>();

        public IDictionary<string, EDMDFunctionalItemShape> FunctionalItemShapes = new Dictionary<string, EDMDFunctionalItemShape>();
        public IDictionary<string, EDMDNonFeatureItemShape> NonFeatureItemShapes = new Dictionary<string, EDMDNonFeatureItemShape>();
        public IDictionary<string, EDMDKeepOut> KeepOuts = new Dictionary<string, EDMDKeepOut>();
        public IDictionary<string, EDMDKeepIn> KeepIns = new Dictionary<string, EDMDKeepIn>();
        public IDictionary<string, EDMDInterStratumFeature> InterStratumFeatures = new Dictionary<string, EDMDInterStratumFeature>();
        public IDictionary<string, EDMDAssemblyComponent> AssemblyComponents = new Dictionary<string, EDMDAssemblyComponent>();
        public IDictionary<string, EDMDStratumTechnology> StratumTechnologies = new Dictionary<string, EDMDStratumTechnology>();
        public IDictionary<string, EDMDSystem> Systems = new Dictionary<string, EDMDSystem>();
        public IDictionary<string, EDMDExternalSource> ExternalSources = new Dictionary<string, EDMDExternalSource>();
        public IDictionary<string, EDMDExternalGeometricModel> ExternalGeometricModels = new Dictionary<string, EDMDExternalGeometricModel>();
        public IDictionary<string, EDMDPerson> Persons = new Dictionary<string, EDMDPerson>();
        public IDictionary<string, EDMDUnit> Units = new Dictionary<string, EDMDUnit>();
        public IDictionary<string, EDMDCompositeCurve> CompositeCurves = new Dictionary<string, EDMDCompositeCurve>();
        public IDictionary<string, EDMDCartesianPoint> Points = new Dictionary<string, EDMDCartesianPoint>();
        public IDictionary<string, EDMDCurve> Curves = new Dictionary<string, EDMDCurve>();
        public IDictionary<string, EDMDStratum> DesignLayerStratums = new Dictionary<string, EDMDStratum>();
        public IDictionary<string, EDMDStratum> Stratums = new Dictionary<string, EDMDStratum>();
        public IDictionary<string, EDMDShapeElement> ShapeElements = new Dictionary<string, EDMDShapeElement>();
        public IDictionary<string, EDMDItemShape> ItemShapes = new Dictionary<string, EDMDItemShape>();
        public IDictionary<string, EDMDItem> Items = new Dictionary<string, EDMDItem>();
        public IDictionary<string, EDMDItem> LayerItems = new Dictionary<string, EDMDItem>();
        public IDictionary<string, EDMDItem> SingleItems = new Dictionary<string, EDMDItem>();
        public IDictionary<string, EDMDItem> AssemblyItems = new Dictionary<string, EDMDItem>();
        public IDictionary<(string, string), EDMDItemInstance> ItemInstances = new Dictionary<(string, string), EDMDItemInstance>();
        public IDictionary<(string, string), string> UserProperties = new Dictionary<(string, string), string>();
        public IDictionary<Identifier, EDMDItem> ItemsByObjectName = new Dictionary<Identifier, EDMDItem>();
        public IDictionary<string, EDMDCurveSet2d> CurveSets = new Dictionary<string, EDMDCurveSet2d>();
        public IDictionary<(string, string), EDMDChange> Changes = new Dictionary<(string, string), EDMDChange>();


        public IDictionary<string, ItemHolder> AllObjectsById = new Dictionary<string, ItemHolder>();
    }

    public class Object
    {

    }

    public class DataSet : Object
    {
        public Body Body;
    }

    public class Body : Object
    {
        public string id { get; set; }

        public bool isAttributeChanged { get; set; }

        public bool isAttributeChangedSpecified { get; set; }

        public IEnumerable<string> text { get; set; }

        public IEnumerable<Object> Items;
    }

    public class ItemInstance : Object
    {
        public IEnumerable<string> instanceName { get; set; }

        public EDMDTransformation transformation { get; set; }

        public string item { get; set; }
    }

    public class Item : Object
    {

        public EDMDItemType itemType { get; set; }

        public EDMDIdentifier[] identifier { get; set; }

        public IEnumerable<string> packageName { get; set; }

        public IEnumerable<ItemInstance> itemInstance { get; set; }

        public string shape { get; set; }

        public EDMDTransformation shapeTransformation { get; set; }

        public EDMDLogicProperty[] accept { get; set; }

        public EDMDClearanceStateProperty[] clearanceState { get; set; }

        public EDMDLogicProperty[] baseLine { get; set; }

        public string referenceName { get; set; }

        public string assembleToName { get; set; }
    }

    //public class TransformationUnused
    //{
    //    private EDMDTransformationType transformationTypeField;

    //    public double xx { get; set; }

    //    public double xy { get; set; }

    //    public System.Nullable<double> xz { get; set; }

    //    public bool xzSpecified { get; set; }

    //    public double yx { get; set; }

    //    public double yy { get; set; }

    //    public System.Nullable<double> yz { get; set; }

    //    public bool yzSpecified { get; set; }

    //    public System.Nullable<double> zx { get; set; }

    //    public bool zxSpecified { get; set; }

    //    public System.Nullable<double> zy { get; set; }

    //    public bool zySpecified { get; set; }

    //    public System.Nullable<double> zz { get; set; }

    //    public bool zzSpecified { get; set; }

    //    public EDMDLengthProperty tx { get; set; }

    //    public EDMDLengthProperty ty { get; set; }

    //    public EDMDLengthProperty tz { get; set; }
    //}
}
