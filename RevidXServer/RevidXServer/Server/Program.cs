﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RevidXServer.Server.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using System.IO;
using Microsoft.AspNetCore.Identity;
using RevidXServer.Server.Models;

namespace RevidXServer.Server
{
    public class Program
    {
        static string logFileLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "RevidXClient", "RevidXServer.log");
        public static void Main(string[] args)
        {

            try
            {
                var seqHostName = "127.0.0.1";
                if (Environment.GetEnvironmentVariable("DEPLOYMENT_PLATFORM") == "docker_postgres")
                {
                    seqHostName = "63.32.118.104";
                }
                var apiKey = Environment.GetEnvironmentVariable("SEQ_API_KEY");
                if (string.IsNullOrEmpty(apiKey))
                {
                    apiKey = "FeCwFUBDowqD4PMMu8BT";
                }
                Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                .Enrich.FromLogContext().WriteTo.Trace()
                .Enrich.FromLogContext().WriteTo.Console()
                .WriteTo.File(logFileLocation, outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}")//, theme: AnsiConsoleTheme.Code)
                .WriteTo.Seq(string.Format("http://{0}:5341", seqHostName), apiKey: apiKey)
                .CreateLogger();

                //Log.Logger = new LoggerConfiguration()
                //    .WriteTo.Trace().WriteTo.File(logFileLocation, rollingInterval: RollingInterval.Hour)
                //    .CreateLogger();
                var host = CreateHostBuilder(args).Build();
                using (var scope = host.Services.CreateScope())
                {
                    if (Environment.GetEnvironmentVariable("USE_EF_DB") == "MYSQL")
                    {
                        var dbContext = scope.ServiceProvider.GetRequiredService<MySqlDbContext>();
                        dbContext.Database.Migrate();
                        var roleManager = scope.ServiceProvider.GetService<RoleManager<IdentityRole>>();
                        var userManager = scope.ServiceProvider.GetService<UserManager<ApplicationUser>>();
                        if (userManager != null && roleManager != null)
                        {
                            DbDataInitializer.SeedData(userManager, roleManager);
                        }
                        else
                        {
                            throw new(String.Format("UserManager and roleManager could not be created. Aboooort. "));
                        }
                    }
                    else if (Environment.GetEnvironmentVariable("USE_EF_DB") == "POSTGRESQL")
                    {
                        var dbContext = scope.ServiceProvider.GetRequiredService<PostgreSqlDbContext>();
                        dbContext.Database.Migrate();

                        dbContext.Database.Migrate();
                        var roleManager = scope.ServiceProvider.GetService<RoleManager<IdentityRole>>();
                        var userManager = scope.ServiceProvider.GetService<UserManager<ApplicationUser>>();
                        if (userManager != null && roleManager != null)
                        {
                            DbDataInitializer.SeedData(userManager, roleManager, dbContext);
                        }
                        else
                        {
                            throw new(String.Format("UserManager and roleManager could not be created. Aboooort. "));
                        }
                    }
                    else
                    {
                        var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                        dbContext.Database.Migrate();
                        var roleManager = scope.ServiceProvider.GetService<RoleManager<IdentityRole>>();
                        var userManager = scope.ServiceProvider.GetService<UserManager<ApplicationUser>>();
                        if (userManager != null && roleManager != null)
                        {
                            DbDataInitializer.SeedData(userManager, roleManager);
                        }
                        else
                        {
                            throw new(String.Format("UserManager and roleManager could not be created. Aboooort. "));
                        }
                    }
                }
                host.Run();
            }
            catch (Exception ex)
            {
                // Log.Logger will likely be internal type "Serilog.Core.Pipeline.SilentLogger".
                if (Log.Logger == null || Log.Logger.GetType().Name == "SilentLogger")
                {
                    // Loading configuration or Serilog failed.
                    // This will create a logger that can be captured by Azure logger.
                    // To enable Azure logger, in Azure Portal:
                    // 1. Go to WebApp
                    // 2. App Service logs
                    // 3. Enable "Application Logging (Filesystem)", "Application Logging (Filesystem)" and "Detailed error messages"
                    // 4. Set Retention Period (Days) to 10 or similar value
                    // 5. Save settings
                    // 6. Under Overview, restart web app
                    // 7. Go to Log Stream and observe the logs
                    Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Debug()
                        .WriteTo.Console()
                        .CreateLogger();
                }

                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>()
                    .UseSerilog();
                });
    }
}
