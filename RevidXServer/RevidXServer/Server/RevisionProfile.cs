﻿using AutoMapper;
using RevidX.ServiceClient.Models;

namespace RevidXServer.Server
{
    public class RevisionProfile : Profile
    {
        public RevisionProfile()
        {
            CreateMap<Revision, RevisionDTO>();
        }
    }
}
