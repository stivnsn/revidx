﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RevidX.ServiceClient;
using RevidX.ServiceClient.Models;
using RevidXServer.Server.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RevidXServer.Server.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RevisionController : ControllerBase
    {
        string UserId { get { return User.FindFirst(ClaimTypes.NameIdentifier)?.Value; } }
        IDataAccessService dataAccessService { get; set; }
        public RevisionController(IDataAccessService service)
        {
            dataAccessService = service;
        }
        // GET: api/<TransactionController>
        [HttpGet("{projectId}")]
        public ActionResult<List<Revision>> Get(string projectId)
        {
            var transactions = dataAccessService.GetProjectRevisions(projectId);
            return Ok(transactions);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }

        /// <summary>
        /// Will get project selection list based taking in account index and size
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="index"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet("{projectId}/{index}/{size}")]
        public ActionResult<List<Revision>> Get(string projectId, int index, int size)
        {
            var transactions = dataAccessService.GetRevisionsByIndexAndSize(projectId, index, size);
            return Ok(transactions);
        }


        /// <summary>
        /// For use with revidx web app
        /// </summary>
        /// <param name="transactionFileId"></param>
        /// <returns></returns>
        // GET: api/<TransactionController>
        [HttpGet("File/{transactionFileId}")]
        public ActionResult<TransactionFile> GetTransactionFile(string transactionFileId)
        {
            var result = dataAccessService.GetTransactionFileById(transactionFileId);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }


        // GET: api/<TransactionController>
        [HttpGet("PCBRevision/{transactionFileId}")]
        public ActionResult<Revision> GetPCBRevisionDataById(string transactionFileId)
        {
            //var transaction = dbContext.Transactions.Where(t => t.Id == transactionId).FirstOrDefault();
            //if (transaction != null)
            //{
            //    transaction.TransactionFiles = dbContext.TransactionFiles.Where(tf => tf.TransactionId == transactionId).ToList();
            //}
            var result = dataAccessService.GetPCBRevisionWithContentById(transactionFileId);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }


        /// <summary>
        /// RevisionSystem pulling revision from userProject data. Will get appropriate file for user depending on project and 'his' state
        /// </summary>
        /// <param name="userProjectId"></param>
        /// <returns></returns>
        // GET: api/<TransactionController>
        [HttpGet("Pull/{userProjectId}")]
        public ActionResult<(RevisionState, Revision)> GetAvailableRevision(string userProjectId)
        {
            RevisionState revisionState;
            var result = dataAccessService.GetAvailableRevisionToPull(out revisionState, userProjectId);
            return Ok((revisionState, result));// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }

        /// <summary>
        /// RevisionSystem pulling revision from userProject data. Will get appropriate file for user depending on project and 'his' state
        /// </summary>
        /// <param name="userProjectId"></param>
        /// <returns></returns>
        // GET: api/<TransactionController>
        [HttpGet("Pull/{transactionId}/{userProjectId}")]
        public ActionResult<TransactionFile> PullTransactionFileWithContent(string transactionId, string userProjectId)
        {
            var result = dataAccessService.PullTransactionFileWithContent(transactionId, userProjectId);
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }

        [HttpGet("Push/{userProjectId}")]
        public ActionResult<List<TransactionFile.TransactionFileType>> GetAvailableFileTypesForPushing(string userProjectId)
        {
            var result = dataAccessService.GetAvailableFileTypesForPushing(userProjectId);
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }

        [HttpPost("Push/{userProjectId}")]
        public ActionResult<Revision> PushRevision([FromBody] Revision revision, string userProjectId)
        {
            var result = dataAccessService.PushRevision(revision, userProjectId);
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }

        [HttpPost("PushGlobal/{userProjectId}")]
        public ActionResult<Revision> PushRevisionGlobal([FromBody] Revision revision, string userProjectId)
        {
            var result = dataAccessService.PushRevision(revision, userProjectId, false);
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }

        [HttpGet("Cancel/{userProjectId}")]
        public ActionResult<Revision> CancelRevision(string userProjectId)
        {
            var userId = dataAccessService.GetUserIdFromIdentityId(UserId);
            if (userId == null)
            {
                return null;
            }
            var result = dataAccessService.CancelRevision(userProjectId, userId);
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }

        [HttpGet("List/{endRevisionId}/{startRevisionId}")]
        public ActionResult<List<Revision>> ListRevisions(string endRevisionId, string startRevisionId)
        {
            List<Revision> result = dataAccessService.GetRevisionDiffSet(endRevisionId, startRevisionId);
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }

        [HttpGet("byId/{revisionId}")]
        public ActionResult<Revision> GetRevisionById(string revisionId)
        {
            Revision result = dataAccessService.GetRevisionById(revisionId);
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }

        [HttpGet("List/{endRevisionId}")]
        public ActionResult<List<Revision>> ListRevisions(string endRevisionId)
        {
            List<Revision> result = dataAccessService.GetRevisionDiffSet(endRevisionId, null);
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }

        [HttpGet("UpdateToRevision/{userProjectId}/{revisionId}")]
        public ActionResult<bool> UpdateUserProjectToRevision(string userProjectId, string revisionId)
        {
            var userId = dataAccessService.GetUserIdFromIdentityId(UserId);
            if (userId == null)
            {
                return null;
            }
            bool result = dataAccessService.UpdateUserProjectToRevision(userProjectId, revisionId, userId);
            return Ok(result);// dbContext.Transactions.Where(t => t.ProjectId == projectId).ToList());
        }


    }
}
