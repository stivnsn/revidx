﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RevidX.ServiceClient.Models;
using RevidXServer.Server.Data;
using System.Collections.Generic;
using System.Security.Claims;

namespace RevidXServer.Server.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SuperadminController : ControllerBase
    {
        string UserId { get { return User.FindFirst(ClaimTypes.NameIdentifier)?.Value; } }
        IDataAccessProviderAdmin dataAccessProviderAdmin { get; set; }
        public SuperadminController(IDataAccessProviderAdmin service)
        {
            dataAccessProviderAdmin = service;
        }

        [HttpGet("getusers")]
        public ActionResult<List<UserProfile>> GetUsers()
        {
            //var userid = SignInManager.IsSignedIn(User) ;
            var userProfiles = dataAccessProviderAdmin.GetAllUserProfiles();
            return userProfiles;
        }
        [HttpGet("getprojects/{userId}")]
        public ActionResult<List<UserProject>> GetProjects(string userId)
        {
            //var userid = SignInManager.IsSignedIn(User) ;
            var userProjects = dataAccessProviderAdmin.GetAllUserProjects(userId);
            return userProjects;
        }

        [HttpGet("getrevisions/{projectId}")]
        public ActionResult<List<Revision>> GetRevisions(string projectId)
        {
            //var userid = SignInManager.IsSignedIn(User) ;
            var userRevisions = dataAccessProviderAdmin.GetAllRevisions(projectId);
            return userRevisions;
        }
    }
}
