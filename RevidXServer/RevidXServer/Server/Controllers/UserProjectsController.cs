﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RevidX.ServiceClient.Models;
using RevidXServer.Server.Data;
using RevidXServer.Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RevidXServer.Server.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserProjectsController : ControllerBase
    {

        string UserId { get { return User.FindFirst(ClaimTypes.NameIdentifier)?.Value; } }
        IDataAccessService dataAccessService { get; set; }

        public UserProjectsController(IDataAccessService service)
        {
            dataAccessService = service;
        }

        // GET: api/<UserProjectsController>
        [HttpGet]
        public ActionResult<IEnumerable<UserProject>> Get()
        {
            var userId = dataAccessService.GetUserIdFromIdentityId(UserId);
            if (userId == null)
            {
                return null;
            }
            var result = dataAccessService.GetUserProjectsWithProjectAndCreator(userId);
            return result;
        }

        // GET api/<ProjectController>/5
        [HttpGet("{id}")]
        public UserProject Get(string id)
        {
            var project = dataAccessService.GetUserProjectById(id);
            return project;
        }

        // PUT api/<UserProjectsController>/5 //To update user project information
        [HttpPut]
        public void Put([FromBody] UserProject userProject)
        {
            var userId = dataAccessService.GetUserIdFromIdentityId(UserId);
            if (userId == null)
            {
                return;
            }
            dataAccessService.UpdateUserProject(ref userProject, userId);
        }
    }
}
