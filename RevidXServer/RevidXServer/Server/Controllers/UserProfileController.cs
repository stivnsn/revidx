﻿using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RevidX.ServiceClient.Models;
using RevidXServer.Server.Data;
using RevidXServer.Server.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RevidXServer.Server.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        string UserId { get { return User.FindFirst(ClaimTypes.NameIdentifier)?.Value; } }
        IDataAccessService dataAccessService { get; set; }
        public UserProfileController(IDataAccessService service)
        {
            dataAccessService = service;
        }


        //RoleManager<ApplicationRo> RoleManager { get; set; }
        // GET: api/<ProfileController>
        [HttpGet]
        public ActionResult<UserProfile> Get()
        {
            //var id = User?.FindFirst(x => x.Type.Equals(JwtClaimTypes.Id))?.Value;
            var userid = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var email = User.FindFirst(ClaimTypes.Email)?.Value;
            //var userid = SignInManager.IsSignedIn(User) ;
            var userProfile = dataAccessService.GetUserProfileByUserId(userid);
            if (userProfile == null || string.IsNullOrWhiteSpace(userProfile.Email))
            {
                userProfile.Email = email;
            }
            return userProfile;
        }


        // GET api/<ProfileController>/5
        [HttpGet("byProject/{projectId}")]
        public ActionResult<List<UserProfile>> GetByProject(string projectId)
        {
            return dataAccessService.GetUserProfilesByProjectId(projectId);
        }

        // GET api/<ProfileController>/5
        [HttpGet("{userProfileId}")]
        public UserProfile Get(string userProfileId)
        {
            return dataAccessService.GetUserProfileByUserId(userProfileId);
        }

        // POST api/<ProfileController>
        [HttpPost]
        public UserProfile Post([FromBody] UserProfile userProfile)
        {
            if(UserId == null || UserId != userProfile.IdentityId)
            {
                Forbid();
                return null;
            }
            return dataAccessService.AddUserProfile(userProfile);
        }

        // PUT api/<ProfileController>/5
        [HttpPut]
        public void Put([FromBody] UserProfile userProfile)
        {
            dataAccessService.UpdateUserProfile(userProfile);
        }

        // DELETE api/<ProfileController>/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            try
            {
                var userId = dataAccessService.GetUserIdFromIdentityId(UserId);
                if(userId == null || userId != id)
                {
                    return;
                }
                dataAccessService.DeleteUserProfile(id);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}
