﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RevidX.ServiceClient.Models;
using RevidXServer.Server.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RevidXServer.Server.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        string UserId { get { return User.FindFirst(ClaimTypes.NameIdentifier)?.Value; } }
        IDataAccessService dataAccessService { get; set; }
        public ProjectController(IDataAccessService service)
        {
            dataAccessService = service;
        }

        // GET: api/<ProjectController>
        [HttpGet]
        public ActionResult<IEnumerable<Project>> Get()
        {
            var userId = dataAccessService.GetUserIdFromIdentityId(UserId);
            if(userId == null)
            {
                return null;
            }
            var projects = dataAccessService.GetUserProjectsByUserId(userId);
            return projects.Select(up => up.Project).ToList();
        }

        [HttpGet("{projectId}")]
        public ActionResult<UserProject> Get(string projectId)
        {
            var userId = dataAccessService.GetUserIdFromIdentityId(UserId);
            if(userId == null)
            { 
                return null;
            }
            var userProject = dataAccessService.GetUserProjectByProjectId(projectId, userId);
            return userProject;
        }

        [HttpGet("messages/{projectId}")]
        public ActionResult<IEnumerable<Message>> GetMessages(string projectId)
        {
            var userId = dataAccessService.GetUserIdFromIdentityId(UserId);

            if (userId == null)
            {
                return null;
            }
            var messages = dataAccessService.GetMessages(projectId, userId);
            return messages;
        }


        // POST api/<ProjectController>
        [HttpPost]
        public ActionResult<Project> Post([FromBody] Project project)
        {
            var userId = dataAccessService.GetUserIdFromIdentityId(UserId);
            if (userId == null)
            {
                return null;
            }
            if (!dataAccessService.AddProjectForUser(ref project, userId))
            {
                return null;
            }
            return project;
        }

        [HttpPut("{inviteeEmail}")]
        public ActionResult<bool> Put([FromBody] Project project, string inviteeEmail)
        {
            //if (project.CreatorId == UserId)
            {
                return dataAccessService.AddUserProjectForUser(project, inviteeEmail);
            }
        }

        // PUT api/<ProjectController>/5
        [HttpPut]
        public void Put([FromBody] Project project)
        {
            dataAccessService.UpdateProjectForUser(ref project);
        }

        // DELETE api/<ProjectController>/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            var userId = dataAccessService.GetUserIdFromIdentityId(UserId);
            if (userId == null)
            {
                return;
            }
            dataAccessService.RemoveProject(id, userId);
        }
    }
}
