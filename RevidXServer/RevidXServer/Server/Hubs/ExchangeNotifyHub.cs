﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using RevidX.ServiceClient.Models;
using RevidXServer.Server.Data;
using Serilog;

namespace RevidXServer.Server.Hubs
{
    public class ExchangeNotifyHub : Hub
    {
        private IServiceProvider _sp;
        //ApplicationDbContext DbContext { get; set; }
        //public ExchangeNotifyHub()
        //{
        //    var i = 0;
        //}

        // dbcontext needs a single specific constructor not a base class
        public ExchangeNotifyHub(IServiceProvider sp)
        {
            _sp = sp;
        }

        //public ExchangeNotifyHub(ApplicationDbContext dbContext)
        //{
        //    DbContext = dbContext;
        //}
        //public ExchangeNotifyHub(PostgreSqlDbContext dbContext)
        //{
        //    DbContext = dbContext;
        //}
        //public ExchangeNotifyHub(MySqlDbContext dbContext)
        //{
        //    DbContext = dbContext;
        //}


        public async Task SendMessage(Message message)
        {
            try
            {
                await Clients.GroupExcept(message.ProjectId, new List<string>() { Context.ConnectionId }).SendAsync("ReceiveMessage", message);
                using (var scope = _sp.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<PostgreSqlDbContext>();
                    if (dbContext != null)
                    {
                        DataAccessService dataAccessService = new DataAccessService(dbContext);
                        message.Id = Guid.NewGuid().ToString();
                        message.Sender = null;
                        message.SendTimeUtc = DateTime.UtcNow;
                        dbContext.Messages.Add(message);
                        dbContext.SaveChanges();
                        message.Sender = dbContext.UserProfiles.Where(up => up.Id == message.SenderId).FirstOrDefault();
                    }
                    else
                    {
                        Log.Error("Message could not be stored to database. dbContext is Null");
                    }
                }
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        public async Task CancelledAction(Message message, bool includeYourself)
        {
            try
            {
                List<string> ignoreConnects = new List<string>();
                if (!includeYourself)
                {
                    ignoreConnects = new List<string> { Context.ConnectionId };
                }
                await Clients.GroupExcept(message.ProjectId, ignoreConnects).SendAsync("CancelledAction", message);
                using (var scope = _sp.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<PostgreSqlDbContext>();
                    if (dbContext != null)
                    {
                        message.Id = Guid.NewGuid().ToString();
                        message.SendTimeUtc = DateTime.UtcNow;
                        message.Sender = null;
                        dbContext.Messages.Add(message);
                        dbContext.SaveChanges();
                        message.Sender = dbContext.UserProfiles.Where(up => up.Id == message.SenderId).FirstOrDefault();
                    }
                    else
                    {
                        Log.Error("Message could not be stored to database. dbContext is Null");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        public async Task FileUploadedToProject(Message message, bool includeYourself)
        {
            try
            {
                List<string> ignoreConnects = new List<string>();
                if(!includeYourself)
                {
                    ignoreConnects = new List<string> { Context.ConnectionId };
                }
                await Clients.GroupExcept(message.ProjectId, ignoreConnects).SendAsync("FileUploadedToProject", message);
                using (var scope = _sp.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<PostgreSqlDbContext>();
                    if (dbContext != null)
                    {
                        message.Id = Guid.NewGuid().ToString();
                        message.SendTimeUtc = DateTime.UtcNow;
                        message.Sender = null;
                        dbContext.Messages.Add(message);
                        dbContext.SaveChanges();
                        message.Sender = dbContext.UserProfiles.Where(up => up.Id == message.SenderId).FirstOrDefault();
                    }
                    else
                    {
                        Log.Error("Message could not be stored to database. dbContext is Null");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
        public async Task FilePulled(Message message)
        {
            try
            {
                await Clients.GroupExcept(message.ProjectId, new List<string> { Context.ConnectionId }).SendAsync("FilePulled", message);
                using (var scope = _sp.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<PostgreSqlDbContext>();
                    if (dbContext != null)
                    {
                        message.Id = Guid.NewGuid().ToString();
                        message.SendTimeUtc = DateTime.UtcNow;
                        message.Sender = null;
                        dbContext.Messages.Add(message);
                        dbContext.SaveChanges();
                        message.Sender = dbContext.UserProfiles.Where(up => up.Id == message.SenderId).FirstOrDefault();
                    }
                    else
                    {
                        Log.Error("Message could not be stored to database. dbContext is Null");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
        public async Task FileUploadedToProjectWeb(Message message)
        {
            try
            {
                await Clients.All.SendAsync("FileUploadedToProject", message);
                using (var scope = _sp.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<PostgreSqlDbContext>();
                    if (dbContext != null)
                    {
                        message.Id = Guid.NewGuid().ToString();
                        message.SendTimeUtc = DateTime.UtcNow;
                        message.Sender = null;
                        dbContext.Messages.Add(message);
                        dbContext.SaveChanges();
                        message.Sender = dbContext.UserProfiles.Where(up => up.Id == message.SenderId).FirstOrDefault();
                    }
                    else
                    {
                        Log.Error("Message could not be stored to database. dbContext is Null");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        public async Task ProjectUpdated(Message message, bool includeYourself = false)
        {
            try
            {
                List<string> ignoreConnects = new List<string>();
                if (!includeYourself)
                {
                    ignoreConnects = new List<string> { Context.ConnectionId };
                }
                await Clients.GroupExcept(message.ProjectId, ignoreConnects).SendAsync("ProjectUpdated", message, true);
                using (var scope = _sp.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<PostgreSqlDbContext>();
                    if (dbContext != null)
                    {
                        message.Id = Guid.NewGuid().ToString();
                        message.SendTimeUtc = DateTime.UtcNow;
                        message.Sender = null;
                        dbContext.Messages.Add(message);
                        dbContext.SaveChanges();
                        message.Sender = dbContext.UserProfiles.Where(up => up.Id == message.SenderId).FirstOrDefault();
                    }
                    else
                    {
                        Log.Error("Message could not be stored to database. dbContext is Null");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        public async Task JoinGroup(string projectId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, projectId);
        }

        public async Task LeaveGroup(string projectId)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, projectId);
        }
    }
}
