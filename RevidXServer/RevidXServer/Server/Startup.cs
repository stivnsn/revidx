using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RevidXServer.Server.Data;
using RevidXServer.Server.Models;
using System.Linq;
using RevidXServer.Server.Hubs;
using Duende.IdentityServer.Services;
using Newtonsoft.Json;
using MatBlazor;
using System;
using Microsoft.AspNetCore.HttpOverrides;
using Serilog;
using Microsoft.IdentityModel.Logging;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace RevidXServer.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        const string MYSQL_USE_DB_NAME = "MYSQL";
        const string MSSQL_USE_DB_NAME = "MSSQL";

        const string POSTGRES_USE_DB_NAME = "POSTGRESQL";

        public const string DOCKER_POSTGRES_PLATFORM = "docker_postgres";

        public IConfiguration Configuration { get; }


        //dotnet ef migrations add InitialCreate --context ApplicationContext --output-dir Migrations/SqlServerMigrations
        //dotnet ef migrations add InitialCreate --context MySqlApplicationContext --output-dir Migrations/MySqlMigrations
        //dotnet tool update --global dotnet-ef 

        //dotnet ef migrations add NewMigration --project .\RevidXServer.Server.csproj --output-dir Migrations/SqlServerMigrations --context ApplicationDbContext -- --provider MSSQL
        //dotnet ef migrations add NewMigration --project .\RevidXServer.Server.csproj --output-dir Migrations/MySqlMigrations --context MySqlApplicationContext -- --provider MYSQL
        //dotnet ef migrations add NewMigration --project .\RevidXServer.Server.csproj --output-dir Migrations/PostgreSqlMigrations --context PostgreSqlDbContext -- --provider POSTGRESQL

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;
            bool testScenario = false;
            var efAddMigrationParam = Configuration.GetValue("PROVIDER", "");
            if (efAddMigrationParam == MYSQL_USE_DB_NAME || Environment.GetEnvironmentVariable("USE_EF_DB") == MYSQL_USE_DB_NAME)
            {
                var host = "127.0.0.1";
                if (Environment.GetEnvironmentVariable("DEPLOYMENT_PLATFORM") == DOCKER_POSTGRES_PLATFORM)
                {
                    host = "host.docker.internal";
                }
                var connectionString = string.Format("database=revidxDB;server={0};user=revidxUser2;password=testPass$5S;port=3306", host);
                services.AddDbContext<MySqlDbContext>(options =>
                    options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString))
                    );
                services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true).AddRoles<IdentityRole>()
                    .AddEntityFrameworkStores<MySqlDbContext>();
                services.AddIdentityServer(options =>
                {
                    options.IssuerUri = "https://*.revidx.com";
                })
                    .AddApiAuthorization<ApplicationUser, MySqlDbContext>();
            }
            else if (efAddMigrationParam == POSTGRES_USE_DB_NAME || Environment.GetEnvironmentVariable("USE_EF_DB") == POSTGRES_USE_DB_NAME)
            {
                var host = "127.0.0.1";
                if (Environment.GetEnvironmentVariable("DEPLOYMENT_PLATFORM") == DOCKER_POSTGRES_PLATFORM)
                {
                    host = "postgres_image";
                }
                var connectionString = string.Format("Server={0};Port=5432;Database=revidxDB;UserId=postgres;Password=postgres;", host);
                services.AddDbContext<PostgreSqlDbContext>(options =>
                    options.UseNpgsql(connectionString)
                    );
                services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true).AddRoles<IdentityRole>()
                    .AddEntityFrameworkStores<PostgreSqlDbContext>().AddDefaultTokenProviders();
                var issuerUri = "https://localhost:5001";
                if (Environment.GetEnvironmentVariable("DEPLOYMENT_PLATFORM") == DOCKER_POSTGRES_PLATFORM)
                {
                    issuerUri = "https://revidx.com";
                }
                services.AddIdentityServer(options =>
                {
                    //options.IssuerUri = issuerUri;
                })
                .AddApiAuthorization<ApplicationUser, PostgreSqlDbContext>();
            }
            else /// test for now
            {
                var connectionString = "Server=127.0.0.1;Port=5432;Database=revidxDB;UserId=postgres;Password=postgres;";
                testScenario = true;
                services.AddDbContext<PostgreSqlDbContext>(options =>
                options.UseNpgsql(
                    connectionString));
                //services.AddEntityFrameworkSqlServer();
                services.AddIdentityServer(options =>
                {
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseSuccessEvents = true;
                    options.IssuerUri = "https://localhost:5001";
                })
                    .AddInMemoryApiResources(TestConfig.GetApiResources())
                    .AddInMemoryClients(TestConfig.GetClients())
                    .AddInMemoryApiScopes(TestConfig.GetScopes())
                    .AddTestUsers(TestConfig.GetUsers()).AddDeveloperSigningCredential();
            }
            services.AddScoped<IDataAccessProviderAdmin, DataAccessProviderAdmin>();
            services.AddScoped<IDataAccessService, DataAccessService>();
            services.AddDatabaseDeveloperPageExceptionFilter();
            services.AddAuthentication()
            .AddIdentityServerJwt().AddJwtBearer(
            options =>
            {
                options.Audience = "RevidXServer.ServerAPI api1";
                // IdentityServer emits a typ header by default, recommended extra check
                options.TokenValidationParameters.ValidTypes = new[] { "at+jwt" };
                options.TokenValidationParameters.ValidateAudience = false;
            })
            ;
            services.AddAuthorization(options =>
            {
                options.AddPolicy("RevidXServer.ServerAPI",
                  policy =>
                  {
                      policy.RequireClaim("scope", "RevidXServer.ServerAPI");
                  });
            });
            if (!testScenario)
            {
                services.AddTransient<IProfileService, ProfileService>();
                services.AddTransient<IEmailSender, EmailSender>(i =>
                     new EmailSender("mail.privateemail.com", 587, true, "noreply@revidx.com", "noreply@revidx.com", "40=sharp=NEITHER=sentence=54")


                    //new EmailSender("email-smtp.eu-west-1.amazonaws.com", 587, true, "noreply@revidx.com", "AKIAZ2GEZQBGAEJRATPZ", "BFaGlcCxEVzshfjosyTMnSY9AIuZA65+EVGdSyTkbv8n")
                    );
                services.AddAuthorization();
            }
            services.AddSignalR();
            services.AddControllersWithViews().AddNewtonsoftJson(
                options => options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
            services.AddRazorPages();
            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseResponseCompression();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
                app.UseWebAssemblyDebugging();
                app.UseBrowserLink();

            }
            else
            {
                app.UseForwardedHeaders(new ForwardedHeadersOptions
                {
                    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
                });
                app.UseWebAssemblyDebugging();
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSerilogRequestLogging();
            app.UseHttpsRedirection();
            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapHub<ExchangeNotifyHub>("/exchangenotify");
                endpoints.MapFallbackToFile("index.html");
            });
        }
    }
}
