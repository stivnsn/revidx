﻿using Braintree;
using Microsoft.Extensions.Configuration;

namespace RevidXServer.Server.Data
{

    public class BraintreeConfiguration : IBraintreeConfiguration
    {
        public string Environment { get; set; }
        public string MerchantId { get; set; }
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
        private IBraintreeGateway BraintreeGateway { get; set; }
        private IConfiguration configuration { get; set; }
        public BraintreeConfiguration(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public IBraintreeGateway CreateGateway()
        {
            Environment = System.Environment.GetEnvironmentVariable("BraintreeEnvironment");
            MerchantId = System.Environment.GetEnvironmentVariable("BraintreeMerchantId");
            PublicKey = System.Environment.GetEnvironmentVariable("BraintreePublicKey");
            PrivateKey = System.Environment.GetEnvironmentVariable("BraintreePrivateKey");

            if (MerchantId == null || PublicKey == null || PrivateKey == null)
            {
                Environment = GetConfigurationSetting("BraintreeSettings:BraintreeEnvironment");
                MerchantId = GetConfigurationSetting("BraintreeSettings:BraintreeMerchantId");
                PublicKey = GetConfigurationSetting("BraintreeSettings:BraintreePublicKey");
                PrivateKey = GetConfigurationSetting("BraintreeSettings:BraintreePrivateKey");
            }

            return new BraintreeGateway(Environment, MerchantId, PublicKey, PrivateKey);
        }

        public string GetConfigurationSetting(string setting)
        {
            return configuration.GetValue<string>(setting);
        }

        public IBraintreeGateway GetGateway()
        {
            if (BraintreeGateway == null)
            {
                BraintreeGateway = CreateGateway();
            }

            return BraintreeGateway;
        }
    }
}

