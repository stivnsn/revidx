﻿using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Options;
using RevidX.ServiceClient.Models;
using RevidXServer.Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RevidXServer.Server.Data
{

    public class MySqlDbContext : ApplicationDbContext
    {
        public MySqlDbContext(DbContextOptions<MySqlDbContext> options, IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //{

        //    var connectionString = "database=revidxDB;server=95.179.152.191;user=revidxUser;password=testPass$5S;port=3306";
        //    options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
        //}
    }
    public class PostgreSqlDbContext : ApplicationDbContext
    {
        public PostgreSqlDbContext(DbContextOptions<PostgreSqlDbContext> options, IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //{

        //    var connectionString = "database=revidxDB;server=95.179.152.191;user=revidxUser;password=testPass$5S;port=3306";
        //    options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
        //}
    }

    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        public DbSet<Message> Messages { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<UserProject> UserProjects { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Revision> Revisions { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionFile> TransactionFiles { get; set; }
        public DbSet<BinaryContent> BinaryContents { get; set; }
        public DbSet<TransactionFileActivity> TransactionFileActivity { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ApplicationUser>().HasKey(p => p.Id);

            builder.Entity<Project>().HasKey(p => p.Id);
            builder.Entity<Project>().HasOne(p => p.Creator).WithMany(up => up.CreatedProjects);
            builder.Entity<Project>().HasOne(p => p.LatestRevision).WithOne(r => r.ReferencedProject).HasForeignKey<Project>(r => r.LatestRevisionId).OnDelete(DeleteBehavior.SetNull);
            builder.Entity<Project>().HasMany(p => p.Users).WithMany(p => p.Projects);

            builder.Entity<UserProfile>().HasKey(p => p.Id);
            builder.Entity<UserProfile>().Ignore(up => up.Name);
            builder.Entity<UserProfile>().HasMany(p => p.Projects).WithMany(p => p.Users);
            builder.Entity<UserProject>().HasKey(up => new { up.Id });
            builder.Entity<UserProject>().HasOne(p => p.Project).WithMany(p => p.UserProjects).HasForeignKey(p => p.ProjectId).OnDelete(DeleteBehavior.Cascade); ;
            builder.Entity<UserProject>().HasOne(p => p.UserProfile).WithMany(p => p.UserProjects).HasForeignKey(p => p.UserProfileId).OnDelete(DeleteBehavior.Cascade); ;
            builder.Entity<UserProject>().Ignore(up => up.NewMessages);
            builder.Entity<UserProject>().Ignore(p => p.Name);
            builder.Entity<UserProfile>().HasMany(s => s.UserSubscriptions).WithOne(us => us.UserProfile).HasForeignKey(s => s.UserProfileId).OnDelete(DeleteBehavior.Cascade);

            //builder.Entity<Transaction>().HasOne(p => p.Project).WithMany(p => p.Transactions).HasForeignKey(p => p.ProjectId).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<TransactionFile>().HasOne(p => p.Transaction).WithMany(p => p.TransactionFiles).HasForeignKey(t => t.TransactionId).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<TransactionFile>().Ignore(p => p.PCBRevisionContent);
            builder.Entity<TransactionFile>().HasOne(bc => bc.BinaryContent).WithOne(bc => bc.TransactionFile).HasForeignKey<BinaryContent>(b => b.TransactionFileId).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<TransactionFile>().HasOne(p => p.UserProfile).WithMany(p => p.TransactionFiles);

            builder.Entity<Revision>().HasOne(p => p.Project).WithMany(p => p.Revisions).HasForeignKey(p => p.ProjectId).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Revision>().HasOne(p => p.BaselineFile);
            builder.Entity<Revision>().HasOne(p => p.ProposalFile);
            builder.Entity<Revision>().HasOne(p => p.ResponseFile);
            builder.Entity<Revision>().HasOne(p => p.LockedByUser).WithMany(p => p.LockedRevisions).HasForeignKey(p => p.LockedByUserId);
            builder.Entity<Revision>().HasOne(p => p.AwaitedByUser).WithMany(p => p.AwaitingRevisions).HasForeignKey(p => p.AwaitedByUserId).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<BinaryContent>().HasOne(p => p.TransactionFile).WithOne(p => p.BinaryContent);

            builder.Entity<Message>().HasOne(m => m.Sender).WithMany(s => s.Messages).HasForeignKey(s => s.SenderId);
            builder.Entity<Message>().HasOne(m => m.Project).WithMany(s => s.Messages).HasForeignKey(s => s.ProjectId).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Subscription>().HasMany(s => s.UserSubscriptions).WithOne(us=> us.Subscription).HasForeignKey(s => s.SubscriptionId);
            builder.Entity<Subscription>().HasMany(sc => sc.SubscriptionCodes).WithOne(us => us.Subscription).HasForeignKey(s => s.SubscriptionId).OnDelete(DeleteBehavior.Cascade);

            //builder.Entity<Subscription>().Property(s => s.Span).HasConversion(new TimeSpanToStringConverter());

            base.OnModelCreating(builder);
        }

    }
}
