﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using RevidX.ServiceClient.Models;
using RevidX.ServiceClient;
using Serilog;

namespace RevidXServer.Server.Data
{
    public interface IDataAccessService
    {
        UserProfile GetUserProfileByUserId(string userId);
        UserProfile GetUserProfileByUserEmail(string userEmail);
        List<UserProject> GetUserProjectsByUserId(string UserId);
        UserProject GetUserProjectById(string Id);
        List<UserProfile> GetUserProfilesByProjectId(string projectId);
        bool AddProjectForUser(ref Project project, string userId);
        bool AddUserProjectForUser(Project project, string inviteeUserId);
        List<UserProject> GetUserProjectsWithProjectAndCreator(string userId);
        List<Revision> GetProjectRevisions(string projectId);
        void UpdateUserProject(ref UserProject userProject, string userId);
        void UpdateProjectForUser(ref Project project);
        // UserProject UpdateLatestUserProjectRevision(string revisionId, string userProjectId)
        TransactionFile GetTransactionFileById(string transactionFileId);
        void RemoveProject(string id, string userId);
        UserProject GetUserProjectByProjectId(string projectId, string identityId);

        //TransactionFile PullTransactionFile(string transactionFileId, string userId);
        List<Revision> GetRevisionsByIndexAndSize(string projectId, int index, int size);

        /// <summary>
        /// Does not seem to be used. use Pushrevision instead.
        /// </summary>
        /// <param name="revision"></param>
        /// <param name="identityId"></param>
        /// <returns></returns>
        Revision GetAvailableRevisionToPull(out RevisionState revisionState, string userProjectId);
        TransactionFile PullTransactionFileWithContent(string transactionFileId, string userProjectId);
        List<TransactionFile.TransactionFileType> GetAvailableFileTypesForPushing(string userProjectId);
        Revision PushRevision(Revision revision, string userProjectId, bool checkIfUserIsAtLatestState = true);
        bool CancelRevision(string userProjectId, string userId);
        List<Message> GetMessages(string projectId, string userId);
        Revision GetPCBRevisionWithContentById(string transactionFileId);
        UserProfile AddUserProfile(UserProfile userProfile);
        bool UpdateUserProfile(UserProfile userProfile);
        bool DeleteUserProfile(string userId);
        List<Revision> GetRevisionDiffSet(string endRevisionId, string startRevisionId);
        bool UpdateUserProjectToRevision(string userProjectId, string revisionId, string userId);
        
        string GetUserIdFromIdentityId(string identityId);
        bool IsIdentityUserRelatedToUserProject(string identityId, string userProjectId);
        Revision GetRevisionById(string revisionId);
    }

    public class DataAccessService : IDataAccessService
    {
        ApplicationDbContext DBContext
        { get; set; }
        public DataAccessService(PostgreSqlDbContext dbContext)
        {
            DBContext = dbContext;
        }
        public DataAccessService(MySqlDbContext dbContext)
        {
            DBContext = dbContext;
        }
        public DataAccessService(ApplicationDbContext dbContext)
        {
            DBContext = dbContext;
        }

        public bool IsIdentityUserRelatedToUserProject(string identityId, string userProjectId)
        {
            var userId = GetUserIdFromIdentityId(identityId);
            return DBContext.UserProjects.Where(up => up.Id == userProjectId && up.UserProfileId == userId).Any();
        }

        public string GetUserIdFromIdentityId(string identityId)
        {
            try
            {
                var userProfile = DBContext.UserProfiles.Where(u => u.IdentityId == identityId).FirstOrDefault();
                if (userProfile == null)
                {
                    throw new Exception("No user in database. Abort. Identity id: " + identityId);
                    //return null;
                }
                return userProfile.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public UserProfile GetUserProfileByUserEmail(string userEmail)
        {
            try
            {
                return DBContext.UserProfiles.Where(u => u.Email.ToLower() == userEmail.ToLower()).Include(u => u.Projects).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public UserProfile GetUserProfileByUserId(string userId)
        {
            try
            {
                return DBContext.UserProfiles.Where(u => u.Id == userId).Include(u => u.Projects).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public List<UserProfile> GetUserProfilesByProjectId(string projectId)
        {
            try
            {
                return DBContext.UserProjects.Where(u => u.ProjectId == projectId).Select(u => u.UserProfile).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }


        public UserProfile AddUserProfile(UserProfile userProfile)
        {
            try
            {
                var profiles = DBContext.UserProfiles.Where(up => up.IdentityId == userProfile.IdentityId);
                if (profiles != null)
                {
                    if (profiles.Count() > 1)
                    {
                        throw new Exception(string.Format("More than one profile in DB for userProfile {0} with identityId {1}", userProfile.Id, userProfile.IdentityId));
                    }
                    else if (profiles.Count() > 0)
                    {
                        return profiles.First();
                    }
                }
                userProfile.Id = Guid.NewGuid().ToString();
                DBContext.UserProfiles.Add(userProfile);
                DBContext.SaveChanges();
                return userProfile;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public UserProject GetUserProjectByProjectId(string projectId, string userId)
        {
            try
            {
                var userProject = DBContext.UserProjects.Where(up => up.UserProfileId == userId && up.ProjectId == projectId)
                    .Include(up => up.Project).ThenInclude(p => p.LatestRevision)
                    .Include(up => up.Project).ThenInclude(p => p.LatestRevision).ThenInclude(p => p.PreviousRevision).FirstOrDefault();
                return userProject;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public bool UpdateUserProfile(UserProfile userProfile)
        {
            try
            {
                DBContext.UserProfiles.Update(userProfile);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return false;
        }

        public bool DeleteUserProfile(string id)
        {
            try
            {
                var userProfile = DBContext.UserProfiles.Where(u => u.Id == id).FirstOrDefault();
                if (userProfile == null)
                {
                    return false;
                }
                DBContext.UserProfiles.Remove(userProfile);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return false;
        }


        public List<UserProject> GetUserProjectsByUserId(string userId)
        {
            try
            {
                return DBContext.UserProjects.Where(up => up.UserProfileId == userId).Include(up => up.CurrentRevision).Include(up => up.Project).Include(up => up.Project.Creator).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public List<Message> GetMessages(string projectId, string userId)
        {
            try
            {
                if (DBContext.UserProjects.Where(up => up.UserProfileId == userId && up.ProjectId == projectId).Count() == 1)
                {
                    var messages = DBContext.Messages.Where(m => m.ProjectId == projectId);
                    return messages.OrderBy(m => m.SendTimeUtc).ToList().TakeLast(50).ToList();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public UserProject GetUserProjectById(string Id)
        {
            try
            {
                return DBContext.UserProjects.Where(up => up.Id == Id)
                    .Include(up => up.UserProfile)
                    .Include(up => up.CurrentRevision)
                    .ThenInclude(up => up.BaselineFile)
                    .Include(up => up.CurrentRevision)
                    .ThenInclude(up => up.ProposalFile)
                    .Include(up => up.CurrentRevision)
                    .ThenInclude(up => up.ResponseFile)
                    .Include(up => up.Project)
                    .ThenInclude(up => up.LatestRevision)
                    .Include(up => up.Project).ThenInclude(p => p.LatestRevision).ThenInclude(p => p.PreviousRevision)
                    .Include(up => up.Project)
                    .ThenInclude(up => up.Creator)
                    .Include(up => up.Project).ThenInclude(p => p.Messages).FirstOrDefault();

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public bool AddProjectForUser(ref Project project, string userId)
        {
            try
            {
                project.CreatorId = userId;
                project.Id = Guid.NewGuid().ToString();
                DBContext.Projects.Add(project);
                DBContext.UserProjects.Add(new UserProject { Id = Guid.NewGuid().ToString(), ProjectId = project.Id, UserProfileId = userId, IsEnabled = true });
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return false;
        }

        public bool AddUserProjectForUser(Project project, string inviteeEmail)
        {
            try
            {
                var user = DBContext.UserProfiles.Where(u => u.Email.ToLower()== inviteeEmail.ToLower()).FirstOrDefault();
                if (user == null)
                {
                    throw new Exception("User does not exists. User: " + inviteeEmail);
                }
                var projectId = project.Id;
                if (DBContext.UserProjects.Where(u => u.ProjectId == projectId && u.UserProfileId == user.Id).Count() > 0)
                {
                    return true;
                }
                var userProject = new UserProject();
                userProject.ProjectId = project.Id;
                userProject.UserProfileId = user.Id;
                userProject.IsEnabled = true;
                userProject.Id = Guid.NewGuid().ToString();
                DBContext.UserProjects.Add(userProject);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return false;
        }

        public List<UserProject> GetUserProjectsWithProjectAndCreator(string userId)
        {
            try
            {
                var projects = DBContext.UserProjects.Where(up => up.UserProfileId == userId)
                    .Include(up => up.CurrentRevision)
                    .Include(up => up.Project).ThenInclude(p => p.Creator);
                return projects.AsNoTracking().ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public List<Revision> GetProjectRevisions(string projectId)
        {
            try
            {
                var revisions = DBContext.Revisions.Where(t => t.ProjectId == projectId)
                    .Include(r => r.LockedByUser)
                    .OrderByDescending(r => r.ModificationTime)
                    .ToList();
                return revisions;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }


        public List<Revision> GetRevisionsByIndexAndSize(string projectId, int index, int size)
        {
            try
            {
                var revisions = DBContext.Revisions.Where(t => t.ProjectId == projectId).Skip(index * size).Take(size)
                   .Include(r => r.LockedByUser)
                   .OrderByDescending(r => r.ModificationTime)
                   .ToList();
                return revisions;

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public void UpdateUserProject(ref UserProject userProject, string userId)
        {
            try
            {
                var userProjectId = userProject.ProjectId;
                var userProfileId = userProject.UserProfileId;
                if (userId != userProject.UserProfileId)
                {
                    return;
                }
                var nrOfProjects = DBContext.UserProjects.Where(up => up.ProjectId == userProjectId && up.UserProfileId == userProfileId).Count();
                if (nrOfProjects > 0)
                {
                    userProject.Project = null;
                    userProject.UserProfile = null;
                    userProject.CurrentRevision = null;
                    var projects = DBContext.UserProjects.Update(userProject);
                }
                else
                {
                    DBContext.UserProjects.Add(userProject);
                }
                DBContext.SaveChanges();

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
        }

        public List<TransactionFileActivity> GetTransactionActivity(UserProfile userProfile, string userProjectId)
        {
            try
            {
                return DBContext.TransactionFileActivity.Where(t => t.UserProjectId == userProjectId).ToList();

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public void UpdateProjectForUser(ref Project project)
        {
            try
            {
                string projectId = project.Id;
                DBContext.Projects.Update(project);
                if (project.UserProjects.Count() > 0)
                {
                    var userProjects = DBContext.UserProjects.Where(up => up.ProjectId == projectId);
                    foreach (var userProject in project.UserProjects)
                    {
                        if (userProjects.Contains(userProject))
                        {
                            DBContext.UserProjects.Update(userProject);
                        }
                        else
                        {
                            DBContext.UserProjects.Add(userProject);
                        }
                    }
                }
                DBContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
        }

        public List<UserProject> GetTransactionUserProjectsByUserId(string identityId)
        {
            try
            {
                var userId = GetUserIdFromIdentityId(identityId);
                return DBContext.UserProjects.Where(up => up.IsEnabled && up.UserProfileId == userId).Include(up => up.LatestPulledTransaction).Include(up => up.LatestPushedTransaction).Include(up => up.Project).Include(up => up.Project.Creator).ToList();

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }


        public Revision GetPCBRevisionWithContentById(string revisionId)
        {
            try
            {
                var revision = DBContext.Revisions.Where(r => r.Id == revisionId).First();
                return revision;

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public void RemoveProject(string id, string userId)
        {
            try
            {
                var project = DBContext.Projects.Where(p => p.Id == id && p.CreatorId == userId).FirstOrDefault();
                if (project != null)
                {
                    var userProjects = DBContext.UserProjects.Where(p => p.ProjectId == id).ToList();
                    foreach (var userProject in userProjects)
                    {
                        DBContext.UserProjects.Remove(userProject);
                        DBContext.SaveChanges();
                    }
                    DBContext.Projects.Remove(project);
                    DBContext.SaveChanges();
                }
                else
                {
                    var userProject = DBContext.UserProjects.Where(p => p.ProjectId == id && p.UserProfileId == userId).FirstOrDefault();
                    if (userProject != null)
                    {
                        DBContext.UserProjects.Remove(userProject);
                        DBContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
        }


        

        UserProject GetUserProjectWithRevisionInfo(string userProjectId)
        {

            return DBContext.UserProjects.Where(up => up.Id == userProjectId).AsNoTracking()
                .Include(up => up.CurrentRevision).AsNoTracking()
                .Include(up => up.Project).ThenInclude(up => up.LatestRevision).AsNoTracking()
                .Include(up => up.Project).ThenInclude(up => up.LatestRevision)
                .ThenInclude(lr => lr.BaselineFile).AsNoTracking()
                .Include(up => up.Project).ThenInclude(up => up.LatestRevision)
                .ThenInclude(lr => lr.ProposalFile).AsNoTracking()
                .Include(up => up.Project).ThenInclude(up => up.LatestRevision)
                .ThenInclude(lr => lr.ResponseFile).AsNoTracking()
                .Include(up => up.Project)
                .ThenInclude(up => up.LatestRevision)
                .ThenInclude(up => up.PreviousRevision).AsNoTracking()
                .Include(up => up.Project)
                .ThenInclude(up => up.LatestRevision)
                .ThenInclude(up => up.PreviousRevision)
                .ThenInclude(up => up.BaselineFile).AsNoTracking()
                .FirstOrDefault();
        }




        /// <summary>
        /// Will cancel proposal sending if not locked by user, or removed lock if 
        /// </summary>
        /// <param name="transactionFile"></param>
        /// <param name="userProjectId"></param>
        /// <returns></returns>
        public bool CancelRevision(string userProjectId, string userId)
        {
            bool retval = true;
            try
            {
                var userProject = GetUserProjectWithRevisionInfo(userProjectId);
                if (userProject == null)
                {
                    return false;
                }
                var latestRevision = userProject.Project.LatestRevision;
                if (latestRevision.LockedByUserId == userId)
                {
                    latestRevision.LockedByUserId = null;
                    latestRevision.LockedByUser = null;
                    latestRevision.PreviousRevision = null;
                    latestRevision.LockedByUser = null;
                    latestRevision.AwaitedByUser = null;
                    latestRevision.ProposalFile = null;
                    latestRevision.BaselineFile = null;
                    latestRevision.ResponseFile = null;
                    latestRevision.Project = null;
                    DBContext.Revisions.Update(latestRevision);
                    DBContext.SaveChanges();
                    retval = true;
                }
                else if (latestRevision.LockedByUserId == null && latestRevision.AwaitedByUserId == userId)
                {
                    string previousRevisionId = latestRevision.PreviousRevisionId;
                    string latestRevisionId = latestRevision.Id;
                    userProject.Project.LatestRevisionId = previousRevisionId;
                    userProject.Project.LatestRevision = null;
                    userProject.Project.Creator = null;
                    DBContext.Projects.Update(userProject.Project);
                    DBContext.SaveChanges();
                    userProject = null;
                    var fetchedRev = DBContext.Revisions.Where(r => r.Id == latestRevisionId).FirstOrDefault();
                    DBContext.Revisions.Remove(fetchedRev);
                    DBContext.SaveChanges();
                    retval = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return retval;
        }

        /// <summary>
        /// Pusihing transaction file to db with preventively checking if revision stack didn't check
        /// </summary>
        /// <param name="transactionFile"></param>
        /// <param name="userProjectId"></param>
        /// <returns></returns>
        public Revision PushRevision(Revision revision, string userProjectId, bool checkIfUserIsAtLatestState)
        {
            try
            {
                var userProject = GetUserProjectWithRevisionInfo(userProjectId);
                if (userProject == null)
                {
                    throw new Exception(string.Format("Retrieved UserProject with Revision Info failed for userProjectId {0}", userProjectId));
                }
                var project = DBContext.Projects.Where(up => up.Id == userProject.ProjectId).FirstOrDefault();
                if (project == null)
                {
                    throw new Exception(string.Format("Could not get projects from projectId {0}", userProject.ProjectId));
                }
                EdmdRevisionSystem revisionSystem = new EdmdRevisionSystem(userProject);
                TransactionFile transactionFile = null;
                bool isResponse = false;
                bool isBaseline = false;
                bool isWebAppliedResponse = false;
                if (revision.ResponseFile != null)
                {
                    //if revision was not previously locked, response is made directly via web.
                    //The users project state should not be updated in this case.
                    isWebAppliedResponse = userProject.Project.LatestRevision.LockedByUserId == null;
                    revision.ResponseFile.Id = Guid.NewGuid().ToString();
                    revision.ResponseFile.BinaryContent.Id = Guid.NewGuid().ToString();
                    revision.ResponseFile.FileName = EdmdRevisionSystem.GenerateFileName(revision.ResponseFile.TransactionFileTypeStr);
                    if (revision.BaselineFile == null)
                    {
                        throw new Exception(string.Format("Baseline file from response revision was missing for userprojectId {0}", userProjectId));
                    }
                    revision.BaselineFile.Id = Guid.NewGuid().ToString();
                    revision.BaselineFile.BinaryContent.Id = Guid.NewGuid().ToString();
                    if (revision.BaselineFile.PCBRevisionContent != null)
                    {
                        revision.BaselineFile.PCBRevisionContent.Id = Guid.NewGuid().ToString();
                    }
                    revision.BaselineFile.FileName = EdmdRevisionSystem.GenerateFileName(revision.BaselineFile.TransactionFileTypeStr);
                    revision.BaselineFile.FullPath = "gen_baseline_" + revision.ResponseFile.FullPath;
                    transactionFile = revision.ResponseFile;
                    isResponse = true;
                    //returning response
                }
                else if (revision.BaselineFile != null)
                {
                    revision.BaselineFile.Id = Guid.NewGuid().ToString();
                    revision.BaselineFile.BinaryContent.Id = Guid.NewGuid().ToString();
                    revision.BaselineFile.FileName = EdmdRevisionSystem.GenerateFileName(revision.BaselineFile.TransactionFileTypeStr);
                    isBaseline = true;
                    revision.Id = Guid.NewGuid().ToString();
                    revision.CreationTime = DateTime.UtcNow;
                    transactionFile = revision.BaselineFile;
                    //pu
                }
                else if (revision.ProposalFile != null)
                {
                    revision.ProposalFile.Id = Guid.NewGuid().ToString();
                    revision.ProposalFile.BinaryContent.Id = Guid.NewGuid().ToString();
                    revision.ProposalFile.FileName = EdmdRevisionSystem.GenerateFileName(revision.ProposalFile.TransactionFileTypeStr);
                    revision.CreationTime = DateTime.UtcNow;
                    transactionFile = revision.ProposalFile;
                    revision.Id = Guid.NewGuid().ToString();
                    revision.AwaitedByUserId = userProject.UserProfileId;
                }
                else
                {
                    // incorrect revision
                    throw new Exception(string.Format("Incorrect revision becauses no transaction files specified for userprojectId {0}", userProjectId));
                }
                bool canPush = false;
                if (checkIfUserIsAtLatestState)
                {
                    var result = revisionSystem.CheckFileForPushing(out canPush, transactionFile.TransactionFileTypeStr);
                    if (!result)
                    {
                        throw new Exception(string.Format("Checking file for pushing failed {0}", userProjectId));
                    }
                }
                else
                {
                    var result = revisionSystem.CheckLastRevisionForPushing(out canPush, transactionFile.TransactionFileTypeStr);
                    if (!result)
                    {
                        throw new Exception(string.Format("Checking file for pushing failed {0}", userProjectId));
                    }
                }
                if (!canPush)
                {
                    throw new Exception(string.Format("Cannot push this revision for userProjectId {0}", userProjectId));
                }
                revision.ModificationTime = DateTime.UtcNow;
                if (isResponse)
                {
                    revision.ReferencedProject = null;
                    revision.LockedByUserId = null;
                    revision.LockedByUser = null;
                    DBContext.TransactionFiles.Add(revision.BaselineFile);
                    revision.BaselineFile.BinaryContentId = revision.BaselineFile.BinaryContent.Id;
                    DBContext.TransactionFiles.Add(revision.ResponseFile);
                    revision.ResponseFile.BinaryContentId = revision.ResponseFile.BinaryContent.Id;
                    revision.Project = null;
                    DBContext.Revisions.Update(revision);
                    DBContext.SaveChanges();
                }
                else
                {
                    if (isBaseline)
                    {
                        DBContext.TransactionFiles.Add(revision.BaselineFile);
                        revision.BaselineFile.BinaryContentId = revision.BaselineFile.BinaryContent.Id;
                    }
                    else
                    {
                        DBContext.TransactionFiles.Add(revision.ProposalFile);
                        revision.ProposalFile.BinaryContentId = revision.ProposalFile.BinaryContent.Id;
                    }
                    DBContext.Revisions.Add(revision);
                    DBContext.SaveChanges();

                    project.LatestRevisionId = revision.Id;
                    DBContext.Projects.Update(project);
                    DBContext.SaveChanges();
                }
                if ((isBaseline || isResponse))
                {
                    userProject.CurrentRevision = null;
                    if (!isWebAppliedResponse)
                    {
                        userProject.CurrentRevisionId = revision.Id; // if baseline and response set users project to latest
                    }
                    userProject.Project = null;
                    DBContext.UserProjects.Update(userProject);
                    DBContext.SaveChanges();
                }
                return revision;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }



        /// <summary>
        /// will get available file types so revidx client can choose file to push to revision
        /// </summary>
        /// <param name="userProjectId"></param>
        /// <returns></returns>
        public List<TransactionFile.TransactionFileType> GetAvailableFileTypesForPushing(string userProjectId)
        {
            try
            {
                var userProject = GetUserProjectWithRevisionInfo(userProjectId);
                EdmdRevisionSystem revisionSystem = new EdmdRevisionSystem(userProject);
                List<TransactionFile.TransactionFileType> transactionFileTypes = null;
                var result = revisionSystem.GetAvailableFileTypesForPushing(out transactionFileTypes);
                if (!result)
                {
                    throw new Exception(string.Format("Could not get available file types for pushing for userProjectId {0}", userProjectId));
                }
                return transactionFileTypes;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        /// <summary>
        /// Will pull specified transaction file, but will also check once again if operation is valid.
        /// </summary>
        /// <param name="transactionFileId"></param>
        /// <param name="userProjectId"></param>
        /// <returns></returns>
        public TransactionFile PullTransactionFileWithContent(string transactionFileId, string userProjectId)
        {
            try
            {
                var userProject = GetUserProjectWithRevisionInfo(userProjectId);
                Revision revision;
                TransactionFile transactionFile;
                EdmdRevisionSystem revisionSystem = new EdmdRevisionSystem(userProject);
                bool retval = revisionSystem.GetRevisionAndFileToPull(out transactionFile, out revision);
                if (!retval)
                {
                    throw new Exception(string.Format("Could not get available file types for pushing for userProjectId {0}", userProjectId));
                }
                else
                {
                    if (revision != null && transactionFile != null && transactionFileId != transactionFile.Id)
                    {
                        throw new Exception(string.Format("selected transaction file id does not equal to file id that was supposed to be pulled. State might have changed for UserProjectId {0}", userProjectId));
                    }
                    if (transactionFile != null)
                    {
                        if (transactionFile.TransactionFileTypeStr == TransactionFile.TransactionFileType.PROPOSAL.ToString())
                        {
                            revision.LockedByUserId = userProject.UserProfileId;
                            revision.PreviousRevision = null;
                            revision.ProposalFile = null;
                            DBContext.Revisions.Update(revision);
                            DBContext.SaveChanges();
                        }
                        else if (transactionFile.TransactionFileTypeStr == TransactionFile.TransactionFileType.BASELINE.ToString()
                            || transactionFile.TransactionFileTypeStr == TransactionFile.TransactionFileType.RESPONSE.ToString())
                        {
                            userProject.CurrentRevisionId = revision.Id;
                            userProject.Project = null;
                            userProject.CurrentRevision = null;
                            if (transactionFile.TransactionFileTypeStr == TransactionFile.TransactionFileType.RESPONSE.ToString())
                            {
                                revision.AwaitedByUserId = null;
                            }
                            revision.BaselineFile = null;
                            revision.ProposalFile = null;
                            revision.ResponseFile = null;
                            revision.PreviousRevision = null;
                            revision.Project = null;
                            DBContext.Revisions.Update(revision);
                            DBContext.UserProjects.Update(userProject);
                            DBContext.SaveChanges();
                        }
                        if (transactionFile.BinaryContent == null)
                        {
                            transactionFile.BinaryContent = DBContext.BinaryContents.Where(bc => bc.Id == transactionFile.BinaryContentId).FirstOrDefault();
                        }
                    }
                }
                return transactionFile;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }
        /// <summary>
        /// Will return revision which user can update to
        /// </summary>
        /// <param name="userProjectId"></param>
        /// <returns></returns>
        public Revision GetAvailableRevisionToPull(out RevisionState revisionState, string userProjectId)
        {
            revisionState = RevisionState.RevisionError;
            try
            {
                var userProject = GetUserProjectWithRevisionInfo(userProjectId);
                Revision revision;
                TransactionFile transactionFile;
                EdmdRevisionSystem revisionSystem = new EdmdRevisionSystem(userProject);
                bool retval = revisionSystem.GetRevisionAndFileToPull(out transactionFile, out revision);
                if (!retval)
                {
                    throw new Exception(string.Format("Could not get revision and file to pull for UserProjectId {0}", userProjectId));
                }
                revisionState = revisionSystem.CurrentRevisionState;
                //if proposal pull, lock transaction, if baseline update current, if 
                //if (!AddTransactionLog(result.Id, userProject.Id, userId, false))
                //{
                //    return null;
                //}

                return revision;

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        /// <summary>
        /// Made to be called to update after diff pull, but could also be essential to make after every pull that is SUCCESSFUL (could avoid rw errors, or processing error)
        /// </summary>
        /// <param name="userProjectId"></param>
        /// <param name="revisionId"></param>
        /// <param name="identityId"></param>
        /// <returns></returns>
        public bool UpdateUserProjectToRevision(string userProjectId, string revisionId, string userId)
        {
            var result = false;
            try
            {
                var userProject = DBContext.UserProjects.Where(up => up.Id == userProjectId).FirstOrDefault();
                if (userProject.UserProfileId != userId)
                {
                    throw new Exception("User profile Id is not matching");
                }
                userProject.CurrentRevision = null;
                userProject.CurrentRevisionId = revisionId;
                DBContext.Update(userProject);
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return result;
        }

        /// <summary>
        /// Will pull revision diffset and update users state. Should update users state afterwards.
        /// </summary>
        /// <param name="endRevisionId"></param>
        /// <param name="startRevisionId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public List<Revision> PullRevisionDiffSet(string endRevisionId, string startRevisionId)
        {
            throw new NotImplementedException();

        }
        /// <summary>
        /// Will get revision diffset and update users state. Should update users state afterwards.
        /// </summary>
        /// <param name="endRevisionId"></param>
        /// <param name="startRevisionId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public List<Revision> GetRevisionDiffSet(string endRevisionId, string startRevisionId)
        {
            throw new NotImplementedException();
            //List<Revision> revisions = null;
            //try
            //{
            //    Revision startRevision = null;
            //    if (startRevisionId != null)
            //    {
            //        startRevision = DBContext.Revisions.Where(r => r.Id == startRevisionId).First();
            //    }
            //    if (endRevisionId == null)
            //    {
            //    }
            //    var endRevision = DBContext.Revisions.Where(r => r.Id == endRevisionId).First();
            //    revisions = DBContext.Revisions.Where(r =>
            //    (r.Id == endRevision.Id
            //    || (endRevision.LastBaseRevisionId != null && r.LastBaseRevisionId == endRevision.LastBaseRevisionId)) // select end revision and any with same last baseline id
            //    && (startRevision == null || r.CreationTime >= startRevision.CreationTime) //make sure that rev is older than start file (if startrev not defined, it will just get all baseline by default)
            //    && (r.CreationTime <= endRevision.CreationTime)).Include(r => r.PCBRevisionContent).OrderBy(r => r.CreationTime).ToList(); // make sure rev is younger than end file
            //}
            //catch (Exception ex)
            //{
            //    Log.Error(ex.Message);
            //    while (ex.InnerException != null)
            //    {
            //        ex = ex.InnerException;
            //        Log.Error(ex.Message);
            //    }
            //}
            //return revisions;

        }


        public TransactionFile GetTransactionFileById(string transactionFileId)
        {
            try
            {
                return DBContext.TransactionFiles.Where(tf => tf.Id == transactionFileId).Include(t => t.BinaryContent).FirstOrDefault();

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public Revision GetRevisionById(string revisionId)
        {
            try
            {
                var revision = DBContext.Revisions.Where(up => up.Id == revisionId)
                    .Include(up => up.PreviousRevision).FirstOrDefault();
                return revision;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }
    }
}
