﻿using Microsoft.AspNetCore.Identity;
using RevidX.ServiceClient.Models;
using RevidXServer.Server.Models;

namespace RevidXServer.Server.Data
{
    public class DbDataInitializer
    {
        private static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("ProjectViewer").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "ProjectViewer";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("ProjectAuthor").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "ProjectAuthor";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("ProjectLeader").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "ProjectLeader";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("ProjectAdmin").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "ProjectAdmin";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }

            if (!roleManager.RoleExistsAsync("Administrator").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Administrator";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }

            if (!roleManager.RoleExistsAsync("Superadmin").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Superadmin";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
        }

        private static void SeedUsers(UserManager<ApplicationUser> userManager, PostgreSqlDbContext dbContext = null)
        {
            if (userManager.FindByEmailAsync("adminosaurus@revidx.com").Result == null)
            {
                ApplicationUser user = new ApplicationUser
                {
                    UserName = "adminosaurus@revidx.com",
                    Email = "adminosaurus@revidx.com",
                    EmailConfirmed = true,
                    FirstName = "Admino",
                    LastName = "Saurus",
                    CompanyName = "RevidX"
                };

                IdentityResult result = userManager.CreateAsync(user, "50=written=DESCRIBE=hunt=77").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Superadmin").Wait();
                    if (dbContext != null)
                    {
                        dbContext.UserProfiles.Add(new UserProfile()
                        {
                            IdentityId = user.Id,
                            Id = user.Id,
                            Email = user.Email,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            CompanyName = user.CompanyName
                        });
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        public static void SeedData (UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, PostgreSqlDbContext dbContext = null)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager, dbContext);

        }
    }
}
