﻿using Microsoft.EntityFrameworkCore;
using RevidX.ServiceClient.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RevidXServer.Server.Data
{
    public interface IDataAccessProviderAdmin
    {
        List<UserProfile> GetAllUserProfiles(); 
        List<UserProject> GetAllUserProjects(string userId);
        List<Revision> GetAllRevisions(string projectId);
    }

    public class DataAccessProviderAdmin : IDataAccessProviderAdmin
    {
        ApplicationDbContext DBContext
        { get; set; }
        public DataAccessProviderAdmin(PostgreSqlDbContext dbContext)
        {
            DBContext = dbContext;
        }
        public DataAccessProviderAdmin(MySqlDbContext dbContext)
        {
            DBContext = dbContext;
        }
        public DataAccessProviderAdmin(ApplicationDbContext dbContext)
        {
            DBContext = dbContext;
        }

        public List<UserProfile> GetAllUserProfiles()
        {
            try
            {
                return DBContext.UserProfiles.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }
        public List<UserProject> GetAllUserProjects(string userId)
        {
            try
            {
                return DBContext.UserProjects.Where(up => up.UserProfileId == userId).Include(up => up.Project).ThenInclude(up => up.Creator).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public List<Revision> GetAllRevisions(string projectId)
        {
            try
            {
                return DBContext.Revisions.Where(r => r.ProjectId == projectId).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Log.Error(ex.Message);
                }
            }
            return null;
        }
    }
}
