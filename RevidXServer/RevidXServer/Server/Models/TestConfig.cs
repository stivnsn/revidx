using Duende.IdentityServer.Models;
using System.Collections.Generic;
using Duende.IdentityServer.Test;
using static Duende.IdentityServer.IdentityServerConstants;

namespace RevidXServer.Server.Models
{

        public class Config
        {
            public static IEnumerable<IdentityResource> GetIdentityResources()
            {
                return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
            };
            }

            public static IEnumerable<ApiScope> GetApiScopes()
            {
                return new List<ApiScope>
            {
                // backward compat
                new ApiScope("api"),
                
                // more formal
                new ApiScope("api.scope1"),
                new ApiScope("api.scope2"),
                
                // scope without a resource
                new ApiScope("scope2"),
                
                // policyserver
                new ApiScope("policyserver.runtime"),
                new ApiScope("policyserver.management")
            };
            }

            public static IEnumerable<ApiResource> GetApis()
            {
                return new List<ApiResource>
            {
                new ApiResource("api", "Demo API")
                {
                    ApiSecrets = { new Secret("secret".Sha256()) },

                    Scopes = { "api", "api.scope1", "api.scope2" }
                }
            };
            }

            public static IEnumerable<Duende.IdentityServer.Models.Client> GetClients()
            {
                return new List<Duende.IdentityServer.Models.Client>
            {
new Duende.IdentityServer.Models.Client
                {
                    ClientId = "RevidXServer.Client",
                    ClientName = "SPAclient",


                },
                new Duende.IdentityServer.Models.Client
                {
                    ClientId = "interactive.public",
                    ClientName = "Interactive client (Code with PKCE)",

                    RedirectUris = { "https://notused" },
                    PostLogoutRedirectUris = { "https://notused" },

                    RequireClientSecret = false,

                    AllowedGrantTypes = GrantTypes.Code,
                    AllowedScopes = { "openid", "profile", "email", "api", "api.scope1", "api.scope2", "scope2" },

                    AllowOfflineAccess = true,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly,
                    RefreshTokenExpiration = TokenExpiration.Sliding
                }
            };
            }
        }
    public class TestConfig
    {
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("RevidXServer.ServerAPI", "My API"){ Scopes = new List<string>{ "api1", "RevidXServer.ServerAPI", "openid", "profile"  } },
                //new ApiResource("api", "My API"),
                //new ApiResource("openid", "My API"),
                //new ApiResource("profile", "profile")
            };
        }

        public static IEnumerable<Duende.IdentityServer.Models.Client> GetClients()
        {
            return new List<Duende.IdentityServer.Models.Client>
            {
                //new IdentityServer4.Models.Client
                //{
                //    ClientId = "client",
                //    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                //    ClientSecrets =
                //    {
                //        new Secret("secret".Sha256())
                //        //new Secret("RevidXServer.Server-C29B115D-DD3E-4AA7-82E4-14CC4C905D9D")
                //    },
                //    AllowOfflineAccess = true,
                //    AllowedScopes = { "api1", "RevidXServer.ServerAPI", "openid", "profile" }
                //},
                new Duende.IdentityServer.Models.Client
                {
                    ClientId = "client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                        //new Secret("RevidXServer.Server-C29B115D-DD3E-4AA7-82E4-14CC4C905D9D")
                    },
                    AllowedScopes = { "api1", "RevidXServer.ServerAPI", "openid", "profile" }
                }
            };
        }
        public static IEnumerable<ApiScope> GetScopes()
        {
            return new[]
            {
                    new ApiScope("api1", "Access to api1"),
                    new ApiScope("RevidXServer.ServerAPI", "Access to RevidXServer.ServerAPI"),
                    new ApiScope("openid", "openid"),
                    new ApiScope("profile", "Access to profile"),
                    //new ApiScope(StandardScopes.OfflineAccess, "Access to refresh tokens?"),
                };
        }

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    IsActive = true,
                    Password = "password"
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    IsActive = true,
                    Password = "password2"
                },

            };
        }
    }
}
