﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace RevidXServer.Server.Models
{
    public class EmailSender : IEmailSender
    {

        // Our private configuration variables
        private string host;
        private int port;
        private bool enableSSL;
        private string userName;
        private string password;
        private string fromEmail;

        // Get our parameterized configuration
        public EmailSender(string host, int port, bool enableSSL, string fromEmail, string userName, string password)
        {
            this.host = host;
            this.port = port;
            this.enableSSL = enableSSL;
            this.userName = userName;
            this.password = password;
            this.fromEmail = fromEmail;
        }

        // Use our configuration to send the email by using SmtpClient
        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {


            var client = new SmtpClient(host, port)
            {
                UseDefaultCredentials = false,
                EnableSsl = enableSSL,
                Credentials = new NetworkCredential(userName, password),
                //DeliveryMethod = SmtpDeliveryMethod.Network,
                //TargetName = "STARTTLS/smtp.office365.com"
            };
            return client.SendMailAsync(
                new MailMessage(fromEmail, email, subject, htmlMessage) { IsBodyHtml = true }
            );
        }
    }

}
//If you want to use outlook mail, you need to set Host as smtp.office365.com and Port as 587,

//"EmailSender": {
//    "Host": "smtp.office365.com",
//    "Port": 587,
//    "EnableSSL": true,
//    "UserName": "your@username.com",
//    "Password":  "Y0urP4ssw0rd!!!"
//  },
