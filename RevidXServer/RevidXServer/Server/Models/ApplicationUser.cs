﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RevidXServer.Server.Models
{
    public class ApplicationUser : IdentityUser
    {
        public bool hasAcceptedTheRisks;

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string CompanyName { get; set; }

        public string CollaborationStandardOther { get; set; }

        public bool isECADUser { get; set; }
        public bool isMCADUser { get; set; }
        public string CollaborationRoleOther { get; set; }
        public bool isIDXUser { get; set; }

        public bool isIDFUser { get; set; }
        public bool isSTEPUser { get; set; }
        public string XCADSoftwareOther { get; set; }
        public string ECADSoftware { get; set; }
        public string MCADSoftware { get; set; }
        public bool hasAcceptedTermsAndPrivacy { get; set; }
        public bool isSubscribedToNewsAndPromotion { get; set; }
    }
}
