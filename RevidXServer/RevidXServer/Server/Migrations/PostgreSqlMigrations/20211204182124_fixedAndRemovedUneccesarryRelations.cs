﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RevidXServer.Server.Migrations.PostgreSqlMigrations
{
    public partial class fixedAndRemovedUneccesarryRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFiles_UserProfiles_UserProfileId",
                table: "TransactionFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Projects_ProjectId",
                table: "Transactions");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFiles_UserProfiles_UserProfileId",
                table: "TransactionFiles",
                column: "UserProfileId",
                principalTable: "UserProfiles",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Projects_ProjectId",
                table: "Transactions",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFiles_UserProfiles_UserProfileId",
                table: "TransactionFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Projects_ProjectId",
                table: "Transactions");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFiles_UserProfiles_UserProfileId",
                table: "TransactionFiles",
                column: "UserProfileId",
                principalTable: "UserProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Projects_ProjectId",
                table: "Transactions",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
