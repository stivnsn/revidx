﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations.PostgreSqlMigrations
{
    public partial class RemovedCascadeDeleteForSelfRefRevisions4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions");

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions",
                column: "LastBaseRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions");

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions",
                column: "LastBaseRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
