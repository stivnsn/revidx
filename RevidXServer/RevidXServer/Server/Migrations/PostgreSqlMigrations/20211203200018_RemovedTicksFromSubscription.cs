﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RevidXServer.Server.Migrations.PostgreSqlMigrations
{
    public partial class RemovedTicksFromSubscription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Span",
                table: "Subscription");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Span",
                table: "Subscription",
                type: "character varying(48)",
                nullable: false,
                defaultValue: "");
        }
    }
}
