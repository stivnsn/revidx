﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations.PostgreSqlMigrations
{
    public partial class RemovedCascadeDeleteForSelfRefRevisions10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Revisions_LatestRevisionId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Projects_LatestRevisionId",
                table: "Projects");

            migrationBuilder.AddColumn<string>(
                name: "ReferencedProjectId",
                table: "Revisions",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RevisionId",
                table: "Projects",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Projects_LatestRevisionId",
                table: "Projects",
                column: "LatestRevisionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Projects_RevisionId",
                table: "Projects",
                column: "RevisionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Revisions_LatestRevisionId",
                table: "Projects",
                column: "LatestRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Revisions_RevisionId",
                table: "Projects",
                column: "RevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Revisions_LatestRevisionId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Revisions_RevisionId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Projects_LatestRevisionId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Projects_RevisionId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "ReferencedProjectId",
                table: "Revisions");

            migrationBuilder.DropColumn(
                name: "RevisionId",
                table: "Projects");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_LatestRevisionId",
                table: "Projects",
                column: "LatestRevisionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Revisions_LatestRevisionId",
                table: "Projects",
                column: "LatestRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
