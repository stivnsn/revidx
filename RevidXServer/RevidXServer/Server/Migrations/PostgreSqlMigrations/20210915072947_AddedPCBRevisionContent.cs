﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations.PostgreSqlMigrations
{
    public partial class AddedPCBRevisionContent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_UserProfiles_AwaitedByUserId",
                table: "Revisions");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFiles_UserProfiles_UserProfileId",
                table: "TransactionFiles");

            migrationBuilder.AddColumn<string>(
                name: "PCBRevisionContenttId",
                table: "TransactionFiles",
                type: "text",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PCBRevisionContent",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    JsonContent = table.Column<string>(type: "text", nullable: true),
                    TransactionFileId = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PCBRevisionContent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PCBRevisionContent_TransactionFiles_TransactionFileId",
                        column: x => x.TransactionFileId,
                        principalTable: "TransactionFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PCBRevisionContent_TransactionFileId",
                table: "PCBRevisionContent",
                column: "TransactionFileId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_UserProfiles_AwaitedByUserId",
                table: "Revisions",
                column: "AwaitedByUserId",
                principalTable: "UserProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFiles_UserProfiles_UserProfileId",
                table: "TransactionFiles",
                column: "UserProfileId",
                principalTable: "UserProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_UserProfiles_AwaitedByUserId",
                table: "Revisions");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFiles_UserProfiles_UserProfileId",
                table: "TransactionFiles");

            migrationBuilder.DropTable(
                name: "PCBRevisionContent");

            migrationBuilder.DropColumn(
                name: "PCBRevisionContenttId",
                table: "TransactionFiles");

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_UserProfiles_AwaitedByUserId",
                table: "Revisions",
                column: "AwaitedByUserId",
                principalTable: "UserProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFiles_UserProfiles_UserProfileId",
                table: "TransactionFiles",
                column: "UserProfileId",
                principalTable: "UserProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
