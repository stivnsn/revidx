﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations.PostgreSqlMigrations
{
    public partial class RemovedPCBRevisionAndLastBaseRevFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Revisions_RevisionId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_PCBRevisionContent_PCBRevisionContentId",
                table: "Revisions");

            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions");

            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions");

            migrationBuilder.DropTable(
                name: "PCBRevisionContent");

            migrationBuilder.DropIndex(
                name: "IX_Revisions_LastBaseRevisionId",
                table: "Revisions");

            migrationBuilder.DropIndex(
                name: "IX_Revisions_PCBRevisionContentId",
                table: "Revisions");

            migrationBuilder.DropIndex(
                name: "IX_Revisions_PreviousRevisionId",
                table: "Revisions");

            migrationBuilder.DropIndex(
                name: "IX_Projects_RevisionId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "LastBaseRevisionId",
                table: "Revisions");

            migrationBuilder.DropColumn(
                name: "PCBRevisionContentId",
                table: "Revisions");

            migrationBuilder.DropColumn(
                name: "RevisionId",
                table: "Projects");

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions");

            migrationBuilder.DropIndex(
                name: "IX_Revisions_PreviousRevisionId",
                table: "Revisions");

            migrationBuilder.AddColumn<string>(
                name: "LastBaseRevisionId",
                table: "Revisions",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PCBRevisionContentId",
                table: "Revisions",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RevisionId",
                table: "Projects",
                type: "text",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PCBRevisionContent",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    JsonContent = table.Column<string>(type: "text", nullable: true),
                    RevisionId = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PCBRevisionContent", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_LastBaseRevisionId",
                table: "Revisions",
                column: "LastBaseRevisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_PCBRevisionContentId",
                table: "Revisions",
                column: "PCBRevisionContentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Projects_RevisionId",
                table: "Projects",
                column: "RevisionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Revisions_RevisionId",
                table: "Projects",
                column: "RevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_PCBRevisionContent_PCBRevisionContentId",
                table: "Revisions",
                column: "PCBRevisionContentId",
                principalTable: "PCBRevisionContent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions",
                column: "LastBaseRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
