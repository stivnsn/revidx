﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations.PostgreSqlMigrations
{
    public partial class NewModsForDiffTracking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PCBRevisionContent_TransactionFiles_TransactionFileId",
                table: "PCBRevisionContent");

            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions");

            migrationBuilder.DropIndex(
                name: "IX_Revisions_PreviousRevisionId",
                table: "Revisions");

            migrationBuilder.DropIndex(
                name: "IX_PCBRevisionContent_TransactionFileId",
                table: "PCBRevisionContent");

            migrationBuilder.DropColumn(
                name: "PCBRevisionContenttId",
                table: "TransactionFiles");

            migrationBuilder.RenameColumn(
                name: "TransactionFileId",
                table: "PCBRevisionContent",
                newName: "RevisionId");

            migrationBuilder.AddColumn<string>(
                name: "LastBaseRevisionId",
                table: "Revisions",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PCBRevisionContentId",
                table: "Revisions",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_LastBaseRevisionId",
                table: "Revisions",
                column: "LastBaseRevisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_PCBRevisionContentId",
                table: "Revisions",
                column: "PCBRevisionContentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_PCBRevisionContent_PCBRevisionContentId",
                table: "Revisions",
                column: "PCBRevisionContentId",
                principalTable: "PCBRevisionContent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions",
                column: "LastBaseRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_PCBRevisionContent_PCBRevisionContentId",
                table: "Revisions");

            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions");

            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions");

            migrationBuilder.DropIndex(
                name: "IX_Revisions_LastBaseRevisionId",
                table: "Revisions");

            migrationBuilder.DropIndex(
                name: "IX_Revisions_PCBRevisionContentId",
                table: "Revisions");

            migrationBuilder.DropIndex(
                name: "IX_Revisions_PreviousRevisionId",
                table: "Revisions");

            migrationBuilder.DropColumn(
                name: "LastBaseRevisionId",
                table: "Revisions");

            migrationBuilder.DropColumn(
                name: "PCBRevisionContentId",
                table: "Revisions");

            migrationBuilder.RenameColumn(
                name: "RevisionId",
                table: "PCBRevisionContent",
                newName: "TransactionFileId");

            migrationBuilder.AddColumn<string>(
                name: "PCBRevisionContenttId",
                table: "TransactionFiles",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId");

            migrationBuilder.CreateIndex(
                name: "IX_PCBRevisionContent_TransactionFileId",
                table: "PCBRevisionContent",
                column: "TransactionFileId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PCBRevisionContent_TransactionFiles_TransactionFileId",
                table: "PCBRevisionContent",
                column: "TransactionFileId",
                principalTable: "TransactionFiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
