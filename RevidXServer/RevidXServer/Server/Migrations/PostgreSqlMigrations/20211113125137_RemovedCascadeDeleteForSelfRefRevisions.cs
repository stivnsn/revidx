﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations.PostgreSqlMigrations
{
    public partial class RemovedCascadeDeleteForSelfRefRevisions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions");

            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions");

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions",
                column: "LastBaseRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions");

            migrationBuilder.DropForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions");

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_LastBaseRevisionId",
                table: "Revisions",
                column: "LastBaseRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Revisions_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
