﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations.PostgreSqlMigrations
{
    public partial class RemovedCascadeDeleteForSelfRefRevisions5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Revisions_LatestRevisionId",
                table: "Projects");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Revisions_LatestRevisionId",
                table: "Projects",
                column: "LatestRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Revisions_LatestRevisionId",
                table: "Projects");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Revisions_LatestRevisionId",
                table: "Projects",
                column: "LatestRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
