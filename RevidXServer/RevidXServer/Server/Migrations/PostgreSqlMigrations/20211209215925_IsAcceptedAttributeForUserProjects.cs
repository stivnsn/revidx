﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RevidXServer.Server.Migrations.PostgreSqlMigrations
{
    public partial class IsAcceptedAttributeForUserProjects : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsAccepted",
                table: "UserProjects",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAccepted",
                table: "UserProjects");
        }
    }
}
