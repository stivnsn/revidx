﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations.MySqlMigrations
{
    public partial class AddedFileType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId",
                table: "TransactionFileActivity");

            migrationBuilder.DropIndex(
                name: "IX_TransactionFileActivity_TransactionFileId",
                table: "TransactionFileActivity");

            migrationBuilder.AddColumn<string>(
                name: "TransactionFileTypeStr",
                table: "TransactionFiles",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "TransactionFileId",
                table: "TransactionFileActivity",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(255)",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "TransactionFileId1",
                table: "TransactionFileActivity",
                type: "varchar(255)",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFileActivity_TransactionFileId1",
                table: "TransactionFileActivity",
                column: "TransactionFileId1");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId1",
                table: "TransactionFileActivity",
                column: "TransactionFileId1",
                principalTable: "TransactionFiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId1",
                table: "TransactionFileActivity");

            migrationBuilder.DropIndex(
                name: "IX_TransactionFileActivity_TransactionFileId1",
                table: "TransactionFileActivity");

            migrationBuilder.DropColumn(
                name: "TransactionFileTypeStr",
                table: "TransactionFiles");

            migrationBuilder.DropColumn(
                name: "TransactionFileId1",
                table: "TransactionFileActivity");

            migrationBuilder.AlterColumn<string>(
                name: "TransactionFileId",
                table: "TransactionFileActivity",
                type: "varchar(255)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFileActivity_TransactionFileId",
                table: "TransactionFileActivity",
                column: "TransactionFileId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId",
                table: "TransactionFileActivity",
                column: "TransactionFileId",
                principalTable: "TransactionFiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
