﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations.MySqlMigrations
{
    public partial class migrationFromMsSql : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId",
                table: "TransactionFileActivity");

            migrationBuilder.DropIndex(
                name: "IX_TransactionFileActivity_TransactionFileId",
                table: "TransactionFileActivity");

            migrationBuilder.DropColumn(
                name: "Content",
                table: "TransactionFiles");

            migrationBuilder.DropColumn(
                name: "IsLocked",
                table: "TransactionFiles");

            migrationBuilder.RenameColumn(
                name: "ModificationTime",
                table: "TransactionFiles",
                newName: "LastWriteTime");

            migrationBuilder.RenameColumn(
                name: "CreationTime",
                table: "TransactionFiles",
                newName: "LastAccessTime");

            migrationBuilder.AddColumn<string>(
                name: "CurrentRevisionId",
                table: "UserProjects",
                type: "varchar(255)",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "BinaryContentId",
                table: "TransactionFiles",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "TransactionFileTypeStr",
                table: "TransactionFiles",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "TransactionFileId",
                table: "TransactionFileActivity",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(255)",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "TransactionFileId1",
                table: "TransactionFileActivity",
                type: "varchar(255)",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "LatestRevisionId",
                table: "Projects",
                type: "varchar(255)",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "BinaryContent",
                columns: table => new
                {
                    Id = table.Column<string>(type: "varchar(255)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Content = table.Column<byte[]>(type: "longblob", nullable: true),
                    TransactionFileId = table.Column<string>(type: "varchar(255)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BinaryContent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BinaryContent_TransactionFiles_TransactionFileId",
                        column: x => x.TransactionFileId,
                        principalTable: "TransactionFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Revisions",
                columns: table => new
                {
                    Id = table.Column<string>(type: "varchar(255)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    ModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    ProjectId = table.Column<string>(type: "varchar(255)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    LockedByUserId = table.Column<string>(type: "varchar(255)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PreviousRevisionId = table.Column<string>(type: "varchar(255)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    BaselineFileId = table.Column<string>(type: "varchar(255)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ProposalFileId = table.Column<string>(type: "varchar(255)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ResponseFileId = table.Column<string>(type: "varchar(255)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Revisions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Revisions_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Revisions_Revisions_PreviousRevisionId",
                        column: x => x.PreviousRevisionId,
                        principalTable: "Revisions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Revisions_TransactionFiles_BaselineFileId",
                        column: x => x.BaselineFileId,
                        principalTable: "TransactionFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Revisions_TransactionFiles_ProposalFileId",
                        column: x => x.ProposalFileId,
                        principalTable: "TransactionFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Revisions_TransactionFiles_ResponseFileId",
                        column: x => x.ResponseFileId,
                        principalTable: "TransactionFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Revisions_UserProfiles_LockedByUserId",
                        column: x => x.LockedByUserId,
                        principalTable: "UserProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_UserProjects_CurrentRevisionId",
                table: "UserProjects",
                column: "CurrentRevisionId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFileActivity_TransactionFileId1",
                table: "TransactionFileActivity",
                column: "TransactionFileId1");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_LatestRevisionId",
                table: "Projects",
                column: "LatestRevisionId");

            migrationBuilder.CreateIndex(
                name: "IX_BinaryContent_TransactionFileId",
                table: "BinaryContent",
                column: "TransactionFileId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_BaselineFileId",
                table: "Revisions",
                column: "BaselineFileId");

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_LockedByUserId",
                table: "Revisions",
                column: "LockedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_PreviousRevisionId",
                table: "Revisions",
                column: "PreviousRevisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_ProjectId",
                table: "Revisions",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_ProposalFileId",
                table: "Revisions",
                column: "ProposalFileId");

            migrationBuilder.CreateIndex(
                name: "IX_Revisions_ResponseFileId",
                table: "Revisions",
                column: "ResponseFileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Revisions_LatestRevisionId",
                table: "Projects",
                column: "LatestRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId1",
                table: "TransactionFileActivity",
                column: "TransactionFileId1",
                principalTable: "TransactionFiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserProjects_Revisions_CurrentRevisionId",
                table: "UserProjects",
                column: "CurrentRevisionId",
                principalTable: "Revisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Revisions_LatestRevisionId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId1",
                table: "TransactionFileActivity");

            migrationBuilder.DropForeignKey(
                name: "FK_UserProjects_Revisions_CurrentRevisionId",
                table: "UserProjects");

            migrationBuilder.DropTable(
                name: "BinaryContent");

            migrationBuilder.DropTable(
                name: "Revisions");

            migrationBuilder.DropIndex(
                name: "IX_UserProjects_CurrentRevisionId",
                table: "UserProjects");

            migrationBuilder.DropIndex(
                name: "IX_TransactionFileActivity_TransactionFileId1",
                table: "TransactionFileActivity");

            migrationBuilder.DropIndex(
                name: "IX_Projects_LatestRevisionId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "CurrentRevisionId",
                table: "UserProjects");

            migrationBuilder.DropColumn(
                name: "BinaryContentId",
                table: "TransactionFiles");

            migrationBuilder.DropColumn(
                name: "TransactionFileTypeStr",
                table: "TransactionFiles");

            migrationBuilder.DropColumn(
                name: "TransactionFileId1",
                table: "TransactionFileActivity");

            migrationBuilder.DropColumn(
                name: "LatestRevisionId",
                table: "Projects");

            migrationBuilder.RenameColumn(
                name: "LastWriteTime",
                table: "TransactionFiles",
                newName: "ModificationTime");

            migrationBuilder.RenameColumn(
                name: "LastAccessTime",
                table: "TransactionFiles",
                newName: "CreationTime");

            migrationBuilder.AddColumn<byte[]>(
                name: "Content",
                table: "TransactionFiles",
                type: "longblob",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsLocked",
                table: "TransactionFiles",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<string>(
                name: "TransactionFileId",
                table: "TransactionFileActivity",
                type: "varchar(255)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFileActivity_TransactionFileId",
                table: "TransactionFileActivity",
                column: "TransactionFileId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId",
                table: "TransactionFileActivity",
                column: "TransactionFileId",
                principalTable: "TransactionFiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
