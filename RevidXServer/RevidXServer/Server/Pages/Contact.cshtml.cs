using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace RevidXServer.Server.Pages
{
    public class ContactModel : PageModel
    {
        private readonly ILogger<ContactModel> _logger;
        private readonly IEmailSender _emailSender;
        public ContactModel(
            ILogger<ContactModel> logger, IEmailSender emailSender)
        {

            _emailSender = emailSender;
            _logger = logger;
        }
        public void OnGet()
        {
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel
        {
            [Required]
            [Display(Name = "First Name")]
            public string FirstName { get; set; }
            [Required]
            [Display(Name = "Last Name")]
            public string LastName { get; set; }
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [Display(Name = "Subject")]
            public string Subject { get; set; }

            [Display(Name = "Company")]
            public string Company { get; set; }


            [Required]
            [Display(Name = "Message")]
            public string Message { get; set; }



        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {

            if (ModelState.IsValid)
            {

                try
                {
                    string message = $"Email: {Input.Email} <hr/> First name:{Input.FirstName} <hr/>Last name: {Input.LastName} <hr/>Company: {Input.Company} <hr/>Message: {Input.Message} <br/>";

                    Log.Information($"Email: {Input.Email} <hr/> First name:{Input.FirstName} <hr/>Last name: {Input.LastName} <hr/>Company: {Input.Company} has send message via contact form.");
                    await _emailSender.SendEmailAsync("support@revidx.com", Input.Subject, message);
                    return RedirectToPage("/ContactSuccess");
                }
                catch(Exception ex)
                {
                    Log.Error(ex.Message);
                    _logger.LogError(ex.Message);
                    ModelState.AddModelError(string.Empty, "Something went wrong with sending email. Contact support directly via email: support@revidx.com.");
                }
            }

            return Page();
        }
    }
}
