﻿using Duende.IdentityServer.Models;
using Duende.IdentityServer.Services;
using Microsoft.AspNetCore.Identity;
using RevidXServer.Server.Data;
using RevidXServer.Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using RevidX.ServiceClient.Models;
using Serilog;
using IdentityModel;

namespace RevidXServer.Server
{
    public class ProfileService : IProfileService
    {
        protected UserManager<ApplicationUser> userManager;
        private ApplicationDbContext dbContext;
        public ProfileService(UserManager<ApplicationUser> userManager, PostgreSqlDbContext dbContext)
        {
            this.userManager = userManager;
            this.dbContext = dbContext;
        }
        public ProfileService(UserManager<ApplicationUser> userManager, MySqlDbContext dbContext)
        {
            this.userManager = userManager;
            this.dbContext = dbContext;
        }
        public ProfileService(UserManager<ApplicationUser> userManager, ApplicationDbContext dbContext)
        {
            this.userManager = userManager;
            this.dbContext = dbContext;
        }
        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            try
            {
                var user = userManager.GetUserAsync(context.Subject).Result;
                var name = $"{user.FirstName} {user.LastName}";
                var claims = new List<Claim>
        {
            new Claim("FullName", name),
            new Claim(ClaimTypes.Name, name),
            new Claim("Name", name),
            new Claim(ClaimTypes.GivenName, name),
            new Claim(ClaimTypes.NameIdentifier, name),
        }; 
                var jwtRoleClaims = context.Subject.FindAll(JwtClaimTypes.Role);
                context.IssuedClaims.AddRange(jwtRoleClaims); 
                var roleClaims = context.Subject.FindAll(ClaimTypes.Role);
                context.IssuedClaims.AddRange(roleClaims);
                var userClaims = dbContext.UserClaims.Where(m => m.UserId == user.Id).ToList();
                userClaims.ForEach(m => claims.Add(new Claim(m.ClaimType, m.ClaimValue)));
                context.IssuedClaims.AddRange(claims);
                var userProfile = dbContext.UserProfiles.Where(u => u.Id == user.Id).FirstOrDefault();
                if (userProfile == null)
                {
                    dbContext.UserProfiles.Add(new UserProfile()
                    {
                        IdentityId = user.Id,
                        Id = user.Id,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        CompanyName = user.CompanyName
                    });
                    dbContext.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                Log.Error("Getting profile data failed: " + ex.Message);
                return Task.FromException(ex);
            }
            return Task.FromResult(0);
        }
        public Task IsActiveAsync(IsActiveContext context)
        {
            var user = userManager.GetUserAsync(context.Subject).Result;
            context.IsActive = user != null && user.LockoutEnd == null;
            return Task.FromResult(0);
        }
    }
}
