using Braintree;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using RevidXServer.Server.Data;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RevidXServer.Server.Areas.Payment
{
    public class CheckoutModel : PageModel
    {
        public IBraintreeConfiguration config;


        [BindProperty]
        public string ClientToken { get; set; }

        [BindProperty]
        public string nonce { get; set; } = "";

        [BindProperty]
        public decimal amount { get; set; } = 0;
        public CheckoutModel(IConfiguration configuration)
        {
            config = new BraintreeConfiguration(configuration);
        }

        public static readonly TransactionStatus[] transactionSuccessStatuses = {
                                                                                    TransactionStatus.AUTHORIZED,
                                                                                    TransactionStatus.AUTHORIZING,
                                                                                    TransactionStatus.SETTLED,
                                                                                    TransactionStatus.SETTLING,
                                                                                    TransactionStatus.SETTLEMENT_CONFIRMED,
                                                                                    TransactionStatus.SETTLEMENT_PENDING,
                                                                                    TransactionStatus.SUBMITTED_FOR_SETTLEMENT
                                                                                };

        public ActionResult OnGetAsync()
        {
            var gateway = config.GetGateway();
            ClientToken = gateway.ClientToken.Generate();
            return Page();
            //ViewBag.ClientToken = clientToken;
            //return View();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var gateway = config.GetGateway();
            //decimal amount;

            //try
            //{
            //    amount = Convert.ToDecimal(amountStr);
            //}
            //catch (FormatException e)
            //{
            //    TempData["Flash"] = "Error: 81503: Amount is an invalid format.";
            //    return RedirectToAction("New");
            //}

            var nonce = this.nonce;
            var request = new TransactionRequest
            {
                Amount = amount,
                PaymentMethodNonce = nonce,
                Options = new TransactionOptionsRequest
                {
                    SubmitForSettlement = true
                }
            };

            Result<Transaction> result = gateway.Transaction.Sale(request);
            if (result.IsSuccess())
            {
                Transaction transaction = result.Target;
                return Redirect("/Identity/Account/Result/" + transaction.Id);
            }
            else if (result.Transaction != null)
            {
                return Redirect("/Identity/Account/Result/" + result.Transaction.Id);
            }
            else
            {
                string errorMessages = "";
                foreach (ValidationError error in result.Errors.DeepAll())
                {
                    errorMessages += "Error: " + (int)error.Code + " - " + error.Message + "\n";
                }
                TempData["Flash"] = errorMessages;
                return Redirect("New");
            }
        }

    }
}
