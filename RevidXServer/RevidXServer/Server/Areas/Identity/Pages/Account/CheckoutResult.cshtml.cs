using Braintree;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using RevidXServer.Server.Data;
using System.Linq;

namespace RevidXServer.Server.Areas.Payment
{
    public class CheckoutResultModel : PageModel
    {
        public IBraintreeConfiguration config;
        public CheckoutResultModel(IConfiguration configuration)
        {
            config = new BraintreeConfiguration(configuration);
        }

        public static readonly TransactionStatus[] transactionSuccessStatuses = {
                                                                                    TransactionStatus.AUTHORIZED,
                                                                                    TransactionStatus.AUTHORIZING,
                                                                                    TransactionStatus.SETTLED,
                                                                                    TransactionStatus.SETTLING,
                                                                                    TransactionStatus.SETTLEMENT_CONFIRMED,
                                                                                    TransactionStatus.SETTLEMENT_PENDING,
                                                                                    TransactionStatus.SUBMITTED_FOR_SETTLEMENT
                                                  };


        [BindProperty]
        public Transaction Transaction { get; set; }

        [BindProperty(SupportsGet = true)]
        public string id { get; set; }
        public ActionResult OnGet()
        {
                var gateway = config.GetGateway();
                Transaction = gateway.Transaction.Find(id);

                if (transactionSuccessStatuses.Contains(Transaction.Status))
                {
                    TempData["header"] = "Sweet Success!";
                    TempData["icon"] = "success";
                    TempData["message"] = "Your test transaction has been successfully processed. See the Braintree API response and try again.";
                }
                else
                {
                    TempData["header"] = "Transaction Failed";
                    TempData["icon"] = "fail";
                    TempData["message"] = "Your test transaction has a status of " + Transaction.Status + ". See the Braintree API response and try again.";
                };
                return Page();
                //ViewBag.Transaction = transaction;
                //return View();
        }

    }
}
