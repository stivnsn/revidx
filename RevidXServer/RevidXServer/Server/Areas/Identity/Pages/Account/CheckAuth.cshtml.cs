using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace RevidXServer.Server.Areas.Identity.Pages.Account
{

    [AllowAnonymous]
    public class CheckAuthModel : PageModel
    {
        public CheckAuthModel()
        {
        }
        public IActionResult OnGet()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Signup");
            }
            else
            {
                return Redirect("/app");
            }
        }
    }
}
