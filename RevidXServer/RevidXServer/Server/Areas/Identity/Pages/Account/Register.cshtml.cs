﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using RevidX.ServiceClient.Models;
using RevidXServer.Server.Data;
using RevidXServer.Server.Models;
using Serilog;

namespace RevidXServer.Server.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private ApplicationDbContext dbContext;

        public RegisterModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            PostgreSqlDbContext postgreSqlDbContext,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            dbContext = postgreSqlDbContext;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [Required]
            [Display(Name = "First Name")]
            public string FirstName { get; set; }
            [Required]
            [Display(Name = "Last Name")]
            public string LastName { get; set; }
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [Display(Name = "Company name")]
            public string CompanyName { get; set; }

            [Display(Name = "Actively used collaboration standard:")]
            public string CollaborationStandardOther { get; set; }

            [Display(Name = "ECAD user")]
            public bool isECADUser { get; set; }

            [Display(Name = "MCAD user")]
            public bool isMCADUser { get; set; }
            [Display(Name = "Your role in collaboration:")]
            public string CollaborationRoleOther { get; set; }

            [Display(Name = "IDX user")]
            public bool isIDXUser { get; set; }

            [Display(Name = "IDF user")]
            public bool isIDFUser { get; set; }

            [Display(Name = "STEP user")]
            public bool isSTEPUser { get; set; }

            [Display(Name = "Which software do you use?")]
            public string XCADSoftwareOther { get; set; }


            [Display(Name = "ECAD software")]
            public string ECADSoftware { get; set; }

            [Display(Name = "MCAD software")]
            public string MCADSoftware { get; set; }

            [Required]
            public bool hasAcceptedTermsAndPrivacy { get; set; }

            [Required]
            public bool isAcceptedTheRisks { get; set; }

            public bool isSubscribedToNewsAndPromotion { get; set; }

        }

        public async Task<IActionResult> OnGetAsync(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("/app");
            }
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = Input.Email,
                    Email = Input.Email,
                    FirstName = Input.FirstName,
                    LastName = Input.LastName,
                    CompanyName = Input.CompanyName,
                    ECADSoftware = Input.ECADSoftware,
                    MCADSoftware = Input.MCADSoftware,
                    isECADUser = Input.isECADUser,
                    isMCADUser = Input.isMCADUser,
                    isIDFUser = Input.isIDFUser,
                    isIDXUser = Input.isIDXUser,
                    isSTEPUser = Input.isSTEPUser,
                    hasAcceptedTheRisks = Input.isAcceptedTheRisks,
                    CollaborationRoleOther = Input.CollaborationRoleOther,
                    CollaborationStandardOther = Input.CollaborationStandardOther,
                    XCADSoftwareOther = Input.XCADSoftwareOther,
                    hasAcceptedTermsAndPrivacy = Input.hasAcceptedTermsAndPrivacy,
                    isSubscribedToNewsAndPromotion = Input.isSubscribedToNewsAndPromotion
                };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {

                    result = await _userManager.AddToRoleAsync(user, "ProjectAuthor");

                    if (!result.Succeeded)
                    {
                        Log.Error($"Could not add {user.Email} to role.");
                        _logger.LogError($"Could not add {user.Email} to role.");
                    }
                    Log.Information($"User {user.Email} created a new account with password. Creating db user...");
                    _logger.LogInformation($"User {user.Email} created a new account with password. Creating db user...");
                    dbContext.UserProfiles.Add(new UserProfile()
                    {
                        IdentityId = user.Id,
                        Id = user.Id,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        CompanyName = user.CompanyName
                    });
                    dbContext.SaveChanges();
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    Log.Information($"User {user.Email} created a new account with password. Success...");
                    _logger.LogInformation($"User {user.Email} created a new account with password. Success...");
                    return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
