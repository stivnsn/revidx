﻿using Microsoft.AspNetCore.Components;
using PCBXRevision;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static RevidXServer.Client.Pages.PreviewComponent;

namespace RevidXServer.Client
{
    
    public class TreeMarkupHelpers
    {

        static string itemInfoTag = "<li><label>{0}</label></li>";
        //static string itemTag = "<label><input id=\"node-4\" data-id=\"custom-1\" type=\"checkbox\">"
        //+ "<MatIconButton Style=\"height: 14px; width: 14px; padding: 0px\" @onclick=\"@(e=> { focusOnMesh(area.ObjectData.ObjectName);})\"><MatIcon>@MatIconNames.Filter_center_focus</MatIcon></MatIconButton>@area.Purpose</label><ul>{0}</ul>";


        public static string GetOutlineDiffString(Outline mainOutline, Outline diffOutline)
        {

            string outlineString = string.Format(itemInfoTag, string.Format("Transformation: {0} -> {1}", mainOutline.Transformation.TransformationString
                , diffOutline.Transformation.TransformationString));
            outlineString += string.Format(itemInfoTag, string.Format("LowerBound: {0} -> {1}", mainOutline.LowerBound, diffOutline.LowerBound));
            outlineString += string.Format(itemInfoTag, string.Format("UpperBound: {0} -> {1}", mainOutline.UpperBound, diffOutline.UpperBound));
            outlineString += string.Format(itemInfoTag, string.Format("Segments: {0} -> {1}", mainOutline.Segments.Count(), diffOutline.Segments.Count));
            return outlineString;
        }

        public static string GetOutlineString(Outline mainOutline)
        {
            string outlineString = string.Format(itemInfoTag, string.Format("Transformation: {0}", mainOutline.Transformation.TransformationString));
            outlineString += string.Format(itemInfoTag, string.Format("LowerBound: {0}", mainOutline.LowerBound));
            outlineString += string.Format(itemInfoTag, string.Format("UpperBound: {0}", mainOutline.UpperBound));
            outlineString += string.Format(itemInfoTag, string.Format("Segments: {0}", mainOutline.Segments.Count()));
            return outlineString;
        }

        public static MarkupString GetChangeTypeLetter(ChangeType? changeType)
        {
            var retString = "<span style=\"font-size:10px\">[A]</span> ";
            if (changeType == null)
            {
                retString = "";
            }
            else if (changeType == ChangeType.DELETION)
            {

                retString = "<span style=\"font-size:10px\">[D]</span> ";
            }
            else if (changeType == ChangeType.MODIFICATION)
            {
                retString = "<span style=\"font-size:10px\">[M]</span> ";
            }
            return (MarkupString)retString;
        }

        public static string GetColorForType(ChangeType? changeType)
        {
            var color = "#00ff90";
            if (changeType == null)
            {
                color = "blue";
            }
            else if (changeType == ChangeType.DELETION)
            {

                color = "#fe5f5f";
            }
            else if (changeType == ChangeType.MODIFICATION)
            {
                color = "#fdffc4";
            }
            return color;
        }

        public static MarkupString GetBoardMarkup(Board mainBoard)
        {
            string ul = string.Format(itemInfoTag, mainBoard.ObjectData.Identifier.GetNameCombined());
            ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", mainBoard.Thickness));
            if (mainBoard.GeometricalShape.Outline != null && mainBoard.GeometricalShape.Outline.Segments != null)
            {
                ul += GetOutlineString(mainBoard.GeometricalShape.Outline);
            }
            return (MarkupString)string.Format("<ul>{0}</ul>", ul);
        }

        public static MarkupString GetBoardDiffMarkup(Board diffBoard, Board mainBoard)
        {
            string ul = string.Format(itemInfoTag, diffBoard.ObjectData.Identifier.GetNameCombined());
            ul += string.Format(itemInfoTag, string.Format("Thickness: {0} -> {1}", diffBoard.Thickness, diffBoard.Thickness));
            if (mainBoard.GeometricalShape.Outline != null && mainBoard.GeometricalShape.Outline.Segments != null
                && diffBoard.GeometricalShape.Outline != null && diffBoard.GeometricalShape.Outline.Segments != null)
            {
                ul += GetOutlineDiffString(mainBoard.GeometricalShape.Outline, diffBoard.GeometricalShape.Outline);
            }
            return (MarkupString)string.Format("<ul>{0}</ul>", ul);
        }

        public static MarkupString GetCutoutMarkup(Cutout mainCutout)
        {
            string ul = string.Format(itemInfoTag, mainCutout.ObjectData.Identifier.GetNameCombined());
            if (mainCutout.GeometricalShape.Outline != null && mainCutout.GeometricalShape.Outline.Segments != null)
            {
                ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", mainCutout.GeometricalShape.Outline.ThicknessCalculated));
                ul += GetOutlineString(mainCutout.GeometricalShape.Outline);
            }
            return (MarkupString)string.Format("<ul>{0}</ul>", ul);
        }

        public static MarkupString GetCutoutDiffMarkup(Cutout diffCutout, Cutout mainCutout)
        {
            string ul = string.Format(itemInfoTag, diffCutout.ObjectData.Identifier.GetNameCombined());
            if (mainCutout == null || diffCutout.ObjectData.ChangeType == ChangeType.ADDITION)
            {
                if (diffCutout.GeometricalShape.Outline != null && diffCutout.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", diffCutout.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineString(diffCutout.GeometricalShape.Outline);
                }
            }
            else if (diffCutout.ObjectData.ChangeType == ChangeType.DELETION)
            {
                if (mainCutout.GeometricalShape.Outline != null && mainCutout.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", mainCutout.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineString(mainCutout.GeometricalShape.Outline);
                }
            }
            else
            {
                if (mainCutout.GeometricalShape.Outline != null && mainCutout.GeometricalShape.Outline.Segments != null
                    && diffCutout.GeometricalShape.Outline != null && diffCutout.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0} -> {1}", mainCutout.GeometricalShape.Outline.ThicknessCalculated
                        , diffCutout.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineDiffString(mainCutout.GeometricalShape.Outline, diffCutout.GeometricalShape.Outline);
                }
            }
            return (MarkupString)string.Format("<ul>{0}</ul>", ul);
        }


        public static MarkupString GetHoleMarkup(Hole mainHole)
        {
            string ul = string.Format(itemInfoTag, mainHole.ObjectData.Identifier.GetNameCombined());
            if (mainHole.GeometricalShape.Outline != null && mainHole.GeometricalShape.Outline.Segments != null)
            {
                ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", mainHole.GeometricalShape.Outline.ThicknessCalculated));
                ul += GetOutlineString(mainHole.GeometricalShape.Outline);
            }
            return (MarkupString)string.Format("<ul>{0}</ul>", ul);
        }

        public static MarkupString GetHoleDiffMarkup(Hole diffHole, Hole mainHole)
        {
            string ul = string.Format(itemInfoTag, diffHole.ObjectData.Identifier.GetNameCombined());

            if (mainHole == null || diffHole.ObjectData.ChangeType == ChangeType.ADDITION)
            {
                if (diffHole.GeometricalShape.Outline != null && diffHole.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", diffHole.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineString(diffHole.GeometricalShape.Outline);
                }
            }
            else if (diffHole.ObjectData.ChangeType == ChangeType.DELETION)
            {
                if (mainHole.GeometricalShape.Outline != null && mainHole.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", mainHole.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineString(mainHole.GeometricalShape.Outline);
                }
            }
            else
            {
                if (mainHole.GeometricalShape.Outline != null && mainHole.GeometricalShape.Outline.Segments != null
        && diffHole.GeometricalShape.Outline != null && diffHole.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0} -> {1}", mainHole.GeometricalShape.Outline.ThicknessCalculated
                        , diffHole.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineDiffString(mainHole.GeometricalShape.Outline, diffHole.GeometricalShape.Outline);
                }
            }
            return (MarkupString)string.Format("<ul>{0}</ul>", ul);
        }


        public static MarkupString GetAreaMarkup(ConstraintArea mainArea)
        {
            string ul = string.Format(itemInfoTag, mainArea.ObjectData.Identifier.GetNameCombined());
            ul += string.Format(itemInfoTag, string.Format("Side: {0}", mainArea.Side.ToString()));
            ul += string.Format(itemInfoTag, string.Format("Purpose: {0}", mainArea.Purpose.ToString()));

            if (mainArea.GeometricalShape.Outline != null && mainArea.GeometricalShape.Outline.Segments != null)
            {
                ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", mainArea.GeometricalShape.Outline.ThicknessCalculated));
                ul += GetOutlineString(mainArea.GeometricalShape.Outline);
            }
            return (MarkupString)string.Format("<ul>{0}</ul>", ul);
        }

        public static MarkupString GetAreaDiffMarkup(ConstraintArea diffArea, ConstraintArea mainArea)
        {
            string ul = string.Format(itemInfoTag, diffArea.ObjectData.Identifier.GetNameCombined());
            if (mainArea == null || diffArea.ObjectData.ChangeType == ChangeType.ADDITION)
            {
                ul += string.Format(itemInfoTag, string.Format("Side: {0}", diffArea.Side.ToString()));
                ul += string.Format(itemInfoTag, string.Format("Purpose: {0}", diffArea.Purpose.ToString()));
                if (diffArea.GeometricalShape.Outline != null && diffArea.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", diffArea.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineString(diffArea.GeometricalShape.Outline);
                }
            }
            else if (diffArea.ObjectData.ChangeType == ChangeType.DELETION)
            {
                ul += string.Format(itemInfoTag, string.Format("Side: {0}", mainArea.Side.ToString()));
                ul += string.Format(itemInfoTag, string.Format("Purpose: {0}", mainArea.Purpose.ToString()));
                if (mainArea.GeometricalShape.Outline != null && mainArea.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", mainArea.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineString(mainArea.GeometricalShape.Outline);
                }
            }
            else
            {
                ul += string.Format(itemInfoTag, string.Format("Side: {0} -> {1}", mainArea.Side.ToString(), diffArea.Side.ToString()));
                ul += string.Format(itemInfoTag, string.Format("Purpose: {0} -> {1}", mainArea.Purpose.ToString(), diffArea.Purpose.ToString()));
                if (mainArea.GeometricalShape.Outline != null && mainArea.GeometricalShape.Outline.Segments != null
            && diffArea.GeometricalShape.Outline != null && diffArea.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0} -> {1}", mainArea.GeometricalShape.Outline.ThicknessCalculated
                        , diffArea.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineDiffString(mainArea.GeometricalShape.Outline, diffArea.GeometricalShape.Outline);
                }
            }
            return (MarkupString)string.Format("<ul>{0}</ul>", ul);
        }


        public static MarkupString GetComponentMarkup(Component mainComponent)
        {
            string ul = string.Format(itemInfoTag, mainComponent.ObjectData.Identifier.GetNameCombined());
            ul += string.Format(itemInfoTag, string.Format("Side: {0}", mainComponent.Side.ToString()));
            ul += string.Format(itemInfoTag, string.Format("Name: {0}", mainComponent.ComponentName.ToString()));
            if (mainComponent.GeometricalShape.Outline != null && mainComponent.GeometricalShape.Outline.Segments != null)
            {
                ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", mainComponent.GeometricalShape.Outline.ThicknessCalculated));
                ul += GetOutlineString(mainComponent.GeometricalShape.Outline);
            }
            return (MarkupString)string.Format("<ul>{0}</ul>", ul);
        }

        public static MarkupString GetComponentDiffMarkup(Component diffComponent, Component mainComponent)
        {
            string ul = string.Format(itemInfoTag, diffComponent.ObjectData.Identifier.GetNameCombined());
            if (mainComponent == null || diffComponent.ObjectData.ChangeType == ChangeType.ADDITION)
            {
                ul += string.Format(itemInfoTag, string.Format("Side: {0}", diffComponent.Side.ToString()));
                ul += string.Format(itemInfoTag, string.Format("Name: {0}", diffComponent.ComponentName.ToString()));
                if (diffComponent.GeometricalShape.Outline != null && diffComponent.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", diffComponent.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineString(diffComponent.GeometricalShape.Outline);
                }
            }
            else if (diffComponent.ObjectData.ChangeType == ChangeType.DELETION)
            {
                ul += string.Format(itemInfoTag, string.Format("Side: {0}", mainComponent.Side.ToString()));
                ul += string.Format(itemInfoTag, string.Format("Name: {0}", mainComponent.ComponentName.ToString()));
                if (mainComponent.GeometricalShape.Outline != null && mainComponent.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0}", mainComponent.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineString(mainComponent.GeometricalShape.Outline);
                }
            }
            else
            {
                ul += string.Format(itemInfoTag, string.Format("Side: {0} -> {1}", mainComponent.Side.ToString(), diffComponent.Side.ToString()));
                ul += string.Format(itemInfoTag, string.Format("Name: {0} -> {1}", mainComponent.ComponentName.ToString(), diffComponent.ComponentName.ToString()));
                if (mainComponent.GeometricalShape.Outline != null && mainComponent.GeometricalShape.Outline.Segments != null
        && diffComponent.GeometricalShape.Outline != null && diffComponent.GeometricalShape.Outline.Segments != null)
                {
                    ul += string.Format(itemInfoTag, string.Format("Thickness: {0} -> {1}", mainComponent.GeometricalShape.Outline.ThicknessCalculated
                        , diffComponent.GeometricalShape.Outline.ThicknessCalculated));
                    ul += GetOutlineDiffString(mainComponent.GeometricalShape.Outline, diffComponent.GeometricalShape.Outline);
                }
            }
            return (MarkupString)string.Format("<ul>{0}</ul>", ul);
        }

        public static MarkupString GetCheckBoxBySendType(SendType sendType, WebUserActionsEnum WebUserActions)
        {
            if (sendType == SendType.PROPOSAL && WebUserActions == WebUserActionsEnum.CanRespondToProposal)
            {
                return (MarkupString)"<input id=\"node-1\" data-id=\"custom-1\" type=\"checkbox\">";
            }
            return (MarkupString)"";
        }




    }
}
