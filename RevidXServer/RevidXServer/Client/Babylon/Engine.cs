﻿using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace BabylonBlazor.Babylon
{
    public class Engine : BabylonObject
    {
        private IJSRuntime _jsRuntime;
        public Engine(IJSRuntime jsRuntime, IJSObjectReference objRef) : base(jsRuntime, objRef) {
            _jsRuntime = jsRuntime;

        }

        public async Task RunRenderLoop(Scene scene)
        {
            await _jsRuntime.InvokeVoidAsync("babylonInterop.runRenderLoop", this._jsObjRef, scene._jsObjRef);
        }
        public async Task StopRenderLoop(Scene scene)
        {
            await _jsRuntime.InvokeVoidAsync("babylonInterop.stopRenderLoop", this._jsObjRef, scene._jsObjRef);
        }
    }
}