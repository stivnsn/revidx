﻿using Microsoft.JSInterop;
using System.Dynamic;
using System.Threading.Tasks;

namespace BabylonBlazor.Babylon
{
    public interface IBabylonFactory
    {
        Task<ArcRotateCamera> CreateArcRotateCamera(string name, double alpha, double beta, double radius, Vector3 target, Scene scene, string canvasId);
        Task<Engine> CreateEngine(string canvasId, bool antialias = false);
        Task<HemisphericLight> CreateHemispehericLight(string name, Vector3 direction, Scene scene);
        Task<PointLight> CreatePointLight(string name, Vector3 direction, Scene scene);
        Task<Scene> CreateScene(Engine engine);
        Task<Mesh> CreateSphere(string name, ExpandoObject options, Scene scene);
        Task<Mesh> CreatePolygon(string name, Scene scene, GeomPath path, Vector3 position, double depth, double zOffset);
        Task<Mesh> CreateShape(string name, ExpandoObject options, Scene scene, IJSObjectReference[] shape, Vector3 position, double height = 0);
        Task<Mesh> ApplyColorToMesh(Mesh mesh, double r, double g, double b, double visibility = 1);
        Task<Mesh> ApplyPositionToMesh(Mesh mesh, double x, double y, double z);
        Task<Mesh> ApplyRotationToMesh(Mesh mesh, double xRotRad, double yRotRad, double zRotRad);

        Task<string[]> IntersectsWithAny(Scene scene, Mesh mesh);
        Task<Vector3> CreateVector3(double x, double y, double z);
        Task<Vector2> CreateVector2(double x, double y);
        Task<bool> ClearScene(Scene scene);
        Task<bool> DisposeObject(BabylonObject babylonObject);
        Task<bool> FocusOnMesh(Scene scene, ArcRotateCamera camera, string objectName);
        Task<bool> FocusOnMeshDiff(Scene scene, ArcRotateCamera camera, string objectName);
        Task<GeomPath> AddArcToPath(GeomPath path, double x1, double y1, double x2, double y2, double x3, double y3, double nrOfPoints = 80);
        Task<GeomPath> AddLineToPath(GeomPath path, double x, double y);
        Task<Mesh> SubtractFromMesh(Scene scene, Mesh mesh, IJSObjectReference[] meshesToSubtract);
        Task<Shape> AppendVectorToShape(Vector3 vector, Shape shape);
        Task<Mesh> AppendStl(Scene scene, string fileName);
        Task<Mesh> AppendStlFromBase64(Scene scene, string base64Model);
    }
}