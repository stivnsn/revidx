﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Dynamic;
using System.Threading.Tasks;

namespace BabylonBlazor.Babylon
{
    public class BabylonFactory : IBabylonFactory
    {
        private readonly IJSRuntime _jsRuntime;

        public BabylonFactory(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
            //_babylonInterop = _jsRuntime.InvokeAsync<IJSObjectReference>(
            //    "import", "js/babylonInterop.js").Result;
        }

        public async Task<ArcRotateCamera> CreateArcRotateCamera(string name, double alpha, double beta, double radius, Vector3 target, Scene scene, string canvasId)
        {
            return new ArcRotateCamera(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.createArcRotateCamera", name, alpha, beta, radius, target._jsObjRef, scene._jsObjRef, canvasId));
        }

        public async Task<Engine> CreateEngine(string canvasId, bool antialias = false)
        {
            return new Engine(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.createEngine", canvasId, antialias));
        }

        public async Task<HemisphericLight> CreateHemispehericLight(string name, Vector3 direction, Scene scene)
        {
            return new HemisphericLight(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.createHemisphericLight", name, direction._jsObjRef, scene._jsObjRef));
        }

        public async Task<PointLight> CreatePointLight(string name, Vector3 direction, Scene scene)
        {
            return new PointLight(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.createPointLight", name, direction._jsObjRef, scene._jsObjRef));
        }

        public async Task<Scene> CreateScene(Engine engine)
        {
            return new Scene(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.createScene", engine._jsObjRef));
        }

        public async Task<Mesh> CreateSphere(string name, ExpandoObject options, Scene scene)
        {
            return new Mesh(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.createSphere", name, options, scene._jsObjRef));
        }

        public async Task<Mesh> CreateShape(string name, ExpandoObject options, Scene scene, IJSObjectReference[] shape, Vector3 position, double height = 0)
        {
            return new Mesh(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.createShape", name, options, scene._jsObjRef, shape, position._jsObjRef, height));
        }

        public async Task<Mesh> CreatePolygon(string name, Scene scene, GeomPath path, Vector3 position, double depth = 0, double zOffset = 0)
        {
            return new Mesh(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.createPolygon", name, scene._jsObjRef, path._jsObjRef, position._jsObjRef, depth, zOffset));
        }

        public async Task<Shape> AppendVectorToShape(Vector3 vector, Shape shape)
        {
            return new Shape(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.appendVectorToShape", vector._jsObjRef, shape == null ? null : shape._jsObjRef));
        }

        public async Task<Mesh> ApplyColorToMesh(Mesh mesh, double r, double g, double b, double visibility)
        {
            return new Mesh(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.applyColorToMesh", mesh._jsObjRef, r, g, b, visibility));
        }

        public async Task<Mesh> ApplyPositionToMesh(Mesh mesh, double x, double y, double z)
        {
            return new Mesh(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.applyPositionToMesh", mesh._jsObjRef, x, y, z));
        }

        public async Task<Mesh> ApplyRotationToMesh(Mesh mesh, double xRotRad, double yRotRad, double zRotRad)
        {
            return new Mesh(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.applyRotationToMesh", mesh._jsObjRef, xRotRad, yRotRad, zRotRad));
        }

        public async Task<string[]> IntersectsWithAny(Scene scene, Mesh mesh)
        {
            return await _jsRuntime.InvokeAsync<string[]>("babylonInterop.intersectsWithAny", scene._jsObjRef, mesh._jsObjRef);
        }

        public async Task<Vector3> CreateVector3(double x, double y, double z)
        {
            return new Vector3(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.createVector3", x, y, z));
        }

        public async Task<Vector2> CreateVector2(double x, double y)
        {
            return new Vector2(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.createVector2", x, y));
        }

        public async Task<Mesh> SubtractFromMesh(Scene scene, Mesh mesh, IJSObjectReference[] meshesToSubtract)
        {
            return new Mesh(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.subtractFromMesh", scene._jsObjRef, mesh._jsObjRef, meshesToSubtract));
        }

        public async Task<bool> ClearScene(Scene scene)
        {
            await _jsRuntime.InvokeVoidAsync("babylonInterop.clearScene", scene._jsObjRef);
            return true;
        }

        public async Task<bool> FocusOnMesh(Scene scene, ArcRotateCamera camera, string objectName)
        {
            await _jsRuntime.InvokeVoidAsync("babylonInterop.focusOnMesh", scene._jsObjRef, camera._jsObjRef, objectName);
            return true;
        }

        public async Task<bool> FocusOnMeshDiff(Scene scene, ArcRotateCamera camera, string objectName)
        {
            await _jsRuntime.InvokeVoidAsync("babylonInterop.focusOnMeshDiff", scene._jsObjRef, camera._jsObjRef, objectName);
            return true;
        }
        public async Task<bool> DisposeObject(BabylonObject babylonObject)
        {
            await _jsRuntime.InvokeVoidAsync("babylonInterop.disposeObject", babylonObject._jsObjRef);
            return true;
        }

        public async Task<Mesh> AppendStl(Scene scene, string filename)
        {
            return new Mesh(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.appendSTLFromFile", scene._jsObjRef, filename));
        }

        public async Task<Mesh> AppendStlFromBase64(Scene scene, string base64Model)
        {
            return new Mesh(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.appendSTLFromBase64", scene._jsObjRef, base64Model));
        }

        public async Task<GeomPath> AddLineToPath(GeomPath path, double x, double y)
        {
            return new GeomPath(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.addLineToPath", path == null ? null : path._jsObjRef, x, y));
        }

        public async Task<GeomPath> AddArcToPath(GeomPath path, double x1, double y1, double x2, double y2, double x3, double y3, double nrOfPoints)
        {
            return new GeomPath(_jsRuntime, await _jsRuntime.InvokeAsync<IJSObjectReference>("babylonInterop.addArcToPath", path == null ? null : path._jsObjRef, x1, y1, x2, y2, x3, y3, nrOfPoints));
        }
    }
}
