﻿using Microsoft.JSInterop;

namespace BabylonBlazor.Babylon
{
    public class PointLight : BabylonObject
    {
        public PointLight(IJSRuntime jsRuntime, IJSObjectReference objRef) : base(jsRuntime, objRef) { }
    }
}