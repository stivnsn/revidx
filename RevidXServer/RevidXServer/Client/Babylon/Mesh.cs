﻿using Microsoft.JSInterop;

namespace BabylonBlazor.Babylon
{
    public class Mesh : BabylonObject
    {
        public Mesh(IJSRuntime jsRuntime, IJSObjectReference objRef) : base(jsRuntime, objRef) { }
    }
}