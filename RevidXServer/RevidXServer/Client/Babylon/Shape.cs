﻿using Microsoft.JSInterop;

namespace BabylonBlazor.Babylon
{
    public class Shape : BabylonObject
    {
        public Shape(IJSRuntime jsRuntime, IJSObjectReference objRef) : base(jsRuntime, objRef) { }
    }
}
