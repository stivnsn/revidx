﻿using Microsoft.JSInterop;

namespace BabylonBlazor.Babylon
{
    public class Scene : BabylonObject
    {
        public Scene(IJSRuntime jsRuntime, IJSObjectReference objRef) : base(jsRuntime, objRef) { }
    }
}