﻿using Microsoft.JSInterop;

namespace BabylonBlazor.Babylon
{
    public class HemisphericLight : BabylonObject
    {
        public HemisphericLight(IJSRuntime jsRuntime, IJSObjectReference objRef) : base(jsRuntime, objRef) { }
    }
}