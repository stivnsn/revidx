﻿using Microsoft.JSInterop;
using System.Text.Json.Serialization;

namespace BabylonBlazor.Babylon
{
    public abstract class BabylonObject
    {
        public IJSObjectReference _jsObjRef;

        //[JsonPropertyName("__jsObjRefId")]
        //public int JsObjectRefId => _jsObjRef.;

        public BabylonObject(IJSRuntime jsRuntime, IJSObjectReference objRef)
        {
            _jsObjRef = objRef;
            //_jsObjRef.JSRuntime = jsRuntime;
        }
    }
}