﻿using Microsoft.JSInterop;

namespace BabylonBlazor.Babylon
{
    public class Polygon : BabylonObject
    {
        public Polygon(IJSRuntime jsRuntime, IJSObjectReference objRef) : base(jsRuntime, objRef) { }
    }
}