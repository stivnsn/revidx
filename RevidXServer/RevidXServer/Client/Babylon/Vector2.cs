﻿using Microsoft.JSInterop;

namespace BabylonBlazor.Babylon
{
    public class Vector2 : BabylonObject
    {
        public Vector2(IJSRuntime jsRuntime, IJSObjectReference objRef) : base(jsRuntime, objRef) { }
    }
}