﻿using Microsoft.JSInterop;

namespace BabylonBlazor.Babylon
{
    public class GeomPath : BabylonObject
    {
        public GeomPath(IJSRuntime jsRuntime, IJSObjectReference objRef) : base(jsRuntime, objRef) { }
    }
}