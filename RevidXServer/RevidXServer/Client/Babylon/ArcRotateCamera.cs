﻿using Microsoft.JSInterop;

namespace BabylonBlazor.Babylon
{
    public class ArcRotateCamera : BabylonObject
    {
        public ArcRotateCamera(IJSRuntime jsRuntime, IJSObjectReference objRef) : base(jsRuntime, objRef) { }
    }
}