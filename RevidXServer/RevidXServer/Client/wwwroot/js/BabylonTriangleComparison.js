﻿// Converted from https://www.babylonjs-playground.com/#4TXNWN#7



function IntersectsMesh(
    bbox1, bbox2,
    triangles1, triangles2) {

    let bbTriangles1 = GetIntersectTriangles(bbox2, triangles1);
    let bbTriangles2 = GetIntersectTriangles(bbox1, triangles2);

    //console.log({
    //    triangles1: triangles1.length,
    //    triangles2: triangles2.length,
    //    bbTriangles1: bbTriangles1.length,
    //    bbTriangles2: bbTriangles2.length
    //});

    for (let t1 of bbTriangles1) {
        for (let t2 of bbTriangles2) {
            if (NoDivTriTriIsect(
                t1[0], t1[1], t1[2],
                t2[0], t2[1], t2[2],
                true, 0.01)) {

                return true;
            }
        }
    }

    return false;
}

function DrawBbox(name, bbox, color, scene) {
    let width = bbox.maximumWorld.x - bbox.minimumWorld.x;
    let height = bbox.maximumWorld.y - bbox.minimumWorld.y;
    let depth = bbox.maximumWorld.z - bbox.minimumWorld.z;

    let mesh = BABYLON.MeshBuilder.CreateBox(name, { width: width, height: height, depth: depth }, scene);
    let mat = new BABYLON.StandardMaterial(`${name}-mat`, scene);
    mat.emissiveColor = color;
    mat.alpha = 0.25;
    mesh.material = mat;
    mesh.position = bbox.centerWorld;
}

function GetIntersectTriangles(bbox, triangles) {
    let bbTriangles = [];
    for (let triangle of triangles) {
        if (IntersectTriangleAabb(triangle, bbox)) {
            // console.log("triangle in bb!");
            bbTriangles.push(triangle);
        }
    }

    return bbTriangles;
}

function IntersectTriangleAabb(t, bbox) {
    let f0 = t[1].subtract(t[0]);
    let f1 = t[2].subtract(t[1]);
    let f2 = t[0].subtract(t[2]);

    let u0 = new BABYLON.Vector3(1, 0, 0);
    let u1 = new BABYLON.Vector3(0, 1, 0);
    let u2 = new BABYLON.Vector3(0, 0, 1);

    let testVectors = [
        u0, u1, u2,
        f0.cross(f1),
        u0.cross(f0), u0.cross(f1), u0.cross(f2),
        u1.cross(f0), u1.cross(f1), u1.cross(f2),
        u2.cross(f0), u2.cross(f1), u2.cross(f2)
    ];

    for (let testVector of testVectors) {
        if (!OverlapOnAxisAabbTriangle(bbox, t, testVector)) {
            return false;
        }
    }

    return true;
}

function OverlapOnAxisAabbTriangle(bbox, triangle, axis) {
    let aabbInt = GetIntervalAabbAxis(bbox, axis);
    let triangleInt = GetIntervalTriangleAxis(triangle, axis);
    return ((triangleInt.min <= aabbInt.max) && (aabbInt.min <= triangleInt.max));
}

function GetIntervalAabbAxis(bbox/*: BABYLON.BoundingBox*/, axis/*: BABYLON.Vector3*/) {
    let vertex = bbox.vectors;
    let min = BABYLON.Vector3.Dot(axis, vertex[0]);
    let max = min;
    for (let i = 1; i < 8; i++) {
        let projection = BABYLON.Vector3.Dot(axis, vertex[i]);
        min = Math.min(projection, min);
        max = Math.max(projection, max);
    }

    return { min, max };
}

function GetIntervalTriangleAxis(triangle/*: BABYLON.Vector3[]*/, axis/*: BABYLON.Vector3*/) {
    let min = BABYLON.Vector3.Dot(axis, triangle[0]);
    let max = min;
    for (let i = 1; i < 3; i++) {
        let val = BABYLON.Vector3.Dot(axis, triangle[i]);
        min = Math.min(min, val);
        max = Math.max(max, val);
    }

    return { min, max };
}

function DrawWireframe(name/*: string*/, triangles/*: BABYLON.Vector3[][]*/, color/*: BABYLON.Color3*/, scene/*: BABYLON.Scene*/) {
    let lineSystem = BABYLON.MeshBuilder.CreateLineSystem(name, { lines: triangles }, scene);
    lineSystem.color = color;
}

function GetTriangles(mesh/*: BABYLON.Mesh*/)/*: BABYLON.Vector3[][] */ {
    mesh.bakeCurrentTransformIntoVertices();
    let indices = mesh.getIndices();
    let positons = mesh.getVerticesData(BABYLON.VertexBuffer.PositionKind);
    let triangles/*: BABYLON.Vector3[][] */ = [];
    for (let i = 0; i < indices.length; i += 3) {
        let index1 = indices[i] * 3;
        let index2 = indices[i + 1] * 3;
        let index3 = indices[i + 2] * 3;
        let triangle = [
            new BABYLON.Vector3(positons[index1], positons[index1 + 1], positons[index1 + 2]),
            new BABYLON.Vector3(positons[index2], positons[index2 + 1], positons[index2 + 2]),
            new BABYLON.Vector3(positons[index3], positons[index3 + 1], positons[index3 + 2]),
        ];
        triangles.push(triangle);
    }

    return triangles;
}

function NoDivTriTriIsect(
    V0/*: BABYLON.Vector3*/, V1/*: BABYLON.Vector3*/, V2/*: BABYLON.Vector3*/,
    U0/*: BABYLON.Vector3*/, U1/*: BABYLON.Vector3*/, U2/*: BABYLON.Vector3*/,
    useEpsilonTest/*: boolean*/, epsilon/*: number*/)/*: boolean */ {

    // Compute plane equation of triangle(V0,V1,V2)
    let E1 = V1.subtract(V0);
    let E2 = V2.subtract(V0);
    const N1 = E1.cross(E2);
    const d1 = -BABYLON.Vector3.Dot(N1, V0);
    // Plane equation 1: N1.X + d1 = 0

    // Put U0,U1,U2 into plane equation 1 to compute signed distances to the plane
    let du0 = BABYLON.Vector3.Dot(N1, U0) + d1;
    let du1 = BABYLON.Vector3.Dot(N1, U1) + d1;
    let du2 = BABYLON.Vector3.Dot(N1, U2) + d1;

    // Coplanarity robustness check
    if (useEpsilonTest) {
        if (Math.abs(du0) < epsilon) du0 = 0;
        if (Math.abs(du1) < epsilon) du1 = 0;
        if (Math.abs(du2) < epsilon) du2 = 0;
    }

    const du0du1 = du0 * du1;
    const du0du2 = du0 * du2;

    if (du0du1 > 0 && du0du2 > 0) { // same sign on all of them + not equal 0 ?
        return false;               // no intersection occurs
    }

    // Compute plane of triangle (U0,U1,U2)
    E1 = U1.subtract(U0);
    E2 = U2.subtract(U0);
    const N2 = E1.cross(E2);
    // console.log("cross(E1, E2)");
    // console.log({
    //     E1, E2, N2
    // });
    const d2 = -BABYLON.Vector3.Dot(N2, U0);
    // console.log(`Plane equation 2: ${N2.x} + ${d2} = 0`);
    // Plane equation 2: N2.X + d2 = 0

    // Put V0,V1,V2 into plane equation 2
    let dv0 = BABYLON.Vector3.Dot(N2, V0) + d2;
    let dv1 = BABYLON.Vector3.Dot(N2, V1) + d2;
    let dv2 = BABYLON.Vector3.Dot(N2, V2) + d2;
    // console.log("dvs");
    // console.log({ dv0, dv1, dv2 });

    if (useEpsilonTest) {
        if (Math.abs(dv0) < epsilon) dv0 = 0;
        if (Math.abs(dv1) < epsilon) dv1 = 0;
        if (Math.abs(dv2) < epsilon) dv2 = 0;
    }

    const dv0dv1 = dv0 * dv1;
    const dv0dv2 = dv0 * dv2;

    if (dv0dv1 > 0 && dv0dv2 > 0) { // same sign on all of them + not equal 0 ?
        return false;               // no intersection occurs
    }

    // Compute direction of intersection line
    const D = N1.cross(N2);

    // Compute and index to the largest component of D
    let max = Math.abs(D.x);
    let index = "x";
    const bb = Math.abs(D.y);
    const cc = Math.abs(D.z);
    if (bb > max) {
        max = bb;
        index = "y";
    }

    if (cc > max) {
        max = cc;
        index = "z";
    }

    // This is the simplified projection onto L
    const vp0 = V0[index];
    const vp1 = V1[index];
    const vp2 = V2[index];

    const up0 = U0[index];
    const up1 = U1[index];
    const up2 = U2[index];

    // Compute interval for triangle 1
    let int1 = NewComputeIntervals(
        vp0, vp1, vp2,
        dv0, dv1, dv2,
        dv0dv1, dv0dv2,
        N1,
        V0, V1, V2,
        U0, U1, U2);
    if (int1.coplanar) {
        // console.log("coplanar");
        // console.log(int1.intersect ? "intersects" : "no intersection");
        return int1.intersect;
    }

    //NEWCOMPUTE_INTERVALS(vp0,vp1,vp2,dv0,dv1,dv2,dv0dv1,dv0dv2,a,b,c,x0,x1);

    // Compute interval for triangle 2
    let int2 = NewComputeIntervals(
        up0, up1, up2,
        du0, du1, du2,
        du0du1, du0du2,
        N1,
        V0, V1, V2,
        U0, U1, U2);
    if (int2.coplanar) {
        // Should in theory never happen? since we also calculate coplanarity in int1
        // console.log("coplanar");
        // console.log(int2.intersect ? "intersects" : "no intersection");
        return int2.intersect;
    }

    // NEWCOMPUTE_INTERVALS(up0,up1,up2,du0,du1,du2,du0du1,du0du2,d,e,f,y0,y1);

    const xx = int1.X0 * int1.X1;
    const yy = int2.X0 * int2.X1;
    const xxyy = xx * yy;

    let tmp = int1.A * xxyy;
    const isect1 = [0, 0];
    isect1[0] = tmp + int1.B * int1.X1 * yy;
    isect1[1] = tmp + int1.C * int1.X0 * yy;

    tmp = int2.A * xxyy;
    const isect2 = [0, 0];
    isect2[0] = tmp + int2.B * xx * int2.X1;
    isect2[1] = tmp + int2.C * xx * int2.X0;

    isect1.sort();
    isect2.sort();
    // console.log("isects");
    // console.log(isect1);
    // console.log(isect2);

    if (isect1[1] < isect2[0] || isect2[1] < isect1[0]) {
        return false;
    }

    // console.log("rules exhausted, intersect");
    return true;
}

function NewComputeIntervals(
    VV0/*: number*/, VV1/*: number*/, VV2/*: number*/,
    D0/*: number*/, D1/*: number*/, D2/*: number*/,
    D0D1/*: number*/, D0D2/*: number*/,
    N1/*: BABYLON.Vector3*/,
    V0/*: BABYLON.Vector3*/, V1/*: BABYLON.Vector3*/, V2/*: BABYLON.Vector3*/,
    U0/*: BABYLON.Vector3*/, U1/*: BABYLON.Vector3*/, U2/*: BABYLON.Vector3*/) {
    // console.log("intParams");
    // console.log({
    //     VV0, VV1, VV2, D0, D1, D2, D0D1, D0D2, N1, V0, V1, V2, U0, U1, U2
    // });

    let A, B, C, X0, X1/*: number = 0*/;
    let coplanar = false;
    let intersect = false;
    if (D0D1 > 0) {
        // Here we know that D0D2 <= 0
        // that is D0, D1 are on the same side, D2 on the other or on the plane
        A = VV2;
        B = (VV0 - VV2) * D2;
        C = (VV1 - VV2) * D2;
        X0 = D2 - D0;
        X1 = D2 - D1;
    } else if (D0D2 > 0) {
        // Here we know that d0d1 <= 0
        A = VV1;
        B = (VV0 - VV1) * D1;
        C = (VV2 - VV1) * D1;
        X0 = D1 - D0;
        X1 = D1 - D2;
    } else if (D1 * D2 > 0 || D0 != 0) {
        // Here we know that d0d1 <= 0 or that D0 != 0
        A = VV0;
        B = (VV1 - VV0) * D0;
        C = (VV2 - VV0) * D0;
        X0 = D0 - D1;
        X1 = D0 - D2;
    } else if (D1 != 0) {
        A = VV1;
        B = (VV0 - VV1) * D1;
        C = (VV2 - VV1) * D1;
        X0 = D1 - D0;
        X1 = D1 - D2;
    } else if (D2 != 0) {
        A = VV2;
        B = (VV0 - VV2) * D2;
        C = (VV1 - VV2) * D2;
        X0 = D2 - D0;
        X1 = D2 - D1;
    } else {
        // Triangles are coplanar
        coplanar = true;
        intersect = CoplanarTriTri(N1, V0, V1, V2, U0, U1, U2);
    }

    let result = {
        coplanar,
        intersect,
        A, B, C,
        X0, X1
    };
    // console.log("intervals");
    // console.log(result);
    return result;
}

function CoplanarTriTri(
    N/*: BABYLON.Vector3*/,
    V0/*: BABYLON.Vector3*/, V1/*: BABYLON.Vector3*/, V2/*: BABYLON.Vector3*/,
    U0/*: BABYLON.Vector3*/, U1/*: BABYLON.Vector3*/, U2/*: BABYLON.Vector3*/)/*: boolean */ {

    // First project onto an axis-aligned plane, that maximizes the area
    // of the triangles, compute indices: i0, i1.
    let i0, i1/*: string*/;
    const A = new BABYLON.Vector3();
    A.x = Math.abs(N.x);
    A.y = Math.abs(N.y);
    A.z = Math.abs(N.z);
    if (A.x > A.y) {
        if (A.x > A.z) {
            i0 = "y";      // A.x is greatest
            i1 = "z";
        }
        else {
            i0 = "x";      // A.z is greatest
            i1 = "y";
        }
    }
    else   // A.x <= A.y
    {
        if (A.z > A.y) {
            i0 = "x";      // A.z is greatest
            i1 = "y";
        }
        else {
            i0 = "x";      // A.y is greatest
            i1 = "z";
        }
    }

    // Test all edges of triangle 1 against the edges of triangle 2
    if (EdgeAgainstTriEdges(V0, V1, U0, U1, U2, i0, i1)) {
        return true;
    }

    if (EdgeAgainstTriEdges(V1, V2, U0, U1, U2, i0, i1)) {
        return true;
    }

    if (EdgeAgainstTriEdges(V2, V0, U0, U1, U2, i0, i1)) {
        return true;
    }

    // Finally, test if tri1 is totally contained in tri2 or vice versa
    if (PointInTri(V0, U0, U1, U2, i0, i1)) {
        return true;
    }

    if (PointInTri(U0, V0, V1, V2, i0, i1)) {
        return true;
    }

    return false;
}

function PointInTri(
    V0/*: BABYLON.Vector3*/,
    U0/*: BABYLON.Vector3*/, U1/*: BABYLON.Vector3*/, U2/*: BABYLON.Vector3*/,
    i0/*: string*/, i1/*: string*/)/*: boolean */ {

    // Is T1 completly inside T2?
    // Check if V0 is inside tri(U0,U1,U2)
    let a = U1[i1] - U0[i1];
    let b = -(U1[i0] - U0[i0]);
    let c = -a * U0[i0] - b * U0[i1];
    let d0 = a * V0[i0] + b * V0[i1] + c;

    a = U2[i1] - U1[i1];
    b = -(U2[i0] - U1[i0]);
    c = -a * U1[i0] - b * U1[i1];
    let d1 = a * V0[i0] + b * V0[i1] + c;

    a = U0[i1] - U2[i1];
    b = -(U0[i0] - U2[i0]);
    c = -a * U2[i0] - b * U2[i1];
    let d2 = a * V0[i0] + b * V0[i1] + c;
    if (d0 * d1 > 0) {
        if (d0 * d2 > 0) return true;
    }

    return false;
}

function EdgeAgainstTriEdges(
    V0/*: BABYLON.Vector3*/, V1/*: BABYLON.Vector3*/,
    U0/*: BABYLON.Vector3*/, U1/*: BABYLON.Vector3*/, U2/*: BABYLON.Vector3*/,
    i0/*: string*/, i1/*: string*/)/*: boolean */ {
    const Ax = V1[i0] - V0[i0];
    const Ay = V1[i1] - V0[i1];

    // Test edge U0,U1 against V0,V1
    if (EdgeEdgeTest(V0, U0, U1, i0, i1, Ax, Ay)) {
        return true;
    }

    // Test edge U1,U2 against V0,V1
    if (EdgeEdgeTest(V0, U1, U2, i0, i1, Ax, Ay)) {
        return true;
    }

    // Test edge U2,U1 against V0,V1
    if (EdgeEdgeTest(V0, U2, U0, i0, i1, Ax, Ay)) {
        return true;
    }

    return false;
}

// This edge to edge test is based on Franlin Antonio's gem:
// "Faster Line Segment Intersection", in Graphics Gems III,
// pp. 199-202
function EdgeEdgeTest(
    V0/*: BABYLON.Vector3*/, U0/*: BABYLON.Vector3*/, U1/*: BABYLON.Vector3*/,
    i0/*: string*/, i1/*: string*/, Ax/*: number*/, Ay/*: number*/)/*: boolean*/ {
    const Bx = U0[i0] - U1[i0];
    const By = U0[i1] - U1[i1];
    const Cx = V0[i0] - U0[i0];
    const Cy = V0[i1] - U0[i1];
    const f = Ay * Bx - Ax * By;
    const d = By * Cx - Bx * Cy;
    if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f)) {
        const e = Ax * Cy - Ay * Cx;
        if (f > 0) {
            if (e >= 0 && e <= f) return true;
        }
        else {
            if (e <= 0 && e >= f) return true;
        }
    }

    return false;
}