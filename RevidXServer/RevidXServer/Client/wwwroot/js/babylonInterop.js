﻿var babylonInterop = babylonInterop || {};

class Shape {
    constructor() {
    }

}


babylonInterop.objRefs = {};
babylonInterop.objRefId = 0;
babylonInterop.objRefKey = '__jsObjRefId';
babylonInterop.storeObjRef = function (obj) {
    var id = babylonInterop.objRefId++;
    babylonInterop.objRefs[id] = obj;
    var objRef = {};
    objRef[babylonInterop.objRefKey] = id;
    return objRef;
}
babylonInterop.removeObjectRef = function (id) {
    delete babylonInterop.objRefs[id];
}

DotNet.attachReviver(function (key, value) {
    if (value &&
        typeof value === 'object' &&
        value.hasOwnProperty(babylonInterop.objRefKey) &&
        typeof value[babylonInterop.objRefKey] === 'number') {
        var id = value[babylonInterop.objRefKey];
        if (!(id in babylonInterop.objRefs)) {
            throw new Error("The JS object reference doesn't exist: " + id);
        }
        const instance = babylonInterop.objRefs[id];
        return instance;
    } else {
        return value;
    }
});

babylonInterop.appendSTLFromFile = async function (scene, fileName) {

    var mesh = null;
    await BABYLON.SceneLoader.ImportMeshAsync("", "/assets_babylon/", fileName, scene).then(function (meshes) {
        //scene.createDefaultCameraOrLight(true, true, true);
        //scene.createDefaultEnvironment();
        console.log("meshes is : " + meshes.meshes);
        console.log("nr of stls loaded: " + meshes.meshes.length);
        if (!!meshes && !!meshes.meshes && meshes.meshes.length > 0) {
            mesh = meshes.meshes[0];
            //mesh.setAbsolutePosition(new BABYLON.Vector3(0, 0, 0));
            console.log("mesh assigned: " + mesh);
            //mesh.checkCollisions = true;
            //mesh.onCollide = function () {
            //    console.log("Collision! Mesh name:" + otherMesh.name);
            //}
        }
        console.log("mesh position" + meshes.position);
    });
    //.then();
    console.log("trying to draw stl");
    //var mesh = await BABYLON.SceneLoader.Append("/assets_babylon/", fileName, scene, function (scene) {
    //});
    console.log("returning mesh: " + mesh);
    //mesh.position = new BABYLON.Vector3(10, 10, 10);
    return mesh;
}

async function b64ToBlob(base64Data) {
    var blob = null;
    try {
        blob = await ((await fetch(base64Data)).blob());
        return blob;
    } catch (e) {
        console.error(e);
    }
}

babylonInterop.appendSTLFromBase64 = async function (scene, base64String) {
    console.log("appending stl!");


    const url = URL.createObjectURL(await b64ToBlob("data:@file/octet-stream;base64," + base64String));
    const fileExtension = ".stl";
    await BABYLON.SceneLoader.ImportMeshAsync(
        "",
        url,
        "",
        scene,
        null,
        fileExtension
    ).then(function (meshes) {
        //scene.createDefaultCameraOrLight(true, true, true);
        //scene.createDefaultEnvironment();
        console.log("meshes is : " + meshes.meshes);
        console.log("nr of stls loaded: " + meshes.meshes.length);
        if (!!meshes && !!meshes.meshes && meshes.meshes.length > 0) {
            mesh = meshes.meshes[0];
            console.log("mesh assigned: " + mesh);
            //mesh.checkCollisions = true;
            //mesh.onCollide = function () {
            //    console.log("Collision! Mesh name:" + otherMesh.name);
            //}
        }
        console.log("mesh position" + meshes.position);
    });
    return mesh;
}

babylonInterop.initCanvas = function (canvasId) {
    var babylonCanvas = document.getElementById(canvasId);
    var babylonEngine = new BABYLON.Engine(babylonCanvas, true, { stencil: true });
    var scene = babylonInterop.createSceneWithSphere(babylonEngine, babylonCanvas);

    babylonEngine.runRenderLoop(function () {
        scene.render();
    });

    window.addEventListener("resize", function () {
        babylonEngine.resize();
    });
    return babylonCanvas;
};

babylonInterop.createSceneWithSphere = function (engine, canvas) {
    var scene = new BABYLON.Scene(engine);

    var camera = new BABYLON.ArcRotateCamera("Camera", Math.PI / 2, Math.PI / 2, 2, new BABYLON.Vector3(0, 0, 5), scene);
    camera.attachControl(canvas, true);

    var light1 = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(1, 1, 0), scene);
    var light2 = new BABYLON.PointLight("light2", new BABYLON.Vector3(0, 1, -1), scene);

    var sphere = BABYLON.MeshBuilder.CreateSphere("sphere", { diameter: 2 }, scene);

    return scene;
};

var expectClickUp = false;

babylonInterop.createArcRotateCamera = function (name, alpha, beta, radius, target, scene, canvasId) {
    var board = scene.getMeshByName("MainBoard");
    console.log("has main board?");
    if (!!board) {
        console.log("MainBoard found!");
        var boundingInfo = board.getBoundingInfo();
        if (!!boundingInfo) {
            target = boundingInfo.boundingBox.center;
            radius = boundingInfo.diagonalLength;
        }

    }
    var position = new BABYLON.Vector3(target.x, target.y, target.z + radius);
    var camera = new BABYLON.ArcRotateCamera(name, alpha, beta, radius, position, scene);
    camera.setTarget(target);
    camera.allowUpsideDown = true;
    camera.lowerBetaLimit = null;
    camera.upperBetaLimit = null;
    var canvas = document.getElementById(canvasId);


    canvas.addEventListener("wheel", evt => evt.preventDefault());

    camera.attachControl(canvas, true);
    scene.activeCamera.panningSensibility = 70;
    //camera.ForceAttachControlToAlwaysPreventDefault = true f
    scene.onPointerUp = function (evt, pick) {
        if (pick.hit) {
            if (expectClickUp) {
                console.log("picked point:" + pick.pickedPoint);
                camera.setTarget(pick.pickedPoint);
                expectClickUp = false;
            }
        }
    }
    scene.onPointerDown = function (evt, pick) {
        expectClickUp = true;
        //if (pick.hit) {
        //    console.log("picked point:" + pick.pickedPoint);
        //    camera.setTarget(pick.pickedPoint);
        //}
    }
    scene.onPointerMove = function (evt, pick) {
        expectClickUp = false;
        //if (pick.hit) {
        //    console.log("picked point:" + pick.pickedPoint);
        //    camera.setTarget(pick.pickedPoint);
        //}
    }
    scene.collisionsEnabled = true;
    showWorldAxis(scene, 10);

    return camera;
}

var selectedMesh = null
var selectedMeshOrigColor = null;
var selectedMeshVisibility = 1;

babylonInterop.focusOnMesh = function (scene, camera, objectname) {
    console.log("focusing on " + objectname);
    var meshToSelect = scene.getMeshByName(objectname);
    if (!meshToSelect) {
        console.log(objectname + " not found");
        return;
    }
    if (!meshToSelect.position) {
        console.log("Mesh to select has no position defined");
        return;
    }
    //camera.setTarget(meshToSelect);
    if (meshToSelect.position.y > 0.1) {
        console.log("Should show over, pos y:" + meshToSelect.position.y);

        setTimeout(() => camera.spinTo("beta", 1 * Math.PI / 4, 120, meshToSelect.position), 10);
        //camera.beta = -7 * Math.PI / 4;
    }
    else {
        //camera.beta = 3 * Math.PI / 4;
        setTimeout(() => camera.spinTo("beta", 3 * Math.PI / 4, 120, meshToSelect.position), 10);
        console.log("Should show under, pos y:" + meshToSelect.position.y);
    }
    if (!!selectedMesh) {
        selectedMesh.material.diffuseColor = selectedMeshOrigColor;
        selectedMesh.visibility = selectedMeshVisibility;
    }
    if (!!meshToSelect.material) {
        selectedMeshOrigColor = meshToSelect.material.diffuseColor;
        selectedMeshVisibility = meshToSelect.visibility;
    }
    selectedMesh = meshToSelect;
    if (!!meshToSelect.material) {
        meshToSelect.visibility = 0.9;
        meshToSelect.material.diffuseColor = new BABYLON.Color3(1, 1, 0);
    }
}

var hide = function (node) {
    BABYLON.Animation.CreateAndStartAnimation("hide", node, "visibility", 30, 30, 1, 0, 0)
}

var show = function (node) {
    BABYLON.Animation.CreateAndStartAnimation("hide", node, "visibility", 30, 30, 0, 0.9, 0)
}

BABYLON.ArcRotateCamera.prototype.spinTo = function (whichprop, targetval, speed, position) {
    var ease = new BABYLON.CubicEase();
    ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
    BABYLON.Animation.CreateAndStartAnimation('at4', this, whichprop, speed, 120, this[whichprop], targetval, 0, ease);
    console.log("position: " + this.target.x + " " + this.target.y + " " + this.target.z)
    console.log("target: " + position.x + " " + position.y + " " + position.z)
    BABYLON.Animation.CreateAndStartAnimation('at4', this, "target.x", speed, 120, this.target.x, position.x, 0, ease);
    BABYLON.Animation.CreateAndStartAnimation('at4', this, "target.y", speed, 120, this.target.y, position.y, 0, ease);
    BABYLON.Animation.CreateAndStartAnimation('at4', this, "target.z", speed, 120, this.target.z, position.z, 0, ease);
    BABYLON.Animation.CreateAndStartAnimation('at4', this, "radius", speed, 120, this.radius, 40, 0, ease);
    console.log("end");
}


babylonInterop.focusOnMeshDiff = function (scene, camera, objectname) {
    var meshDiffToSelect = scene.getMeshByName(objectname + "_DIFF");
    if (!meshDiffToSelect) {

        console.log(objectname + " is addition");
    }
    var meshToSelect = scene.getMeshByName(objectname);
    if (!meshToSelect && !meshDiffToSelect) {
        console.log(objectname + " not found");
        return;
    }
    if (!!meshToSelect) {
        if (meshToSelect.position.y > 0.1) {
            console.log("Should show over, pos y:" + meshToSelect.position.y);
            setTimeout(() => camera.spinTo("beta", 1 * Math.PI / 4, 120, meshToSelect.position), 10);
        }
        else {
            setTimeout(() => camera.spinTo("beta", 3 * Math.PI / 4, 120, meshToSelect.position), 10);
            console.log("Should show under, pos y:" + meshToSelect.position.y);
        }
        camera.setTarget(meshToSelect);
        if (!!selectedMesh) {
            selectedMesh.material.diffuseColor = selectedMeshOrigColor;
            selectedMesh.visibility = selectedMeshVisibility;
        }
        if (!!meshToSelect.material && !!meshToSelect.material.diffuseColor) {
            selectedMeshOrigColor = meshToSelect.material.diffuseColor;
            selectedMeshVisibility = meshToSelect.visibility;
        }
        show(meshToSelect);
        meshToSelect.visibility = 0;
        show(meshToSelect);
        selectedMesh = meshToSelect;
        if (!!meshToSelect.material) {
            meshToSelect.visibility = 0.9;
            meshToSelect.material.diffuseColor = new BABYLON.Color3(1, 1, 0);
        }
    }
    if (!!meshDiffToSelect) {
        if (meshDiffToSelect.position.y > 0.1) {
            console.log("Should show over, pos y:" + meshDiffToSelect.position.y);

            setTimeout(() => camera.spinTo("beta", 1 * Math.PI / 4, 120, meshDiffToSelect.position), 10);
        }
        else {
            setTimeout(() => camera.spinTo("beta", 3 * Math.PI / 4, 120, meshDiffToSelect.position), 10);
            console.log("Should show under, pos y:" + meshDiffToSelect.position.y);
        }
        camera.setTarget(meshDiffToSelect);
        meshDiffToSelect.material.alpha = 0.9;
        meshDiffToSelect.visibility = 0.9;
        if (!!selectedMesh) {
            selectedMesh.material.diffuseColor = selectedMeshOrigColor;
            selectedMesh.visibility = selectedMeshVisibility;
        }
        if (!!meshDiffToSelect.material && !!meshDiffToSelect.material.diffuseColor) {
            selectedMeshOrigColor = meshDiffToSelect.material.diffuseColor;
            selectedMeshVisibility = meshDiffToSelect.visibility;
        }
        hide(meshDiffToSelect);
        selectedMesh = meshDiffToSelect;
        //if (meshDiffToSelect && !!meshDiffToSelect.material) {
        //    meshDiffToSelect.material.alpha = 0;
        //    meshDiffToSelect.visibility = 0;
        //}
    }
}

//babylonInterop.focusOnMeshDiff = function (scene, camera, objectname) {
//    console.log("focusing on diff " + objectname);
//    var meshDiffToSelect = scene.getMeshByName(objectname + "_DIFF");
//    if (!meshDiffToSelect) {

//        console.log(objectname + " is addition");
//    }
//    var meshToSelect = scene.getMeshByName(objectname);
//    if (!meshToSelect && !meshDiffToSelect) {
//        console.log(objectname + " not found");
//        return;
//    }
//    else if (!!meshToSelect) {
//        //camera.setTarget(meshToSelect);
//        if (meshToSelect.position.y > 0.1) {
//            console.log("Should show over, pos y:" + meshToSelect.position.y);
//            setTimeout(() => camera.spinTo("beta", 1 * Math.PI / 4, 120, meshToSelect.position), 10);
//        }
//        else {
//            setTimeout(() => camera.spinTo("beta", 3 * Math.PI / 4, 120, meshToSelect.position), 10);
//            console.log("Should show under, pos y:" + meshToSelect.position.y);
//        }
//        camera.setTarget(meshToSelect);
//    } else if (!!meshDiffToSelect) {
//        if (meshDiffToSelect.position.y > 0.1) {
//            console.log("Should show over, pos y:" + meshDiffToSelect.position.y);

//            setTimeout(() => camera.spinTo("beta", 1 * Math.PI / 4, 120, meshDiffToSelect.position), 10);
//        }
//        else {
//            setTimeout(() => camera.spinTo("beta", 3 * Math.PI / 4, 120, meshDiffToSelect.position), 10);
//            console.log("Should show under, pos y:" + meshDiffToSelect.position.y);
//        }
//        camera.setTarget(meshDiffToSelect);
//    }
//    if (!!selectedMesh) {
//        selectedMesh.material.diffuseColor = selectedMeshOrigColor;
//        selectedMesh.visibility = selectedMeshVisibility;
//    }
//    if (!!meshToSelect && !!meshToSelect.material && !!meshToSelect.material.diffuseColor) {
//        selectedMeshOrigColor = meshToSelect.material.diffuseColor;
//        selectedMeshVisibility = meshToSelect.visibility;
//    }
//    // diff should be set as visible, then hide, then set invisible again
//    if (!!meshDiffToSelect) {
//        meshDiffToSelect.material.alpha = 0.9;
//        meshDiffToSelect.visibility = 0.9;
//    }
//    if (!!meshToSelect) {
//        meshToSelect.visibility = 0;
//    }
//    if (!!meshDiffToSelect) {
//        hide(meshDiffToSelect);
//    }
//    if (!!meshToSelect) {
//        show(meshToSelect);
//        meshToSelect.visibility = 0;
//        show(meshToSelect);
//    }
//    if (!!meshToSelect) {
//        selectedMesh = meshToSelect;
//    }
//    else if (!!meshDiffToSelect) {
//        selectedMesh = meshDiffToSelect;
//    }
//    if (!!meshToSelect && !!meshToSelect.material) {
//        meshToSelect.visibility = 0.9;
//        meshToSelect.material.diffuseColor = new BABYLON.Color3(1, 1, 0);
//    }
//    if (!!meshDiffToSelect && !!meshDiffToSelect.material) {
//        meshDiffToSelect.material.alpha = 0;
//        meshDiffToSelect.visibility = 0;
//    }
//}

babylonInterop.createEngine = function (canvasId, antialias) {
    //console.log("Creating engine.");
    var babylonCanvas = document.getElementById(canvasId);
    var babylonEngine = new BABYLON.Engine(babylonCanvas, antialias);
    window.addEventListener("resize", function () {
        babylonEngine.resize();
    });
    return babylonEngine;
}

babylonInterop.createHemisphericLight = function (name, direction, scene) {
    var light = new BABYLON.HemisphericLight(name, direction, scene);
    return light;
}

babylonInterop.createPointLight = function (name, direction, scene) {
    return new BABYLON.PointLight(name, direction, scene);
}
babylonInterop.createScene2 = function () {
    return new BABYLON.Vector3(0, 0, 0);//babylonInterop.storeObjRef(new BABYLON.Scene(engine));
}
babylonInterop.createScene = function (engine) {
    return new BABYLON.Scene(engine);//babylonInterop.storeObjRef(new BABYLON.Scene(engine));
}

babylonInterop.clearScene = function (scene) {
    console.log("Clearing scene.");
    scene.dispose()
    return true;//babylonInterop.storeObjRef(new BABYLON.Scene(engine));
}

babylonInterop.createShape = function (name, options, scene, shape, position, height) {
    var len = shape.length;
    console.log("trying to print shapes: ");
    for (i = 0; i < len; ++i) {
        var vector = shape[i];
        console.log(vector);
        console.log(vector.x + " " + vector.y + " " + vector.z);
    }	//Polygon shape in XZ plane
    console.log("height:" + height)
    console.log("name:" + name)
    const shape2 = [
        new BABYLON.Vector3(4, 0, -4),
        new BABYLON.Vector3(2, 0, 0),
        new BABYLON.Vector3(5, 0, 2),
        new BABYLON.Vector3(1, 0, 2),
        new BABYLON.Vector3(-5, 0, 5),
        new BABYLON.Vector3(-3, 0, 1),
        new BABYLON.Vector3(-4, 0, -4),
        new BABYLON.Vector3(-2, 0, -3),
        new BABYLON.Vector3(2, 0, -3)
    ];

    //Holes in XoZ plane
    const holes = [];
    var extrudedPoly = BABYLON.MeshBuilder.ExtrudePolygon(name, { shape: shape, holes: holes, depth: height, sideOrientation: BABYLON.Mesh.FRONTSIDE }, scene);
    extrudedPoly.material = new BABYLON.StandardMaterial(name + "_material", scene);
    extrudedPoly.material.diffuseColor = new BABYLON.Color3(0, 0, 1);
    extrudedPoly.material.intensity = 0.5;
    //console.log("Positioning to " + position.x + " " + position.y + " " + position.z )
    extrudedPoly.position = position;
    return extrudedPoly;
}


babylonInterop.createPolygon = function (name, scene, shape, position, depth, zOffset) {
    var poly = new BABYLON.PolygonMeshBuilder(name, shape, scene);
    var builtPoly = poly.build(false, depth);
    builtPoly.material = new BABYLON.StandardMaterial(name + "_material", scene);

    builtPoly.material.backFaceCulling = false;
    //console.log("Positioning from " + builtPoly.position.x + " " + builtPoly.position.y + " " + builtPoly.position.z)
    //console.log("Positioning to " + position.x + " " + position.y + " " + position.z)
    builtPoly.position.y = position.y;
    builtPoly.position.x = position.x;
    builtPoly.position.z = position.z;
    builtPoly.material.zOffset = zOffset;
    console.log("Set to be checked for collisions.");
    builtPoly.checkCollisions = true;
    return builtPoly;
}

babylonInterop.addHole = function (name, poly, shape) {
    return poly.addHole(shape);
}

babylonInterop.createSphere = function (name, options, scene) {
    return BABYLON.MeshBuilder.CreateSphere(name, options, scene);
}

babylonInterop.createVector3 = function (x, y, z) {
    return new BABYLON.Vector3(x, y, z);
}

babylonInterop.createVector2 = function (x, y) {
    return new BABYLON.Vector2(x, y);
}
function createHoles(scene, layer, box) {
    //console.log('CREATING A NEW HOLE')
    const name = layer.name
    const innerCSG = BABYLON.CSG.FromMesh(box)
    const outerCSG = BABYLON.CSG.FromMesh(layer)

    layer.dispose()
    box.dispose()

    const subCSG = outerCSG.subtract(innerCSG)

    scene.removeMesh(innerCSG)
    scene.removeMesh(outerCSG)

    layer = subCSG.toMesh(name, null, scene)
    scene.removeMesh(subCSG)
    layer.visibility = false

    return layer
}
let pinData = {
    h: 0.4, //
    d: 0.2, //
    n: {
        x: 4, //
        z: 4
    },
    s: {
        x: 0.2, //
        z: 0.4
    }
}


babylonInterop.subtractFromMesh = function (scene, mesh, meshesToSubtract) {
    var meshName = mesh.name;
    var meshMaterial = mesh.material;
    var meshCSG = BABYLON.CSG.FromMesh(mesh);
    mesh.dispose();
    if (meshesToSubtract.length == 0) { return; }
    var selectedMeshes = [];
    selectedMeshes.push(meshesToSubtract[0])
    //var holes = [];
    //var firstmeshtosubtract = meshestosubtract[0];
    for (var i = 0; i < meshesToSubtract.length; i++) {
        var meshclone = meshesToSubtract[i].clone("hole_" + meshesToSubtract[i].name);
        meshclone.material = new BABYLON.StandardMaterial();
        meshclone.visibility = 0.1;
        //meshclone.setEnabled(false);
        //meshclone.position = meshestosubtract[i].position;
        //holes.push(meshclone);

        //if (i > 0) {
        //    meshestosubtract[i].dispose();
        //}
    }
    //firstMeshToSubtract.dispose();
    let holeStamp = BABYLON.Mesh.MergeMeshes(meshesToSubtract, true, true);
    let holePlate = BABYLON.CSG.FromMesh(holeStamp);
    holeStamp.dispose();
    //var meshToSubtractCSG = BABYLON.CSG.FromMesh(holeStamp);
    meshCSG.subtractInPlace(holePlate);
    scene.removeMesh(holePlate);
    //var firstMeshToSubtract = meshesToSubtract[0];
    //for (var i = 0; i < meshesToSubtract.length; i++) {
    //    var meshClone = firstMeshToSubtract.clone("");
    //    meshClone.position = meshesToSubtract[i].position;
    //    var meshToSubtractCSG = BABYLON.CSG.FromMesh(meshClone);
    //    if (i > 0) {
    //        meshesToSubtract[i].dispose();
    //    }
    //    meshCSG = meshCSG.subtract(meshToSubtractCSG);
    //    scene.removeMesh(meshToSubtractCSG);
    //}
    //firstMeshToSubtract.dispose();
    mesh = meshCSG.toMesh(meshName, null, scene);
    scene.removeMesh(meshCSG);
    mesh.visibility = true;
    mesh.material = meshMaterial;
    //mesh.material = new BABYLON.StandardMaterial();
    //mesh.material.diffuseColor = new BABYLON.Color3(0.1, 0.6, 0.1);
    //mesh.material.backFaceCulling = false;
    //console.log("finished subtracting !");
    return mesh;
}

babylonInterop.addLineToPath = function (path, x, y) {
    //console.log("writting path");
    if (!path) {
        //console.log("creating path " + x + " " + y);
        path = new BABYLON.Path2(x, y);
    }
    else {
        //console.log("adding point " + x + " " + y);
        path.addLineTo(x, y);
    }
    return path;
}

babylonInterop.addArcToPath = function (path, x1, y1, x2, y2, x3, y3, nrOfPoints = 100) {
    if (nrOfPoints == 10) {
        console.log("Terrible things happen when nr is 10!")
    };
    if (!path) {
        //console.log("creating arc path " + x1 + " " + y1);
        path = new BABYLON.Path2(x1, y1);
    }
    else {
        //console.log("adding point before arc  " + x1 + " " + y1);
        path.addLineTo(x1, y1);
    }
    //console.log("adding arc mid and end  " + x2 + " " + y2 + " " + x3 + " " + y3);
    path.addArcTo(x2, y2, x3, y3, nrOfPoints);
    return path;
}

babylonInterop.appendVectorToShape = function (vector, shape) {
    if (!shape || !shape.vectors) {
        //console.log("Shape is null, creating new shape.")
        var shape = new Shape();
        shape.vectors = new Array();
        //console.log(shape.vectors.length);
    }
    var len = shape.vectors.length;
    for (i = 0; i < len; ++i) {
        //console.log((shape.vectors[i])[0] + " " + shape.vectors[i][1] + " " + shape.vectors[i][2]);
        //console.log(shape.vectors[i].x + " " + shape.vectors[i].y + " " + shape.vectors[i].z);
    }

    shape.vectors[len] = vector;
    console.log(shape.vectors.length);
    return shape;
}

babylonInterop.applyColorToMesh = function (mesh, r, g, b, visibility) {
    if (!mesh) {
        return;
    }
    if (!mesh.material) {
        return;
    }
    mesh.material.diffuseColor = new BABYLON.Color3(r, g, b);
    mesh.material.color = new BABYLON.Color3(r, g, b);
    //mesh.material.transparencyMode = BABYLON.Material.MATERIAL_ALPHABLEN
    //mesh.visibility = visibility;

    mesh.material.alpha = visibility;
    return mesh;
}

babylonInterop.applyPositionToMesh = function (mesh, x, y, z) {
    if (!mesh) {
        return;
    }
    console.log("old position of mesh: " + mesh.position.x + " " + mesh.position.y + " " + mesh.position.z);
    console.log("Applying position to mesh: " + x + " " + y + " " + z);
    //mesh.position.x = x;
    //mesh.position.y = y;

    //mesh.setAbsolutePosition(new BABYLON.Vector3(0,0, 0));
    //mesh.absolutePosition = new BABYLON.Vector3(x, y, z);
    //mesh.translate(new BABYLON.Vector3(x,y, z), 1, BABYLON.Space.WORLD);
    mesh.position = new BABYLON.Vector3(x, y, z);
    return mesh;
}

babylonInterop.applyRotationToMesh = function (mesh, xRot, yRot, zRot) {
    if (!mesh) {
        return;
    }
    console.log("old rotation of mesh: " + mesh.rotation.x + " " + mesh.rotation.y + " " + mesh.rotation.z);
    console.log("Applying rotation  to mesh: " + xRot + " " + yRot + " " + zRot);

    mesh.rotation = new BABYLON.Vector3(xRot, yRot, zRot);
    //mesh.setAbsoluteRotation(new BABYLON.Vector3(xRot, yRot, zRot));
    return mesh;
}

babylonInterop.intersectsWithAny = function (scene, mesh) {
    var collisions = [];
    /* mesh.showBoundingBox = true;*/
    var oldStlPosition = mesh.position;
    var oldStlRotation = mesh.rotation;
    var stlTri = GetTriangles(mesh);
    console.log("stl triangles size: " + stlTri.length);
    const stlAbounds = mesh.getBoundingInfo().boundingBox;
    //mesh.showBoundingBox = true;
    t0 = performance.now();

    for (i = 0; i < scene.meshes.length; ++i) {
        var meshIter = scene.meshes[i];
        if (meshIter.name == "TextPlane" || meshIter.name == "axisX" || meshIter.name == "axisZ" || meshIter.name == "axisY"
            || meshIter.name.endsWith("_DIFF") || meshIter.name.startsWith("hole_")) {
            continue;
        }
        /* meshIter.showBoundingBox = true;*/
        if (mesh != meshIter) {
            if (mesh.intersectsMesh(meshIter, true)) {
                var pcbTri = GetTriangles(meshIter);
                console.log("stl triangles size: " + pcbTri.length);
                const pcbAbounds = meshIter.getBoundingInfo().boundingBox;
                var isColliding = IntersectsMesh(stlAbounds, pcbAbounds, stlTri, pcbTri);
                console.log("x&y = " + isColliding);
                if (isColliding) {
                    collisions.push(meshIter.name);
                    console.log("Mesh " + mesh.name + " intersects with mesh " + meshIter.name);
                }
            }
        }
    }
    t1 = performance.now();
    mesh.position = oldStlPosition;
    mesh.rotation = oldStlRotation;
    console.log("Intersect took " + (t1 - t0) + " milliseconds.");
    return collisions;
}

babylonInterop.runRenderLoop = function (engine, scene) {

    console.log("starting rendering scene.");
    engine.runRenderLoop(function () {
        scene.render();
    });
    console.log("rendering.");
}
babylonInterop.stopRenderLoop = function (engine, scene) {

    console.log("stopping rendering scene.");
    engine.stopRenderLoop();
    console.log("stopping rendering.");
}

babylonInterop.disposeObject = function (someObject) {
    if (!!someObject) {
        someObject.dispose();
    }
}


function showWorldAxis(scene, size) {
    var makeTextPlane = function (text, color, size) {
        var dynamicTexture = new BABYLON.DynamicTexture("DynamicTexture", 50, scene, true);
        dynamicTexture.hasAlpha = true;
        dynamicTexture.drawText(text, 5, 40, "bold 36px Arial", color, "transparent", true);
        var plane = BABYLON.Mesh.CreatePlane("TextPlane", size, scene, true);
        plane.material = new BABYLON.StandardMaterial("TextPlaneMaterial", scene);
        plane.material.backFaceCulling = false;
        plane.material.specularColor = new BABYLON.Color3(0, 0, 0);
        plane.material.diffuseTexture = dynamicTexture;
        return plane;
    };
    var axisX = BABYLON.Mesh.CreateLines("axisX", [
        BABYLON.Vector3.Zero(), new BABYLON.Vector3(size, 0, 0), new BABYLON.Vector3(size * 0.95, 0.05 * size, 0),
        new BABYLON.Vector3(size, 0, 0), new BABYLON.Vector3(size * 0.95, -0.05 * size, 0)
    ], scene);
    axisX.color = new BABYLON.Color3(1, 0, 0);
    var xChar = makeTextPlane("X", "red", size / 10);
    xChar.position = new BABYLON.Vector3(0.9 * size, -0.05 * size, 0);
    var axisY = BABYLON.Mesh.CreateLines("axisY", [
        BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, size, 0), new BABYLON.Vector3(-0.05 * size, size * 0.95, 0),
        new BABYLON.Vector3(0, size, 0), new BABYLON.Vector3(0.05 * size, size * 0.95, 0)
    ], scene);
    axisY.color = new BABYLON.Color3(0, 1, 0);
    var yChar = makeTextPlane("Y", "green", size / 10);
    yChar.position = new BABYLON.Vector3(0, 0.9 * size, -0.05 * size);
    var axisZ = BABYLON.Mesh.CreateLines("axisZ", [
        BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, 0, size), new BABYLON.Vector3(0, -0.05 * size, size * 0.95),
        new BABYLON.Vector3(0, 0, size), new BABYLON.Vector3(0, 0.05 * size, size * 0.95)
    ], scene);
    axisZ.color = new BABYLON.Color3(0, 0, 1);
    var zChar = makeTextPlane("Z", "blue", size / 10);
    zChar.position = new BABYLON.Vector3(0, 0.05 * size, 0.9 * size);
}


var gltf = `{
    "asset": {
        "generator": "COLLADA2GLTF",
        "version": "2.0"
    },
    "scene": 0,
    "scenes": [
        {
            "nodes": [
                0
            ]
        }
    ],
    "nodes": [
        {
            "children": [
                1
            ],
            "matrix": [
                1.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                -1.0,
                0.0,
                0.0,
                1.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                1.0
            ]
        },
        {
            "mesh": 0
        }
    ],
    "meshes": [
        {
            "primitives": [
                {
                    "attributes": {
                        "NORMAL": 1,
                        "POSITION": 2
                    },
                    "indices": 0,
                    "mode": 4,
                    "material": 0
                }
            ],
            "name": "Mesh"
        }
    ],
    "accessors": [
        {
            "bufferView": 0,
            "byteOffset": 0,
            "componentType": 5123,
            "count": 36,
            "max": [
                23
            ],
            "min": [
                0
            ],
            "type": "SCALAR"
        },
        {
            "bufferView": 1,
            "byteOffset": 0,
            "componentType": 5126,
            "count": 24,
            "max": [
                1.0,
                1.0,
                1.0
            ],
            "min": [
                -1.0,
                -1.0,
                -1.0
            ],
            "type": "VEC3"
        },
        {
            "bufferView": 1,
            "byteOffset": 288,
            "componentType": 5126,
            "count": 24,
            "max": [
                0.5,
                0.5,
                0.5
            ],
            "min": [
                -0.5,
                -0.5,
                -0.5
            ],
            "type": "VEC3"
        }
    ],
    "materials": [
        {
            "pbrMetallicRoughness": {
                "baseColorFactor": [
                    0.800000011920929,
                    0.0,
                    0.0,
                    1.0
                ],
                "metallicFactor": 0.0
            },
            "name": "Red"
        }
    ],
    "bufferViews": [
        {
            "buffer": 0,
            "byteOffset": 576,
            "byteLength": 72,
            "target": 34963
        },
        {
            "buffer": 0,
            "byteOffset": 0,
            "byteLength": 576,
            "byteStride": 12,
            "target": 34962
        }
    ],
    "buffers": [
        {
            "byteLength": 648,
            "uri": "data:application/octet-stream;base64,AAAAAAAAAAAAAIA/AAAAAAAAAAAAAIA/AAAAAAAAAAAAAIA/AAAAAAAAAAAAAIA/AAAAAAAAgL8AAAAAAAAAAAAAgL8AAAAAAAAAAAAAgL8AAAAAAAAAAAAAgL8AAAAAAACAPwAAAAAAAAAAAACAPwAAAAAAAAAAAACAPwAAAAAAAAAAAACAPwAAAAAAAAAAAAAAAAAAgD8AAAAAAAAAAAAAgD8AAAAAAAAAAAAAgD8AAAAAAAAAAAAAgD8AAAAAAACAvwAAAAAAAAAAAACAvwAAAAAAAAAAAACAvwAAAAAAAAAAAACAvwAAAAAAAAAAAAAAAAAAAAAAAIC/AAAAAAAAAAAAAIC/AAAAAAAAAAAAAIC/AAAAAAAAAAAAAIC/AAAAvwAAAL8AAAA/AAAAPwAAAL8AAAA/AAAAvwAAAD8AAAA/AAAAPwAAAD8AAAA/AAAAPwAAAL8AAAA/AAAAvwAAAL8AAAA/AAAAPwAAAL8AAAC/AAAAvwAAAL8AAAC/AAAAPwAAAD8AAAA/AAAAPwAAAL8AAAA/AAAAPwAAAD8AAAC/AAAAPwAAAL8AAAC/AAAAvwAAAD8AAAA/AAAAPwAAAD8AAAA/AAAAvwAAAD8AAAC/AAAAPwAAAD8AAAC/AAAAvwAAAL8AAAA/AAAAvwAAAD8AAAA/AAAAvwAAAL8AAAC/AAAAvwAAAD8AAAC/AAAAvwAAAL8AAAC/AAAAvwAAAD8AAAC/AAAAPwAAAL8AAAC/AAAAPwAAAD8AAAC/AAABAAIAAwACAAEABAAFAAYABwAGAAUACAAJAAoACwAKAAkADAANAA4ADwAOAA0AEAARABIAEwASABEAFAAVABYAFwAWABUA"
        }
    ]
}`;