﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.Xml.Serialization;
using EDMDProcessor;
using EDMDSerialization;
using Newtonsoft.Json;
using PCBXRevision;
using RevidX.ServiceClient;
using RevidX.ServiceClient.Models;
using static RevidX.ServiceClient.Models.BinaryContent;

namespace RevidXServer.Shared
{
    public class LightIDXReader
    {
        public static PCBContainer ReadIDXProps(byte[] pcbBinaryContent)
        {
            //XmlSerializer xmlSerializer = new XmlSerializer()
            using (MemoryStream stream = new(pcbBinaryContent))
            {
                EDMDReader edmdReader = new EDMDReader(stream);
                var dataset = edmdReader.DeserializeEDMDFile();
                return EDMDReader.ConstructPCBFromFile(dataset);
            }
        }

        public static PCBContainer ConstructPCB(EDMDDataSet dataset)
        {
            return EDMDReader.ConstructPCBFromFile(dataset);
        }

        public static EDMDDataSet ApplyDiffToProposal(byte[] proposalBinaryContent, PCBContainer pcbContainer)
        {
            using (MemoryStream proposalMS = new(proposalBinaryContent))
            {
                EDMDReader proposalEdmdReader = new EDMDReader(proposalMS);
                var proposalDataSet = proposalEdmdReader.DeserializeEDMDFile();
                var responseDataSet = EDMDReader.ApplyAndConvertToResponse(proposalDataSet, pcbContainer);
                return responseDataSet;
            }
        }

        public static EDMDDataSet ApplyResponse(byte[] baselineBinaryContent, EDMDDataSet responseDataSet)
        {
            using (MemoryStream baselineMS = new(baselineBinaryContent))
            {
                EDMDReader baselineEdmdReader = new EDMDReader(baselineMS);
                var newBaselineDS = baselineEdmdReader.ApplyResponse(responseDataSet);
                return newBaselineDS;
            }
        }
        public static bool ValidateProposal(byte[] baselineBinaryContent, byte[] proposalBinaryContent, out string errors)
        {
            using (MemoryStream baselineMS = new(baselineBinaryContent))
            using (MemoryStream proposalMS = new(proposalBinaryContent))
            {
                EDMDReader proposalEdmdReader = new EDMDReader(proposalMS);
                EDMDReader baselineEdmdReader = new EDMDReader(baselineMS);
                var result = baselineEdmdReader.ValidateProposal(proposalEdmdReader.DeserializeEDMDFile(), out errors);
                return result;
            }
        }
        public static async Task<PCBContainer> GetPCBFromRevisionAsync(Revision r, RevidXApiCaller revidXApiCaller)
        {
            PCBContainer PCBContainer = null;
            if (r == null)
            {
                throw new ArgumentNullException(nameof(r));
            }
            if (r.ResponseFileId != null)
            {
                var transactionFile = await revidXApiCaller.GetTransactionFile(r.ResponseFileId);
                if (transactionFile.BinaryContent.Content == null)
                {
                    throw new Exception(String.Format("Binary content for response {0}", r.ResponseFileId));
                }
                PCBContainer = ReadIDXProps(await FileUtils.GetIDXFromContentAsync(transactionFile.BinaryContent));
            }
            else if (r.ProposalFileId != null)
            {
                var transactionFile = await revidXApiCaller.GetTransactionFile(r.ProposalFileId);
                if (transactionFile.BinaryContent.Content == null)
                {
                    throw new Exception(String.Format("Binary content for proposal {0}", r.ResponseFileId));
                }
                PCBContainer = ReadIDXProps(await FileUtils.GetIDXFromContentAsync(transactionFile.BinaryContent));
            }
            else if (r.BaselineFileId != null)
            {
                var transactionFile = await revidXApiCaller.GetTransactionFile(r.BaselineFileId);
                if (transactionFile.BinaryContent.Content == null)
                {
                    throw new Exception(String.Format("Binary content for baseline {0}", r.ResponseFileId));
                }
                PCBContainer = ReadIDXProps(await FileUtils.GetIDXFromContentAsync(transactionFile.BinaryContent));
            }
            else
            {
                throw new Exception(String.Format("No content from revision {0}", r.Id));
            }
            return PCBContainer;
        }

        public static PCBContainer CompileDiffsToSingle(List<PCBContainer> deserializedContainers)
        {

            return PCBContainerUtils.CompileDiffsToSingle(deserializedContainers);
        }

        public static byte[] OutputStream(EDMDDataSet dataSet)
        {
            return EDMDReader.OutputToStream(dataSet);
        }
        public static PCBContainer GetPCBFromDS(EDMDDataSet dataSet)
        {
            return EDMDReader.ConstructPCBFromFile(dataSet);
        }


    }
}
