﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevidXServer.Shared
{
    public class FileData
    {
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public DateTime CreatedOn { get; set; }
        public string FileType { get; set; }
        public string Extension { get; set; }
        public string FilePath { get; set; }
    }
}
