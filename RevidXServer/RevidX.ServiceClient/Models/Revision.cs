﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class Revision
    {
        public Revision()
        {
            CreationTime = DateTime.UtcNow;
            ModificationTime = DateTime.UtcNow;
        }

        public string Id { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime ModificationTime { get; set; }

        public string ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public string LockedByUserId { get; set; }
        public UserProfile LockedByUser { get; set; }

        public string AwaitedByUserId { get; set; }
        public UserProfile AwaitedByUser { get; set; }

        public string PreviousRevisionId { get; set; }
        public virtual Revision PreviousRevision { get; set; }

        public string BaselineFileId { get; set; }
        public TransactionFile BaselineFile { get; set; }

        public string ProposalFileId { get; set; }
        public TransactionFile ProposalFile { get; set; }

        public string ResponseFileId { get; set; }
        public TransactionFile ResponseFile { get; set; }
        public string Description { get; set; }

        public string ReferencedProjectId { get; set; }
        public virtual Project ReferencedProject { get; set; }

        public TransactionFile.TransactionFileType GetRevisionTransactionType()
        {
            if (ResponseFileId != null)
            {
                return TransactionFile.TransactionFileType.RESPONSE;
            }
            else if (ProposalFileId != null)
            {
                return TransactionFile.TransactionFileType.PROPOSAL;
            }
            else if (BaselineFileId != null)
            {
                return TransactionFile.TransactionFileType.BASELINE;
            }
            else
            {
                return TransactionFile.TransactionFileType.UNKNOWN;
            }
        }
    }
}
