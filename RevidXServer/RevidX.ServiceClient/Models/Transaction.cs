﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class Transaction
    {

        public string Id { get; set; }

        public DateTime CreationTime { get; set; }
        
        public string ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public bool IsClosed { get; set; }

        public virtual IEnumerable<TransactionFile> TransactionFiles { get; set; }
    }
}
