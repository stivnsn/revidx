﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class UserProject
    {
        public UserProject()
        {
            IsEnabled = true;
        }
        
        
        public string Name
        {
            get { return Project?.Name; }
        }

        public string Id { get; set; }

        public string UserProfileId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        public string ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public string CurrentRevisionId { get; set; }
        public Revision CurrentRevision { get; set; }


        public string LatestPulledTransactionId { get; set; }
        public Transaction LatestPulledTransaction { get; set; }
        public string LatestPushedTransactionId { get; set; }
        public Transaction LatestPushedTransaction { get; set; }

        public string MonitoredPath { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsAccepted { get; set; }


        public List<Message> NewMessages { get; set; } = new List<Message>();
    }
}
