﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class TransactionFileActivity
    {
        public TransactionFileActivity()
        {
            IsPushed = false;
        }

        public string Id { get; set; }

        public DateTime StartTime { get; set; }
        
        public bool IsPushed { get; set; } // used for pulling activity for now

        public string UserProjectId { get; set; }
        public virtual UserProject UserProject { get; set; }

        public string TransactionFileId { get; set; }
        public virtual TransactionFile TransactionFile { get; set; }
    }
}
