﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class UserProfile
    {
        public string Email { get; set; }
        public string DirectoryPath { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Id { get; set; }


        public string IdentityId { get; set; }

        public string Name
        {
            get
            {
                var name = FirstName + " " + LastName;
                if (string.IsNullOrWhiteSpace(name))
                {
                    return Email;
                }
                return name;
            }
        }

        public bool IsMCAD { get; set; }

        public virtual IEnumerable<Project> CreatedProjects { get; set; }

        public virtual IEnumerable<Project> Projects { get; set; }

        public virtual List<UserProject> UserProjects { get; set; }

        public virtual IEnumerable<Revision> LockedRevisions { get; set; }

        public virtual IEnumerable<Revision> AwaitingRevisions { get; set; }

        public virtual IEnumerable<TransactionFile> TransactionFiles { get; set; }
        public virtual IEnumerable<TransactionFileActivity> TransactionFileActivities { get; set; }
        public virtual IEnumerable<Message> Messages { get; set; }
        public virtual IEnumerable<UserSubscription> UserSubscriptions { get; set; }
        public string CompanyName { get; set; }
    }
}
