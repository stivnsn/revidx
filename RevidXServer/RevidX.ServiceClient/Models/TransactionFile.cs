﻿using System;

namespace RevidX.ServiceClient.Models
{
    public class TransactionFile
    {
        public enum TransactionFileType
        {
            BASELINE,
            PROPOSAL,
            RESPONSE,
            UNKNOWN
        }

        public static TransactionFileType GetTransactionFileTypeEnum(string transactionFileTypeStr)
        {
            if (transactionFileTypeStr == TransactionFileType.BASELINE.ToString())
            {
                return TransactionFileType.BASELINE;
            }
            else if (transactionFileTypeStr == TransactionFileType.PROPOSAL.ToString())
            {
                return TransactionFileType.PROPOSAL;
            }
            else if (transactionFileTypeStr == TransactionFileType.RESPONSE.ToString())
            {
                return TransactionFileType.RESPONSE;
            }
            else
            {
                return TransactionFileType.UNKNOWN;
            }
        }

        public TransactionFile()
        {
            LastAccessTime = DateTime.UtcNow;
            LastWriteTime = DateTime.UtcNow;
        }

        public TransactionFile(TransactionFile tf)
        {
            Id = tf.Id;
            LastWriteTime = tf.LastWriteTime;
            LastAccessTime = tf.LastAccessTime;
            FullPath = tf.FullPath;
            TransactionId = tf.TransactionId;
            UserProfileId = tf.UserProfileId;
            TransactionFileActivityId = tf.TransactionFileActivityId;
            TransactionFileTypeStr = tf.TransactionFileTypeStr;
        }

        public TransactionFileType GetCurrentTransactionType() {
            return GetTransactionFileTypeEnum(TransactionFileTypeStr);
        }

        public string Id { get; set; }

        public DateTime LastWriteTime { get; set; }

        public DateTime LastAccessTime { get; set; }

        public string FileName { get; set; }

        public string FullPath { get; set; }

        public string UserProfileId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        public string TransactionId { get; set; }
        public virtual Transaction Transaction { get; set; }
        //public string RevisionId { get; set; }
        //public virtual Revision Revision { get; set; }

        public string TransactionFileActivityId { get; set; }

        public string TransactionFileTypeStr { get; set; }

        public string BinaryContentId { get; set; }
        public BinaryContent BinaryContent { get; set; }

        public PCBRevisionContent PCBRevisionContent { get; set; }

        public string Description { get; set; }
    }
}
