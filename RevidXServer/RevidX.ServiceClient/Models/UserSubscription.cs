﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public  class UserSubscription
    {
        public string Id { get; set; }
        public string UserProfileId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public bool IsActive { get; set; }
        public string SubscriptionId { get; set; }
        public virtual Subscription Subscription { get; set; }
        public string SubscriptionCodeId { get; set; }
        public virtual SubscriptionCode SubscriptionCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsReccuring { get; set; }
        public decimal PricePaid { get; set; }
        public string CurrencyPaid { get; set; }
        public string InvoiceId { get; set; }
    }
}
