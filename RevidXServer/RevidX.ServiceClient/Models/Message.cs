﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class Message
    {
        public string Id { get; set; }
        public string Text { get; set; }

        public DateTime SendTimeUtc { get; set; }

        public string SenderId { get; set; }
        public UserProfile Sender { get; set; }
        public string ProjectId { get; set; }
        public Project Project { get; set; }
        public TransactionFile ReferencedTransaction { get; set; }
    }
}
