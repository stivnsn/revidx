﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class RevisionDTO
    {

            public string Id { get; set; }

            public DateTime CreationTime { get; set; }

            public DateTime ModificationTime { get; set; }

            public string ProjectId { get; set; }

            public string LockedByUserId { get; set; }

            public string AwaitedByUserId { get; set; }

            public string PreviousRevisionId { get; set; }

            public string BaselineFileId { get; set; }

            public string ProposalFileId { get; set; }

            public string ResponseFileId { get; set; }

            public string PCBRevisionContentId { get; set; }

            public string LastBaseRevisionId { get; set; }
            public string Description { get; set; }

            public string ReferencedProjectId { get; set; }
        }
}
