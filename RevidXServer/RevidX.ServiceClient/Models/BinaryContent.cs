﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class BinaryContent
    {
        public enum FILE_TYPE
        {
            IDX,
            IDZ
        }
        public BinaryContent(byte[] content, string fileType)
        {
            Content = content;
            FileType = fileType;
        }

        public BinaryContent()
        {
        }
        public string Id { get; set; }
        public byte[] Content { get; set; }

        public string FileType { get; set; } // may be zip or other enum

        public string TransactionFileId { get; set; }
        public TransactionFile TransactionFile { get; set; }
    }
}
