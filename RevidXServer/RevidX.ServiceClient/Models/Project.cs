﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class Project
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public virtual UserProfile Creator {get; set;}
        public string CreatorId { get; set; }
   		public virtual IEnumerable<Transaction> Transactions { get; set; }
   		
        public virtual IEnumerable<Revision> Revisions { get; set; }


        public string LatestRevisionId { get; set; }
        public virtual Revision LatestRevision { get; set; }

        public virtual IEnumerable<UserProfile> Users {get; set;}
        public virtual List<UserProject> UserProjects { get; set; }

        public virtual IEnumerable<Message> Messages { get; set; }
    }
}
