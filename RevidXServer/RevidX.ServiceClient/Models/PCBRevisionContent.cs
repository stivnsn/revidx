﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class PCBRevisionContent
    {

        public string Id { get; set; }
        public string JsonContent { get; set; }

        public string RevisionId { get; set; }
        public virtual Revision Revision{ get; set; }

    }
}
