﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RevidX.ServiceClient.Models
{
    public class Subscription
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        //public TimeSpan Span { get; set; } = TimeSpan.FromDays(30);
        public bool IsActive { get; set; }
        public virtual IEnumerable<UserSubscription> UserSubscriptions { get; set; }
        public virtual IEnumerable<SubscriptionCode> SubscriptionCodes { get; set; }
    }
}
