﻿using System;
using System.Collections.Generic;
using System.Linq;
using RevidX.ServiceClient.Models;
using Serilog;

namespace RevidX.ServiceClient
{
    public enum RevisionState
    {
        AwaitingProposalPull,
        AwaitingResponsePush,
        ExpectingResponsePush,
        ProposalAvailable,
        ResponseAvailable,
        UpdateToLatest,
        UpdateToPrevious,
        AtLatestState,
        AtPreviousState,
        NoRevisions,
        //
        //
        RevisionError
    }
    public class EdmdRevisionSystem
    {

        public static string GenerateFileName(string fileType, string extension = ".idx")
        {
            return DateTime.UtcNow.ToString("yyMMddHHmmssffff_") + fileType + extension;
        }


        UserProject UserProject { get; set; }
        public EdmdRevisionSystem(UserProject userProject)
        {
            UserProject = userProject;
        }

        public RevisionState CurrentRevisionState { get; set; }

        public string CurrentStateText { get; set; }

        public bool GetRevisionAndFileToPull(out TransactionFile fileToPull, out Revision revision)
        {
            CurrentRevisionState = RevisionState.RevisionError;
            revision = null;
            fileToPull = null;
            string stateText = string.Empty;
            if (UserProject == null || UserProject.Project == null)
            {
                return false;
            }
            Revision latestRevision = UserProject.Project.LatestRevision;
            if (latestRevision != null)
            {
                bool isUserAtLatestRevision = latestRevision.Id == UserProject.CurrentRevisionId;
                bool isLocked = latestRevision.LockedByUserId != null;
                bool isResponsePushExpected = latestRevision.LockedByUserId == UserProject.UserProfileId;
                bool isUserAtPrevRevision = latestRevision.PreviousRevisionId == UserProject.CurrentRevisionId;
                bool isAwaitingForResponse = latestRevision.AwaitedByUserId == UserProject.UserProfileId;
                if (!isUserAtLatestRevision && !isUserAtPrevRevision && !isAwaitingForResponse)
                {
                    stateText += "Not at latest revision. ";
                    if (isResponsePushExpected)
                    {
                        stateText += "Expected response push, yet not at latest revision. ";
                        Log.Error("Expected response push, yet not at latest revision. Fatal error" + latestRevision.Id);
                        CurrentRevisionState = RevisionState.RevisionError;
                        return false;
                    }
                    if (latestRevision.BaselineFile != null)
                    {
                        stateText += "Baseline file of latest transaction is available. ";
                        //suggest this baseline/delta file
                        revision = latestRevision;
                        fileToPull = latestRevision.BaselineFile;
                        CurrentRevisionState = RevisionState.UpdateToLatest;
                    }
                    else if (latestRevision.PreviousRevision != null && latestRevision.PreviousRevision.BaselineFile != null)
                    {
                        stateText += "Latest revision is in pending state. But you can update to latest complete revision.";
                        revision = latestRevision.PreviousRevision;
                        fileToPull = latestRevision.PreviousRevision.BaselineFile;
                        CurrentRevisionState = RevisionState.UpdateToPrevious;
                    }
                }
                else
                {
                    if (isUserAtLatestRevision)
                    {
                        CurrentRevisionState = RevisionState.AtLatestState;
                    }
                    else
                    {
                        bool result = GetAvailableFileForPulling(out fileToPull, latestRevision, UserProject.UserProfileId);
                        if (!result)
                        {
                            Log.Error("Something is wrong with revision system.");
                            CurrentRevisionState = RevisionState.RevisionError;
                            return false;
                        }
                        if (!isAwaitingForResponse && !isResponsePushExpected)
                        {
                            CurrentRevisionState = RevisionState.AtLatestState;
                            if (!isLocked)
                            {
                                if (fileToPull != null && fileToPull.TransactionFileTypeStr == TransactionFile.TransactionFileType.BASELINE.ToString())
                                {
                                    CurrentRevisionState = RevisionState.UpdateToLatest;
                                }
                                else if (fileToPull != null && !isLocked && fileToPull.TransactionFileTypeStr == TransactionFile.TransactionFileType.PROPOSAL.ToString())
                                {
                                    CurrentRevisionState = RevisionState.ProposalAvailable;
                                }
                                else
                                {
                                    throw new Exception(string.Format("Fatal revision error. Transaction is not locked, yet not a proposal awaiting, nor update to latest available."));
                                }
                            }
                            else
                            {
                                fileToPull = null;
                                CurrentRevisionState = RevisionState.AtPreviousState;
                            }
                        }
                        else if (isAwaitingForResponse && isResponsePushExpected)
                        {
                            Log.Fatal("Fatal error: awaiting for response and expected for reesponse");
                            CurrentRevisionState = RevisionState.RevisionError;
                        }
                        else if (isAwaitingForResponse)
                        {
                            if (fileToPull != null && fileToPull.TransactionFileTypeStr == TransactionFile.TransactionFileType.RESPONSE.ToString())
                            {
                                CurrentRevisionState = RevisionState.ResponseAvailable;
                            }
                            else if (isLocked)
                            {
                                CurrentRevisionState = RevisionState.AwaitingResponsePush;
                                fileToPull = null;
                            }
                            else
                            {
                                CurrentRevisionState = RevisionState.AwaitingProposalPull;
                                fileToPull = null;
                            }
                        }
                        else  /// isResponsePushExpoected
                        {
                            CurrentRevisionState = RevisionState.ExpectingResponsePush;
                            fileToPull = null;
                        }
                        if (fileToPull != null)
                        {
                            revision = latestRevision;
                        }
                    }
                }
            }
            else
            {
                CurrentRevisionState = RevisionState.NoRevisions;
            }
            CurrentStateText = stateText;
            return true;
        }

        public bool GetAvailableFileTypesForPushing(out List<TransactionFile.TransactionFileType> transactionFileTypes)
        {

            transactionFileTypes = new List<TransactionFile.TransactionFileType>();
            var availableTypes = new[] { TransactionFile.TransactionFileType.BASELINE, TransactionFile.TransactionFileType.PROPOSAL, TransactionFile.TransactionFileType.RESPONSE };
            foreach (var type in availableTypes)
            {
                var result = CheckFileForPushing(out bool shouldPush, type.ToString());
                if (result && shouldPush)
                {
                    transactionFileTypes.Add(type);
                }
            }
            return true;
        }

        /* revision should be latest revision available, and userproject latest revision should be of latest or prevoius revision. 
         * If no current revision
         *  latest baseline should  be pushed
         * If at latest revision
         *  latest file should be baseline or proposal
         * If at previous revision and this revision is locked
         *  latest file should be response
         * choose latest file to upload, and notify if multiple files available if latest
         * if latest is 
         * 
         */
        public bool CheckFileForPushing(out bool shouldPush, string sendTypeStr)
        {
            var result = false;
            shouldPush = false;
            try
            {
                //var allTransactionFiles = Revisions.SelectMany(t => t.TransactionFiles).OrderByDescending(tf => tf.ModificationTime).ToList();\
                Revision latestRevision = UserProject.Project.LatestRevision;
                bool isBaseline = sendTypeStr == TransactionFile.TransactionFileType.BASELINE.ToString();
                bool isProposal = sendTypeStr == TransactionFile.TransactionFileType.PROPOSAL.ToString();
                bool isResponse = sendTypeStr == TransactionFile.TransactionFileType.RESPONSE.ToString();
                if (isResponse)
                {
                    shouldPush = latestRevision != null && latestRevision.PreviousRevision != null
                        && latestRevision.PreviousRevision.Id == UserProject.CurrentRevisionId
                        && latestRevision.ProposalFileId != null
                        && latestRevision.ResponseFileId == null
                        && latestRevision.AwaitedByUserId != null
                        && latestRevision.LockedByUserId == UserProject.UserProfileId;
                }
                else if (isProposal)
                {
                    bool lastTransactionIsCompleted = latestRevision != null && (latestRevision.ProposalFileId != null
                        && latestRevision.ResponseFileId != null) ||
                        (latestRevision.ProposalFileId == null
                        && latestRevision.ResponseFileId == null
                        );
                    shouldPush = lastTransactionIsCompleted && latestRevision.Id == UserProject.CurrentRevisionId
                        && latestRevision.LockedByUser == null;
                }
                if (isBaseline)
                {
                    bool lastTransactionIsCompleted = latestRevision == null
                        || (
                        (latestRevision.ProposalFileId != null
                        && latestRevision.ResponseFileId != null) ||
                        (latestRevision.ProposalFileId == null
                        && latestRevision.ResponseFileId == null)
                        );
                    shouldPush = lastTransactionIsCompleted;
                    //|| (latestRevision.Id == UserProject.CurrentRevisionId
                    //&& latestRevision.LockedByUser == null && lastTransactionIsCompleted);
                }
                result = true;
            }
            catch (Exception ex)
            {
                Log.Error("Exception thrown on checking file for pushing. " + ex.Message);
            }
            return result;
        }



        /* revision should be latest revision available, and userproject latest revision should be of latest or prevoius revision. 
         * If no current revision
         *  latest baseline should  be pushed
         * If at latest revision
         *  latest file should be baseline or proposal
         * If at previous revision and this revision is locked
         *  latest file should be response
         * choose latest file to upload, and notify if multiple files available if latest
         * if latest is 
         * 
         */
        public bool CheckLastRevisionForPushing(out bool shouldPush, string sendTypeStr)
        {
            var result = false;
            shouldPush = false;
            try
            {
                //var allTransactionFiles = Revisions.SelectMany(t => t.TransactionFiles).OrderByDescending(tf => tf.ModificationTime).ToList();\
                Revision latestRevision = UserProject.Project.LatestRevision;
                bool isBaseline = sendTypeStr == TransactionFile.TransactionFileType.BASELINE.ToString();
                bool isProposal = sendTypeStr == TransactionFile.TransactionFileType.PROPOSAL.ToString();
                bool isResponse = sendTypeStr == TransactionFile.TransactionFileType.RESPONSE.ToString();
                if (isResponse)
                {
                    shouldPush = latestRevision != null && latestRevision.PreviousRevision != null
                        //&& latestRevision.PreviousRevision.Id == UserProject.CurrentRevisionId
                        && latestRevision.ProposalFileId != null
                        && latestRevision.ResponseFileId == null
                        && latestRevision.AwaitedByUserId != null
                        && latestRevision.LockedByUserId == null;
                }
                else if (isProposal)
                {
                    bool lastTransactionIsCompleted = latestRevision != null && (latestRevision.ProposalFileId != null
                        && latestRevision.ResponseFileId != null) ||
                        (latestRevision.ProposalFileId == null
                        && latestRevision.ResponseFileId == null
                        );
                    shouldPush = lastTransactionIsCompleted
                        //&& latestRevision.Id == UserProject.CurrentRevisionId
                        && latestRevision.LockedByUser == null;
                }
                if (isBaseline)
                {
                    bool lastTransactionIsCompleted = latestRevision == null
                        || (
                        (latestRevision.ProposalFileId != null
                        && latestRevision.ResponseFileId != null) ||
                        (latestRevision.ProposalFileId == null
                        && latestRevision.ResponseFileId == null)
                        );
                    shouldPush = lastTransactionIsCompleted;
                    //|| (
                    //latestRevision.Id == UserProject.CurrentRevisionId &&
                    //latestRevision.LockedByUser == null && lastTransactionIsCompleted);
                }
                result = true;
            }
            catch (Exception ex)
            {
                Log.Error("Exception thrown on checking file for pushing. " + ex.Message);
            }
            return result;
        }

        /* 
         * should be used only for pulling latest file available. If nothing to pull, take baseline from previousRevision, or respond if you locked current transaction
         * fileToPull will be null if no file to pull, or will contain file to pull
         * returns false if error in revision system. Should not be tolerated.
         */
        private bool GetAvailableFileForPulling(out TransactionFile fileToPull, Revision latestRevision, string currentUserId)
        {
            var result = false;
            fileToPull = null;
            try
            {
                var baselineExists = latestRevision.BaselineFileId != null;
                var proposalExists = latestRevision.ProposalFileId != null;
                var responseExists = latestRevision.ResponseFileId != null;
                var isLocked = latestRevision.LockedByUser != null;
                var isAwaited = latestRevision.AwaitedByUser != null;
                if (!isLocked) // nije lockano, naci ce fajl ili null za prijasnju reviziju
                {
                    if (baselineExists)
                    {
                        if (responseExists && proposalExists && latestRevision.AwaitedByUserId == currentUserId) // latestRevision.ProposalFile.UserProfileId == currentUserId 
                        {
                            // this will ensure user will get appropriate file when pulling state
                            fileToPull = latestRevision.ResponseFile;
                        }
                        else if (UserProject.CurrentRevisionId != latestRevision.Id)
                        {
                            fileToPull = latestRevision.BaselineFile;
                        }
                    }
                    else if (responseExists)
                    {
                        throw new Exception("Response should not be available without baseline");
                    }
                    else if (proposalExists)
                    {
                        fileToPull = latestRevision.ProposalFile; // Add lock to transaction!!
                    }
                    else
                    {
                        throw new Exception("Mising files for this revision. Fatal error for revision: " + latestRevision.Id);
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                Log.Error("Fatal error in finding file to pull . " + ex.Message);
            }
            return result;
        }
    }
}