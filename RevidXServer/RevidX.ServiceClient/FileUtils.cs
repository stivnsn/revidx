﻿using RevidX.ServiceClient.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading.Tasks;
using static RevidX.ServiceClient.Models.BinaryContent;

namespace RevidX.ServiceClient
{
    public static class FileUtils
    {
        public static async Task<byte[]> AddToArchiveAsync(byte[] pcbBinaryContent, string filename)
        {
            if (pcbBinaryContent.Length == 0)
            {
                throw new Exception(string.Format("File stream has size 0 before compression"));
            }
            byte[] retBytes = null;
            var ms = new MemoryStream();
            using (var zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true))
            {
                var fileInArchive = zipArchive.CreateEntry(filename, CompressionLevel.Optimal);
                using (var entryStream = fileInArchive.Open())
                {
                    using (var tempMS = new MemoryStream(pcbBinaryContent))
                    {
                        await tempMS.CopyToAsync(entryStream);
                    }
                }

            }
            retBytes = ms.ToArray();
            if (retBytes.Length == 0)
            {

                throw new Exception(string.Format("Returning bytes are zero"));
            }
            var newBytes = await UnpackArchiveAsync(retBytes);
            if (newBytes.Length == 0)
            {

                throw new Exception(string.Format("Could not unpack what was packed."));
            }
            return retBytes;

        }

        public static async Task<byte[]> UnpackArchiveAsync(byte[] archiveBinContent)
        {
            using (MemoryStream archiveStream = new MemoryStream(archiveBinContent))
            using (var zipArchive = new ZipArchive(archiveStream, mode: ZipArchiveMode.Read, true))
            {
                if (zipArchive == null)
                {

                    throw new Exception(string.Format("Could not read zip. Storing idz."));
                }
                if (zipArchive.Entries.Count != 1)
                {
                    throw new Exception(string.Format("Throwing because there was {0} files in zip", zipArchive.Entries.Count));
                }
                var stream = zipArchive.Entries[0].Open();

                //if (stream.Length == 0)
                //{
                //    throw new Exception(string.Format("Extracted streaam {0} has size 0 after extraction. {1}", zipArchive.Entries[0].Name, stream.ToString()));
                //}
                var ms = new MemoryStream();
                await stream.CopyToAsync(ms);
                if (ms.Length == 0)
                {

                    throw new Exception(string.Format("Stream from compressed archive is empty."));
                }
                return ms.ToArray();
            }
        }

        public static async Task<byte[]> GetIDXFromContentAsync(BinaryContent content)
        {
            if (content.FileType == FILE_TYPE.IDZ.ToString())
            {
                if (content.Content.Length == 0)
                {
                    throw new Exception(string.Format("Zip has size 0 before extraction"));
                }
                return await UnpackArchiveAsync(content.Content);
            }
            return content.Content;
        }

        public static async Task<BinaryContent> GetContentFromIDXAsync(byte[] pcbBinaryContent, string filename)
        {
            var zipStream = await AddToArchiveAsync(pcbBinaryContent, filename);
            return new BinaryContent(zipStream, FILE_TYPE.IDZ.ToString());
        }
    }
}
