﻿using Newtonsoft.Json;
using RevidX.ServiceClient.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using static RevidX.ServiceClient.EdmdRevisionSystem;

namespace RevidX.ServiceClient
{
    static class GlobalURI
    {
        static string customURI;
        public static string CustomURI
        {
            get
            {
                if (string.IsNullOrWhiteSpace(customURI))
                {
                    //return "https://mservice.azurewebsites.net";
                    return "http://localhost:44397"; // "https://localhost:44301";
                }
                return customURI;
            }
            set
            {
                customURI = value;
            }
        }
    }

    public class JWTHttpClient : HttpClient
    {
        public JWTHttpClient(string token, Uri uri)
        {
            // Set your base address
            BaseAddress = uri;

            if (!string.IsNullOrEmpty(token))
            {
                DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            }
        }
    }

    public class RevidXApiCaller
    {
        HttpClient httpClient { get; set; }
        public RevidXApiCaller(HttpClient client)
        {
            httpClient = client;
        }

        public RevidXApiCaller(string token, Uri uri)
        {
            httpClient = new JWTHttpClient(token, uri);
        }

        public async Task<UserProfile> GetUserProfileAsync()
        {
            UserProfile userProfile = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "UserProfile"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                userProfile = JsonConvert.DeserializeObject<UserProfile>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting user profile call failed" + ex.Message);
            }
            return userProfile;
        }
        public async Task<List<Revision>> GetRevisionsAdminAsync(string projectId)
        {
            List<Revision> revisions = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Superadmin", "getrevisions", projectId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                revisions = JsonConvert.DeserializeObject<List<Revision>>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting user profile call failed" + ex.Message);
            }
            return revisions;
        }
        public async Task<List<UserProject>> GetUserProjectsAdminAsync(string userId)
        {
            List<UserProject> userProjects = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Superadmin", "getprojects", userId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                userProjects = JsonConvert.DeserializeObject<List<UserProject>>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting user profile call failed" + ex.Message);
            }
            return userProjects;
        }
        public async Task<List<UserProfile>> GetUsersAdminAsync()
        {
            List<UserProfile> userProfiles = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Superadmin", "getusers"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                userProfiles = JsonConvert.DeserializeObject<List<UserProfile>>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting user profile call failed" + ex.Message);
            }
            return userProfiles;
        }

        public async Task<UserProfile> GetUserProfileAsync(string userProfileId)
        {
            UserProfile userProfile = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "UserProfile", userProfileId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                userProfile = JsonConvert.DeserializeObject<UserProfile>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting user profile call failed" + ex.Message);
            }
            return userProfile;
        }

        public async Task<List<UserProfile>> GetUserProfilesByProjectId(string projectId)
        {
            List<UserProfile> userProfiles = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "UserProfile", "byProject", projectId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                userProfiles = JsonConvert.DeserializeObject<List<UserProfile>>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting user profile call failed" + ex.Message);
            }
            return userProfiles;
        }

        public async Task<List<UserProject>> GetUserProjects()
        {
            List<UserProject> userProjects = null;
            try
            {
                string baseAddress = httpClient.BaseAddress.ToString();
                var response = await httpClient.GetAsync(Path.Combine(baseAddress, "api", "UserProjects"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                userProjects = JsonConvert.DeserializeObject<List<UserProject>>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting user profile call failed: " + ex.Message);
            }
            return userProjects;
        }

        public async Task<UserProject> GetUserProject(string id)
        {
            UserProject userProject = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "UserProjects", id));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = response.Content.ReadAsStringAsync();
                userProject = JsonConvert.DeserializeObject<UserProject>(content.Result);
            }
            catch (Exception ex)
            {
                Log.Error("Getting user profile call failed: " + ex.Message);
            }
            return userProject;
        }

        public async Task<bool> CreateUserProfileAsync(UserProfile userProfile)
        {
            try
            {
                var response = await httpClient.PostAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "UserProfile"),
                    new StringContent(JsonConvert.SerializeObject(userProfile), Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();

                var items = JsonConvert.DeserializeObject<UserProfile>(content);
                userProfile.Id = items.Id;
            }
            catch (Exception ex)
            {
                Log.Error("Creating user profile call failed: " + ex.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> UpdateUserProfileAsync(UserProfile userProfile)
        {
            try
            {
                var response = await httpClient.PutAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "UserProfile"),
                    new StringContent(JsonConvert.SerializeObject(userProfile), Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Updating user profile call failed: " + ex.Message);
                return false;
            }
            return true;
        }



        public async Task<bool> DeleteUserProfileAsync(UserProfile userProfile)
        {
            try
            {
                var response = await httpClient.DeleteAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "UserProfile", userProfile.Id));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Updating user profile call failed: " + ex.Message);
                return false;
            }
            return true;
        }

        public async Task<List<Message>> GetProjectMessages(string id)
        {
            List<Message> messages = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Project", "messages", id));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = response.Content.ReadAsStringAsync();
                messages = JsonConvert.DeserializeObject<List<Message>>(content.Result);
            }
            catch (Exception ex)
            {
                Log.Error("Getting user profile call failed: " + ex.Message);
            }
            return messages;
        }

        public async Task<bool> AddUserToThisProject(Project project, string usernameToAdd)
        {
            try
            {
                var response = await httpClient.PutAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Project", usernameToAdd),
                    new StringContent(JsonConvert.SerializeObject(project), Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = response.Content.ReadAsStringAsync();
                if(bool.TryParse(content.Result, out bool result))
                {
                    return result;
                }

            }
            catch (Exception ex)
            {
                Log.Error("Updating project call failed: " + ex.Message);
                return false;
            }
            return true;
        }

        public async Task<UserProject> GetUserProjectFromProject(string projectId)
        {
            UserProject userProject = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Project", projectId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = response.Content.ReadAsStringAsync();
                userProject = JsonConvert.DeserializeObject<UserProject>(content.Result); ;
            }
            catch (Exception ex)
            {
                Log.Error("Updating user project call failed: " + ex.Message);
                return null;
            }
            return userProject;
        }

        public async Task<bool> UpdateUserProject(UserProject userProjects)
        {
            try
            {
                var response = await httpClient.PutAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "UserProjects"),
                    new StringContent(JsonConvert.SerializeObject(userProjects), Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Updating user project call failed: " + ex.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> UpdateProject(Project project)
        {
            try
            {
                var response = await httpClient.PutAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Project"),
                    new StringContent(JsonConvert.SerializeObject(project), Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Updating project call failed: " + ex.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> DeleteProject(string projectid)
        {
            try
            {
                var response = await httpClient.DeleteAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Project",
                    projectid));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Deleting project call failed: " + ex.Message);
                return false;
            }
            return true;
        }

        public async Task<Project> CreateProject(Project project)
        {
            Project retProject;
            try
            {
                var response = await httpClient.PostAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Project"),
                    new StringContent(JsonConvert.SerializeObject(project), Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();

                var item = JsonConvert.DeserializeObject<Project>(content);
                retProject = item;
            }
            catch (Exception ex)
            {
                Log.Error("Creating project call failed: " + ex.Message);
                return null;
            }
            return retProject;
        }


        public async Task<Transaction> CreateTransaction(Transaction transaction)
        {
            Transaction retTransaction = null;
            try
            {
                var response = await httpClient.PostAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Transaction"),
                   new StringContent(JsonConvert.SerializeObject(transaction), Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();

                var items = JsonConvert.DeserializeObject<Transaction>(content);
                retTransaction = items;
            }
            catch (Exception ex)
            {
                Log.Error("Creating transaction call failed: " + ex.Message);
                return null;
            }
            return retTransaction;
        }

        public async Task<TransactionFile> GetTransactionFile(string transactionFileId, bool tryFetchPcbContainerOnly = false)
        {
            TransactionFile transactionFile = null;
            try
            {
                HttpResponseMessage response  = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "File", transactionFileId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                transactionFile = JsonConvert.DeserializeObject<TransactionFile>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting transaction call failed: " + ex.Message);
            }
            return transactionFile;
        }


        /// <summary>
        /// Get revision set betewen first to second revision. If second is null, it will take it's latest baseline
        /// </summary>
        /// <param name="endRevisionId"></param>
        /// <param name="startRevisionId"></param>
        /// <returns></returns>
        public async Task<List<Revision>> GetRevisionDiffSet(string endRevisionId, string startRevisionId)
        {
            List<Revision> revisionList = null;
            try
            {
                HttpResponseMessage response = null;
                if (startRevisionId != null)
                {
                    response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "List", endRevisionId, startRevisionId));
                }
                else
                {
                    response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "List", endRevisionId));

                }
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                revisionList = JsonConvert.DeserializeObject<List<Revision>>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting transaction call failed: " + ex.Message);
            }
            return revisionList;
        }

        
            public async Task<Revision> GetRevisionById(string projectId)
        {
            Revision revision = null;
            try
            {
                HttpResponseMessage response = null;
                    response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "byId", projectId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                revision = JsonConvert.DeserializeObject<Revision>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting transaction call failed: " + ex.Message);
            }
            return revision;
        }
        /// <summary>
        /// Revision system
        /// </summary>
        /// <param name="transactionFileId"></param>
        /// <returns></returns>
        public async Task<(RevisionState, Revision)> GetAvailableRevision(string userProjectId)
        {
            (RevisionState, Revision) revisionMessageTuple = (RevisionState.RevisionError, null);
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "Pull", userProjectId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                revisionMessageTuple = JsonConvert.DeserializeObject<(RevisionState, Revision)>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Pulling transaction call failed: " + ex.Message);
            }
            return revisionMessageTuple;
        }
        /// <summary>
        /// RevisionSystem pull
        /// </summary>
        /// <param name="transactionFileId"></param>
        /// <returns></returns>
        public async Task<TransactionFile> PullTransactionFile(string transactionFileId, string userProjectId)
        {
            TransactionFile transactionFile = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "Pull", transactionFileId, userProjectId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                transactionFile = JsonConvert.DeserializeObject<TransactionFile>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Pulling transaction call failed: " + ex.Message);
            }
            return transactionFile;
        }

        /// <summary>
        /// Revision system push check
        /// </summary>
        /// <param name="transactionFileId"></param>
        /// <returns></returns>
        public async Task<List<TransactionFile.TransactionFileType>> GetAvailableFileTypes(string userProjectId)
        {
            List<TransactionFile.TransactionFileType> transactionFileTypes = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "Push", userProjectId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                transactionFileTypes = JsonConvert.DeserializeObject<List<TransactionFile.TransactionFileType>>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Pulling transaction call failed: " + ex.Message);
            }
            return transactionFileTypes;
        }

        public async Task<bool> CancelRevision(string userProjectId)
        {
            bool result = false;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "Cancel", userProjectId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<bool>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Cancel revision call failed: " + ex.Message);
            }
            return result;

        }

        public async Task<bool> UpdateUserProjectToRevision(string userProjectId, string revisionId)
        {
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "UpdateToRevision", userProjectId, revisionId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Updating user profile call failed: " + ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Revision system revision push
        /// </summary>
        /// <param name="transactionFileId"></param>
        /// <returns></returns>
        public async Task<Revision> PushRevision(Revision revision, string userProjectId)
        {
            Revision retRevision = null;
            try
            {
                var response = await httpClient.PostAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "Push", userProjectId),
                   new StringContent(JsonConvert.SerializeObject(revision), Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                retRevision = JsonConvert.DeserializeObject<Revision>(content);
                //var content = await response.Content.ReadAsStringAsync();

                //var items = JsonConvert.DeserializeObject<Project>(content);
                //transaction.Id = items.Id;
            }
            catch (Exception ex)
            {
                Log.Error("Pulling transaction call failed: " + ex.Message);
            }
            return retRevision;
        }

        /// <summary>
        /// Revision system revision push global
        /// </summary>
        /// <param name="transactionFileId"></param>
        /// <returns></returns>
        public async Task<Revision> PushRevisionGlobal(Revision revision, string userProjectId)
        {
            Revision retRevision = null;
            try
            {
                var response = await httpClient.PostAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "PushGlobal", userProjectId),
                   new StringContent(JsonConvert.SerializeObject(revision), Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                retRevision = JsonConvert.DeserializeObject<Revision>(content);
                //var content = await response.Content.ReadAsStringAsync();

                //var items = JsonConvert.DeserializeObject<Project>(content);
                //transaction.Id = items.Id;
            }
            catch (Exception ex)
            {
                Log.Error("Pulling transaction call failed: " + ex.Message);
            }
            return retRevision;
        }


        public async Task<List<Revision>> GetRevisions(string projectId)
        {
            List<Revision> transactions = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", projectId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                transactions = JsonConvert.DeserializeObject<List<Revision>>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Pulling transaction call failed: " + ex.Message);
            }
            return transactions;
        }


        public async Task<TransactionFileActivity> GetLatestPulledTransaction(string userProjectId)
        {
            TransactionFileActivity transactionFileActivity = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", "Activity", userProjectId));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                transactionFileActivity = JsonConvert.DeserializeObject<TransactionFileActivity>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting latest transaction call failed: " + ex.Message);
            }
            return transactionFileActivity;
        }


        public async Task<List<Revision>> GetRevisionsByPageParameters(string projectId, int index, int size)
        {
            List<Revision> transactions = null;
            try
            {
                var response = await httpClient.GetAsync(Path.Combine(httpClient.BaseAddress.ToString(), "api", "Revision", projectId, index.ToString(), size.ToString()));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.StatusCode + " " + response.ReasonPhrase);
                }
                var content = await response.Content.ReadAsStringAsync();
                transactions = JsonConvert.DeserializeObject<List<Revision>>(content);
            }
            catch (Exception ex)
            {
                Log.Error("Getting transactions by page parameters failed call failed: " + ex.Message);
            }
            return transactions;
        }
    }
}