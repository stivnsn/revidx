﻿using IdentityModel.OidcClient;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RevidX.ServiceClient
{

    public class CustomTokenProvider
    {
        private OidcClient OidcClient { get; set; }
        string AccessToken { get; set; }
        string RefreshToken { get; set; }
        DateTimeOffset AccessTokenExpiration { get; set; }
        public CustomTokenProvider(OidcClient oidcClient, LoginResult result)
        {
            OidcClient = oidcClient;
            AccessToken = result.AccessToken;
            AccessTokenExpiration = result.AccessTokenExpiration;
            RefreshToken = result.RefreshToken;
        }

        public async Task<string> GetAccessToken()
        {
            try
            {
                if (AccessTokenExpiration.DateTime > DateTime.Now.AddSeconds(-30))
                {
                    return AccessToken;
                }
                else
                {
                    var result = await OidcClient.LoginAsync(new LoginRequest());
                    Log.Error("Attempting new login instead of refreshing token. Asp.net problem needs fix");
                    //var result = await OidcClient.RefreshTokenAsync(RefreshToken);
                    AccessToken = result.AccessToken;
                    AccessTokenExpiration = result.AccessTokenExpiration;
                    RefreshToken = result.RefreshToken;
                    return AccessToken;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Could not get access token> " + ex.Message);
            }
            return null;
        }
    }
}
