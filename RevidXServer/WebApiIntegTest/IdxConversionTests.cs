﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EDMDProcessor;
using EDMDSerialization;
using NUnit.Framework;
using RevidX.ServiceClient;
using Serilog;
using Serilog.Events;

namespace WebApiIntegTest
{
    public class IdxConversionTests
    {


        const string boardOnlyBaseline = @"../../../test_data/baseline_board_only.idx";
        const string indFull = @"../../../test_data/baseline_ind_full.idx";
        const string baselineMcdFewItems = @"../../../test_data/baseline_ms_starter_01.idx";
        const string msStarter = @"../../../test_data/baseline_ms_starter_01.idx";
        const string cadenceCMBaseline = @"../../../test_data/cadence_baseline_arduino.idx";
        const string mgArduinoBaseline = @"../../../test_data/baseline_mgc_arduino_custom.idx";
        const string mgArduinoProposal = @"../../../test_data/proposal_mgc_arduino_custom.idx";
        const string mgArduinoResponse = @"../../../test_data/response_mgc_arduino_custom.idx";


        readonly static string pixRacerDataDir = @"../../../test_data/pixracer_proposal_scenario";
        readonly static string pixRacerOutputDir = Path.Combine(pixRacerDataDir, @"output");

        readonly string pixBaseline = Path.Combine(pixRacerDataDir, @"baseline_06.idx");
        readonly string pixProposal1 = Path.Combine(pixRacerDataDir, @"proposal_01.idx");
        readonly string pixProposal2 = Path.Combine(pixRacerDataDir, @"proposal_02.idx");
        readonly string pixProposal3 = Path.Combine(pixRacerDataDir, @"proposal_03.idx");
        readonly string pixProposal4 = Path.Combine(pixRacerDataDir, @"proposal_04.idx");
        readonly string pixResponse1 = Path.Combine(pixRacerDataDir, @"response_00.idx");
        readonly string pixResponse2 = Path.Combine(pixRacerDataDir, @"response_01.idx");
        readonly string pixBaselineGen = Path.Combine(pixRacerOutputDir, @"baseline_gen_06.idx");

        const string testDataDir = @"../../../test_data/test_1_scenario";
        const string testDataOutputDir = testDataDir + @"/output";

        readonly string test_01_bs = Path.Combine(testDataDir, @"01_bs_test_initial_bo_ro_02.idx");
        readonly string test_02_prop = Path.Combine(testDataDir, @"02_prop_added_comp_01.idx");
        readonly string test_03_resp = Path.Combine(testDataDir, @"03_resp_added_comp_01.idx");
        readonly string test_04_prop = Path.Combine(testDataDir, @"04_prop_rem_comp_02.idx");
        readonly string test_05_resp = Path.Combine(testDataDir, @"05_resp_rem_comp_02.idx");
        readonly string test_06_prop = Path.Combine(testDataDir, @"06_prop_added_multiple_03.idx");
        readonly string test_07_resp = Path.Combine(testDataDir, @"07_resp_added_multiple_03.idx");
        readonly string output_applied_response_01 = Path.Combine(testDataOutputDir, @"01_applied_response_03.idx");

        public IdxConversionTests()
        {
        }


        [SetUp]
        public void Setup()
        {
            string logFileLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "RevidXClient", "RevidXClientTest.log");
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Trace()
                .WriteTo.File(logFileLocation, outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}")

                .CreateLogger();

            if (!Directory.Exists(testDataOutputDir))
            {
                Directory.CreateDirectory(testDataOutputDir);
            }
            else
            {
                foreach (var file in Directory.GetFiles(testDataOutputDir))
                {
                    File.Delete(file);
                }
            }
            if (!Directory.Exists(pixRacerOutputDir))
            {
                Directory.CreateDirectory(pixRacerOutputDir);
            }
            else
            {
                foreach (var file in Directory.GetFiles(pixRacerOutputDir))
                {
                    File.Delete(file);
                }
            }
        }


        [Test]
        public void BoardOnlyConversion()
        {
            EDMDReader reader = new EDMDReader(boardOnlyBaseline);
            Assert.NotNull(reader);
            var origDataSet = reader.DeserializeEDMDFile();
            var pcbContainer = EDMDReader.ConstructPCBFromFile(origDataSet);
            Assert.NotNull(pcbContainer);
            IdxDataSetConstructor idxDataSetConstructor = new IdxDataSetConstructor();
            var dataSet = idxDataSetConstructor.ReconstructIdxFile(pcbContainer);
            Assert.NotNull(dataSet);


            
        }
    }
}
