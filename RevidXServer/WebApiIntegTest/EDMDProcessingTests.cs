﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EDMDProcessor;
using NUnit.Framework;
using RevidX.ServiceClient;
using Serilog;
using Serilog.Events;

namespace WebApiIntegTest
{
    public class EDMDProcessingTests
    {


        const string boardOnlyBaseline = @"../../../test_data/baseline_board_only.idx";
        const string indFull = @"../../../test_data/baseline_ind_full.idx";
        const string baselineMcdFewItems = @"../../../test_data/baseline_ms_starter_01.idx";
        const string msStarter = @"../../../test_data/baseline_ms_starter_01.idx";
        const string cadenceCMBaseline = @"../../../test_data/cadence_baseline_arduino.idx";
        const string mgArduinoBaseline = @"../../../test_data/baseline_mgc_arduino_custom.idx";
        const string mgArduinoProposal = @"../../../test_data/proposal_mgc_arduino_custom.idx";
        const string mgArduinoResponse = @"../../../test_data/response_mgc_arduino_custom.idx";


        readonly static string pixRacerDataDir = @"../../../test_data/pixracer_proposal_scenario";
        readonly static string pixRacerOutputDir = Path.Combine(pixRacerDataDir, @"output");

        readonly string pixBaseline = Path.Combine(pixRacerDataDir, @"baseline_06.idx");
        readonly string pixProposal1 = Path.Combine(pixRacerDataDir, @"proposal_01.idx");
        readonly string pixProposal2 = Path.Combine(pixRacerDataDir, @"proposal_02.idx");
        readonly string pixProposal3 = Path.Combine(pixRacerDataDir, @"proposal_03.idx");
        readonly string pixProposal4 = Path.Combine(pixRacerDataDir, @"proposal_04.idx");
        readonly string pixResponse1 = Path.Combine(pixRacerDataDir, @"response_00.idx");
        readonly string pixResponse2 = Path.Combine(pixRacerDataDir, @"response_01.idx");
        readonly string pixBaselineGen = Path.Combine(pixRacerOutputDir, @"baseline_gen_06.idx");

        const string testDataDir = @"../../../test_data/test_1_scenario";
        const string testDataOutputDir = testDataDir + @"/output";

        readonly string test_01_bs = Path.Combine(testDataDir, @"01_bs_test_initial_bo_ro_02.idx");
        readonly string test_02_prop = Path.Combine(testDataDir, @"02_prop_added_comp_01.idx");
        readonly string test_03_resp = Path.Combine(testDataDir, @"03_resp_added_comp_01.idx");
        readonly string test_04_prop = Path.Combine(testDataDir, @"04_prop_rem_comp_02.idx");
        readonly string test_05_resp = Path.Combine(testDataDir, @"05_resp_rem_comp_02.idx");
        readonly string test_06_prop = Path.Combine(testDataDir, @"06_prop_added_multiple_03.idx");
        readonly string test_07_resp = Path.Combine(testDataDir, @"07_resp_added_multiple_03.idx");
        readonly string output_applied_response_01 = Path.Combine(testDataOutputDir, @"01_applied_response_03.idx");

        public EDMDProcessingTests()
        {
        }


        [SetUp]
        public void Setup()
        {
            string logFileLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "RevidXClient", "RevidXClientTest.log");
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Trace()
                .WriteTo.File(logFileLocation, outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}")

                .CreateLogger();

            if (!Directory.Exists(testDataOutputDir))
            {
                Directory.CreateDirectory(testDataOutputDir);
            }
            else
            {
                foreach (var file in Directory.GetFiles(testDataOutputDir))
                {
                    File.Delete(file);
                }
            }
            if (!Directory.Exists(pixRacerOutputDir))
            {
                Directory.CreateDirectory(pixRacerOutputDir);
            }
            else
            {
                foreach (var file in Directory.GetFiles(pixRacerOutputDir))
                {
                    File.Delete(file);
                }
            }
        }

        [Test]
        public async Task IDZBasicFunctionTest()
        {

            byte[] fileBinary = null;
            using (var _fileMStream = new MemoryStream())
            {
                using (var fs = new FileStream(pixBaseline, FileMode.Open))
                {
                    fs.CopyTo(_fileMStream);
                    _fileMStream.Seek(0, SeekOrigin.Begin);
                    Assert.NotNull(_fileMStream);
                    fileBinary = _fileMStream.ToArray();
                }
            }
            string tempName = "znjFile";
            var fileMS = await FileUtils.AddToArchiveAsync(fileBinary, tempName);
            Assert.NotNull(fileMS);
            var extractedFile = await FileUtils.UnpackArchiveAsync(fileMS);
            Assert.NotNull(extractedFile);
            using (var ms = new MemoryStream(extractedFile))
            {
                EDMDReader reader = new(ms);
                var result = reader.DeserializeEDMDFile();
                Assert.NotNull(result);
            }
        }

        [Test]
        public void PixResponseApplyingTest()
        {
            EDMDReader reader = new EDMDReader(pixBaseline);
            Assert.NotNull(reader);
            //var pcbContainer = processor.ConstructPCBFromFile(processor.DeserializeEDMDFile());
            EDMDReader respReader = new EDMDReader(pixResponse1);
            Assert.NotNull(respReader);
            var respDS = respReader.DeserializeEDMDFile();
            Assert.NotNull(respDS);
            var appliedDataset = reader.ApplyResponse(respDS);
            Assert.NotNull(appliedDataset);
            EDMDReader.OutputToFile(pixBaselineGen, appliedDataset);
            Assert.IsTrue(File.Exists(pixBaselineGen));
            EDMDReader resultProcessor = new EDMDReader(pixBaselineGen);
            Assert.NotNull(resultProcessor);
            var resultDS = resultProcessor.DeserializeEDMDFile();
            Assert.NotNull(resultDS);
            var resultPCB = EDMDReader.ConstructPCBFromFile(resultDS);
            Assert.NotNull(resultPCB);
        }

        [Test]
        public void PixConsecutiveResponseApplyingTest()
        {
            EDMDReader reader = new EDMDReader(pixBaseline);
            Assert.NotNull(reader);
            //var pcbContainer = processor.ConstructPCBFromFile(processor.DeserializeEDMDFile());
            EDMDReader respReader = new EDMDReader(pixResponse1);
            Assert.NotNull(respReader);
            var respDS = respReader.DeserializeEDMDFile();
            Assert.NotNull(respDS);
            var appliedDataset = reader.ApplyResponse(respDS);
            Assert.NotNull(appliedDataset);
            EDMDReader.OutputToFile(pixBaselineGen, appliedDataset);
            Assert.IsTrue(File.Exists(pixBaselineGen));
            EDMDReader resultReader = new EDMDReader(pixBaselineGen);
            Assert.NotNull(resultReader);
            //var resultDS = resultReader.DeserializeEDMDFile();
            //Assert.NotNull(resultDS);
            //var resultPCB = resultReader.ConstructPCBFromFile(resultDS);
            //Assert.NotNull(resultPCB);

            EDMDReader respReader2 = new EDMDReader(pixResponse2);
            Assert.NotNull(respReader2);
            var respDS2 = respReader2.DeserializeEDMDFile();
            Assert.NotNull(respDS2);
            var appliedDataset2 = resultReader.ApplyResponse(respDS2);
            Assert.NotNull(appliedDataset2);
        }

        [Test]
        public void PixConsecutiveIdenticalResponseApplyingTest()
        {
            EDMDReader reader = new EDMDReader(pixBaseline);
            Assert.NotNull(reader);
            //var pcbContainer = processor.ConstructPCBFromFile(processor.DeserializeEDMDFile());
            EDMDReader respReader = new EDMDReader(pixResponse1);
            Assert.NotNull(respReader);
            var respDS = respReader.DeserializeEDMDFile();
            Assert.NotNull(respDS);
            var appliedDataset = reader.ApplyResponse(respDS);
            Assert.NotNull(appliedDataset);
            EDMDReader.OutputToFile(pixBaselineGen, appliedDataset);
            Assert.IsTrue(File.Exists(pixBaselineGen));
            EDMDReader resultReader = new EDMDReader(pixBaselineGen);
            Assert.NotNull(resultReader);
            //var resultDS = resultReader.DeserializeEDMDFile();
            //Assert.NotNull(resultDS);
            //var resultPCB = resultReader.ConstructPCBFromFile(resultDS);
            //Assert.NotNull(resultPCB);

            EDMDReader respReader2 = new EDMDReader(pixResponse1);
            Assert.NotNull(respReader2);
            var respDS2 = respReader2.DeserializeEDMDFile();
            Assert.NotNull(respDS2);
            var appliedDataset2 = resultReader.ApplyResponse(respDS2);
            Assert.NotNull(appliedDataset2);
        }

        [Test]
        public void ConsecutiveResponseApplication()
        {
            EDMDReader reader = new EDMDReader(test_01_bs);
            Assert.NotNull(reader);
            //var pcbContainer = processor.ConstructPCBFromFile(processor.DeserializeEDMDFile());
            EDMDReader respReader = new EDMDReader(test_03_resp);
            Assert.NotNull(respReader);
            var deseriealizedResp = respReader.DeserializeEDMDFile();
            Assert.NotNull(deseriealizedResp);
            var appliedResp = reader.ApplyResponse(deseriealizedResp);
            Assert.NotNull(appliedResp);
            var resultDataset = EDMDReader.OutputToFile(output_applied_response_01, appliedResp);
            Assert.NotNull(resultDataset);
            EDMDReader resultReader = new EDMDReader(output_applied_response_01);
            Assert.NotNull(resultReader);
            //var resultDataSet = resultReader.DeserializeEDMDFile();
            //Assert.NotNull(resultDataSet);
            //var resultPCB = resultReader.ConstructPCBFromFile(resultDataSet);
            //Assert.NotNull(resultPCB);
            EDMDReader response2Processor = new EDMDReader(test_05_resp);
            Assert.NotNull(response2Processor);
            var response2DS = response2Processor.DeserializeEDMDFile();
            Assert.NotNull(response2DS);
            var applied2Dataset = resultReader.ApplyResponse(response2DS);
            Assert.NotNull(applied2Dataset);
        }




        [Test]
        public void ManualProposalApplication()
        {
            EDMDReader reader = new EDMDReader(test_01_bs);
            Assert.NotNull(reader);
            //var pcbContainer = processor.ConstructPCBFromFile(processor.DeserializeEDMDFile());
            EDMDReader propReader = new EDMDReader(test_02_prop);
            Assert.NotNull(propReader);
            var deseriealizedProp = propReader.DeserializeEDMDFile();
            Assert.NotNull(deseriealizedProp);
            var propPCB = EDMDReader.ConstructPCBFromFile(deseriealizedProp);
            Assert.NotNull(propPCB);
            foreach (var component in propPCB.Components.Values)
            {
                Assert.IsNull(component.ObjectData.Accepted);
                component.ObjectData.IsAccepted = true;
                Assert.IsNotNull(component.ObjectData.Accepted);
                Assert.IsTrue(component.ObjectData.IsAccepted);
            }
            var appliedResp = EDMDReader.ApplyAndConvertToResponse(deseriealizedProp, propPCB);
            Assert.NotNull(appliedResp);
            var respPCB = EDMDReader.ConstructPCBFromFile(appliedResp);
            foreach (var component in respPCB.Components.Values)
            {
                Assert.IsTrue(component.ObjectData.Accepted);
            }
        }

        [Test]
        public void ManualDeletionProposalApplication()
        {
            EDMDReader reader = new EDMDReader(test_01_bs);
            Assert.NotNull(reader);
            //var pcbContainer = processor.ConstructPCBFromFile(processor.DeserializeEDMDFile());
            EDMDReader propReader = new EDMDReader(test_04_prop);
            Assert.NotNull(propReader);
            var deseriealizedProp = propReader.DeserializeEDMDFile();
            Assert.NotNull(deseriealizedProp);
            var propPCB = EDMDReader.ConstructPCBFromFile(deseriealizedProp);
            Assert.NotNull(propPCB);
            Assert.AreEqual(propPCB.Components.Count, 1);
            propPCB.Components.Values.First().ObjectData.IsAccepted = true;
            Assert.IsNotNull(propPCB.Components.Values.First().ObjectData.Accepted);
            Assert.IsTrue(propPCB.Components.Values.First().ObjectData.IsAccepted);
            var appliedResp = EDMDReader.ApplyAndConvertToResponse(deseriealizedProp, propPCB);
            Assert.NotNull(appliedResp);
            var respPCB = EDMDReader.ConstructPCBFromFile(appliedResp);
            Assert.IsTrue(propPCB.Components.Values.First().ObjectData.Accepted);
        }

        [Test]
        public void ManualPixProposalApplication()
        {
            EDMDReader reader = new EDMDReader(pixBaseline);
            Assert.NotNull(reader);
            //var pcbContainer = processor.ConstructPCBFromFile(processor.DeserializeEDMDFile());
            EDMDReader propReader = new EDMDReader(pixProposal1);
            Assert.NotNull(propReader);
            var deseriealizedProp = propReader.DeserializeEDMDFile();
            Assert.NotNull(deseriealizedProp);
            var propPCB = EDMDReader.ConstructPCBFromFile(deseriealizedProp);
            Assert.NotNull(propPCB);
            Assert.IsNull(propPCB.Board.ObjectData.Accepted);
            propPCB.Board.ObjectData.IsAccepted = true;
            Assert.IsNotNull(propPCB.Board.ObjectData.Accepted);
            Assert.IsTrue(propPCB.Board.ObjectData.IsAccepted);
            var appliedResp = EDMDReader.ApplyAndConvertToResponse(deseriealizedProp, propPCB);
            Assert.NotNull(appliedResp);
            var respPCB = EDMDReader.ConstructPCBFromFile(appliedResp);
            Assert.IsTrue(respPCB.Board.ObjectData.Accepted);
        }

        [Test]
        public void ManualConsecutiveProposalApplication()
        {
            EDMDReader reader = new EDMDReader(pixBaseline);
            Assert.NotNull(reader);
            //var pcbContainer = processor.ConstructPCBFromFile(processor.DeserializeEDMDFile());
            EDMDReader propReader = new EDMDReader(pixProposal1);
            Assert.NotNull(propReader);
            var deseriealizedProp = propReader.DeserializeEDMDFile();
            Assert.NotNull(deseriealizedProp);
            var propPCB = EDMDReader.ConstructPCBFromFile(deseriealizedProp);
            Assert.NotNull(propPCB);
            Assert.IsNull(propPCB.Board.ObjectData.Accepted);
            propPCB.Board.ObjectData.IsAccepted = true;
            Assert.IsNotNull(propPCB.Board.ObjectData.Accepted);
            Assert.IsTrue(propPCB.Board.ObjectData.IsAccepted);
            var appliedResp = EDMDReader.ApplyAndConvertToResponse(deseriealizedProp, propPCB);
            Assert.NotNull(appliedResp);
            var respPCB = EDMDReader.ConstructPCBFromFile(appliedResp);
                Assert.IsTrue(respPCB.Board.ObjectData.Accepted);

            EDMDReader propReader2 = new EDMDReader(pixProposal2);
            Assert.NotNull(propReader2);
            var deseriealizedProp2 = propReader2.DeserializeEDMDFile();
            Assert.NotNull(deseriealizedProp2);
            var propPCB2 = EDMDReader.ConstructPCBFromFile(deseriealizedProp2);
            Assert.NotNull(propPCB2);
            foreach (var area in propPCB2.ConstraintAreas.Values)
            {
                Assert.IsNull(area.ObjectData.Accepted);
                area.ObjectData.IsAccepted = true;
                Assert.IsNotNull(area.ObjectData.Accepted);
                Assert.IsTrue(area.ObjectData.IsAccepted);
            }
            var appliedResp2 = EDMDReader.ApplyAndConvertToResponse(deseriealizedProp2, propPCB2);
            Assert.NotNull(appliedResp2);
            var respPCB2 = EDMDReader.ConstructPCBFromFile(appliedResp2);
            foreach (var area in respPCB2.ConstraintAreas.Values)
            {
                Assert.IsTrue(area.ObjectData.Accepted);
            }


            EDMDReader propReader3 = new EDMDReader(pixProposal3);
            Assert.NotNull(propReader3);
            var deseriealizedProp3 = propReader3.DeserializeEDMDFile();
            Assert.NotNull(deseriealizedProp3);
            var propPCB3 = EDMDReader.ConstructPCBFromFile(deseriealizedProp3);
            Assert.NotNull(propPCB3);
            Assert.Greater(propPCB3.Components.Count, 0);
            foreach (var component in propPCB3.Components.Values)
            {
                Assert.IsNull(component.ObjectData.Accepted);
                component.ObjectData.IsAccepted = true;
                Assert.IsNotNull(component.ObjectData.Accepted);
                Assert.IsTrue(component.ObjectData.IsAccepted);
            }
            var appliedResp3 = EDMDReader.ApplyAndConvertToResponse(deseriealizedProp3, propPCB3);
            Assert.NotNull(appliedResp3);
            var respPCB3 = EDMDReader.ConstructPCBFromFile(appliedResp3);
            Assert.NotNull(respPCB3);
            Assert.Greater(respPCB3.Components.Count, 0);
            foreach (var component in respPCB3.Components.Values)
            {
                Assert.IsTrue(component.ObjectData.Accepted);
            }


            EDMDReader propReader4 = new EDMDReader(pixProposal4);
            Assert.NotNull(propReader4);
            var deseriealizedProp4 = propReader4.DeserializeEDMDFile();
            Assert.NotNull(deseriealizedProp4);
            var propPCB4 = EDMDReader.ConstructPCBFromFile(deseriealizedProp4);
            Assert.NotNull(propPCB4);
            Assert.Greater(propPCB4.Components.Count, 0);
            foreach (var component in propPCB4.Components.Values)
            {
                Assert.IsNull(component.ObjectData.Accepted);
                component.ObjectData.IsAccepted = true;
                Assert.IsNotNull(component.ObjectData.Accepted);
                Assert.IsTrue(component.ObjectData.IsAccepted);
            }
            var appliedResp4 = EDMDReader.ApplyAndConvertToResponse(deseriealizedProp4, propPCB4);
            Assert.NotNull(appliedResp4);
            var respPCB4 = EDMDReader.ConstructPCBFromFile(appliedResp4);
            Assert.NotNull(respPCB4);
            Assert.Greater(respPCB4.Components.Count, 0);
            foreach (var component in respPCB4.Components.Values)
            {
                Assert.IsTrue(component.ObjectData.Accepted);
            }

        }

    }
}
