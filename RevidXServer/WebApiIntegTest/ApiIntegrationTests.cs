using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using RevidX.ServiceClient;
using RevidX.ServiceClient.Models;
using RevidXServer.Server;
using Serilog;
using Serilog.Events;

namespace WebApiIntegTest
{
    [TestFixture]
    public class Tests
    {

        private HttpClient _httpClient2;
        private HttpClient _httpClient;
        private TokenClient _tokenClient;
        private TestServer _server;
        const string SERVER_URI = "https://localhost:5001";


        [SetUp]
        public async Task Setup()
        {
            string logFileLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "RevidXClient", "RevidXServerTest.log");
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Trace()
                .WriteTo.File(logFileLocation, outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}")

                .CreateLogger();
            var webhost = new WebHostBuilder()
           .UseUrls("https://localhost:5001")
           .UseStartup<Startup>().ConfigureAppConfiguration((context, builder) =>
           {
               builder.AddJsonFile("appsettings.json");
           }).UseUrls("https://localhost:5001").UseSerilog(); //
            //Environment.SetEnvironmentVariable
            //("USE_EF_DB", "MYSQL");
            _server = new TestServer(webhost);
            _httpClient = _server.CreateClient();
            //_httpClient = new HttpClient();
            _httpClient2 = _server.CreateClient();

            var disco = await _httpClient.GetDiscoveryDocumentAsync("https://localhost:5001/.well-known/openid-configuration");
            _tokenClient = new TokenClient(_httpClient, new TokenClientOptions (){
                Address = disco.TokenEndpoint,
                ClientId = "client", 
                ClientSecret = "secret",
                ClientCredentialStyle = ClientCredentialStyle.AuthorizationHeader
            });;
        }

        [Test]
        public async Task ShouldReturnValuesForAuthenticatedUser()
        {
            var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("alice", "password", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient.SetBearerToken(tokenResponse.AccessToken);
            //_httpClient.BaseAddress = new Uri("https://localhost:5001");
            RevidXApiCaller revidXApiCaller = new RevidXApiCaller(_httpClient);
            var result = await revidXApiCaller.GetUserProjects();
            //var result = await _httpClient.GetStringAsync(SERVER_URI + "/api/Profile");
            //Assert.AreEqual("[\"value1\",\"value2\"]", result);
        }
        [Test]
        public async Task ShouldNotAllowAnonymousUser()
        {

            RevidXApiCaller revidXApiCaller = new RevidXApiCaller(_httpClient);
            var result = await revidXApiCaller.GetUserProfileAsync();
            Assert.AreEqual(result, null);
            Assert.Pass();
        }

        /// user testing (should be role based at some point)
        /// case1
        /// create userprofile, read userprofile, delete userprofile
        ///
        [Test]
        public async Task UserProfileCrud()
        {

            var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("alice", "password", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient.SetBearerToken(tokenResponse.AccessToken);
            RevidXApiCaller revidXApiCaller = new RevidXApiCaller(_httpClient);
            UserProfile userProfile = new UserProfile() { Email = "alice", IdentityId = "2" };
            bool result = false;
            result = await revidXApiCaller.CreateUserProfileAsync(userProfile);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile, null);
            Assert.AreNotEqual(userProfile.Id, null);
            userProfile = await revidXApiCaller.GetUserProfileAsync(userProfile.Id);
            Assert.AreNotEqual(userProfile, null);
            userProfile.FirstName = "John2";
            userProfile.LastName = "Smith2";
            result = await revidXApiCaller.UpdateUserProfileAsync(userProfile);
            Assert.AreNotEqual(userProfile, null);
            Assert.IsTrue(result);
            userProfile = await revidXApiCaller.GetUserProfileAsync(userProfile.Id);
            Assert.AreNotEqual(userProfile, null);
            Assert.AreEqual(userProfile.FirstName, "John2");
            Assert.AreEqual(userProfile.LastName, "Smith2");
            result = await revidXApiCaller.DeleteUserProfileAsync(userProfile);
            Assert.IsTrue(result);
            userProfile = await revidXApiCaller.GetUserProfileAsync(userProfile.Id);
            Assert.AreNotEqual(userProfile, null);
            Assert.AreNotEqual(userProfile.Id, null);
            Assert.Pass();
        }


        /// project testing
        /// 
        ///case 2
        /// create userprofile
        /// create project, read project, delete project
        /// 
        [Test]
        public async Task UserProjectCrud()
        {

            var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("alice", "password", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient.SetBearerToken(tokenResponse.AccessToken);
            RevidXApiCaller revidXApiCaller = new RevidXApiCaller(_httpClient);
            UserProfile userProfile = new UserProfile();
            bool result = false;
            result = await revidXApiCaller.CreateUserProfileAsync(userProfile);
            Assert.AreNotEqual(userProfile, null);
            Assert.AreNotEqual(userProfile.Id, null);
            Assert.IsTrue(result);
            Project project = new Project();
            project.Name = "TestProjectTest2";
            project = await revidXApiCaller.CreateProject(project);
            Assert.AreNotEqual(project, null);
            Assert.AreNotEqual(project.Id, null);
            var projectList = await revidXApiCaller.GetUserProjects();
            var projCount = projectList.Count;
            Assert.Greater(projCount, 0);
            result = await revidXApiCaller.DeleteProject(project.Id);
            Assert.IsTrue(result);
            projectList = null;
            projectList = await revidXApiCaller.GetUserProjects();
            Assert.AreEqual(projectList.Count, projCount - 1);
            Assert.Pass();
        }

        /// case 3
        /// create userprofile, second userprofile
        /// user1: create user1 project
        /// user2: fails to get project
        /// user1: add user2
        /// user2: get project
        /// user2: fail to delete user project
        /// user1: delete userproject
        /// delete userprofile, second userprofile
        ///


        [Test]
        public async Task DoubleUserProjectCrud()
        {
            var tokenResponse2 = await _tokenClient.RequestPasswordTokenAsync("bob", "password2", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient2.SetBearerToken(tokenResponse2.AccessToken);
            RevidXApiCaller revidXApiCaller2 = new RevidXApiCaller(_httpClient2);
            var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("alice", "password", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient.SetBearerToken(tokenResponse.AccessToken);
            RevidXApiCaller revidXApiCaller = new RevidXApiCaller(_httpClient);
            UserProfile userProfile = new UserProfile() { Email = "alice", IdentityId = "1"};
            bool result = false;
            result = await revidXApiCaller.CreateUserProfileAsync(userProfile);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile, null);
            Assert.AreNotEqual(userProfile.Id, null);
            UserProfile userProfile2 = new UserProfile() { Email = "bob", IdentityId = "2" };
            result = await revidXApiCaller2.CreateUserProfileAsync(userProfile2);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile2, null);
            Assert.AreNotEqual(userProfile2.Id, null);
            Project project = new Project();
            project.Name = "TestProjectTest2";
            project = await revidXApiCaller.CreateProject(project);
            Assert.AreNotEqual(project, null);
            Assert.AreNotEqual(project.Id, null);
            var projects = await revidXApiCaller2.GetUserProjects();
            var projCount = projects.Count;
            result = await revidXApiCaller.AddUserToThisProject(project, userProfile2.Email);
            Assert.IsTrue(result);
            projects = await revidXApiCaller2.GetUserProjects();
            Assert.Greater(projects.Count, projCount);
            projCount = projects.Count;
            result = await revidXApiCaller.DeleteProject(project.Id);
            Assert.IsTrue(result);
            projects = await revidXApiCaller2.GetUserProjects();
            Assert.AreEqual(projects.Count, projCount - 1);
            //projects = await revidXApiCaller.GetUserProjects();
            //Assert.Equals(projects.Count, 0);
            Assert.Pass();
        }

        /// revision testing
        /// 
        /// case 4
        /// create userprofile
        /// user1 create user1 project
        /// add revision, get revision, delete revision
        /// remove project
        /// delete userprofile, second userprofile
        ///

        [Test]
        public async Task InitialRevisionTest()
        {
            var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("alice", "password", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient.SetBearerToken(tokenResponse.AccessToken);
            RevidXApiCaller revidXApiCaller = new RevidXApiCaller(_httpClient);
            UserProfile userProfile = new UserProfile() { Email = "alice", IdentityId = "1" };
            bool result = false;
            result = await revidXApiCaller.CreateUserProfileAsync(userProfile);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile, null);
            Assert.AreNotEqual(userProfile.Id, null);
           
            Project project = new Project();
            project.Name = "TestProjectTest2";
            project = await revidXApiCaller.CreateProject(project);
            Assert.AreNotEqual(project, null);
            Assert.AreNotEqual(project.Id, null);
            var userProject = await revidXApiCaller.GetUserProjectFromProject(project.Id);
            Assert.AreNotEqual(userProject, null);
            var revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.NoRevisions);
            Revision revision = new Revision()
            {
                ProjectId = project.Id,
                BaselineFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "BASELINE",
                    UserProfileId = userProfile.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            revision = await revidXApiCaller.PushRevision(revision, userProject.Id);
            Assert.AreNotEqual(revision, null);
            var revisions = await revidXApiCaller.GetRevisions(project.Id);
            Assert.Greater(revisions.Count, 0);
            revisionState= await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AtLatestState);
            result = await revidXApiCaller.DeleteProject(project.Id);
            Assert.IsTrue(result);
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item1, RevisionState.RevisionError);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.Pass();
        }


        /// revision state testing
        /// 
        /// case 5
        /// create userprofile
        /// user1 create user1 project
        /// >--fixture
        /// get state, "initialstaate"
        /// create revision
        /// get current revision, state "atlateststate"
        /// create proposal revision
        /// get same revision, state "awaitingproposalpull"
        /// cancel proposaal revision
        /// get same revision. state "atlateststate"
        /// and revision
        /// get no revision, state "initial state"
        /// ---> fixture
        ///  remove project
        ///  remove users
        ///

        [Test]
        public async Task ProposalRevisionTest()
        {
            var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("alice", "password", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient.SetBearerToken(tokenResponse.AccessToken);
            RevidXApiCaller revidXApiCaller = new RevidXApiCaller(_httpClient);
            UserProfile userProfile = new UserProfile() { Email = "alice", IdentityId = "1" };
            bool result = false;
            result = await revidXApiCaller.CreateUserProfileAsync(userProfile);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile, null);
            Assert.AreNotEqual(userProfile.Id, null);

            Project project = new Project();
            project.Name = "TestProjectTest2";
            project = await revidXApiCaller.CreateProject(project);
            Assert.AreNotEqual(project, null);
            Assert.AreNotEqual(project.Id, null);
            Revision revision = new Revision()
            {
                ProjectId = project.Id,
                BaselineFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "BASELINE",
                    UserProfileId = userProfile.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            var userProject = await revidXApiCaller.GetUserProjectFromProject(project.Id);
            Assert.AreNotEqual(userProject, null);
            revision = await revidXApiCaller.PushRevision(revision, userProject.Id);
            Assert.AreNotEqual(revision, null);
            var revisions = await revidXApiCaller.GetRevisions(project.Id);
            Assert.Greater(revisions.Count, 0);
            var revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AtLatestState);
            Revision proposalRevision = new Revision()
            {
                PreviousRevisionId = revision.Id,
                ProjectId = project.Id,
                ProposalFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "PROPOSAL",
                    UserProfileId = userProfile.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            revision = await revidXApiCaller.PushRevision(proposalRevision, userProject.Id);
            Assert.AreNotEqual(revision, null);
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AwaitingProposalPull);
            result = await revidXApiCaller.CancelRevision(userProject.Id);
            Assert.IsTrue(result);
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AtLatestState);
            result = await revidXApiCaller.DeleteProject(project.Id);
            Assert.IsTrue(result);
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item1, RevisionState.RevisionError);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.Pass();
        }


        /// case 6
        ///
        /// create user profile second userprofile
        /// user1 create project, add user2
        /// user2 push baseline
        /// check state "push proposal/baseline"
        /// <---- fixture
        /// get state, "initialstaate"
        /// create revision
        /// get current revision, state "atlateststate"
        /// create proposal revision
        /// get same revision, state "awaitingproposalpull"
        /// user1: state "pullproposal" pull proposal,
        /// user2: state "awaitingresponsepush"
        /// user1: push response, state "atlateststate"
        /// user2: state "pullresponse", pull response  --->might be issues because new revision may come
        /// user1: atlateststate
        /// remove proposaal revision
        /// get same revision. state "atlateststate"
        /// and revision
        /// get no revision, state "initial state"
        /// ---> fixture
        ///  remove project
        ///  remove users
        ///
        [Test]
        public async Task DoubleRevisionExchangeCrud()
        {
            var tokenResponse2 = await _tokenClient.RequestPasswordTokenAsync("bob", "password2", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient2.SetBearerToken(tokenResponse2.AccessToken);
            RevidXApiCaller revidXApiCaller2 = new RevidXApiCaller(_httpClient2);
            var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("alice", "password", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient.SetBearerToken(tokenResponse.AccessToken);
            RevidXApiCaller revidXApiCaller = new RevidXApiCaller(_httpClient);
            UserProfile userProfile = new UserProfile() { Email = "alice", IdentityId = "1" };
            bool result = false;
            //user1
            result = await revidXApiCaller.CreateUserProfileAsync(userProfile);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile, null);
            Assert.AreNotEqual(userProfile.Id, null);
            //user2
            UserProfile userProfile2 = new UserProfile() { Email = "bob", IdentityId = "2" };
            result = await revidXApiCaller2.CreateUserProfileAsync(userProfile2);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile2, null);
            Assert.AreNotEqual(userProfile2.Id, null);
            Project project = new Project();
            project.Name = "TestProjectTest2";
            //user1
            project = await revidXApiCaller.CreateProject(project);
            Assert.AreNotEqual(project, null);
            Assert.AreNotEqual(project.Id, null);
            result = await revidXApiCaller.AddUserToThisProject(project, userProfile2.Email);
            Assert.IsTrue(result);
            //user1
            var userProject = await revidXApiCaller.GetUserProjectFromProject(project.Id);
            Assert.AreNotEqual(userProject, null);
            //user2
            var userProject2 = await revidXApiCaller2.GetUserProjectFromProject(project.Id);
            Assert.AreNotEqual(userProject2, null);
            //user1
            var revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.NoRevisions);
            //user2
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.NoRevisions);
            Revision revision = new Revision()
            {
                ProjectId = project.Id,
                BaselineFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "BASELINE",
                    UserProfileId = userProfile.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            //user1
            revision = await revidXApiCaller.PushRevision(revision, userProject.Id);
            Assert.AreNotEqual(revision, null);
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AtLatestState);
            //user2
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreNotEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.UpdateToLatest);
            var pulledFile = await revidXApiCaller2.PullTransactionFile(revisionState.Item2.BaselineFileId, userProject2.Id);
            Assert.AreNotEqual(pulledFile, null);
            Revision proposalRevision = new Revision()
            {
                PreviousRevisionId = revision.Id,
                ProjectId = project.Id,
                ProposalFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "PROPOSAL",
                    UserProfileId = userProfile2.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            proposalRevision = await revidXApiCaller2.PushRevision(proposalRevision, userProject2.Id);
            Assert.AreNotEqual(proposalRevision, null);
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AwaitingProposalPull);
            //user1
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreNotEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.ProposalAvailable);
            pulledFile = await revidXApiCaller.PullTransactionFile(revisionState.Item2.ProposalFileId, userProject.Id);
            Assert.AreNotEqual(pulledFile, null);
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.ExpectingResponsePush);
            //user 2
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AwaitingResponsePush);
            //user 1
            proposalRevision.ResponseFile = new TransactionFile()
            {
                FullPath = "some_file.idx",
                TransactionFileTypeStr = "RESPONSE",
                UserProfileId = userProfile.Id,
                BinaryContent = new BinaryContent()
                {
                    Content = new byte[4]
                }
            };
            proposalRevision.BaselineFile = new TransactionFile()
            {
                FullPath = "some_file2.idx",
                TransactionFileTypeStr = "BASELINE",
                UserProfileId = userProfile.Id,
                BinaryContent = new BinaryContent()
                {
                    Content = new byte[4]
                }
            };
            proposalRevision = await revidXApiCaller.PushRevision(proposalRevision, userProject.Id);
            Assert.AreNotEqual(proposalRevision, null);
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AtLatestState);
            //user2
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreNotEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.ResponseAvailable);
            pulledFile = await revidXApiCaller2.PullTransactionFile(revisionState.Item2.ResponseFileId, userProject2.Id);
            Assert.AreNotEqual(pulledFile, null);
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AtLatestState);

            result = await revidXApiCaller.DeleteProject(project.Id);
            Assert.IsTrue(result);
            Assert.Pass();
        }


        /// case 6
        ///
        /// create user profile second userprofile
        /// user1 create project, add user2
        /// user2 push baseline
        /// check state "push proposal/baseline"
        /// <---- fixture
        /// get state, "initialstaate"
        /// create revision
        /// get current revision, state "atlateststate"
        /// create proposal revision
        /// get same revision, state "awaitingproposalpull"
        /// user1: state "pullproposal" pull proposal,
        /// user2: state "awaitingresponsepush"
        /// user1: push response, state "atlateststate"
        /// user2: state "pullresponse", pull response  --->might be issues because new revision may come
        /// user1: atlateststate
        /// remove proposaal revision
        /// get same revision. state "atlateststate"
        /// and revision
        /// get no revision, state "initial state"
        /// ---> fixture
        ///  remove project
        ///  remove users
        ///
        [Test]
        public async Task FailingCasesForExchangeCrud()
        {
            var tokenResponse2 = await _tokenClient.RequestPasswordTokenAsync("bob", "password2", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient2.SetBearerToken(tokenResponse2.AccessToken);
            RevidXApiCaller revidXApiCaller2 = new RevidXApiCaller(_httpClient2);
            var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("alice", "password", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient.SetBearerToken(tokenResponse.AccessToken);
            RevidXApiCaller revidXApiCaller = new RevidXApiCaller(_httpClient);
            UserProfile userProfile = new UserProfile() { Email = "alice", IdentityId = "1" };
            bool result = false;
            //user1
            result = await revidXApiCaller.CreateUserProfileAsync(userProfile);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile, null);
            Assert.AreNotEqual(userProfile.Id, null);
            //user2
            UserProfile userProfile2 = new UserProfile() { Email = "bob", IdentityId = "2" };
            result = await revidXApiCaller2.CreateUserProfileAsync(userProfile2);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile2, null);
            Assert.AreNotEqual(userProfile2.Id, null);
            Project project = new Project();
            project.Name = "TestProjectTest2";
            //user1
            project = await revidXApiCaller.CreateProject(project);
            Assert.AreNotEqual(project, null);
            Assert.AreNotEqual(project.Id, null);
            result = await revidXApiCaller.AddUserToThisProject(project, userProfile2.Email);
            Assert.IsTrue(result);
            //user1
            var userProject = await revidXApiCaller.GetUserProjectFromProject(project.Id);
            Assert.AreNotEqual(userProject, null);
            //user2
            var userProject2 = await revidXApiCaller2.GetUserProjectFromProject(project.Id);
            Assert.AreNotEqual(userProject2, null);
            //user1
            var revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.NoRevisions);
            //user2
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.NoRevisions);
            Revision revision = new Revision()
            {
                ProjectId = project.Id,
                BaselineFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "BASELINE",
                    UserProfileId = userProfile.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            //user1
            revision = await revidXApiCaller.PushRevision(revision, userProject.Id);
            Assert.AreNotEqual(revision, null);
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AtLatestState);
            //user2
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreNotEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.UpdateToLatest);
            var pulledFile = await revidXApiCaller2.PullTransactionFile(revisionState.Item2.BaselineFileId, userProject2.Id);
            Assert.AreNotEqual(pulledFile, null);
            Revision proposalRevision = new Revision()
            {
                PreviousRevisionId = revision.Id,
                ProjectId = project.Id,
                ProposalFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "PROPOSAL",
                    UserProfileId = userProfile2.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            proposalRevision = await revidXApiCaller2.PushRevision(proposalRevision, userProject2.Id);
            Assert.AreNotEqual(proposalRevision, null);
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AwaitingProposalPull);
            //should fail
            Revision proposalRevision2 = new Revision()
            {
                PreviousRevisionId = revision.Id,
                ProjectId = project.Id,
                ProposalFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "PROPOSAL",
                    UserProfileId = userProfile2.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            proposalRevision2 = await revidXApiCaller2.PushRevision(proposalRevision2, userProject2.Id);
            Assert.AreEqual(proposalRevision2, null);
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AwaitingProposalPull);
            //user1 should fail
            Revision proposalRevision3 = new Revision()
            {
                PreviousRevisionId = revision.Id,
                ProjectId = project.Id,
                ProposalFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "PROPOSAL",
                    UserProfileId = userProfile.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            proposalRevision3 = await revidXApiCaller.PushRevision(proposalRevision3, userProject.Id);
            Assert.AreEqual(proposalRevision3, null);
            //should not fail
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreNotEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.ProposalAvailable);
            pulledFile = await revidXApiCaller.PullTransactionFile(revisionState.Item2.ProposalFileId, userProject.Id);
            Assert.AreNotEqual(pulledFile, null);
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.ExpectingResponsePush);
            //

            //user2 should fail pushing baseline and proposal
            Revision baselineRevision4 = new Revision()
            {
                PreviousRevisionId = revision.Id,
                ProjectId = project.Id,
                ProposalFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "BASELINE",
                    UserProfileId = userProfile2.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            baselineRevision4 = await revidXApiCaller2.PushRevision(baselineRevision4, userProject2.Id);
            Assert.AreEqual(baselineRevision4, null);

            Revision proposalRevision4 = new Revision()
            {
                PreviousRevisionId = revision.Id,
                ProjectId = project.Id,
                ProposalFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "PROPOSAL",
                    UserProfileId = userProfile2.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            proposalRevision4 = await revidXApiCaller2.PushRevision(proposalRevision4, userProject2.Id);
            Assert.AreEqual(proposalRevision4, null);
            //user 2
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AwaitingResponsePush);
            //user 1
            proposalRevision.ResponseFile = new TransactionFile()
            {
                FullPath = "some_file.idx",
                TransactionFileTypeStr = "RESPONSE",
                UserProfileId = userProfile.Id,
                BinaryContent = new BinaryContent()
                {
                    Content = new byte[4]
                }
            };
            proposalRevision.BaselineFile = new TransactionFile()
            {
                FullPath = "some_file2.idx",
                TransactionFileTypeStr = "BASELINE",
                UserProfileId = userProfile.Id,
                BinaryContent = new BinaryContent()
                {
                    Content = new byte[4]
                }
            };
            proposalRevision = await revidXApiCaller.PushRevision(proposalRevision, userProject.Id);
            Assert.AreNotEqual(proposalRevision, null);
            revisionState = await revidXApiCaller.GetAvailableRevision(userProject.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AtLatestState);
            //user2
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreNotEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.ResponseAvailable);
            // user2 should fail pushing proposal when response is available
            Revision proposalRevision5 = new Revision()
            {
                PreviousRevisionId = revision.Id,
                ProjectId = project.Id,
                ProposalFile = new TransactionFile()
                {
                    FullPath = "some_file.idx",
                    TransactionFileTypeStr = "PROPOSAL",
                    UserProfileId = userProfile2.Id,
                    BinaryContent = new BinaryContent()
                    {
                        Content = new byte[4]
                    }
                }
            };
            proposalRevision5 = await revidXApiCaller2.PushRevision(proposalRevision5, userProject2.Id);
            Assert.AreEqual(proposalRevision5, null);

            pulledFile = await revidXApiCaller2.PullTransactionFile(revisionState.Item2.ResponseFileId, userProject2.Id);
            Assert.AreNotEqual(pulledFile, null);
            revisionState = await revidXApiCaller2.GetAvailableRevision(userProject2.Id);
            Assert.AreEqual(revisionState.Item2, null);
            Assert.AreEqual(revisionState.Item1, RevisionState.AtLatestState);

            result = await revidXApiCaller.DeleteProject(project.Id);
            Assert.IsTrue(result);
            Assert.Pass();
        }



        //The answer is that TestServer requires HubConnectionBuilder to use the HttpMessageHandler
        //created by it in order to be able to call our Hub(s).
        //To provide the HttpMessageHandler, WithUrl() method provides
        //an oveload that accepts an Action?HttpConnectionOptions?
        //configureHttpConnection as you can see in the code below; https://lurumad.github.io/integration-tests-in-aspnet-core-signalr

        [Test]
        public async Task MessagesWithDbTest()
        {

            var tokenResponse2 = await _tokenClient.RequestPasswordTokenAsync("bob", "password2", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient2.SetBearerToken(tokenResponse2.AccessToken);
            RevidXApiCaller revidXApiCaller2 = new RevidXApiCaller(_httpClient2);
            var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("alice", "password", "api1 RevidXServer.ServerAPI openid profile");
            _httpClient.SetBearerToken(tokenResponse.AccessToken);
            RevidXApiCaller revidXApiCaller = new RevidXApiCaller(_httpClient);
            var uri = "http://localhost/" + "exchangenotify";
            var bobHC = new HubConnectionBuilder()
                .WithUrl(uri, options =>
                {
                    options.AccessTokenProvider = async () =>
                    {
                        //var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("bob", "password2", "api1 RevidXServer.ServerAPI openid profile");
                        return tokenResponse2.AccessToken;
                    };
                    options.HttpMessageHandlerFactory = _ => _server.CreateHandler();
                }).WithAutomaticReconnect()
                .Build();
            await bobHC.StartAsync();
            var aliceHC= new HubConnectionBuilder()
                .WithUrl(uri, options =>
                {
                    options.AccessTokenProvider = async () =>
                    {
                        //var tokenResponse = await _tokenClient.RequestPasswordTokenAsync("alice", "password", "api1 RevidXServer.ServerAPI openid profile");
                        return tokenResponse.AccessToken;
                    };
                    options.HttpMessageHandlerFactory = _ => _server.CreateHandler();
                }).WithAutomaticReconnect()
                .Build();
            aliceHC.On<Message>("ReceiveMessage", (message) =>
            {
            });
            aliceHC.On<Message>("FileUploadedToProject", (message) =>
            {
            });
            await aliceHC.StartAsync();
            //user1

            UserProfile userProfile = new UserProfile() { Email = "alice", IdentityId = "1" };
            var result = await revidXApiCaller.CreateUserProfileAsync(userProfile);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile, null);
            Assert.AreNotEqual(userProfile.Id, null);
            //user2
            UserProfile userProfile2 = new UserProfile() { Email = "bob", IdentityId = "2" };
            result = await revidXApiCaller2.CreateUserProfileAsync(userProfile2);
            Assert.IsTrue(result);
            Assert.AreNotEqual(userProfile2, null);
            Assert.AreNotEqual(userProfile2.Id, null);
            Project project = new Project();
            project.Name = "TestProjectTest2";
            //user1
            project = await revidXApiCaller.CreateProject(project);
            Assert.AreNotEqual(project, null);
            Assert.AreNotEqual(project.Id, null);
            result = await revidXApiCaller.AddUserToThisProject(project, userProfile2.Email);
            Assert.IsTrue(result);

            Message message = new Message();
            message.SenderId = userProfile.Id;
            message.ProjectId = project.Id;
            message.Text = "Test1";
            await aliceHC.SendAsync("JoinGroup", project.Id);
            await aliceHC.SendAsync("SendMessage", message);
        }
    }
}