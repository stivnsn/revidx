﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations
{
    public partial class TransactonActvitycascadedelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId",
                table: "TransactionFileActivity");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId",
                table: "TransactionFileActivity",
                column: "TransactionFileId",
                principalTable: "TransactionFiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId",
                table: "TransactionFileActivity");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId",
                table: "TransactionFileActivity",
                column: "TransactionFileId",
                principalTable: "TransactionFiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
