﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations
{
    public partial class PushPullTimeFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LatestPull",
                table: "UserProjects",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LatestPush",
                table: "UserProjects",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "TransactionFileActivityId",
                table: "TransactionFiles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TransactionFileActivity",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsPushed = table.Column<bool>(type: "bit", nullable: false),
                    UserProfileId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TransactionFileId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionFileActivity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionFileActivity_TransactionFiles_TransactionFileId",
                        column: x => x.TransactionFileId,
                        principalTable: "TransactionFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransactionFileActivity_UserProfiles_UserProfileId",
                        column: x => x.UserProfileId,
                        principalTable: "UserProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFileActivity_TransactionFileId",
                table: "TransactionFileActivity",
                column: "TransactionFileId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFileActivity_UserProfileId",
                table: "TransactionFileActivity",
                column: "UserProfileId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransactionFileActivity");

            migrationBuilder.DropColumn(
                name: "LatestPull",
                table: "UserProjects");

            migrationBuilder.DropColumn(
                name: "LatestPush",
                table: "UserProjects");

            migrationBuilder.DropColumn(
                name: "TransactionFileActivityId",
                table: "TransactionFiles");
        }
    }
}
