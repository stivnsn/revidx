﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations
{
    public partial class ImprovedTransactionActivitySupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Id",
                table: "UserProjects",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserProjectId",
                table: "TransactionFileActivity",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserProjectProjectId",
                table: "TransactionFileActivity",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserProjectUserProfileId",
                table: "TransactionFileActivity",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFileActivity_UserProjectUserProfileId_UserProjectProjectId",
                table: "TransactionFileActivity",
                columns: new[] { "UserProjectUserProfileId", "UserProjectProjectId" });

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFileActivity_UserProjects_UserProjectUserProfileId_UserProjectProjectId",
                table: "TransactionFileActivity",
                columns: new[] { "UserProjectUserProfileId", "UserProjectProjectId" },
                principalTable: "UserProjects",
                principalColumns: new[] { "UserProfileId", "ProjectId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFileActivity_UserProjects_UserProjectUserProfileId_UserProjectProjectId",
                table: "TransactionFileActivity");

            migrationBuilder.DropIndex(
                name: "IX_TransactionFileActivity_UserProjectUserProfileId_UserProjectProjectId",
                table: "TransactionFileActivity");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "UserProjects");

            migrationBuilder.DropColumn(
                name: "UserProjectId",
                table: "TransactionFileActivity");

            migrationBuilder.DropColumn(
                name: "UserProjectProjectId",
                table: "TransactionFileActivity");

            migrationBuilder.DropColumn(
                name: "UserProjectUserProfileId",
                table: "TransactionFileActivity");
        }
    }
}
