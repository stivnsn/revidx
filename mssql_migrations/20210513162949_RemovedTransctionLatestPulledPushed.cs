﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations
{
    public partial class RemovedTransctionLatestPulledPushed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LatestPull",
                table: "UserProjects");

            migrationBuilder.DropColumn(
                name: "LatestPush",
                table: "UserProjects");

            migrationBuilder.AddColumn<string>(
                name: "LatestPulledTransactionId",
                table: "UserProjects",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LatestPushedTransactionId",
                table: "UserProjects",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserProjects_LatestPulledTransactionId",
                table: "UserProjects",
                column: "LatestPulledTransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProjects_LatestPushedTransactionId",
                table: "UserProjects",
                column: "LatestPushedTransactionId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserProjects_Transactions_LatestPulledTransactionId",
                table: "UserProjects",
                column: "LatestPulledTransactionId",
                principalTable: "Transactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserProjects_Transactions_LatestPushedTransactionId",
                table: "UserProjects",
                column: "LatestPushedTransactionId",
                principalTable: "Transactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserProjects_Transactions_LatestPulledTransactionId",
                table: "UserProjects");

            migrationBuilder.DropForeignKey(
                name: "FK_UserProjects_Transactions_LatestPushedTransactionId",
                table: "UserProjects");

            migrationBuilder.DropIndex(
                name: "IX_UserProjects_LatestPulledTransactionId",
                table: "UserProjects");

            migrationBuilder.DropIndex(
                name: "IX_UserProjects_LatestPushedTransactionId",
                table: "UserProjects");

            migrationBuilder.DropColumn(
                name: "LatestPulledTransactionId",
                table: "UserProjects");

            migrationBuilder.DropColumn(
                name: "LatestPushedTransactionId",
                table: "UserProjects");

            migrationBuilder.AddColumn<DateTime>(
                name: "LatestPull",
                table: "UserProjects",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LatestPush",
                table: "UserProjects",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
