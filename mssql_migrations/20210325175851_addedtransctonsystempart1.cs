﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations
{
    public partial class addedtransctonsystempart1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_UserProfiles_UserProfileId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_UserProfileId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "UserProfileId",
                table: "Transactions");

            migrationBuilder.AddColumn<bool>(
                name: "IsMCAD",
                table: "UserProfiles",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsClosed",
                table: "Transactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsLocked",
                table: "TransactionFiles",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UserProfileId",
                table: "TransactionFiles",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFiles_UserProfileId",
                table: "TransactionFiles",
                column: "UserProfileId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFiles_UserProfiles_UserProfileId",
                table: "TransactionFiles",
                column: "UserProfileId",
                principalTable: "UserProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFiles_UserProfiles_UserProfileId",
                table: "TransactionFiles");

            migrationBuilder.DropIndex(
                name: "IX_TransactionFiles_UserProfileId",
                table: "TransactionFiles");

            migrationBuilder.DropColumn(
                name: "IsMCAD",
                table: "UserProfiles");

            migrationBuilder.DropColumn(
                name: "IsClosed",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "IsLocked",
                table: "TransactionFiles");

            migrationBuilder.DropColumn(
                name: "UserProfileId",
                table: "TransactionFiles");

            migrationBuilder.AddColumn<string>(
                name: "UserProfileId",
                table: "Transactions",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserProfileId",
                table: "Transactions",
                column: "UserProfileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_UserProfiles_UserProfileId",
                table: "Transactions",
                column: "UserProfileId",
                principalTable: "UserProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
