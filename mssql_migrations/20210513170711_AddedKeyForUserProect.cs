﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevidXServer.Server.Migrations
{
    public partial class AddedKeyForUserProect : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFileActivity_UserProjects_UserProjectUserProfileId_UserProjectProjectId",
                table: "TransactionFileActivity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserProjects",
                table: "UserProjects");

            migrationBuilder.DropIndex(
                name: "IX_TransactionFileActivity_UserProjectUserProfileId_UserProjectProjectId",
                table: "TransactionFileActivity");

            migrationBuilder.DropColumn(
                name: "UserProjectProjectId",
                table: "TransactionFileActivity");

            migrationBuilder.DropColumn(
                name: "UserProjectUserProfileId",
                table: "TransactionFileActivity");

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "UserProjects",
                type: "nvarchar(450)",
                nullable: false,
                defaultValueSql: "newId()",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProjectId",
                table: "UserProjects",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "UserProfileId",
                table: "UserProjects",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "UserProjectId",
                table: "TransactionFileActivity",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserProjects",
                table: "UserProjects",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_UserProjects_UserProfileId",
                table: "UserProjects",
                column: "UserProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFileActivity_UserProjectId",
                table: "TransactionFileActivity",
                column: "UserProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFileActivity_UserProjects_UserProjectId",
                table: "TransactionFileActivity",
                column: "UserProjectId",
                principalTable: "UserProjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFileActivity_UserProjects_UserProjectId",
                table: "TransactionFileActivity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserProjects",
                table: "UserProjects");

            migrationBuilder.DropIndex(
                name: "IX_UserProjects_UserProfileId",
                table: "UserProjects");

            migrationBuilder.DropIndex(
                name: "IX_TransactionFileActivity_UserProjectId",
                table: "TransactionFileActivity");

            migrationBuilder.AlterColumn<string>(
                name: "UserProfileId",
                table: "UserProjects",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProjectId",
                table: "UserProjects",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "UserProjects",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "UserProjectId",
                table: "TransactionFileActivity",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserProjectProjectId",
                table: "TransactionFileActivity",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserProjectUserProfileId",
                table: "TransactionFileActivity",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserProjects",
                table: "UserProjects",
                columns: new[] { "UserProfileId", "ProjectId" });

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFileActivity_UserProjectUserProfileId_UserProjectProjectId",
                table: "TransactionFileActivity",
                columns: new[] { "UserProjectUserProfileId", "UserProjectProjectId" });

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFileActivity_UserProjects_UserProjectUserProfileId_UserProjectProjectId",
                table: "TransactionFileActivity",
                columns: new[] { "UserProjectUserProfileId", "UserProjectProjectId" },
                principalTable: "UserProjects",
                principalColumns: new[] { "UserProfileId", "ProjectId" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
